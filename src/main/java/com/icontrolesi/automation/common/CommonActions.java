package com.icontrolesi.automation.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

public interface CommonActions{
	By bottomMenuInGridView = By.id("menu");

	public default void waitFor(int timeInSeconds){
		System.out.println( Logger.getLineNumber() + "Waiting for " + timeInSeconds + " seconds...");
		try {
			Thread.sleep(timeInSeconds * 1000);
		} catch (InterruptedException e) {
			System.err.println("Waiting interrupted...");
		}
		
		System.out.println( Logger.getLineNumber() + "Waiting time finished...");
	}
	
	public default void waitFor(WebDriver driver, int timeInSeconds){
		System.out.println( Logger.getLineNumber() + "Waiting for " + timeInSeconds + " seconds...");
		try {
			Thread.sleep(timeInSeconds * 1000);
		} catch (InterruptedException e) {
			System.err.println("Waiting interrupted...");
		}
		
		System.out.println( Logger.getLineNumber() + "Waiting time finished...");
	}
	
	public default WebElement waitForElement(By elementLocator){
		waitForVisibilityOf(elementLocator);
		
		return getElement(elementLocator);
	}
	
	public default void waitForElementToBeClickable(By element){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		System.out.println( Logger.getLineNumber() + "\nElement is now clickable... [" + element.toString() + "]...\n");
	}
	
	
	public default void waitForVisibilityOf(By element){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		}catch(ElementNotVisibleException env){
			System.err.println("Error:: " + env.getMessage());
		}
		System.out.println( Logger.getLineNumber() + "\nElement ["+ element.toString() + "] visible...\n");
	}
	
	public default void waitForVisibilityOf(By element, int waitTime){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), waitTime);
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		}catch(ElementNotVisibleException env){
			System.err.println("Error:: " + env.getMessage());
		}
		System.out.println( Logger.getLineNumber() + "\nElement ["+ element.toString() + "] visible...\n");
	}
	
	public default void waitForVisibilityOf(WebDriver driver, By element){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		}catch(ElementNotVisibleException env){
			System.err.println("Error:: " + env.getMessage());
		}
		System.out.println( Logger.getLineNumber() + "\nElement ["+ element.toString() + "] visible...\n");
	}
	
	
	public default void waitForInVisibilityOf(By element){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
		System.out.println( Logger.getLineNumber() + "\nElement ["+ element.toString() + "] invisible...\n");
	}
	
	public default void waitForInVisibilityOf(By element, int waitTime){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), waitTime);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
		System.out.println( Logger.getLineNumber() + "\nElement ["+ element.toString() + "] invisible...\n");
	}
	
	public default void waitForAttributeIn(By element, String attribute, String value){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.attributeContains(element, attribute, value));
		System.out.println( Logger.getLineNumber() + "\nAttribute [" + value+ "] for [" + attribute + "] loaded...\n");
	}
	
	public default void waitForAttributeIn(WebDriver driver, By element, String attribute, String value){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.attributeContains(element, attribute, value));
		System.out.println( Logger.getLineNumber() + "\nAttribute [" + value+ "] for [" + attribute + "] loaded...\n");
	}
	
	
	public default void waitForAttributeIn(WebElement element, String attribute, String value){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.attributeContains(element, attribute, value));
		System.out.println( Logger.getLineNumber() + "\nAttribute [" + value+ "] for [" + attribute + "] loaded...\n");
	}
	
	
	public default void waitForAttributeEqualsIn(By element, String attribute, String value){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.attributeToBe(element, attribute, value));
		System.out.println( Logger.getLineNumber() + "\nAttribute [" + value+ "] for [" + attribute + "] loaded...\n");
	}
	
	public default void waitForAttributeEqualsIn(WebDriver driver, By element, String attribute, String value){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.attributeToBe(element, attribute, value));
		System.out.println( Logger.getLineNumber() + "\nAttribute [" + value+ "] for [" + attribute + "] loaded...\n");
	}
	
	public default void waitForNumberOfElementsToBePresent(By element, int elementCount){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.numberOfElementsToBe(element, elementCount));
		System.out.println( Logger.getLineNumber() + "\nElement visible...\n");
	}
	
	public default void waitForNumberOfElementsToBePresent(By element, int elementCount, int waitTime){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), waitTime);
		wait.until(ExpectedConditions.numberOfElementsToBe(element, elementCount));
		System.out.println( Logger.getLineNumber() + "\nElement visible...\n");
	}
	
	public default void waitForNumberOfElementsToBePresent(WebDriver driver, By element, int elementCount){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.numberOfElementsToBe(element, elementCount));
		System.out.println( Logger.getLineNumber() + "\nElement visible...\n");
	}
	
	public default void waitForNumberOfElementsToBeGreater(By element, int elementCount){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(element, elementCount));
		System.out.println( Logger.getLineNumber() + "\nElement [" +element.toString() + "] visible...\n");
	}
	
	public default void waitForNumberOfElementsToBeLessThan(By element, int elementCount){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.numberOfElementsToBeLessThan(element, elementCount));
		System.out.println( Logger.getLineNumber() + "\nElement [" +element.toString() + "] visible...\n");
	}
	
	public default void switchToDefaultFrame(){
		Driver.getDriver().switchTo().defaultContent();
		System.out.println( Logger.getLineNumber() + "\nFrame switched to DEFAULT content...\n");
	}
	
	public default void switchToDefaultFrame(WebDriver driver){
		driver.switchTo().defaultContent();
		System.out.println( Logger.getLineNumber() + "\nFrame switched to DEFAULT content...\n");
	}
	
	public default void switchToFrame(int frameNumber){
		Driver.getDriver().switchTo().frame(frameNumber);
		System.out.println( Logger.getLineNumber() + "\nFrame switched to " + (frameNumber + 1) + "...\n");
	}
	
	public default void switchToFrame(WebElement frame){
		Driver.getDriver().switchTo().frame(frame);
		System.out.println( Logger.getLineNumber() + "\nFrame switched to " + frame.toString() + "...\n");
	}
	
	
	public default void waitForFrameToLoad(String frameName){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		waitFor(1);
		System.out.println( Logger.getLineNumber() + "\nFrame switched to " + frameName + "...\n");
	}
	
	public default void waitForFrameToLoad(WebElement frame){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
		waitFor(1);
		System.out.println( Logger.getLineNumber() + "\nFrame switched to " + frame.toString() + "...\n");
	}
	
	public default void waitForFrameToLoad(WebDriver driver, String frameName){
		//switchToDefaultFrame();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		waitFor(driver, 1);
		System.out.println( Logger.getLineNumber() + "\nFrame switched to " + frameName + "...\n");
	}
	
	
	public default void performQuickSearch(String searchTerm){
	try{
		waitForElementToBeClickable(SearchPage.runSearchButton);
		editText(By.id("quick"), searchTerm+"\n");

		waitforViewDocumentPage();
		
		System.out.println( Logger.getLineNumber() + "\nQuick Search completed...\n");
		}catch (Exception e) {
			System.err.println("Error while performing quick search:: " + e.getMessage());
			throw new SkipException("Quick Search not performed!");
		}
	}
		
	public default void performSearch(){
		try{
		waitForElementToBeClickable(SearchPage.runSearchButton);
		tryClick(SearchPage.runSearchButton);

		waitforViewDocumentPage();
		
		System.out.println( Logger.getLineNumber() + "\nSearch completed...\n");
		}catch (Exception e) {
			System.err.println("Error while performing search:: " + e.getMessage());
			throw new SkipException("Search not performed!");
		}
	}
	
	public default void performSearch(WebDriver driver){
		try{
			waitForElementToBeClickable(SearchPage.runSearchButton);
			tryClick(SearchPage.runSearchButton);

			waitforViewDocumentPage();
			
			System.out.println( Logger.getLineNumber() + "\nSearch completed...\n");
			}catch (Exception e) {
				System.err.println("Error while performing search:: " + e.getMessage());
				throw new SkipException("Search not performed!");
			}
	}
	
	
	public default void waitforViewDocumentPage(){
		waitForPageLoad();
		if(Configuration.getConfig("selenium.browser").equalsIgnoreCase("ie11"))
			waitFor(30);
		else{
			waitForVisibilityOf(bottomMenuInGridView);
			waitForNumberOfElementsToBeGreater(By.cssSelector("#grid > tbody > tr"), 1);
		}
		//System.out.println( Logger.getLineNumber() + "\nSearch completed...\n");
	}
	
	public default void waitForPageLoad(){
		try{
				Driver.getDriver().manage().timeouts().pageLoadTimeout(5, TimeUnit.MINUTES);
				System.out.println( Logger.getLineNumber() + "Page load completed...");
		}catch(TimeoutException toe){
			System.out.println( Logger.getLineNumber() + toe.getMessage());
		}
	}
	
	public default void waitForPageLoad(WebDriver driver){
		try{
				driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.MINUTES);
				System.out.println( Logger.getLineNumber() + "Page load completed...");
		}catch(TimeoutException toe){
			System.out.println( Logger.getLineNumber() + toe.getMessage());
		}
	}
	
	
	public default int getRandomNumberInRange(int min, int max){
		if (min >= max) {
			throw new IllegalArgumentException("Range must be given properly (i.e. max must exceeds min).");
		}
		 Random rand = new Random();
		 return rand.nextInt((max - min) + 1) + min;
	} 
	
	public default WebElement getParentOf(WebElement element){
		return element.findElement(By.xpath(".."));
	}
	
	public default WebElement getParentOf(WebDriver driver, WebElement element){
		return element.findElement(By.xpath(".."));
	}
	
	
	public default WebElement getParentOf(WebDriver driver, By element){
		WebElement nodeElement = driver.findElement(element);
		return nodeElement.findElement(By.xpath(".."));
	}
	
	public default WebElement getChildOf(By element, By child){
		WebElement nodeElement = getElement(element);
		return nodeElement.findElement(By.xpath(".//"+child));
	}
	
	public default WebElement getSiblingOf(By element){
		return getElement(element).findElement(By.xpath("following-sibling::*"));
	}

	public default WebElement getSiblingOf(By element, String with){
		return getElement(element).findElement(By.xpath("following-sibling::" + with));
	}

	public default WebElement getSiblingOf(WebElement element, String with){
		System.err.println(element.getTagName() + "*****************************************");
		return element.findElement(By.xpath("following-sibling::" + with));
	}
	
	public default void waitForAlertToBePresent(){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.alertIsPresent());
	}
	
	public default void waitForAlertToBePresent(WebDriver driver){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());
	}
	
	public default void dismissAlert(WebDriver driver){
		try{
			waitForAlertToBePresent(driver);
			Alert alert = driver.switchTo().alert();
			System.err.println("Alert text: " + alert.getText());
			waitFor(driver, 2);
			alert.dismiss();
			System.out.println( Logger.getLineNumber() + "Alert dismissed...");
		}catch(Exception exception){
			System.err.println("No alert present...");
		}
	}
	
	public default void acceptAlert(){
		try{
			waitForAlertToBePresent();
			Alert alert = Driver.getDriver().switchTo().alert();
			System.err.println("Alert text: " + alert.getText());
			waitFor(2);
			alert.accept();
			System.out.println( Logger.getLineNumber() + "Alert accepted...");
		}catch(Exception exception){
			System.err.println("No alert present...");
		}
	}
	
	public default void acceptAlert(WebDriver driver){
		try{
			waitForAlertToBePresent(driver);
			Alert alert = driver.switchTo().alert();
			System.err.println("Alert text: " + alert.getText());
			waitFor(driver, 2);
			alert.accept();
			System.out.println( Logger.getLineNumber() + "Alert accepted...");
		}catch(Exception exception){
			System.err.println("No alert present...");
		}
	}
	
	
	public default String getAlertMsg(WebDriver driver){
		String alertText = null;
		try{
			waitForAlertToBePresent(driver);
			Alert alert = driver.switchTo().alert();
			System.err.println("Alert text: " + alert.getText());
			alertText = alert.getText();
			waitFor(driver, 2);
			alert.accept();
		}catch(Exception exception){
			System.err.println("No alert present...");
		}
		
		return alertText;
	}
	
	
	public default String getAlertMsg(){
		String alertText = null;
		try{
			waitForAlertToBePresent();
			Alert alert = Driver.getDriver().switchTo().alert();
			System.err.println("Alert text: " + alert.getText());
			alertText = alert.getText();
			waitFor(2);
			alert.accept();
		}catch(Exception exception){
			System.err.println("No alert present...");
		}
		
		return alertText;
	}
	
	 default void click(WebElement element) {
	        if ("IE11".equalsIgnoreCase(Configuration.getConfig("selenium.browser"))) {
	            try{
	                if(element.getTagName().equals("button")){
	                    element.sendKeys("\n");
	                }else element.click();
	            }catch(NullPointerException e){
	                element.sendKeys("\n");
	            }
	           
	        } else {
	            element.click();
	        }
	    }
	 
	 
	 default void tryClick(By element){
		 waitForVisibilityOf(element);
		 getElement(element).click();
		 System.out.println( Logger.getLineNumber() + "Element [ " + element.toString() + " ] clicked...");
	 }
	 
	 default void tryClick(By element, int waitTime){
		 tryClick(element);
		 System.out.println( Logger.getLineNumber() + "Element [ " + element.toString() + " ] clicked...");
		 System.out.println( Logger.getLineNumber() + "Waiting for [ " + waitTime + " seconds ] for page load...");
		 waitFor(waitTime);
	 }
	 
	 default void tryClick(By element, By waitFor){
		 waitForVisibilityOf(element);
		 getElement(element).click();
		 System.out.println( Logger.getLineNumber() + "Element [ " + element.toString() + " ] clicked...");
		 System.out.println( Logger.getLineNumber() + "Waiting for [ " + waitFor.toString() + " ] to be visible...");
		 waitForVisibilityOf(waitFor);
	 }
	 
	 default void tryClick(WebElement element, int waitTime){
		 element.click();
		 System.out.println( Logger.getLineNumber() + "Element [ " + element.toString() + " ] clicked...");
		 System.out.println( Logger.getLineNumber() + "Waiting for [ " + waitTime + " seconds ] for page load...");
		 waitFor(waitTime);
	 }
	 
	 default void tryClick(WebElement element, By waitForElem){
		 element.click();
		 waitForVisibilityOf(waitForElem);
		 System.out.println( Logger.getLineNumber() + "Element [ " + element.toString() + " ] clicked...");
	 }
	 
	 default void enterText(By element, String text){
		waitForVisibilityOf(element);
		getElement(element).sendKeys(text);
		waitFor(1);
	 }
	 
	 
	 default void enterText(WebElement element, String text){
			element.sendKeys(text);
			waitFor(1);
	 }
	 
	 default void editText(By element, String text){
		 	waitForVisibilityOf(element);
		 	getElement(element).clear();
		 	waitFor(2);
			enterText(element, text);
	 }
	 
	 default void editText(WebElement element, String text){
		 	element.clear();
			enterText(element, text);
	 }
	 
	 default String getText(By element){
		 	waitForVisibilityOf(element);
		 	String tagName = getElement(element).getTagName();
		 	if(tagName.equals("input") || tagName.equals("textarea"))
		 		return getElement(element).getAttribute("value").trim();
		 	
		 	return getElement(element).getText().trim();
	 }
	 
	 default String getText(WebElement element){
		 	//waitForVisibilityOf(element);
		 	String tagName = element.getTagName();
		 	if(tagName.equals("input") || tagName.equals("textarea"))
		 		return element.getAttribute("value").trim();
		 	
		 	return element.getText().trim();
	 }
	 
	 default String getAttribute(By element, String attribute){
		 return getElement(element).getAttribute(attribute).trim();
	 }
	 
	 default String getAttribute(WebElement element, String attribute){
		 return element.getAttribute(attribute).trim();
	 }
	 
	 default String getCssValue(By element, String attribute){
		 System.out.println( Logger.getLineNumber() + "#####"+attribute + ": " + getElement(element).getCssValue(attribute)+"####");
		 return getElement(element).getCssValue(attribute).trim();
	 }
	 
	 default String getCssValue(WebElement element, String attribute){
		 System.out.println( Logger.getLineNumber() + "#####"+attribute + ": " + element.getCssValue(attribute)+"####");
		 return element.getCssValue(attribute).trim();
	 }
	 
	 /**
	  * 
	  * @param element Element from which color is to get
	  * @param attribute Attribute for which color is to get (ex:. backgroun-color, color etc.)
	  * @return HEX equivalent color for the element passed as By type
	  */
	 default String getColorFromElement(By element, String attribute){
		 String color = getCssValue(element, attribute);
		 
		 if(color.startsWith("#") == false){
			 String [] splittedColor = color.split(",");
			 int red = Integer.valueOf(splittedColor[0].replaceAll("\\D+", "").trim());
			 int green = Integer.valueOf(splittedColor[1].replaceAll("\\D+", "").trim());
			 int blue = Integer.valueOf(splittedColor[2].replaceAll("\\D+", "").trim());
			 
			 color = String.format("#%1$2s%2$2s%3$2s", Utils.toHexValue(red), Utils.toHexValue(green), Utils.toHexValue(blue));
		 }
		 
		 return color;
	 }
	 
	 default String getColorFromElement(WebElement element, String attribute){
		 String color = element.getCssValue(attribute);
		 
		 if(color.startsWith("#") == false){
			 String [] splittedColor = color.split(",");
			 int red = Integer.valueOf(splittedColor[0].replaceAll("\\D+", "").trim());
			 int green = Integer.valueOf(splittedColor[1].replaceAll("\\D+", "").trim());
			 int blue = Integer.valueOf(splittedColor[2].replaceAll("\\D+", "").trim());
			 
			 color = String.format("#%1$2s%2$2s%3$2s", Utils.toHexValue(red), Utils.toHexValue(green), Utils.toHexValue(blue));
		 }
		 
		 return color;
	 }
	 
	 default String getCssValueInUpperCase(By element, String attribute){
		 return getElement(element).getCssValue(attribute).trim().toUpperCase();
	 }
	 
	 default void selectFromDrowdownByIndex(By element, int itemPosition){
		//  waitForVisibilityOf(element);
		 Select select = new Select(getElement(element));
		 try{
			 select.selectByIndex(itemPosition);
			 System.out.println(String.format("'%s' selected at position '%d'...", select.getOptions().get(itemPosition).getText(), itemPosition));
		 }catch (Exception e) {
			System.err.println("Err:: No item at position: " + (itemPosition + 1));
		}
		 waitFor(1);
	 }
	 
	 default void selectFromDrowdownByText(By element, String itemName){
		 if(itemName.equals("")){
			 System.err.println("No text provided...");
			 return;
		 }
		 waitForVisibilityOf(element);
		 Select select = new Select(getElement(element));
		 try{
			 select.selectByVisibleText(itemName);
		 }catch (Exception e) {
			System.err.println("Err:: No such option: '" + itemName + "'... ");
		}
		 waitFor(1);
	 }
	 
	 default void selectFromDrowdownByText(WebElement element, String itemName){
		 //waitForVisibilityOf(element);
		 Select select = new Select(element);
		 select.selectByVisibleText(itemName);
		 waitFor(1);
	 }
	 
	 default int getTotalElementCount(By element){
		 return Driver.getDriver().findElements(element).size();
	 }
	 
	 default WebElement getElement(By element){
		 WebElement elem = Driver.getDriver().findElement(element);
		 ((JavascriptExecutor)Driver.getDriver()).executeScript("arguments[0].style.border='3px solid crimson'", elem);

		 return elem;
	 }
	 
	 default List<WebElement> getElements(By element){
		 return Driver.getDriver().findElements(element);
	 }
	 
	 
	 default boolean isElementChecked(By element){
		 return getElement(element).isSelected();
	 }
	 
	 default boolean isElementEnabled(By element){
		 return getElement(element).isEnabled();
	 }
	 
	 default String getSelectedItemFroDropdown(By element){
		 return new Select(getElement(element)).getFirstSelectedOption().getText().trim();
	 }
	 
	 default String getSelectedItemFroDropdown(WebElement element){
		 return new Select(element).getFirstSelectedOption().getText().trim();
	 }
	 
	 default List<WebElement> getItemsFromDropdown(By element){
		 return new Select(getElement(element)).getOptions();
	 }
	 
	 default List<String> getItemsValueFromDropdown(By element){
		 return getListOfItemsFrom(getItemsFromDropdown(element), item -> item.getText().trim());
	 }
	 
	 default <T, U> List<U> getListOfItemsFrom(By source, Function<WebElement, U> function){
		 return getElements(source).stream().map(function).collect(Collectors.toList());
	 }
	 
	 default <T, U> List<U> getListOfItemsFrom(List<T> source, Function<T, U> function){
		 return source.stream().map(function).collect(Collectors.toList());
	 }
	 
	 default List<WebElement> getItemsFromDropdown(WebElement element){
		 
		 return new Select(element).getOptions();
	 }
	 
	 default public void switchToFrameEndsWith(String frameName){
		 List<WebElement> frameList = Driver.getDriver().findElements(By.tagName("iframe"));
			WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
			
			for(WebElement frame : frameList){
				System.out.println( Logger.getLineNumber() + frame.getAttribute("src")+"*****@");
				if(frame.getAttribute("src").endsWith(frameName)){
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
					System.out.println( Logger.getLineNumber() + "Framed switched...");
					break;
				}
			}
	 } 
	 
	 default public void switchToFrame(String frameAttribute, String attributeValue){
		 List<WebElement> frameList = Driver.getDriver().findElements(By.tagName("iframe"));
			WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
			
			for(WebElement frame : frameList){
				System.out.println( Logger.getLineNumber() + frame.getAttribute(frameAttribute)+"*****@");
				if(frame.getAttribute(frameAttribute).endsWith(attributeValue)){
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
					System.out.println( Logger.getLineNumber() + "Framed switched...");
					break;
				}
			}
	 } 
	 
	 
	 default public void switchToWindow(int windowNumber){
		 String mainWindowHandle = Driver.getDriver().getWindowHandle(); 
				 
		 Set<String> windHandles = Driver.getDriver().getWindowHandles();
		 boolean isSwitched = false;
		 for(String windowHandle : windHandles){
			 if(windowHandle.equals(mainWindowHandle) == false){
				 Driver.getDriver().switchTo().window(windowHandle);
				 
				 System.out.printf("Switched to browser window %s.\n", Driver.getDriver().getTitle());
				 
				 isSwitched = true;
				 
				 break;
			 }
		 }
		 
		 if(isSwitched == false){
			 throw new SkipException(String.format("No such browser window exists at %d.", windowNumber));
		 }
	 } 
	 
	 default public void switchToWindow(String windowName){
		 Set<String> windHandles = Driver.getDriver().getWindowHandles();
		 boolean isSwitched = false;
		 for(String windowHandle : windHandles){
			 if(windowHandle.equals(windowName)){
				 Driver.getDriver().switchTo().window(windowHandle);
				 
				 System.out.printf("Switched to browser window %s.\n", Driver.getDriver().getTitle());
				 
				 isSwitched = true;
				 
				 break;
			 }
		 }
		 
		 if(isSwitched == false){
			 throw new SkipException(String.format("No such browser window exists called %s.", windowName));
		 }
	 } 
	 
	 
	 default void unzip(String zipFilePath, String destDir) {
        File dir = new File(destDir);
        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                System.out.println( Logger.getLineNumber() + "Unzipping to "+newFile.getAbsolutePath());
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
                }
                fos.close();
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
     }
	 
	 
	 default public ZipFile getZipFile(String filePath){
		 ZipFile zipFile = null; 
		 try {
			 zipFile = new ZipFile(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		 return zipFile;
	 }
	 
	 default public void closeZipFile(ZipFile zipFile){
		 try {
			zipFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
	 
	 
	 default boolean isZipContentsOk(ZipFile zipFile, String entryMatcher){
		 	Enumeration<?> zipEntries = zipFile.entries();
			while (zipEntries.hasMoreElements()) {
				String zipEntryExtracted = zipEntries.nextElement().toString();
			    System.out.println( Logger.getLineNumber() + zipEntryExtracted);
			    if(zipEntryExtracted.matches(entryMatcher)){
			    	return true;
			    }
			}
			
			return false;
	 } 
	 
	 default void hoverOverElement(By element){
		 Actions actions = new Actions(Driver.getDriver());
		 actions.moveToElement(getElement(element)).build().perform();
		 waitFor(1);
		 System.out.println( Logger.getLineNumber() + "Hovering performed over [ " + element.toString() + " ]...");
	 }
	 
	 default void hoverOverElement(WebElement element){
		 Actions actions = new Actions(Driver.getDriver());
		 actions.moveToElement(element).build().perform();
		 waitFor(1);
		 System.out.println( Logger.getLineNumber() + "Hovering performed over [ " + element.toString() + " ]...");
	 }
}
