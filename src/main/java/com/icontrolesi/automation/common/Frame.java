package com.icontrolesi.automation.common;

import java.util.Stack;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

import com.icontrolesi.automation.platform.util.Logger;

public class Frame {
	public static Stack<String> FRAME_STACK = new Stack<String>();
	
	public static final String EXTERNAL_PAGE = "_externalPage";
	
	public static void switchToExternalFrame(){
		switchToFrame(EXTERNAL_PAGE);
	}
	
	public static void pushFrameInStack(final String frameName){
		FRAME_STACK.push(frameName);
		System.err.printf("Frame '%s' PUSHED into FRAME_STACK. Script is now on it.\n", frameName);
	}
	
	public static String pullFrameFromStack(){
		String currentFrame = FRAME_STACK.pop();
		System.err.printf("Frame '%s' PULLED out of the FRAME_STACK. Script is now on it.\n", currentFrame);
		return currentFrame;
	}
	
	public static int getTotalFrameInStack(){
		System.err.printf("Total of %d frame(s) in the FRAME_STACK.\n", FRAME_STACK.size());
		
		return FRAME_STACK.size();
	}
	
	public static void switchToDefaultFrame(){
		Driver.getDriver().switchTo().defaultContent();
		FRAME_STACK.clear();
		System.err.println( Logger.getLineNumber() + "\nFrame switched to DEFAULT_FRAME. FRAME_STACK is cleared...\n");
	}
	
	public static void waitForFrameToLoad(final String frameToLoad){
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameToLoad));
		
		System.err.println( Logger.getLineNumber() + "\nFrame switched to " + frameToLoad + "...\n");
	}
	
	public static void switchToFrame(String frameName){
		waitForFrameToLoad(frameName);
		pushFrameInStack(frameName);
	}
}
