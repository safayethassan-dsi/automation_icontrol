package com.icontrolesi.automation.framework.root;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.Logger;

public class ClassRetriever {
	public static void main(String[] args) throws ClassNotFoundException {
		String a = "test/hello/fuchka/";
		String a2 = "test\\hello\\fucka\\";
		System.out.println(a2.replaceAll("/|\\\\", "/"));
	}

	@Test(enabled = false)
	public void helloTest() {

	}

	@SuppressWarnings("unused")
	public static List<Method> getMethodsAnnotatedWith(final Class<?> type,
			final Class<? extends Annotation> annotation) {
		final List<Method> methods = new ArrayList<Method>();
		Class<?> klass = type;
		while (klass != Object.class) {
			final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(klass.getDeclaredMethods()));
			for (final Method method : allMethods) {
				if (method.isAnnotationPresent(annotation)) {
					methods.add(method);
					Test t = method.getAnnotation(Test.class);
				}
			}
			klass = klass.getSuperclass();
		}
		return methods;
	}

	public static List<String> getDisabledTestcases(final Class<?> type, final Class<? extends Annotation> annotation) {
		final List<String> disabledMethods = new ArrayList<String>();
		Class<?> klass = type;
		while (klass != Object.class) {
			final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(klass.getDeclaredMethods()));
			for (final Method method : allMethods) {
				if (method.isAnnotationPresent(annotation) && method.getAnnotation(Test.class).enabled() == false) {
					disabledMethods.add(method.getName());
					@SuppressWarnings("unused")
					Test t = method.getAnnotation(Test.class);
				}
			}
			klass = klass.getSuperclass();
		}
		return disabledMethods;
	}

	public static List<String> retrieveAllClasses(String packageNameOrClass) throws FileNotFoundException {
		List<String> classList = new ArrayList<String>();
		//String fileSeperator = AppConstant.PATH_SEPERATOR;
		String fileSeperator = "/";
		String projectRoot = System.getProperty("user.dir").replaceAll("/|\\\\", fileSeperator);
		String projectSrcFolder = projectRoot + fileSeperator + "src" + fileSeperator + "main" + fileSeperator + "java"
				+ fileSeperator;
		String classPathLocation = "";
		packageNameOrClass = packageNameOrClass.trim();
		if (packageNameOrClass.endsWith(".java")) {
			classPathLocation = projectSrcFolder
					+ packageNameOrClass.replace(".java", "").replaceAll("\\.", fileSeperator).concat(".java");
		} else {
			classPathLocation = projectSrcFolder + packageNameOrClass.replaceAll("\\.", fileSeperator);
		}

		File file = new File(classPathLocation);

		if (!file.exists()) {
			throw new FileNotFoundException("Specified package/class: " + classPathLocation + " does not exist!");
		} else {
			if (!file.isDirectory()) {
				String fileFullPath = file.getAbsolutePath().replaceAll("/|\\\\", fileSeperator);
				String fullySpecifiedName = fileFullPath.replace(projectSrcFolder, "").replaceAll(fileSeperator, "\\.")
						.replace(".java", "");

				classList.add(fullySpecifiedName);
			} else {
				File[] classes = file.listFiles();

				for (File clazz : classes) {
					if (!clazz.isDirectory()) {
						String fileFullPath = clazz.getAbsolutePath().replaceAll("/|\\\\", fileSeperator);
						String fullySpecifiedName = fileFullPath.replace(projectSrcFolder, "")
								.replaceAll(fileSeperator, "\\.").replace(".java", "");

						classList.add(fullySpecifiedName);
					}
				}
			}
		}

		return classList;
	}
}
