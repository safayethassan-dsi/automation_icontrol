package com.icontrolesi.automation.framework.root;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.atlassian.clover.reporters.json.JSONException;
import com.atlassian.clover.reporters.json.JSONObject;
import com.icontrolesi.automation.platform.util.db.Database;

public class TestCaseExecutorFromDB {
	public static Map<String, String> getSubmittedDataFromDB(){
		String sqlQuery = "SELECT * FROM testcaseconfiguration_submittedtestcases where is_run=0 ORDER BY submission_date"; 
	
		Connection connection = Database.getConnection();
		
		ResultSet resultSet = Database.getQueryResult(connection, sqlQuery);
		
		Map<String, String> test_info = new TreeMap<String, String>();
		try {
			resultSet.next();
			test_info.put("id", resultSet.getString("id"));
			test_info.put("test_submission_name", resultSet.getString("test_submission_name"));
			test_info.put("test_information", resultSet.getString("test_information"));
			test_info.put("submitted_by", resultSet.getString("submitted_by"));
			test_info.put("submission_date", resultSet.getTimestamp("submission_date").toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Database.closeConnection(connection);
		
		return test_info;
	}
	
	
	public static List<String> getSubmittedTestcaseList(Map<String, String> submittedData){
		String tcs = submittedData.get("test_information");
		List<String> tc_list = new ArrayList<>();
		
		try {
			JSONObject json_obj = new JSONObject(tcs);
			String [] tc_ts = json_obj.getString("test_cases").split(";");
			
			for(String tc : tc_ts){
				String [] temp = tc.split(":");
				tc_list.add(temp[0]+"."+temp[1]);
			}
			
			System.out.println(tc_list);
			
		} catch (JSONException e) {
			e.printStackTrace();
		} 
		
		
		return tc_list;
	}
	
	public static void main(String[] args) {
		System.err.println(getSubmittedTestcaseList(getSubmittedDataFromDB()));
		TestcaseDataTable.generateTestNGXML(getSubmittedTestcaseList(getSubmittedDataFromDB()), "testng_x.xml");
	}
}
