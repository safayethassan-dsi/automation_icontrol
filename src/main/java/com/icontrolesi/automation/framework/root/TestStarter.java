package com.icontrolesi.automation.framework.root;
import java.awt.GridLayout;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.jfree.chart.JFreeChart;
import org.testng.TestNG;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.ChartCreator;
import com.icontrolesi.automation.platform.util.DialogManager;
import com.icontrolesi.automation.platform.util.MailSender;
import com.icontrolesi.automation.platform.util.PDFCreator;
import com.icontrolesi.automation.report.CustomSummaryReport;

import com.icontrolesi.automation.platform.util.Logger;

public class TestStarter {
	/*private static char [] password = new char[100];
	private static String userID = "";*/
	private static String userID = Configuration.getConfig("system.email_sender");
	private static char [] password = Configuration.getConfig("system.sender_password").toCharArray();

	private List<String> suites;
	private TestNG testng;
	
	
	private long startTime = 0;
	private long endTime = 0;
	public static String START_DATE = "";
	public static String END_DATE = "";
	
	/** this will run the whole test suite */
	public void startTest() {
		
		/*if(MailSender.isMailSendingOptionEnabled()){
			readUserCredentials();
		}*/
		
		try	{
			testng = new TestNG();
			testng.setVerbose(0);
			
			suites = new ArrayList<String>();
			suites.add(AppConstant.TESTNG_XML);
		
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yy_hh-mm-ss");
			SimpleDateFormat sdf_2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date date = new Date();
			
			String fileName = sdf.format(date);
			Configuration.setConfig("system.reportFolderName", fileName);
			System.out.println( Logger.getLineNumber() + "Date: " + fileName);
			String reportDirectory = "TestResults/"+Configuration.getConfig("selenium.browser") + "/" + fileName + "/"; // need to understand this line
			
			File outputDir = new File(reportDirectory);
			if(!outputDir.exists())
				outputDir.mkdir();
			//String reportDirectory = "TestResults/";
			testng.setOutputDirectory(reportDirectory);
				
			testng.setTestSuites(suites);
			//testng.addListener(tla);
			
			startTime = System.currentTimeMillis();
			START_DATE = sdf_2.format(new Date());
			Configuration.setConfig("system.startdate", START_DATE);
			Configuration.setConfig("system.latest_screenshots_path", AppConstant.SCREENSHOT_DIRECTORY + Configuration.getConfig("system.startdate").replaceAll(":", "_"));
			//need to understand line no.79
			testng.run();
			
			endTime = System.currentTimeMillis();
			END_DATE = sdf_2.format(new Date());
			Configuration.setConfig("system.enddate", END_DATE);
			
			TestcaseDataTable.showDuration(endTime - startTime);
		  }catch(Exception ex) {
			 endTime = System.currentTimeMillis();
			 TestcaseDataTable.showDuration(endTime - startTime);
			  
			 String errorMsg = "Error reported on: "+new Date()+"\nEither Testsuite '"+testng.getDefaultSuiteName()+"' and TestCases in '"+testng.getDefaultTestName()+"' does not exist.\n";
			 System.out.println( Logger.getLineNumber() + errorMsg+" Message - "+ex.getMessage());
			 ex.printStackTrace();
		}finally{
			JFreeChart chart = ChartCreator.generatePieChartForTestExecution(CustomSummaryReport.getReportSummaryMap());
			PDFCreator.create(chart, 400, 500, AppConstant.ATTACHED_PDF_PATH);
			
			if(MailSender.isMailSendingOptionEnabled()){
				List<String> attachments = Arrays.asList(
															AppConstant.ATTACHED_PDF_PATH,
															testng.getOutputDirectory() + "emailable-report.html"
														);
				//MailSender.sendMail(userID, password, "Test cases execution completed...", CustomSummaryReport.getExecutionSummary(), attachments);
				MailSender.sendMail(userID, Configuration.getConfig("system.sender_password").toCharArray(), "Test cases execution completed...", CustomSummaryReport.getExecutionSummary(), attachments);
			}
			
			DialogManager.showTimedDialogue(CustomSummaryReport.getExecutionSummary());
		}
	}
	
	
	public void readUserCredentials(){
	   JPanel userPanel = new JPanel(new GridLayout(2, 1));
	   JPanel userIDPanel = new JPanel();
	   JPanel passwordPanel = new JPanel();
	   JLabel userIdLabel = new JLabel("Enter User ID/Email  :  ");
	   JTextField userIdField = new JTextField(Configuration.getConfig("system.email_sender"), 10);
	   JLabel userPassLabel = new JLabel("Enter user password: ");
	   JPasswordField passField = new JPasswordField(10);
	  
	   userIDPanel.add(userIdLabel);
	   userIDPanel.add(userIdField);
	   
	   passwordPanel.add(userPassLabel);
	   passwordPanel.add(passField);
	   
	   userPanel.add(userIDPanel);
	   userPanel.add(passwordPanel);
	   
	   String[] options = new String[]{"OK", "Cancel"};
	   int option = JOptionPane.showOptionDialog(null, userPanel, "The title", JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
	   if(option == 0) // pressing OK button
	   {
	       password = passField.getPassword();
	       userID = userIdField.getText();
	       System.out.println( Logger.getLineNumber() + "Your User ID is: " + new String(userID));
	   }	
	   
	   if(!userID.equals("iceautomationtest@gmail.com")){
	     	 userID = Configuration.getConfig("system.email_sender");
	   }
	  
	   if(!password.equals("")){
	     	 password = Configuration.getConfig("system.sender_password").toCharArray();
	   }
	   
	   System.err.println(userID);
	}
}
