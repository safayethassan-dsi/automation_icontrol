package com.icontrolesi.automation.framework.root;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.PropertyReader;
import com.icontrolesi.automation.platform.util.TestHelper;

import com.icontrolesi.automation.platform.util.Logger;

@SuppressWarnings("serial")
public class TestcaseDataTable extends JFrame implements TableCellRenderer, ActionListener, TableModelListener{
	static Boolean continueNext = false;
	ButtonGroup buttonGroupForBrowsers = new ButtonGroup();
	
	JCheckBox checkAll;
	JCheckBox hideBrowserWindow;
	JCheckBox runLatestFailedTestcases;
	JLabel statusOfSelectedTcs;
	JComboBox<String> urlSelectionOption;
	JComboBox<String> retryCountOption;
	
	JTextField testClassOrPakckageName = new JTextField(20);
	JButton addTestCaseOrPackageName = new JButton("Add");
	
	private JTable table = null;
	JTable table2 = new JTable();
	
	JTextField searchBox = new JTextField(38);
	JTextField searchBox2 = new JTextField(38);
	JLabel searchLabel = new JLabel("Search: ");
	
	JCheckBox sendEmail = null;
	
	static int totalSelectedTCNumber = 0;
	
	List<String> disabledTCs = new ArrayList<String>();
	Map<String, String> tcWiseComment = new HashMap<String, String>();
	long totalDisabledTCs = 0;
	
	String [] columnNames = {"Serial# ", "TC#", "TC Suite/Package", "TC Name", "Is Selected?"};
	String [] enabledTCs = {"Serial# ", "TC#", "TC Suite/Package", "TC Name", "Disabled?", "Comment"};
	
	//Properties tc_props = PropertyReader.getPropertiesFrom(AppConstant.RESOURCE_FOLDER + "testcase_status.properties");
	Properties tc_props = PropertyReader.getPropertiesFrom(AppConstant.TESTCASE_STATUS_FILE);
	
	String projectSelected = AppConstant.PROJECT_SELECTED;
	
	String testcasesConfigfile = projectSelected + ".testcases.properties";
	
	public TestcaseDataTable() {
		super("Testcase selector window");
		
		Object [][] data = fillData(AppConstant.TESTCASE_CONFIG_FILE);
		
		totalDisabledTCs = getCountOfDisabledTCs();
		
		DefaultTableModel defaultModel = new DefaultTableModel(data, columnNames);
		
		table = new JTable(defaultModel){
			@Override
			public Class<?> getColumnClass(int column) {
				return (getValueAt(0, column).getClass());
			}

			@Override          
            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                
                String cellValue = getValueAt(rowIndex, 3).toString();

                try {
                		if(disabledTCs.get(rowIndex).equals(cellValue) || disabledTCs.contains(cellValue)){
                			String comment = "No reason specified for this test case.";
                			String tc_info = tc_props.getProperty((String)getValueAt(rowIndex, 3));
                			try {
								comment = tc_info.split(";")[1].trim();
							} catch (IndexOutOfBoundsException iobe) {
								System.out.println( Logger.getLineNumber() + iobe.getMessage());
							}
                			tip = "<HTML><BODY><P style='color:yellow; background:teal; padding: 5px 15px'> Test case <B style='color: white'><U>" + getValueAt(rowIndex, 3).toString() + "</U></B> is disabled. Can't be selected.</P>";
                			tip += "<P style='color:yellow; background:teal; padding: 5px 15px'> Comment: <B style='color: white'>" + comment + "</P></BODY></HTML>";
                		}else{
                			tip = "<HTML><BODY><P style='color:yellow; background:teal; padding: 5px 15px'><B><U>" + getColumnName(colIndex) + ":</U></B> " + getValueAt(rowIndex, colIndex).toString() + "</P></BODY></HTML>";
                		}
                } catch (RuntimeException e1) {
            
                }

                return tip;
            }
			
			
		    
			
		   @Override
           public boolean isCellEditable(int rowIndex, int columnIndex) {
                String cellValue = getValueAt(rowIndex, 3).toString();
                if(disabledTCs.get(rowIndex).equals(cellValue) || disabledTCs.contains(cellValue)){
                    return false;
                }
                return true;
            }
            
        };
        
        if(table.getValueAt(0, 2).toString().equals("No data found!")){
        	table.setTableHeader(null);
        }
        
   	
		table.setDefaultRenderer(Object.class, this);
		
		TableRowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(table.getModel());
		
		table.setRowSorter(rowSorter);
		
		searchBox.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent documentEvent) {
				
				String text = searchBox.getText();
				
				if(text.length() == 0){
					rowSorter.setRowFilter(null);
				}else{
					rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
				}
			}
			
			@Override
			public void insertUpdate(DocumentEvent documentEvent) {
				String text = searchBox.getText();
				
				if(text.length() == 0){
					rowSorter.setRowFilter(null);
				}else{
					rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
				}
			}
			
			@Override
			public void changedUpdate(DocumentEvent documentEvent) {
				throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
				
			}
		});
		
		table.getModel().addTableModelListener(this);
		
		
		
		Object [][] data2 = getDataForDisabledTableItems(table);
		
		DefaultTableModel defaultModel2 = new DefaultTableModel(data2, enabledTCs);
		
		
		table2 = new JTable(defaultModel2){
			@Override
			public Class<?> getColumnClass(int column) {
				return (getValueAt(0, column).getClass());
			}

			@Override          
            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                
                String cellValue = getValueAt(rowIndex, 3).toString();

                try {
                		if(disabledTCs.get(rowIndex).equals(cellValue) || disabledTCs.contains(cellValue)){
                			String comment = "No reason specified for this test case.";
                			String tc_info = tc_props.getProperty((String)getValueAt(rowIndex, 3));
                			try {
								comment = tc_info.split(";")[1].trim();
							} catch (IndexOutOfBoundsException iobe) {
								System.err.println( Logger.getLineNumber() + iobe.getMessage()+"************");
							}
                			tip = "<HTML><BODY><P style='color:yellow; background:teal; padding: 5px 15px'> Test case <B style='color: white'><U>" + getValueAt(rowIndex, 3).toString() + "</U></B> is disabled. Can't be selected.</P>";
                			tip += "<P style='color:yellow; background:teal; padding: 5px 15px'> Comment: <B style='color: white'>" + comment + "</P></BODY></HTML>";
                		}else{
                			tip = "<HTML><BODY><P style='color:yellow; background:teal; padding: 5px 15px'><B><U>" + getColumnName(colIndex) + ":</U></B> " + getValueAt(rowIndex, colIndex).toString() + "</P></BODY></HTML>";
                		}
                } catch (RuntimeException e1) {
            
                }

                return tip;
            }
			
			
		    
		/*	
		   @Override
           public boolean isCellEditable(int rowIndex, int columnIndex) {
                String cellValue = getValueAt(rowIndex, 3).toString();
                if(disabledTCs.get(rowIndex).equals(cellValue) || disabledTCs.contains(cellValue)){
                    return false;
                }
                return true;
            }*/
            
        };
		
        
        
        
        
        
        table2.setDefaultRenderer(Object.class, this);
		
		
		table2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				int selectedColumn = ((JTable)e.getSource()).getSelectedColumn();

				if(selectedColumn == enabledTCs.length - 1){
					int selectedRow = ((JTable)e.getSource()).getSelectedRow();
					
					String commentInserted = JOptionPane.showInputDialog("Put your comment here: ");
					
					tcWiseComment.put(table2.getValueAt(selectedRow, 3).toString(), commentInserted);
					
					System.err.println("You clicked at: " + selectedRow + "\nComment: "+ commentInserted);
				}
			}
		});
		
			
		TableRowSorter<TableModel> rowSorter2 = new TableRowSorter<TableModel>(table2.getModel());
		
		table2.setRowSorter(rowSorter2);
		
		searchBox2.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent documentEvent) {
				
				String text = searchBox.getText();
				
				if(text.length() == 0){
					rowSorter2.setRowFilter(null);
				}else{
					rowSorter2.setRowFilter(RowFilter.regexFilter("(?i)" + text));
				}
			}
			
			@Override
			public void insertUpdate(DocumentEvent documentEvent) {
				String text = searchBox.getText();
				
				if(text.length() == 0){
					rowSorter2.setRowFilter(null);
				}else{
					rowSorter2.setRowFilter(RowFilter.regexFilter("(?i)" + text));
				}
			}
			
			@Override
			public void changedUpdate(DocumentEvent documentEvent) {
				throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
				
			}
		});
		
		
		table2.getModel().addTableModelListener(this);
        
		
		
		UIManager.put("MenuBar.background", new Color(122, 170, 170));
		UIManager.put("Menu.selectionBackground",  new Color(180, 180, 180));
		UIManager.put("Menu.selectionForeground", Color.WHITE);
		UIManager.put("MenuItem.background", new Color(129, 192, 180));
		UIManager.put("MenuItem.foreground", Color.WHITE);
		UIManager.put("MenuItem.selectionForeground", new Color(129, 129, 120));
		
		JMenu fileMenu = new JMenu("App");
		JMenu helpMenu = new JMenu("Help");
		
		JMenuItem restart = new JMenuItem("Restart App");
		restart.setActionCommand("restart");
		restart.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		restart.addActionListener(this);
		
		JMenuItem exit = new JMenuItem("Exit Window");
		exit.setActionCommand("exit");
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		exit.addActionListener(this);
		
		fileMenu.add(restart);
		fileMenu.add(exit);
				
		JMenuItem helpContent = new JMenuItem("Help Content");
		helpContent.setActionCommand("help_content");
		helpContent.addActionListener(this);
		
		JMenuItem about = new JMenuItem("About this Project");
		about.setActionCommand("about_project");
		about.addActionListener(this);
		
		helpMenu.add(helpContent);
		helpMenu.add(about);
		
		JMenuBar menuBar = new JMenuBar();
		
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		
		setJMenuBar(menuBar);
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		JPanel testCasePanel = new JPanel();
		testCasePanel.add(scrollPane, BorderLayout.CENTER);
		
		JPanel searchPanel = new JPanel();
		searchPanel.add(searchLabel, BorderLayout.WEST);
		searchPanel.add(searchBox, BorderLayout.EAST);
		
		JPanel statusPanel = new JPanel();
		statusOfSelectedTcs = new JLabel((totalSelectedTCNumber == 0) ?  "<html><p style='color:red'><b>Total TCs: " + data.length + " [" + totalDisabledTCs + " disabled ].<br />No testcases selected yet.</b></span></html>" : "<html>Total testcases selected: <span style='color:green'><b>" + totalSelectedTCNumber + "</b</span>></html>");
		statusPanel.add(statusOfSelectedTcs, BorderLayout.LINE_START);
			
		testCasePanel.add(searchPanel, BorderLayout.NORTH);
		testCasePanel.add(statusPanel);
		
		
		JScrollPane scrollPane2 = new JScrollPane(table2);
		JPanel table2Pane = new JPanel(); 
		table2Pane.add(scrollPane2);
		JPanel enableDisableTC = new JPanel();
		
		
		JPanel updateTCPanel = new JPanel();
		JButton enableDisableSelectedTCsBtn = new JButton("Update Selected");
		enableDisableSelectedTCsBtn.setActionCommand("enableDisableSelectedTCs");
		enableDisableSelectedTCsBtn.addActionListener(this);
		
		JPanel searchPanel2 = new JPanel();
		searchPanel2.add(searchLabel);
		searchPanel2.add(searchBox2);
		
		
		updateTCPanel.add(enableDisableSelectedTCsBtn);
		
		enableDisableTC.add(table2Pane, BorderLayout.NORTH);
		enableDisableTC.add(searchPanel2, BorderLayout.CENTER);
		enableDisableTC.add(updateTCPanel, BorderLayout.SOUTH);
		
		JPanel settingPanel = new JPanel();
		String [] urlList = AppConstant.URL.split(";");
		urlSelectionOption = new JComboBox<String>(urlList);
		urlSelectionOption.setEditable(true);
		urlSelectionOption.setActionCommand("url");
		urlSelectionOption.addActionListener(this);
		
		
		JButton addNewURL = new JButton("Add New URL");
		addNewURL.setActionCommand("addNewURL");
		addNewURL.addActionListener(this);
		
		JButton removeSelectedURL = new JButton("Remove Selected");
		removeSelectedURL.setActionCommand("removeSelectedURL");
		removeSelectedURL.addActionListener(this);
		
		JPanel urlSelectionPanel = new JPanel();
		urlSelectionPanel.setBorder(BorderFactory.createTitledBorder("URL to Run test: "));
		urlSelectionPanel.add(urlSelectionOption);
		urlSelectionPanel.add(addNewURL);
		urlSelectionPanel.add(removeSelectedURL);
		
		settingPanel.add(urlSelectionPanel);
		
		String seletedBrowser = Configuration.getConfig("selenium.browser");
		
		JRadioButton firefox = new JRadioButton("FireFox", seletedBrowser.equalsIgnoreCase("firefox"));
		firefox.setActionCommand("firefox");
		JRadioButton ie11 = new JRadioButton("Internet Explorer 11", seletedBrowser.equalsIgnoreCase("ie11"));
		ie11.setActionCommand("ie11");
		JRadioButton chrome = new JRadioButton("Chrome", seletedBrowser.equalsIgnoreCase("chrome"));
		chrome.setActionCommand("chrome");
		JRadioButton safari = new JRadioButton("Safari", seletedBrowser.equalsIgnoreCase("safari"));
		safari.setActionCommand("safari");
		
		buttonGroupForBrowsers.add(firefox);
		buttonGroupForBrowsers.add(ie11);
		buttonGroupForBrowsers.add(chrome);
		buttonGroupForBrowsers.add(safari);
		
		JPanel browserPanel = new JPanel(); 
		browserPanel.setBorder(BorderFactory.createTitledBorder("Browser:"));
		browserPanel.add(firefox);
		browserPanel.add(ie11);
		browserPanel.add(chrome);
		browserPanel.add(safari);
		
		settingPanel.add(browserPanel, BorderLayout.PAGE_START);
		
		JPanel checkAllPanel = new JPanel();
		checkAllPanel.setBorder(BorderFactory.createTitledBorder("Testing Type:"));
		checkAll = new JCheckBox("Regression?");
		checkAll.setActionCommand("checkAll");
		checkAll.addActionListener(this);
		checkAllPanel.add(checkAll);
		
		settingPanel.add(checkAllPanel, BorderLayout.CENTER);
		
		JPanel automaticRetryCount = new JPanel();
		
		JLabel retryCountLabel = new JLabel("Maximum Re-try: ");
		retryCountOption = new JComboBox<>(new String [] {"No Retry", "1 time", "2 times", "3 times", "4 times", "5 times"});
		retryCountOption.setActionCommand("maxRetry");
		retryCountOption.addActionListener(this);
		automaticRetryCount.add(retryCountLabel);
		automaticRetryCount.add(retryCountOption);
		
		settingPanel.add(automaticRetryCount);
		
		addButtonForFailedTCs(settingPanel);
		
		
		JPanel emailSendingPanel = new JPanel();
		sendEmail = new JCheckBox("Send Email?", false);

		JPanel browserTweakingPanel = new JPanel();
		hideBrowserWindow = new JCheckBox("Hide Browser?", true);

		browserTweakingPanel.add(hideBrowserWindow);

		emailSendingPanel.add(sendEmail);

		
		settingPanel.add(emailSendingPanel);
		settingPanel.add(browserTweakingPanel);
		
		
		JButton btn2 = new JButton("Execute");
		btn2.setBackground(new Color(200, 200, 200));
		btn2.setForeground(new Color(52, 73, 94));
		btn2.setFont(new Font("Arial", Font.BOLD, 25));
		btn2.setActionCommand("execute");
		btn2.addActionListener(this);
		add(btn2, BorderLayout.SOUTH);
		
		
		JPanel updateOrAddTC = new JPanel(new GridLayout(1, 3));
			
		JPanel tcTextFieldPanel = new JPanel();
		JPanel addBtnPanel = new JPanel();
		
		addTestCaseOrPackageName.setActionCommand("addTestCaseOrPackage");
		addTestCaseOrPackageName.addActionListener(this);
				
		tcTextFieldPanel.add(testClassOrPakckageName);
		addBtnPanel.add(addTestCaseOrPackageName, FlowLayout.LEFT);
		
		updateOrAddTC.add(tcTextFieldPanel, FlowLayout.LEFT);
		updateOrAddTC.add(addBtnPanel);
		
		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.add(testCasePanel, "Testcase Submission");
		tabbedPane.add(updateOrAddTC, "Update/Add Testcase");
		tabbedPane.add(enableDisableTC, "Enable/Disable Testcase");
		tabbedPane.add(settingPanel, "Settings");
		
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if(tabbedPane.getSelectedIndex() == 1){
					JOptionPane.showMessageDialog(null, "This is feature is not implemented yet.\nPlease, add/update test cases using configuration file.\n"
							+ "Find configuration file called '" + testcasesConfigfile +"'\nunder the directory: " + AppConstant.RESOURCE_FOLDER);
				}
			}
		});
		
		add(tabbedPane);
		
		searchBox.requestFocusInWindow();
		
		setSize(540, 630);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}
	
	
	private Object[][] getDataForDisabledTableItems(JTable table){
		int totalRow = table.getRowCount();
		
		System.err.println(table.getValueAt(0, 3));
		
		System.err.println(totalRow);
		
		Object[][] data = new Object[totalRow][];
		
		if(totalRow == 1 && table.getValueAt(0, 2).toString().equals("No data found!")){
			data[0] = new Object[enabledTCs.length];
			data[0][0] = "";
			data[0][1] = "";
			data[0][2] = "No data found!";
			data[0][3] = "";
			data[0][4] = "";
			data[0][5] = "";
			
			return data;
		}
		
		for(int i = 0; i < totalRow; i++){
			data[i] = new Object[enabledTCs.length];
			
			String comment = "";
			
			if(tc_props.getProperty(disabledTCs.get(i)) == null){
				comment = "No comment found!";
			}else{
				String [] temp = tc_props.getProperty(disabledTCs.get(i)).split(";");
				comment = (temp.length == 2) ? temp[1] : "No comment found!";
			}
			
			data[i][0] = i + 1;
			data[i][1] = table.getValueAt(i, 1);
			data[i][2] = table.getValueAt(i, 2);
			data[i][3] = table.getValueAt(i, 3);
			data[i][4] = disabledTCs.get(i).equals(data[i][3]);
			data[i][5] = comment;
		}
		
		return data;
	}
	
	
	private Object[][] fillData(String dataSourcePath){
		List<String> classList = new ArrayList<String>();
		BufferedReader br = null;
		try {
				String packageName = "";	
				br = new BufferedReader(new FileReader(dataSourcePath));
				int lineNumber  = 0;
				int commentedPackage = 0;
				
				while(true){
					
					packageName = br.readLine();
					
					if(packageName == null && lineNumber == 0){
						System.err.println("No testcase in the file: " + (new File(dataSourcePath).getName()) + ". Please, add testcase(s) to run...");
						JOptionPane.showMessageDialog(null, "No testcase in the file '"
								+ (new File(dataSourcePath).getName())
								+ "'.\nPlease, add testcase(s) to run...", "Testcase not found", JOptionPane.WARNING_MESSAGE);
						System.exit(0);
					}else if(packageName == null && commentedPackage == lineNumber){
						JOptionPane.showMessageDialog(null ,"It seems some of the testcases in '"
								+ (new File(dataSourcePath).getName()) 
								+ "' are commented out.\nPlease, uncomment those first to run...", "Commented out testcases", JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);
					}else if(packageName != null){
							if(packageName.startsWith("#")){
								commentedPackage++;
							}else{
								classList.addAll(ClassRetriever.retrieveAllClasses(packageName));
							}
					}else{
						break;
					}	
					
					lineNumber++;
				}
				
					
			} catch (IOException e) {
				System.err.println(e.getMessage());
				
				Object [][] data = new Object[1][columnNames.length];
//				data[0] = new Object[1];
				data[0][0] = "";
				data[0][1] = "";
				data[0][2] = "No data found!";
				data[0][3] = "";
				data[0][4] = "";
//				table.setTableHeader(null);
				
				return data;
				//System.exit(0);
			}finally {
				try {
					if(br != null)
						br.close();
					
				} catch (Exception e) {
					System.err.println("No files to close!");
				}
			}
		
		int indexCount = classList.size();
		Object [][] data = new Object[indexCount][] ;
		
		int counter = 0;
		
		for(String clazz : classList){
			String [] temp = clazz.split("\\.");
			String testCaseName = temp[temp.length - 1];
			Class<?> clazzName = null;
			
			try {
				clazzName = Class.forName(clazz);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			Method method = ClassRetriever.getMethodsAnnotatedWith(clazzName, Test.class).get(0);
			String testcaseID = method.getName().split("_")[1];
			
				data[counter] = new Object[columnNames.length];
				
				data[counter][0] = counter + 1;
				data[counter][1] = testcaseID;
				data[counter][2] = clazz.replace("." + testCaseName, "");
				data[counter][3] = testCaseName;
				data[counter][4] = false;
//				data[counter][5] = "";
				
				String tc_info = tc_props.getProperty(testCaseName);
				
				String tc_status = "";
				
				try{
					String [] tempInfo = tc_info.split(";");
					tc_status = tempInfo[0];
				}catch (ArrayIndexOutOfBoundsException aie) {
					System.err.println("No reason provided for the disabled test case: " + testCaseName);
				}catch(NullPointerException npe){
					//bypassed for enabled TCs
				}
				 
				
				if((tc_status != null && tc_status.equals("false")) || method.getAnnotation(Test.class).enabled() == false){
					disabledTCs.add(testCaseName);
				}else{
					disabledTCs.add("");
				}
				
				counter++;
		}
		
		return data;
	}
	
	
	private int getTotalTestcaseNumberFrom(String testCaseFile){
		List<String> classList = new ArrayList<String>();
		try {
			String packageNameOrClass = "";	
			BufferedReader br = new BufferedReader(new FileReader(testCaseFile));
			
			while((packageNameOrClass = br.readLine()) != null){
				classList.addAll(ClassRetriever.retrieveAllClasses(packageNameOrClass));
			}	
			br.close();
		} catch (IOException e) {
			System.out.println( Logger.getLineNumber() + "No failed test cases found.");
		}
		
		return classList.size();
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object object, boolean isSelected, boolean hasFocus, int row, int column) {
		Component renderer = new DefaultTableCellRenderer().getTableCellRendererComponent(table,object, isSelected, hasFocus, row, column);
		Color foreground, background;
		
		String cellValue = table.getValueAt(row, 3).toString();
		
		if(isSelected){
			foreground = Color.RED;
			background = Color.YELLOW;
		}else{
			if(row % 2 == 0){
				foreground = Color.BLACK;
				background = Color.LIGHT_GRAY;
			}else{
				foreground = Color.BLACK;
				background = Color.WHITE;
			}
		}
		
		if(disabledTCs.size() > 0 && (disabledTCs.get(row).equals(cellValue) || disabledTCs.contains(cellValue))){
			foreground = new Color(200, 200, 200);
			background = new Color(220,80,80);
		}
		
		renderer.setForeground(foreground);
		renderer.setBackground(background);
		
		return renderer;
	}
	
	
	private List<String>  getSelectedTestCases(){
		TableModel data = table.getModel();
		
		List<String> classList = new ArrayList<String>();
		for(int i = 0; i < data.getRowCount(); i++){
			if((boolean)data.getValueAt(i, data.getColumnCount() - 1) == true){
				String packageName =  (String) data.getValueAt(i, 2);
				String clazzName = (String) data.getValueAt(i, 3);
				String fullClazzName = packageName + "." + clazzName;
				classList.add(fullClazzName);
			}
		}
		
		return classList;
	}
	
	protected static List<String> getTestCaseListFromFile(String fileName){
		List<String> classList = new ArrayList<String>();
		
		
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader((new File("src/main/resources/" + fileName))));
			
			String packageName = "";
			while((packageName = (fileReader.readLine())) != null){
				classList.addAll(ClassRetriever.retrieveAllClasses(packageName));
			}
			
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return classList;
	}
	
	private boolean isTestCaseSelected(){
		TableModel data = table.getModel();
	
		if(table.getRowCount() == 1 && table.getValueAt(0, 2).toString().equals("No data found!")){
			return false;
		}
		for(int i = 0; i < data.getRowCount(); i++){
			if((boolean)data.getValueAt(i, data.getColumnCount() - 1) == true){
				return true;
			}
		}
		
		return false;
	}
	
	private void saveTestCasesInFile(String testCaseFileName){
		List<String> classList = getSelectedTestCases();
		
		generateTestNGXML(classList, testCaseFileName);
	}
	
	

	public static void generateTestNGXML(List<String> classList, String testCaseFileName){
		String defaultTimeOut = Configuration.getConfig("system.testcase_timeout");
		while(true){
			if(defaultTimeOut == null || defaultTimeOut.equals("") ){
				defaultTimeOut = JOptionPane.showInputDialog(null, "\nNo time-out defined in the configuration file.\nPlease, input a default time-out (in minutes)...\n\n", 20);
			}else{
				break;
			}
		}
		
		int testcaseTimeout = (defaultTimeOut == null) ? 30 * 60 * 1000 : Integer.parseInt(defaultTimeOut) * 60 * 1000;
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(testCaseFileName));
			bw.append("<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\" >\n");
			bw.append("<suite name=\"Demo Suite\" time-out=\"" + testcaseTimeout + "\" verbose=\"1\" >\n");
			bw.append("\t<listeners>\n");
			bw.append("\t\t<listener class-name=\"com.icontrolesi.automation.platform.util.RetryListener\" />\n");
			bw.append("\t\t<listener class-name=\"com.icontrolesi.automation.report.CustomSummaryReport\" />\n");
			bw.append("\t</listeners>\n");
			bw.append("\t<test name=\"Recenseo Test\" >\n");
			bw.append("\t\t<classes>\n");
			
				
			for(String clazz : classList){
				bw.append("\t\t\t<class name=\"" + clazz + "\" />\n");
				bw.flush();
			}
			
			bw.append("\t\t</classes>\n");
			bw.append("\t</test>\n");
			bw.append("</suite>\n");
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@SuppressWarnings("unused")
	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		String actionCommand = actionEvent.getActionCommand();
		System.err.println("\nCommand: " + actionCommand + "\n");
		//String selectedURL = urlSelectionOption.getSelectedItem().toString().trim();
		String selectedBrowser = buttonGroupForBrowsers.getSelection().getActionCommand();
		
		String retryCount = retryCountOption.getSelectedItem().toString();
		
		retryCount = retryCount.equalsIgnoreCase("No Retry") ? "0" : retryCount.replaceAll("\\D+", "");
		
		if(actionCommand.equals("about_project")){
			JOptionPane.showMessageDialog(null, "This is an Automation Project for Recenseo", "About this Project", JOptionPane.INFORMATION_MESSAGE);
		}else if(actionCommand.equals("help_content")){
			JOptionPane.showMessageDialog(null, "No content yet. Under development.", "Help content", JOptionPane.INFORMATION_MESSAGE);
		}else if(actionCommand.equals("restart")){
			int choice = JOptionPane.showConfirmDialog(null, "Are you sure to restart?", "Restart??", JOptionPane.YES_NO_OPTION);
			
			if(choice == JOptionPane.YES_OPTION){
				dispose();
				new TestcaseDataTable();
			}
		}else if(actionCommand.equals("exit")){
			int choice = JOptionPane.showConfirmDialog(null, "Are you sure to quit?", "Exit??", JOptionPane.YES_NO_OPTION);
			
			if(choice == JOptionPane.YES_OPTION)
				System.exit(0);
		}else if(actionCommand.equals("enableDisableSelectedTCs")){
			Properties props = new Properties();
			int totalTCs = table2.getRowCount();
			
			
			for(int i = 0; i < totalTCs; i++){
				String tc = (String)table2.getValueAt(i, 3);
				boolean status = !(boolean)table2.getValueAt(i, 4);
				String comment = (String)table2.getValueAt(i, 5);
				String status_comment = String.valueOf(status) + ";" + comment;
				
				
				props.setProperty(tc, status_comment);
				
				for(Map.Entry<String, String> entry : tcWiseComment.entrySet()){
					String tc_completeStatusWithComment = props.getProperty(entry.getKey());
					String statusExtracted = tc_completeStatusWithComment == null ? "true" : String.valueOf((tc_completeStatusWithComment.split(";")[0]));
					String status_comment2 = statusExtracted + ";" + entry.getValue();
					props.setProperty(entry.getKey(), status_comment2);
				}
			}
			
			
			try{
				File file = new File(AppConstant.TESTCASE_STATUS_FILE);
				FileOutputStream fileOut = new FileOutputStream(file);
				props.store(fileOut, "Test cases status");
				fileOut.close();
				
				System.err.println(tcWiseComment);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			dispose();
			new TestcaseDataTable();
		}else if(actionCommand.equals("addNewURL")){
			String url = JOptionPane.showInputDialog("Enter a new URL", "URL");
			
			try {
				URL url_provided = new URL(url);
				URLConnection conn = url_provided.openConnection();
			} catch (IOException e2) {
				JOptionPane.showMessageDialog(null, "URL not valid!\nURL: "+url, "Invalid URL", JOptionPane.ERROR_MESSAGE);
			}
			
			urlSelectionOption.addItem(url);
			
			Properties props = new Properties();
			try {
				FileInputStream in = new FileInputStream(AppConstant.PROJECT_CONFIG_FILE);
				props.load(in);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			
			props.setProperty(String.format("%s.url", AppConstant.PROJECT_SELECTED), Configuration.getConfig(AppConstant.PROJECT_SELECTED+".url")+url+";");
			System.out.println(Configuration.getConfig(AppConstant.PROJECT_SELECTED+".url")+";"+url+"\n\n\n");
			
			
			try{
				File file = new File(String.format("%s%s.properties", AppConstant.RESOURCE_FOLDER,AppConstant.PROJECT_SELECTED));
				System.out.println(String.format("%s%s.properties", AppConstant.RESOURCE_FOLDER,AppConstant.PROJECT_SELECTED));
				FileOutputStream fileOut = new FileOutputStream(file);
				props.store(fileOut, "Project's configuration info");
				fileOut.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			/*if(url.equals("URL") == false){
				dispose();
				new TestcaseDataTable();
			}*/
		}else if(actionCommand.equals("removeSelectedURL")){
			int option = JOptionPane.showConfirmDialog(null, "Are you sure about the action?", "Removing URL...", JOptionPane.YES_OPTION);
			
			if(option == 0){
				Object selectedOption = urlSelectionOption.getSelectedItem().toString();
				urlSelectionOption.removeItem(selectedOption);
				
				String urlList = AppConstant.URL;
				String updatedURLList = urlList.replaceAll(selectedOption.toString(), "");
				
				Properties props = new Properties();
				
				try {
					FileInputStream in = new FileInputStream(AppConstant.PROJECT_CONFIG_FILE);
					props.load(in);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				
				try{
					props.setProperty(String.format("%s.url", AppConstant.PROJECT_SELECTED), updatedURLList);
					
					File file = new File(String.format("%s%s.properties", AppConstant.RESOURCE_FOLDER,AppConstant.PROJECT_SELECTED));
					FileOutputStream fileOut = new FileOutputStream(file);
					props.store(fileOut, "Project's configuration info");
					fileOut.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				JOptionPane.showMessageDialog(null, "URL deleted:\n" + selectedOption);
			}
			System.out.println(option);
		}
		
		if(actionCommand.equals("execute")){
			String selectedURL = urlSelectionOption.getSelectedItem().toString().trim();
			
			if(sendEmail.isSelected()){
				Configuration.setConfig("system.sendmail", "yes");
			}

			if(hideBrowserWindow.isSelected()){
				Configuration.setConfig("system.hideBrowserWindow", "yes");
			}
			
			
			if(runLatestFailedTestcases != null && runLatestFailedTestcases.isSelected()){
				List<String> classList = new ArrayList<String>();
				
				classList = getTestCaseListFromFile("lastRunFailedTestcases.properties");
				
				if(classList.size() == 0){
					JOptionPane.showMessageDialog(null, "There's no failed testcase from previous execution :)", "Happy buddy!!!", JOptionPane.INFORMATION_MESSAGE);
				}else{
					generateTestNGXML(classList, AppConstant.TESTNG_XML);
					setTitle("Testcases execution started with latest failed testcases...");
					
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					dispose();
					
					Configuration.setConfig("selenium.browser", selectedBrowser);
					Configuration.setConfig(projectSelected+".url", selectedURL);
					Configuration.setConfig("selenium.maxRetry", retryCount);
					
					TestHelper.printSystemInfo();
					
					TestStarter starter = new TestStarter();
			    	starter.startTest();
			    	
			    	TestHelper.printSystemInfo();
				}
			}else if(isTestCaseSelected()){
				saveTestCasesInFile(AppConstant.TESTNG_XML);
				setTitle("Testcases execution is being started...");
				
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				
				dispose();
				
			   	Configuration.setConfig("selenium.browser", selectedBrowser);
			   	Configuration.setConfig(projectSelected+".url", selectedURL);
			   	Configuration.setConfig("selenium.maxRetry", retryCount);
			   	
			   	TestHelper.printSystemInfo();
			   	
		    	TestStarter starter = new TestStarter();
		    	
		    	starter.startTest();
		    	TestHelper.printSystemInfo();
			}else{
				JOptionPane.showMessageDialog(null, "No testcase selected... \nPlease select at least one testcase to begin execution.");
			}
			
		}else if(actionCommand.equals("checkAll")){
			if(checkAll.isSelected()){
				totalSelectedTCNumber = 0;
				
				TableModel data = table.getModel();
				int totalRows = data.getRowCount();
				
				List<String> testcaseList = new ArrayList<String>();
				for(int i = 0; i < totalRows; i++){
					String testcaseName = table.getValueAt(i, 3).toString(); 
					String disabledTC = disabledTCs.get(i);
					boolean isSelected = (boolean)table.getValueAt(i, 4);
					if(!isSelected && (!disabledTC.equals(testcaseName)|| !disabledTCs.contains(testcaseName))){
						testcaseList.add(testcaseName);
						data.setValueAt(true, i, 4);
					}
				}
				
				JOptionPane.showMessageDialog(null, "You selected all the testcases for regression.", "Regression test selected...", JOptionPane.INFORMATION_MESSAGE);
			}else{
				TableModel data = table.getModel();
				int totalRows = data.getRowCount();
				List<String> testcaseList = new ArrayList<String>();
				for(int i = 0; i < totalRows; i++){
					if((boolean)data.getValueAt(i, data.getColumnCount() - 1) == true){
						String tcName = (String) data.getValueAt(i, 3);
						testcaseList.add(tcName);
						data.setValueAt(false, i, 4);
					}
				}
				
				JOptionPane.showMessageDialog(null, "You deselected all the testcases for regression.", "Regression test deselected...", JOptionPane.INFORMATION_MESSAGE);
			}
		}else if(actionCommand.equals("addTestCaseOrPackage")){
			String testCaseOrPackage = testClassOrPakckageName.getText();
			
			String testCaseOrTestPackage = testCaseOrPackage.endsWith(".java") ? "class" : "package"; 
			
			try{
				ClassRetriever.retrieveAllClasses(testCaseOrPackage);
				
				
				BufferedReader br = new BufferedReader(new FileReader(AppConstant.RESOURCE_FOLDER + testcasesConfigfile));
				
				boolean isExist = false;
				while(true){
					String line = br.readLine();
					if(line == null)
						break;
					
					if(line.matches("^#?" + testCaseOrPackage + "$")){
						isExist = true;
						break;
					}
					
				}
				
				br.close();
				
				if(isExist){
					JOptionPane.showMessageDialog(null, String.format("Test %s already added!!!\nPlease, add a new test %s.", testCaseOrTestPackage, testCaseOrTestPackage), String.format("\"Existing test %s!!!\"", testCaseOrTestPackage), JOptionPane.WARNING_MESSAGE);
				}else{
					FileWriter fw = new FileWriter(AppConstant.RESOURCE_FOLDER + testcasesConfigfile, true);
										
					fw.write("\n" + testCaseOrPackage);
					
					JOptionPane.showMessageDialog(null, String.format("The follwing test is added:\n%s\n\n<HTML><P style='background-color: teal'>To see the newly addes test cases <SPAN style='color:red'><B><I>RESTART</I></B></SPAN> the App.</P></HTML>", testCaseOrPackage), "Test " + testCaseOrTestPackage + " added!!!", JOptionPane.INFORMATION_MESSAGE);
					
					fw.close();
				}
				
			}catch (Exception e) {
				JOptionPane.showMessageDialog(null, String.format("No such test %s. Please, edit your entry.", testCaseOrTestPackage), String.format("Invalid test %s!!!", testCaseOrTestPackage), JOptionPane.WARNING_MESSAGE);
				System.err.println("Adding testcases/suite: " + e.getMessage());
			}
		}
	}
	
	public void addButtonForFailedTCs(JPanel settingPanel){
		int totalTestCases = getTotalTestcaseNumberFrom("src/main/resources/lastRunFailedTestcases.properties");
		
		JPanel reRunFailedTC = new JPanel();
		
		Border border = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		if(totalTestCases == 0){
			JLabel noRecentFailedTc = new JLabel("No recently failed Testcases :)");
			
			noRecentFailedTc.setOpaque(true);
			noRecentFailedTc.setBorder(border);
			noRecentFailedTc.setBackground(new Color(111, 222, 23));
			noRecentFailedTc.setForeground(Color.WHITE);
			
			reRunFailedTC.add(noRecentFailedTc);
			
			settingPanel.add(reRunFailedTC);
		}else{
			runLatestFailedTestcases = new JCheckBox("Run "+ totalTestCases + " recent failed TC" + (totalTestCases > 1 ? "s" : "") + "?");
			
			runLatestFailedTestcases.setOpaque(true);
			runLatestFailedTestcases.setBorder(border);
			runLatestFailedTestcases.setBackground(new Color(220,30,80));
			runLatestFailedTestcases.setActionCommand("runRecentFailedTestcases");
			runLatestFailedTestcases.setForeground(Color.WHITE);
			runLatestFailedTestcases.addActionListener(this);
			
			reRunFailedTC.add(runLatestFailedTestcases);
			
			settingPanel.add(reRunFailedTC);
		}
	}
	
	
	@Override
	public void tableChanged(TableModelEvent event) {
		int selectionColumnPosition = table.getColumnCount() - 1;
		
		int totalRows = table.getRowCount();
		int totalTCs =  disabledTCs.size();
		
		int totalSelectedTCs = 0;
		for(int i = 0; i < totalRows; i++){
			boolean isTCSelected = (boolean) table.getValueAt(i, selectionColumnPosition);
			if(isTCSelected)
				totalSelectedTCs++;
		}
		
		statusOfSelectedTcs.setText((totalSelectedTCs == 0) ? "<html><p style='color:red'><b>Total TCs: " + totalTCs + " [ " + totalDisabledTCs + " disabled ].<br />No testcases selected yet.</b></span></html>" : "<html><span style='color:red'>Total TCs: " + totalTCs + ". [ " + totalDisabledTCs + " disabled ]</span><br />Total testcases selected: <span style='color:teal;background-color:yellow'><b>" + totalSelectedTCs + "</b></span></html>");
	}
	
	
	private long getCountOfDisabledTCs(){
		return disabledTCs.stream().filter(tc -> !tc.equals("")).count();
	}
	
	public static void showDuration(long totalTimeInMillis){
		long totalTimeInSeconds = totalTimeInMillis / 1000;
		long totalTimeInMinutes = totalTimeInSeconds / 60;
		long totalTimeInHours = totalTimeInMinutes / 60;
		
		totalTimeInMillis = totalTimeInMillis % 1000;
		totalTimeInSeconds = totalTimeInSeconds % 60;
		totalTimeInMinutes = totalTimeInMinutes % 60;
		
		String totalTimeTaken = "";
			
		totalTimeTaken += totalTimeInHours > 0 ? (totalTimeInHours == 1 ? "1 hour " : totalTimeInHours + " hours ") : "";
		totalTimeTaken += totalTimeInMinutes > 0 ? (totalTimeInMinutes == 1 ? "1  min " : totalTimeInMinutes+ " mins ") : "";
		totalTimeTaken += totalTimeInSeconds > 0 ? (totalTimeInSeconds == 1 ? "1 sec " : totalTimeInSeconds+  " secs ") : "";
		totalTimeTaken += totalTimeInMillis > 0 ? (totalTimeInMillis == 1 ? "1 mili " : totalTimeInMillis+  " milis ") : "";
	
		System.err.println("Total time spent: " + totalTimeTaken.trim() + "\n");
	}
	
	public static void main(String[] args) {
		new TestcaseDataTable();
	}
}
