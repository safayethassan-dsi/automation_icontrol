package com.icontrolesi.automation.platform.exception;

/**
 * @author atiq
 */
public class AnnotationNotFoundException extends RuntimeException {
    public AnnotationNotFoundException(String message) {
        super(message);
    }
}
