package com.icontrolesi.automation.platform.io;

import java.util.Date;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * @author atiq
 */
public class ConsoleWriter implements ResultWriter {
    @Override
    synchronized public void write(Result result) {
        System.out.println( Logger.getLineNumber() + "=====================================================================================");
        System.out.println( Logger.getLineNumber() + "Test Case Name:\t" + result.getTestCaseName());
        System.out.println( Logger.getLineNumber() + "Issue Number:\t" + result.getIssueNumber());
        System.out.println( Logger.getLineNumber() + "Execution Time:\t" + dateFormat.format(new Date(result.getExecutionTime())));
        System.out.println( Logger.getLineNumber() + "=====================================================================================");
        System.out.println( Logger.getLineNumber() + result.normalize());
        System.out.println( Logger.getLineNumber() + "\n\n");
    }
}
