package com.icontrolesi.automation.platform.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

import com.icontrolesi.automation.platform.config.Configuration;

/**
 * @author atiq
 */
public class FileWriter implements ResultWriter {
    private static final String FILE_PATH = "io.file.root";
    private static String dirPath = Configuration.getConfig(FILE_PATH) + "/" + Configuration.getConfig("selenium.browser");
    public FileWriter() throws IOException {
        //String dirPath = Configuration.getConfig(FILE_PATH) + "/" + Configuration.getConfig(Configuration.PROJECT_NAME)+ "/" +Configuration.getConfig("selenium.browser");
        Files.createDirectories(Paths.get(dirPath));

        String filePath = dirPath + "/summary.txt";
        Path path = Paths.get(filePath);
        BufferedWriter writer = Files.newBufferedWriter(path);
        writer.write("AutoId(CaseId)\t\tStatus\n");
        writer.write("==========================\n");
        writer.flush();
        writer.close();
    }

    @Override
    public void write(Result result) {
    	//String dirPath = Configuration.getConfig(FILE_PATH) + "/" + Configuration.getConfig(Configuration.PROJECT_NAME);
        try {
            writeSummary(result, dirPath);
            writeDetails(result, dirPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeDetails(Result result, String rootDir) throws IOException {
        String filePath = rootDir + "/" + result.getIssueNumber() + ".txt";
        Path path = Paths.get(filePath);
        BufferedWriter writer = Files.newBufferedWriter(path);

        writer.write("=====================================================================================");
        writer.write("\n");
        writer.write("Test Case Name:\t" + result.getTestCaseName());
        writer.write("\n");
        writer.write("Issue Number:\t" + result.getIssueNumber());
        writer.write("\n");
        writer.write("Execution Time:\t" + dateFormat.format(new Date(result.getExecutionTime())));
        writer.write("\n");
        writer.write("Test Result:\t" + result.getStatus());
        writer.write("\n");
        writer.write("=====================================================================================");
        writer.write("\n");
        writer.write("\n");
        writer.write(result.normalize());

        writer.flush();
        writer.close();
    }

    private void writeSummary(Result result, String rootDir) throws IOException {
        String filePath = rootDir + "/summary.txt";
        Path path = Paths.get(filePath);
        BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.APPEND);
        writer.write(result.getIssueNumber() + "\t\t" + result.getStatus() + "\n");
        writer.flush();
        writer.close();
    }
}