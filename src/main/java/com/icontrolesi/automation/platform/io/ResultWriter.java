package com.icontrolesi.automation.platform.io;

import java.text.SimpleDateFormat;

/**
 * @author atiq
 */
public interface ResultWriter {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    void write(Result result);
}
