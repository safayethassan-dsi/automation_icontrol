package com.icontrolesi.automation.platform.io;

import java.io.IOException;

/**
 * @author atiq
 */
public class ResultWriterMgr {
    private static ResultWriter resultWriter;

    public static void init(String outputConfig) throws IOException {
        //Logger.log("Result output config: " + outputConfig);
        switch (outputConfig) {
            case "io.file": resultWriter = new FileWriter();break;
            default: resultWriter = new ConsoleWriter();
        }
    }

    public static ResultWriter getWriter() {
        return resultWriter;
    }
}
