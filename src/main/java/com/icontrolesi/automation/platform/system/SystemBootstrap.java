package com.icontrolesi.automation.platform.system;

import java.io.IOException;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.io.ResultWriterMgr;

/**
 * @author atiq
 */
public class SystemBootstrap {
    private static final String RESULT_OUTPUT = "system.result.output";
   
    public static void initialize() throws IOException {
        Configuration.loadConfigurations();
        ResultWriterMgr.init(Configuration.getConfig(RESULT_OUTPUT));
    }
}