package com.icontrolesi.automation.platform.util;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.openqa.selenium.WebDriver;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class AutomationDataCleaner implements CommonActions{
	public static Set<String> listOfSavedSearchesCreated = new LinkedHashSet<String>();
	public static Set<String> listOfTagsCreated = new LinkedHashSet<String>();
	public static Set<String> listOfAssignmentsCreated = new LinkedHashSet<String>();
	
	private static WebDriver driver = null;
	
	private static AutomationDataCleaner automationDataCleaner = new AutomationDataCleaner();
	
	public static void popSavedSearchFromList(){
		Iterator<String> it = listOfSavedSearchesCreated.iterator();
		it.next();
		it.remove();
	}
	
	
	public static void popAssignmentFromList(){
		Iterator<String> it = listOfAssignmentsCreated.iterator();
		it.next();
		it.remove();
	}
	
	
	public static void popTagFromList(){
		Iterator<String> it = listOfTagsCreated.iterator();
		it.next();
		it.remove();
	}
	
	public static void performCleanUp(){
		
		if(listOfSavedSearchesCreated.size() > 0 || listOfAssignmentsCreated.size() > 0 || listOfTagsCreated.size() > 0){
			openUpRepositoryWithWorkflow("Recenseo Demo DB", "Search");
		}else{
			System.out.println( Logger.getLineNumber() + "No items to perform cleanup!!");
			System.exit(0);
		}
		
		if(listOfSavedSearchesCreated.size() == 0){
			System.err.println("*** No saved searches to cleanup. ***");
		}else{
			System.err.println("\nSaved Search cleaning up process started...\n");
			
			SearchesAccordion.open();
			SearchesAccordion.selectFilter("All");
			
			Iterator<String> savedSearchIterator = listOfSavedSearchesCreated.iterator();
			
			while(savedSearchIterator.hasNext()){
				String savedSearchName = savedSearchIterator.next();
				SearchesAccordion.deleteSavedSearch(savedSearchName);
				//savedSearchIterator.remove();
			}
			
			System.err.println(listOfSavedSearchesCreated.size() == 0 ? "*** All SavedSearches cleared. ***" : "*** Remaining SavedSearches to be cleaned up: " + listOfSavedSearchesCreated + " ***");
		}
		
		if(listOfAssignmentsCreated.size() == 0){
			System.err.println("*** No assignments to cleanup. ***");
		}else{
			System.err.println("\nAssignments cleaning up process started...\n");
			SearchPage.gotoViewDocuments();
			new DocView().clickActionMenuItem("Manage Review Assignments");
			automationDataCleaner.switchToFrame(1);
			automationDataCleaner.waitFor(driver, 40);
			
			Iterator<String> assignmentIterator = listOfAssignmentsCreated.iterator();
			
			while(assignmentIterator.hasNext()){
				String assignmentName = assignmentIterator.next();
				ManageReviewAssignment.deleteAssignment(assignmentName);
				//assignmentIterator.remove();
			}
			
			System.err.println(listOfAssignmentsCreated.size() == 0 ? "*** All Assignments cleared. ***" : "*** Remaining Assignments to be cleaned up: " + listOfAssignmentsCreated + " ***");
		}
		
		
		if(listOfTagsCreated.size() == 0){
			System.err.println("*** No Tags to cleanup. ***");
		}else{
			System.err.println("\nTags cleaning up process started...\n");
			Iterator<String> tagsIterator = listOfTagsCreated.iterator();
			
			SearchPage.gotoViewDocuments();
			TagManagerInGridView.open();
			
			while(tagsIterator.hasNext()){
				String tagName = tagsIterator.next();
				TagManagerInGridView.deleteTag(tagName);
				//tagsIterator.remove();
			}		
			
			System.err.println(listOfAssignmentsCreated.size() == 0 ? "*** All Tags cleared. ***" : "*** Remaining Tags to be cleaned up: " + listOfAssignmentsCreated + " ***");
		}
		
		driver.quit();
	}
	
	
	public static void openUpRepositoryWithWorkflow(String repositoyrName, String workflowName){
		Driver.getDriver().get(Configuration.getConfig("recenseo.url"));
		//new TestHelper().//loginToRecenseo();
		new SelectRepo(repositoyrName);
		SearchPage.setWorkflow(workflowName);
	}
	
	
/*	public static void deleteSavedSearch(String savedSearchName){
		SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        
		List<WebElement> savedSearchList = automationDataCleaner.getElements(SearchesAccordion.savedSearchItemLocator);
		
		if(savedSearchList.size() == 0){
			System.err.println("No SavedSearch '" + savedSearchName + "' to delete...");
		}else{
			for(WebElement savedSearchItem : savedSearchList){
				if(savedSearchItem.getText().trim().equals(savedSearchName)){
					Actions actions = new Actions(Driver.getDriver());
					actions.moveToElement(savedSearchItem).build().perform();
					automationDataCleaner.waitFor(driver, 1);
				
					WebElement deleteSavedSearchButton = driver.findElement(SearchesAccordion.savedSearchDeleteBtnLocator);
					
					jse.executeScript("arguments[0].click();", deleteSavedSearchButton);
					
					automationDataCleaner.acceptAlert(driver);
					automationDataCleaner.acceptAlert(driver);
					automationDataCleaner.waitFor(driver, 5);
					
					System.out.println( Logger.getLineNumber() + "Saved search '" + savedSearchName + "' deleted...");
					
					break;
				}
			}
		}
	}*/
}
