package com.icontrolesi.automation.platform.util;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Set;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleEdge;

public class ChartCreator {
	public static JFreeChart generatePieChart(Map<String, ? extends Number> data){
		DefaultPieDataset dataSet = new DefaultPieDataset();
		
		Set<String> keys = data.keySet();
		
		for(String key : keys){
			dataSet.setValue(key, data.get(key));
		}
		
		JFreeChart chart = ChartFactory.createPieChart("Random Data for Countries", dataSet, true, true, true);
		
		return chart;
	}
	
	public static JFreeChart generatePieChartForTestExecution(Map<String, ? extends Number> data){
		DefaultPieDataset dataSet = new DefaultPieDataset();
		
		final String PASSED = "Passed"; 
		final String SKIPPED = "Skipped";
		final String FAILED = "Failed";
		
		Set<String> keys = data.keySet();
		
		for(String key : keys){
			dataSet.setValue(key, data.get(key));
		}
		
		
		JFreeChart chart = ChartFactory.createPieChart("Recenseo Test cases execution summary", dataSet, true, true, true);
		
		PiePlot plot = (PiePlot) chart.getPlot();
		
		plot.setSectionPaint(PASSED, Color.GREEN);
		plot.setSectionPaint(FAILED, Color.RED);
		plot.setSectionPaint(SKIPPED, Color.YELLOW);
		
		plot.setExplodePercent(PASSED, 0.05);
		plot.setExplodePercent(FAILED, 0.05);
		plot.setExplodePercent(SKIPPED, 0.05);
		
		PieSectionLabelGenerator sectionLabelGenerator = new StandardPieSectionLabelGenerator(
	            "{0}: {1} ({2})", new DecimalFormat("0"), new DecimalFormat("0%"));
		
		plot.setLabelGenerator(sectionLabelGenerator);
		
		LegendTitle legendTitle = chart.getLegend();
		legendTitle.setPosition(RectangleEdge.RIGHT);
		legendTitle.setBackgroundPaint(Color.LIGHT_GRAY);
		
		return chart;
	}
}
