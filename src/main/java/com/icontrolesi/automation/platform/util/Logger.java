package com.icontrolesi.automation.platform.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import com.icontrolesi.automation.platform.config.Configuration;


/**
 * @author atiq
 */
public class Logger {
	static FileWriter fileWriter = null;
	static FileWriter fileWriter2 = null;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /*public static void log(String msg) {
        String timestamp = dateFormat.format(new Date());
        System.out.println( Logger.getLineNumber() + "[" + timestamp + "] " + msg);
    }*/
    
    public static void init(){
    	try {
//    		File file = new File("testcaseStatus.txt");
    		File file = new File(AppConstant.RESOURCE_FOLDER + "TestCaseStatus");
    		if(!file.exists() && !file.isDirectory()){
    			file.mkdir();
    			System.out.println("Folder created!!");
    		}
    		
//    		Configuration.getConfig("system.reportFolderName") + ".txt"
    		
    		int totalFilesInPath = file.listFiles().length;
    		
    		File logFile = new File(AppConstant.RESOURCE_FOLDER + "TestCaseStatus/" + Configuration.getConfig("system.reportFolderName") + "_" + (totalFilesInPath + 1) + ".txt").getAbsoluteFile();
    			
			fileWriter = new FileWriter(logFile, true);
			System.out.println( Logger.getLineNumber() + "\nSystem logger [ testcaseStatus.txt ] initialized...\n");
		} catch (IOException e) {
			System.err.println("***Error: " + e.getMessage() + "***");
		}
    }
    
    public static void init(String fileName){
    	try {
    		File file = new File("src/main/resources/" + fileName);
    		if(!file.exists())
    			file.createNewFile();
    		else {
    			file.delete();
    			file.createNewFile();
			}
    			
    		fileWriter2 = new FileWriter(file, true);
			System.out.println( Logger.getLineNumber() + "\nCustom logger [" + fileName + "] initialized...\n");
		} catch (IOException e) {
			System.err.println("***Error: " + e.getMessage() + "***");
		}
    }
    
    public static void log(String text){
    	String timestamp = dateFormat.format(new Date());
        //System.out.println( Logger.getLineNumber() + "[" + timestamp + "] " + text);
    	text = "[" + timestamp + "]" + text + "\n";
    	
		try {
			fileWriter.write(text);
			fileWriter.flush();
		} catch (IOException e) {
			System.err.println("~~~~~~~~~~~~~~~~~~~~~\nError: " + e.getMessage() + "~~~~~~~~~~~~~~~~~~~~~\n");
		}
    	
    }
    
    public static void logData(String text){
    	text = text + "\n";
    	
		try {
			fileWriter2.write(text);
			fileWriter2.flush();
		} catch (IOException e) {
			System.err.println("~~~~~~~~~~~~~~~~~~~~~\nError: " + e.getMessage() + "~~~~~~~~~~~~~~~~~~~~~\n");
		}
    	
    }
    
    public static void logData(Set<String> set){
    	
		try {
			for(String element : set){
				fileWriter2.write(element+"\n");
				System.out.println( Logger.getLineNumber() + element+"#####################");
				fileWriter2.flush();
			}
			
			
		} catch (IOException e) {
			System.err.println("~~~~~~~~~~~~~~~~~~~~~\nError: " + e.getMessage() + "~~~~~~~~~~~~~~~~~~~~~\n");
		}
    	
    }
    
    public static void close(){
    	try {
			fileWriter.close();
		} catch (IOException e) {
			System.err.println("~~~~~~~~~~~~~~~~~~~~~\nError: " + e.getMessage() + "~~~~~~~~~~~~~~~~~~~~~\n");
		}
    }
    
    public static void closeLogger(){
    	try {
			fileWriter2.close();
		} catch (IOException e) {
			System.err.println("~~~~~~~~~~~~~~~~~~~~~\nError: " + e.getMessage() + "~~~~~~~~~~~~~~~~~~~~~\n");
		}
    }
    
   /* public static int getLineNumber(){
    	return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }*/
    
    /*public static String getLineNumber(){
    	Thread thread = Thread.currentThread();
    	return String.format("[In %s, Line# %d]:: ", thread.getName().toUpperCase(), thread.getStackTrace()[2].getLineNumber());
    }*/
    
    
    public static String getLineNumber(){
    	Thread thread = Thread.currentThread();
    	StackTraceElement [] stackTraceElements =  thread.getStackTrace();
    	String calledMethodLocation = String.format("[In %s, Line# %d]:: ", stackTraceElements[2].getFileName(),stackTraceElements[2].getLineNumber());;
    	return calledMethodLocation;
    }
    
    
    public static void main(String[] args) {
    	System.out.println(getLineNumber());
	}
}
