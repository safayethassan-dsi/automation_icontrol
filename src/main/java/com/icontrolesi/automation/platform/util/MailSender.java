package com.icontrolesi.automation.platform.util;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.icontrolesi.automation.platform.config.Configuration;

public class MailSender {
   public static boolean isMailSendingOptionEnabled(){
	  return Configuration.getConfig("system.sendmail").equalsIgnoreCase("yes");
   }
	
	
   public static void sendMail(String userID, char [] password, String subject, String messageBody, List<String> attachmentPathList) {   
	  Properties props = new Properties();
      props.put("mail.smtp.auth", "true");
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", "smtp.gmail.com");
      props.put("mail.smtp.port", "587");
      
     
      Session session = Session.getInstance(props,
      new javax.mail.Authenticator() {
         protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(userID, new String(password));
         }
      });

      try {
         Message message = new MimeMessage(session);
         
         message.setFrom(new InternetAddress(userID));

         message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Configuration.getConfig("system.email_recipients")));
//         message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("abdul.awal@dsinnovators.com"));

         message.setSubject(subject);

         BodyPart messageBodyPart = new MimeBodyPart();

         messageBodyPart.setText(messageBody);

         Multipart multipart = new MimeMultipart();

         multipart.addBodyPart(messageBodyPart);

         if(attachmentPathList == null){
        	 System.out.println("No attachement being sent!");
         }else{
	         for(String attachmentPath : attachmentPathList){
	        	 File file = new File(attachmentPath);
	        	 messageBodyPart = new MimeBodyPart();
	             DataSource source = new FileDataSource(file.getAbsolutePath());
	             messageBodyPart.setDataHandler(new DataHandler(source));
	             //messageBodyPart.setFileName(fileNameSplitted[fileNameSplitted.length - 1]);
	             messageBodyPart.setFileName(file.getName());
	             multipart.addBodyPart(messageBodyPart);
	         }
         }

         message.setContent(multipart);

         Transport.send(message);

         System.out.println( Logger.getLineNumber() + "Sent message successfully....");
      }catch (MessagingException mex) {
    	 //System.out.println( Logger.getLineNumber() + mex);
         System.err.println("Error:: " + mex.getMessage());
         mex.printStackTrace();
      }
   }
   
   public static void main(String[] args) {
	   List<String> attachments = Arrays.asList(
			   "D:\\icontrolworkspace\\icontrolautomatedtest\\TestResults\\chrome\\11-29-17_08-06-25\\emailable-report.html", 
			   "D:\\icontrolworkspace\\icontrolautomatedtest\\TestResults\\chrome\\11-29-17_08-06-25\\emailable-report.html"
			   									);
	   sendMail("iceautomationtest@gmail.com", "iceautomationtestQ@".toCharArray(), "TestMail","This is demo\nDon't be paniked.\n\tTest summary:\n\t\tPassed: 100\n\t\tFailed: 2\n\t\tSkipped: 4\n\t\tTotal: 106", attachments);
   }
}