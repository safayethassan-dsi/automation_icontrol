package com.icontrolesi.automation.platform.util;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import com.icontrolesi.automation.platform.config.Configuration;

public class RetryAnalyzer implements IRetryAnalyzer {
    private int retryCount         = 0;
    private int maxRetryCount     = Integer.valueOf(Configuration.getConfig("selenium.maxRetry"));   // retry a failed test 2 additional times
    
    @Override
    public boolean retry(ITestResult result) {
    	//System.out.println( Logger.getLineNumber() + "Max retry: "+ maxRetryCount);
        if (retryCount < maxRetryCount) {
            retryCount++;
            return true;
        }
        return false;
    }
}