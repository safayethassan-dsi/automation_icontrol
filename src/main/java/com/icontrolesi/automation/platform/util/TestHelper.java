package com.icontrolesi.automation.platform.util;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import java.util.List;
import java.util.ListIterator;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.RecenseoSupport;

import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class TestHelper implements RecenseoSupport, EnvitySupport{
	public static Map<String, String> testCaseWiseStatus = new HashMap<String, String>();
	public SoftAssert softAssert = new SoftAssert();
	
	protected  WebDriver driver = null;
	Set<String> testCaseStatusContainer = new LinkedHashSet<String>();
	String individualTestStatus = ""; 
	
    protected String PROP_BROWSER = "selenium.browser";
    protected String PROP_IE = "selenium.driver.ie";
    protected String PROP_IE_PATH = "selenium.driver.ie.path";

    protected static final String BROWSER_FIREFOX = "firefox";
    protected static final String BROWSER_IE11 = "ie11";
    protected static final String BROWSER_CHROME = "chrome";
    protected static final String BROWSER_SAFARI = "safari";
    
    String tc_name;
    String classNameFullyQualified;
    
    public static Set<String> failedTestCases = new LinkedHashSet<String>();
    public static Set<String> failedTestCasesName = new LinkedHashSet<String>();
    public static Set<String> testcaseNameList = new LinkedHashSet<String>();
    
    int testcaseRun = 0;
    
    public static final String PROJECT_SELECTED = Configuration.getConfig("system.project.name");
    
    @BeforeTest
    public void initializeSystem(){
    	//printSystemInfo();
    	Logger.init();
    	Logger.init("lastRunFailedTestcases.properties");
    }
    
    @BeforeMethod
    public void initBrowser(Method method){
    	testcaseRun ++;
    	tc_name = method.getName();
    	classNameFullyQualified = method.getDeclaringClass().toString().replaceAll("class\\.*", "") + ".java".trim();
    	
		System.out.println( Logger.getLineNumber() + "Testcase '" + tc_name + "' started...");
    	driver = setDriver();
    	if(driver != null)
    		Driver.setWebDriver(driver);
    	
    	if(PROJECT_SELECTED.equals("recenseo")){
    		loginToRecenseo();
    	}else{
    		loginToEnvity();
    	}
	}
    
    @AfterMethod
    public void afterMethod(ITestResult result){
    	 String testcaseStatusInSingleRun = "";
    	 String testCaseMsg = "INFO:: " + tc_name + "\n\tExecution# " + testcaseRun +"'  status: ";
    	 System.out.println( Logger.getLineNumber() + testCaseMsg + "finished executing...");  
    	 System.out.println("[ Line# " + Logger.getLineNumber() + " ]:: " );
    	 if(result.getStatus() == ITestResult.SUCCESS){
    		 testcaseStatusInSingleRun = "Passed"; 
    	 }else if(result.getStatus() == ITestResult.FAILURE){
    		 testcaseStatusInSingleRun = "Failed";
    		 //testcaseNameList.add(tc_name);
    		 testcaseNameList.add(tc_name.split("_")[1]);
    		 failedTestCasesName.add(tc_name.split("_")[2]);
    		 failedTestCases.add(classNameFullyQualified);
    		 
    		 File file = new File(Configuration.getConfig("system.latest_screenshots_path"));
    		 if(!file.exists())
    			 file.mkdir();
    		 
    		 ScreenshotHandler.captureScreenAndSave(Configuration.getConfig("system.latest_screenshots_path") + "/" + tc_name.split("_")[2]);
    	 }else if(result.getStatus() == ITestResult.SKIP){
    		 testcaseStatusInSingleRun = "Skipped";
    		 File file = new File(Configuration.getConfig("system.latest_screenshots_path"));
    		 if(!file.exists())
    			 file.mkdir();
    		 
    		 ScreenshotHandler.captureScreenAndSave(Configuration.getConfig("system.latest_screenshots_path") + "/" + tc_name.split("_")[2]);
    	 }else{
    		 testcaseStatusInSingleRun = "Unknown";
    	 }
    	 
    	 System.out.println( Logger.getLineNumber() + testCaseMsg + testcaseStatusInSingleRun);
    	 testCaseStatusContainer.add(testcaseStatusInSingleRun);
    	 individualTestStatus = testcaseStatusInSingleRun;
    	 
    	 if(driver != null)
    		 driver.quit();
    }
    
    @AfterClass
    public void closeBrowser(){
    	
    	String testCaseMsg = String.format("%-70s:\tTotal run: %02d %s\t | \tSTATUS: ", tc_name, testcaseRun, (testcaseRun > 1 ? "times" : " time"));
    	try{
	    	if(driver != null){
	    		driver.quit();
	    	}
    	}catch (Exception e) {
			System.out.println( Logger.getLineNumber() + "AfterClass: Something went wrong..." + e.getMessage());
		}
    	
    	testCaseWiseStatus.put(tc_name.split("_")[2], individualTestStatus);
    	Logger.log(testCaseMsg + individualTestStatus);
    	System.err.println(testCaseMsg + individualTestStatus);
    } 
    
    
    @AfterTest
    public void brushUpSystem(){
    	//System.out.println( Logger.getLineNumber() + getFormattedFailedTestcaseList(failedTestCases));
    	Logger.close();
    	Logger.logData(failedTestCases);
    	Logger.closeLogger();
    	
    	//AutomationDataCleaner.performCleanUp();
    }
    

    protected WebDriver setDriver() {
        String browser = Configuration.getConfig(PROP_BROWSER);
        //String driverIE = Configuration.getConfig(PROP_IE);
        String driverIEPath = Configuration.getConfig(PROP_IE_PATH);

        WebDriver currentDriver = null;

        if (BROWSER_FIREFOX.equalsIgnoreCase(browser)) {
        	// System.setProperty("webdriver.gecko.driver", AppConstant.RESOURCE_FOLDER + "binaries/geckodriver.exe");
		//	currentDriver = new FirefoxDriver();

			System.setProperty("webdriver.firefox.bin", "C:\\Program Files\\Mozilla Firefox\\firefox.exe");

			currentDriver = new FirefoxDriver();

			// Desired Capabilities
/*
			System.setProperty("webdriver.gecko.driver", "/home/user/bin");

			 DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			 capabilities.setCapability("marionette", true);
			 currentDriver = new FirefoxDriver(capabilities);

		
*/
			
			// options.addArguments("--kiosk");	//	--start-maximized and --kiosk do the same thing. 
			
			// Safayet code with the help of asif vai
/*
			@Override
        public WebDriver getDriver(DesiredCapabilities desiredCapabilities, Boolean headless) {
				WebDriver driver = null;
				List<String> versions = WebDriverManager.FirefoxDriver().arch64().getVersion();

				ListIterator li = versions.listIterator(versions.size());

				while(li.hasPrevious()){
					try{
						String version = li.previous().toString();
						WebDriverManager.FirefoxDriver().version(version).setup();
						FirefoxOptions options = new FirefoxOptions();
						if(headless){
							//options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors", "--silent");

						}
						else{
							options.setHeadless(headless);
						}
						driver = new FirefoxDriver(options);

						if(null  != driver)
						{
							break;
						}

					}catch (Exception ex){
						//ex.printStackTrace();
					}
				}
				return driver;
		} */
        } else if (BROWSER_IE11.equalsIgnoreCase(browser)) {
          	File ieFile = new File(driverIEPath);
        	System.setProperty("webdriver.ie.driver", ieFile.getAbsolutePath());
        	DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
        	caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, Configuration.getConfig("recenseo.url"));
        	caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
        	//currentDriver = new InternetExplorerDriver(caps);
            
        }else if(BROWSER_CHROME.equalsIgnoreCase(browser)){
        //	ChromeDriverManager.getInstance().setup(); //setup proper system variable for chrome
			WebDriverManager.chromedriver().version("75").setup();
			ChromeOptions options = new ChromeOptions();
        	//options.addArguments("--enable-automation");
			options.addArguments("disable-infobars");
			options.addArguments("start-maximized");
			// options.addArguments("--kiosk");	//	--start-maximized and --kiosk do the same thing. 
        	
        	currentDriver = new ChromeDriver(options);
        }else if (BROWSER_SAFARI.equalsIgnoreCase(browser)) {
        	currentDriver = new SafariDriver();
        }

       /*  if(Configuration.getConfig("system.hideBrowserWindow").equals("yes")) {
			System.err.println("Browser is being hidden...");
			currentDriver.manage().window().setPosition(new Point(-12000, -12000));
		}
 */
		if(!browser.equals("chrome")){
			currentDriver.manage().window().maximize();
		}
/* 
		Window dm = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getFullScreenWindow();

		Dimension screenSize = new Dimension(dm.getWidth(), dm.getHeight());

		currentDriver.manage().window().setSize(screenSize); */

        return currentDriver;
    }
    
    public WebDriver getDriver(){
    	return this.driver;
    }
    
    public void loginToRecenseo(){
    	landOnLoginPage();
    	performLoginToRecenseo();
    }
    
    public void loginToEnvity(){
    	String userName = Configuration.getConfig(PROJECT_SELECTED + ".username");
    	String password = Configuration.getConfig(PROJECT_SELECTED + ".password");
    	
    	landOnLoginPage();
    	performLoginWith(userName, password);
    }
    
    public void landOnLoginPage(){
    	System.err.println(Configuration.getConfig(String.format("%s.url", AppConstant.PROJECT_SELECTED))+"****************************************");
    	Driver.getDriver().get(Configuration.getConfig(String.format("%s.url", AppConstant.PROJECT_SELECTED)));
    }
    
    public void landOnLoginPage(final String URL){
    	Driver.getDriver().get(URL);
    }
    
    public void switchToNewWindow(){
    	Set<String> windows = Driver.getDriver().getWindowHandles();
    	
    	for(String handle : windows){
    		Driver.getDriver().switchTo().window(handle);
    	}
    }
    
    public static void printSystemInfo(){
    	int typeLength = 25;
    	int valueLength = 65;
    	System.out.println(String.format("\t%-80s", "#########################################System Information###########################################"));
    	System.out.println(String.format("\t##%98s##", ""));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Project Name", AppConstant.PROJECT_SELECTED.toUpperCase()));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Test URL", Configuration.getConfig(String.format("%s.url", AppConstant.PROJECT_SELECTED))));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Selected Browser", AppConstant.BROWSER.toUpperCase()));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Test User", AppConstant.USER));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Total Threads", AppConstant.TOTAL_THREADS));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Maximum Instant Retry", AppConstant.MAX_RETRY));
    	System.out.println(String.format("\t#\t%-" + typeLength + "s:  %-" + valueLength + "s#", "Download Location", AppConstant.DOWNLOAD_DIRECTORY));
    	System.out.println(String.format("\t##%98s##", ""));
    	System.out.println(String.format("\t%-80s", "######################################################################################################"));
    }
    
    
    public static String getFormattedFailedTestcaseList(Set<String> failedTCList){
    	StringBuffer testCase = new StringBuffer();
    	
    	
    	int i = 1;
    	
    	for(String tc : failedTCList){
    		System.err.println(tc+"********");
    		String [] temp = tc.split("_");
    		tc = temp[1];
    		if(i % 10 == 0){
    			testCase.append(String.format("%10s", tc + "\n"));
    		}else{
    			testCase.append(String.format("%10s", tc));
    		}
    		
    		i++;
    	}
    	
    	return testCase.toString();
    }
}
