package com.icontrolesi.automation.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.xml.XmlSuite;

import com.icontrolesi.automation.platform.util.Logger;

public class CustomSummaryReport implements IReporter{
	public static StringBuffer summaryMessage = new StringBuffer();
	public static Map<String, Integer> reportSummaryMap = new HashMap<String, Integer>();
	public static List<String> failedTCList = new ArrayList<String>();

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		summaryMessage.append("\n\nTestcase Execution Summary::\n");
		summaryMessage.append("===============================\n\n");
		
		for(ISuite suite : suites){
			String suiteName = suite.getName();
			
			summaryMessage.append("\t\tSummary for Suite " + suiteName.toUpperCase()+ "\n");
			summaryMessage.append("\t\t--------------------------------------\n");
			
			Map<String, ISuiteResult> suiteResultList = suite.getResults();
			
			for(ISuiteResult suiteResult : suiteResultList.values()){
				ITestContext testContext = suiteResult.getTestContext();
				
				int passedCount = testContext.getPassedTests().size();
				//int skippedCount = testContext.getSkippedTests().getAllResults().size();
				int failedCount = testContext.getFailedTests().size();
				int totalCount = testContext.getAllTestMethods().length;
				int skippedCount = totalCount - (passedCount + failedCount);
				//int totalCount = passedCount + failedCount;
				
				reportSummaryMap.put("Passed", passedCount);
				reportSummaryMap.put("Failed", failedCount);
				reportSummaryMap.put("Skipped", skippedCount);
				//reportSummaryMap.put("Total", totalCount);
				
				
				summaryMessage.append("\t\t\t   Passed: " + passedCount + "\n");
				summaryMessage.append("\t\t\t  Skipped: " + skippedCount + "\n");
				summaryMessage.append("\t\t\t   Failed: " + failedCount + "\n");
				summaryMessage.append("\t\t--------------------------------------\n");
				summaryMessage.append("\t\t\tTotal Run: " + totalCount + "\n");
				summaryMessage.append("\t\t--------------------------------------\n\n");
			}
		}
		
		System.out.println( Logger.getLineNumber() + summaryMessage);
	}
	
	public static String getExecutionSummary(){
		return summaryMessage.toString();
	}

	public static Map<String, Integer> getReportSummaryMap() {
		return reportSummaryMap;
	}

	public static void setReportSummaryMap(Map<String, Integer> reportSummaryMap) {
		CustomSummaryReport.reportSummaryMap = reportSummaryMap;
	}
	
	/*public static List<String> getFailedTCList(){
		
	}*/
}
