package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;

public class CALDashboardPage implements CommonActions{
	public static final By PAGE_TITLE = By.className("configPageTitle");
	public static final By TOTAL_DOCUMENT_COUNT = By.cssSelector("#docCount b");
	public static final By QUEUED_DOCUMENT_COUNT = By.cssSelector("#queued b");
	public static final By FOUND_DOCUMENT_COUNT = By.cssSelector("#found b");
	public static final By REVIEWED_DOCUMENT_COUNT = By.cssSelector("#reviewd b");
	public static final By FOUND_RATE = By.cssSelector("#plainFoundRate b");
	
	public static final By BATCHNAME_COLUMN = By.cssSelector("#batchSummary tbody tr td:nth-child(1)");
	public static final By BATCH_ORDER = By.cssSelector("#batchSummary tbody tr td:nth-child(1)");
}
