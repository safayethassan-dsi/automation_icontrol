package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.Logger;

public interface EnvitySupport extends CommonActions{
	 public static By userNameLabel = By.cssSelector(".login-div div:nth-child(2) label");
	 public static By passwordLabel = By.cssSelector(".login-div div:nth-child(3) label");
	 public static By userNameLocator = By.id("username");
	 public static By passwordLocator = By.id("password");
	 public static By loginBtnLocator = By.cssSelector("input[value='Sign In']");
	 public static By loginErrorMsgLocator = By.className("alert-danger");
	 
	 public static By FORGOT_PASSWORD_LNK = By.linkText("Forgot your password?");
	 
	 
	 public static interface ResetPassword{
		 public static By USERNAME_LABEL = By.cssSelector("#forgotPassword label.input-label");
		 public static By USERNAME_FIELD = By.id("userId");
		 public static By RESET_BTN = By.className("resetPassword");
		 public static By LOGIN_LNK = By.cssSelector("#forgot a");
		 public static By ERR_MSG_LOCATOR = EnvitySupport.loginErrorMsgLocator;
	 }
	 
	 
	 default void gotoForgotPasswordPage(){
		 tryClick(FORGOT_PASSWORD_LNK, ResetPassword.USERNAME_FIELD);
		 
		 System.out.println("Forgot password page loaded...\n");
	 }
	
	 default void performLoginToEnvity(){
		 String projectSelected = Configuration.getConfig("system.project.name");
		/* String userName = Configuration.getConfig(projectSelected + ".username");
		 String password = Configuration.getConfig(projectSelected + ".password");*/
		 String userName = Configuration.getConfig("envity.username");
		 String password = Configuration.getConfig("envity.password");
		 
		//performLoginWith(AppConstant.USER, AppConstant.PASS);
		 performLoginWith(userName, password);
	}
	 
	default void inputLoginCredentials(String userName, String password){
		enterText(userNameLocator, userName);
        enterText(passwordLocator, password);
	}
	 
	default void performLoginWith(String userName, String password){
		System.out.printf("%s Performing Login for '%s' ...", Logger.getLineNumber(), userName.toUpperCase());
		
    	System.out.println( Logger.getLineNumber() + "Waiting to type user name...");
    	
    	inputLoginCredentials(userName, password);
        
		//tryClick(loginBtnLocator, ProjectPage.createNewProjectDropdown);
		tryClick(loginBtnLocator, ProjectPage.userProfileMenuLocator);
        
        System.out.printf("%s Login performed for '%s' ...", Logger.getLineNumber(), userName.toUpperCase());
	}
}
