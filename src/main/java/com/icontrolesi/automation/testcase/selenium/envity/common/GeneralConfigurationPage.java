package com.icontrolesi.automation.testcase.selenium.envity.common;

import com.icontrolesi.automation.common.CommonActions;

import org.openqa.selenium.By;

public class GeneralConfigurationPage implements CommonActions{
    public static final By PAGE_HEADER_LOCATOR = By.className("configPageTitle");

    //  Left panel menu items
    public static final By INSTANCES_LOCATOR = By.xpath("//a[contains(text(), 'Instances')]");
    
    private final static GeneralConfigurationPage generalConfigurationPage  = new GeneralConfigurationPage();

    public static class Instances{
        public static final By PAGE_HEADER = By.className("configPageTitle");
        public static final By INSTANCE_FILTER_LOCATOR = By.name("DataTables_Table_0_length");
        public static final By DATATABLE_INFO = By.id("DataTables_Table_0_info");

        public static final By TABLE_ROW = By.cssSelector("#DataTables_Table_0 tbody tr");
        /**
         *  Columns locators
         */
        public static final By INSTANCE_NAME = By.cssSelector("table.instanceListTable th:nth-child(2)");
        public static final By IDENTIFIER = By.cssSelector("table.instanceListTable th:nth-child(3)");
        public static final By APP_URL = By.cssSelector("table.instanceListTable th:nth-child(4)");
        public static final By LAST_STARTED_TIME = By.cssSelector("table.instanceListTable th:nth-child(5)");
        public static final By LOCKED = By.cssSelector("table.instanceListTable th:nth-child(6)");
        public static final By STATUS = By.cssSelector("table.instanceListTable th:nth-child(7)");;
        public static final By SEVENTH_COLUMN_HEADER = By.cssSelector("table.instanceListTable th:nth-child(8)");

        public static int getSelectedNumberOfInstances(){
            return Integer.valueOf(generalConfigurationPage.getSelectedItemFroDropdown(INSTANCE_FILTER_LOCATOR));
        }

        public static int getTotalDisplayedInstances(){
            return generalConfigurationPage.getTotalElementCount(TABLE_ROW);
        }


        public static int getTotalInstanceCount(){
            String [] pageInfo = generalConfigurationPage.getText(DATATABLE_INFO).split("\\D+");
            return Integer.valueOf(pageInfo[pageInfo.length - 1]);
        }

        public static void gotoInstancePage(){
            generalConfigurationPage.tryClick(INSTANCES_LOCATOR, INSTANCE_NAME);
            System.err.println("\nInstances page loaded for General Configuration page...\n");
        }
    }

}