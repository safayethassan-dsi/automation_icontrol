package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import com.icontrolesi.automation.common.CommonActions;

public class LeftPanelForProjectPage implements CommonActions{
	private static final LeftPanelForProjectPage leftPanelForProjectPage = new LeftPanelForProjectPage();
	
	public static final By OVERVIEW_LINK_LOCATOR = By.id("a.dashboard");
	public static final By TRAIN_LINK_LOCATOR = By.cssSelector("a.train");
	public static final By PREDICT_LINK_LOCATOR = By.cssSelector("a.predict");
	public static final By REVIEW_LINK_LOCATOR = By.cssSelector("a.review");
	
	public static final By MODIFY_POPULATION_LINK_LOCATOR = By.cssSelector("div.sideMenu.action li a");
	public static final By CREATE_SAMPLE_LINK_LOCATOR = By.cssSelector("div.sideMenu.action li a:nth-child(2)");
	public static final By SYNC_LINK_LOCATOR = By.id("btnSync");
	public static final By TASKQUEUE_LINK_LOCATOR = By.className("taskQueue");
	public static final By INDEX_LINK_LOCATOR = By.id("btnIndexing");

	public static class Overview{

	}
	
	public static class Train{
		public static final By TITLE_LOCATOR = By.xpath("//h3[text()='Train']");
		
		/**
		 * 		Data table item locators (column)
		 */
		public static final By CREATED_COLUMN = By.cssSelector("#DataTables_Table_0 tbody tr td:nth-child(1)");
		public static final By TYPE_COLUMN = By.cssSelector("#DataTables_Table_0 tbody tr td:nth-child(2)");
		public static final By SAVEDSEARCH_COLUMN = By.cssSelector("#DataTables_Table_0 tbody tr td:nth-child(3)");
		public static final By DOCUMENT_COLUMN = By.cssSelector("#DataTables_Table_0 tbody tr td:nth-child(4)");
		public static final By REVIEWED_COLUMN = By.cssSelector("#DataTables_Table_0 tbody tr td:nth-child(5)");
		public static final By RESPONSIVE_COLUMN = By.cssSelector("#DataTables_Table_0 tbody tr td:nth-child(6)");
		
		public static String getJudgementalSample(){
			List<WebElement> savedSearchList = leftPanelForProjectPage.getElements(SAVEDSEARCH_COLUMN);
			
			for(WebElement ss : savedSearchList){
				System.err.println("*********"+leftPanelForProjectPage.getParentOf(ss).findElements(By.tagName("td")).get(1).getText().trim());
				if(leftPanelForProjectPage.getParentOf(ss).findElements(By.tagName("td")).get(1).getText().trim().equals("Judgmental")){
					return ss.getText().trim();
				}
			}
			
			return null;
		}
	}

	public static class Prediction{
		public static final By TITLE_LOCATOR = By.className("configPageTitle");
		public static final By CREATE_PREDICT_BTN = By.id("prediction-create");

		public static final By MODAL_WINDOW_CONTENT_LOCATOR = By.className("modal-content");
		public static final By MODAL_CONFIRM_BTN_LOCATOR = By.cssSelector(".modal-footer .btn-primary");

		public static void perform(){
			try{
				leftPanelForProjectPage.tryClick(CREATE_PREDICT_BTN, 1);
				leftPanelForProjectPage.waitForVisibilityOf(MODAL_CONFIRM_BTN_LOCATOR, 2);
				leftPanelForProjectPage.tryClick(MODAL_CONFIRM_BTN_LOCATOR);
				System.err.println("\n\nQuick Prediction is being created!!!\n");
			}catch(TimeoutException | NoSuchElementException exception){
				leftPanelForProjectPage.tryClick(By.xpath("/html/body/div[6]/div[2]/div[1]/div/svg/g[6]/g[2]/path[298]"));
				leftPanelForProjectPage.tryClick(By.name("createQc"));
				leftPanelForProjectPage.tryClick(By.id("predict-create-btn"));

				System.err.println("\n\nPrediction is being created!!!\n");
			}

			leftPanelForProjectPage.waitForNumberOfElementsToBeGreater(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1);
			leftPanelForProjectPage.waitForNumberOfElementsToBePresent(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1, 1800);

			System.out.println("\n\nPrediction completed!!!\n");
		}
	}
	
	/**
	 * 
	 * @author Abdul Awal
	 * 
	 * Represents 'Create Sample' window
	 *
	 */
	public static class Samples{
		public static final By CONTROL_SAMPLE_BTN = By.id("control-label");
		public static final By JUDGEMENTAL_SAMPLE_BTN = By.id("judgemental-label");
		public static final By ACTIVE_SAMPLE_BTN = By.id("active-label");
		public static final By RANDOM_SAMPLE_BTN = By.id("random-label");
		
		public static final By SAMPLE_WINDOW_LOCATOR = By.cssSelector("div[aria-describedby='dlg-sample']");
		
		public static final By SAVEDSEARCH_DROPDOWN_FOR_JSAMPLE = By.id("savedSearches");
		public static final By CREATE_BTN = By.xpath("//span[text()='Create']");
		public static final By CLOSE_BTN = By.xpath("//span[text()='Close']");
		
		public static final By ACTIVE_SAMPLE_SIZE_DROPDOWN = By.name("activeSampleSize");
		
		public static final By RANDOM_SAMPLE_SIZE_DROPDOWN = By.name("randomSampleSize");
		
		public static final By CONTROL_SAMPLE_TYPE_DROPDOWN = By.name("control-sample-type");
		public static final By CONTROL_SAMPLE_SIZE_DROPDOWN = By.name("controlSampleSize");
		public static final By CONFIDENCE_LEVEL_DROPDOWN = By.name("confidenceLevel");
		public static final By CONFIDENCE_INTERVAL_DROPDOWN = By.name("confidenceInterval");
		
		public static final By SAMPLE_WINDOW_LOADER = By.cssSelector("blockUI.blockMsg.blockElement");
		
		public static void clickCreateBtn() {
			leftPanelForProjectPage.tryClick(Samples.CREATE_BTN, TaskHistoryPage.TASK_PROGRESS_LOCATOR);
			//leftPanelForProjectPage.waitForInVisibilityOf(Samples.SAMPLE_WINDOW_LOCATOR);
			TaskHistoryPage.waitforTaskToComplete();
			
			leftPanelForProjectPage.waitFor(1);
		}
	}
	
	public static class Sync{
		public static final By CONFIRM_BTN = By.xpath("//button[contains(text(), 'Confirm')]");
		public static final By CANCEL_BTN = By.xpath("//button[contains(text(), 'Cancel')]");
		
		public static void perform() {
			leftPanelForProjectPage.tryClick(SYNC_LINK_LOCATOR, 1);
			leftPanelForProjectPage.tryClick(CONFIRM_BTN);
			
			leftPanelForProjectPage.waitForNumberOfElementsToBeGreater(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1);
			leftPanelForProjectPage.waitForNumberOfElementsToBePresent(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1, 1800);
			
			System.out.println("Manual/forceful SYNC performed...\n");
		}
	}
	
	public static class Indexing{
		public static final By CONFIRM_BTN = By.xpath("//button[contains(text(), 'Confirm')]");
		public static final By CANCEL_BTN = By.xpath("//button[contains(text(), 'Cancel')]");
		
		public static void perform() {
			leftPanelForProjectPage.tryClick(INDEX_LINK_LOCATOR, 1);
			leftPanelForProjectPage.tryClick(CONFIRM_BTN);
			
			leftPanelForProjectPage.waitForNumberOfElementsToBeGreater(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1);
			leftPanelForProjectPage.waitForNumberOfElementsToBePresent(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1, 1800);
			
			System.out.println("Manual/forceful IDEXING performed...\n");
		}
	}

	public static void gotoOverviewPage() {
		leftPanelForProjectPage.tryClick(OVERVIEW_LINK_LOCATOR, OverviewPage.DOCUMENT_COUNT_LOCATOR);
		leftPanelForProjectPage.waitFor(1);
		System.err.println("\nOverview page loaded...\n");
	}
	
	public static void gotoTrainPage(){
		leftPanelForProjectPage.tryClick(TRAIN_LINK_LOCATOR, Train.TITLE_LOCATOR);
		System.out.println("Train page opened...");
	}
	
	public static void openSampleWindow(String sampleType){
		leftPanelForProjectPage.tryClick(CREATE_SAMPLE_LINK_LOCATOR, Samples.SAMPLE_WINDOW_LOCATOR);
		leftPanelForProjectPage.waitFor(2);
		System.out.println("Sample window appeared...");
		
		try{
			leftPanelForProjectPage.tryClick(By.id(sampleType + "-label"));
		}catch(Exception ex){
			throw new SkipException("Your provided sample type is not valid. Select either of the values: control/judgemental/active/random.");
		}
		
				
		System.out.printf("%s option selected for sample...\n", sampleType.toUpperCase());
	}
	/**
	 * 
	 * @param type	refers to the type of 'Control Sample' i.e. Statistical or Fixed
	 * @param values referes to the fields 'Confidence Level' and 'Confidence Interval'
	 */
	public static void addControlSample(String type, int ... values){
		openSampleWindow("control");
		leftPanelForProjectPage.selectFromDrowdownByText(Samples.CONTROL_SAMPLE_TYPE_DROPDOWN, type);
		
		if(type.equalsIgnoreCase("Statistical") || type.equals("")) {
			//throw new SkipException("Statistical Sample method not implemented yet. Muri khau.... :D");
			leftPanelForProjectPage.selectFromDrowdownByText(Samples.CONFIDENCE_LEVEL_DROPDOWN, String.valueOf(values[0]));
			leftPanelForProjectPage.selectFromDrowdownByText(Samples.CONFIDENCE_INTERVAL_DROPDOWN, String.valueOf(values[1]));
		}else {
			leftPanelForProjectPage.selectFromDrowdownByText(Samples.CONTROL_SAMPLE_SIZE_DROPDOWN, String.valueOf(values[0]));
		}
		
		Samples.clickCreateBtn();
		
		System.out.printf("Control Sample of type '%s' added ...\n", type);
	}
	
	public static void addJudgementalSample(String savedSearchName){
		openSampleWindow("judgemental");
		leftPanelForProjectPage.selectFromDrowdownByText(Samples.SAVEDSEARCH_DROPDOWN_FOR_JSAMPLE, savedSearchName);
		
		Samples.clickCreateBtn();

		System.out.printf("Judgemental Sample added with saved search named %s...\n", savedSearchName.toUpperCase());
	}
	
	public static void addActiveSample(int documentSize){
		openSampleWindow("active");
		leftPanelForProjectPage.selectFromDrowdownByText(Samples.ACTIVE_SAMPLE_SIZE_DROPDOWN, "100");
		
		Samples.clickCreateBtn();

		System.out.printf("Active Sample added with size %d...\n", documentSize);
	}	
	
	public static void syncProject(){
		leftPanelForProjectPage.tryClick(SYNC_LINK_LOCATOR);
	}
	
	public static void gotoTaskQueuePage() {
		leftPanelForProjectPage.tryClick(TASKQUEUE_LINK_LOCATOR, TaskHistoryPage.TASK_HISTORY_ITEM);
		leftPanelForProjectPage.waitFor(1);
		System.err.println("\nTask History page loaded...\n");
	}

	public static void gotoPredictPage() {
		leftPanelForProjectPage.tryClick(PREDICT_LINK_LOCATOR, Prediction.TITLE_LOCATOR);
		leftPanelForProjectPage.waitFor(1);
		System.err.println("\nPredict page loaded...\n");
	}
}
