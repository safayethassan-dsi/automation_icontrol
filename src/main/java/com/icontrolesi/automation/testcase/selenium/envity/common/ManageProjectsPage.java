package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

public class ManageProjectsPage implements CommonActions{
	static final ManageProjectsPage manageProjectsPage = new ManageProjectsPage();
	
	public static final By PAGE_TITLE_LOCATOR = By.xpath("//h3[text()='Manage Projects']");
	
	public static final By searchBoxLocator = By.cssSelector("input[placeholder='Search']");
	
	/**
	 * 		Project table item locators
	 */
	
	public static final By PROJECT_ITEM_LOCATOR = By.cssSelector("#projectListTable tbody tr");
	public static final By PROJECT_CLUMN_HEADER = By.cssSelector("#projectListTable thead th");
	public static final By PROJECT_COLUMN = By.cssSelector("#projectListTable tbody tr td:nth-child(1)");

	public static final By PROJECT_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(1)");
	public static final By WORKSPACE_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(2)");
	public static final By PROJECT_TYPE_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(3)");
	public static final By LAST_MODIFIED_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(4)");
	public static final By JOB_STATUS_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(5)");
	public static final By APP_INSTANCE_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(6)");
	public static final By SEVENTH_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(7)");
	public static final By EIGHTTH_COL = By.cssSelector("#projectListTable tbody tr td:nth-of-type(8)");

	public static final By CONFIRM_WINDOW_HEADER = By.cssSelector("div.modal-header > h4");
	public static final By CONFIRM_BTN = By.xpath("//button[contains(text(), 'Confirm')]");
	public static final By CANCEL_BTN = By.xpath("//button[contains(text(), 'Cancel')]");
	public static final By DELET_PROJECT_ON_PROGRESS_WINDOW = By.xpath("//span[text()='Project is being deleted']");

	public static final By JOB_STATUS_LOCATOR = By.xpath("//a[contains(text(), 'Monitoring Job')]");
	
	public static final By START_JOB_BTN = By.xpath("//a[text()='Start Monitoring Job']");
	public static final By STOP_JOB_BTN = By.xpath("//a[text()='Stop Monitoring Job']");
	public static final By ENABLE_SUCCESS_MSG_LOCATOR = By.xpath("//div[contains(text(), 'Enabling monitoring job - Successful')]");
	public static final By DISABLE_SUCCESS_MSG_LOCATOR = By.xpath("//div[contains(text(), 'Disabling monitoring job - Successful')]");

	public static boolean isProjectPresent(String projectName){
		manageProjectsPage.editText(searchBoxLocator, projectName);
		List<WebElement> projectList = manageProjectsPage.getElements(PROJECT_ITEM_LOCATOR);

		for(WebElement project : projectList){
			String name = project.findElement(By.tagName("td")).getText().trim();
			if(name.equals(projectName)){
				return true;
			}
		}

		System.err.println("\nNo such project found...\n");
		
		return false;
	}
	
	public static String getProjectTitle(){
		return "";
	}
	

	public static String getJobStatus(String projectName){
		manageProjectsPage.editText(searchBoxLocator, projectName);
		return manageProjectsPage.getText(JOB_STATUS_LOCATOR);
	}

	public static void stopJob(String projectName){
		manageProjectsPage.editText(searchBoxLocator, projectName);
		manageProjectsPage.tryClick(STOP_JOB_BTN, DISABLE_SUCCESS_MSG_LOCATOR);
		System.out.printf("\nJob Stopped for project %s successfully.\n", projectName);
	}

	/**
	 * 
	 * @param projectName
	 */
	public static void startJob(String projectName){
		manageProjectsPage.editText(searchBoxLocator, projectName);
		manageProjectsPage.tryClick(START_JOB_BTN, ENABLE_SUCCESS_MSG_LOCATOR);
		System.out.printf("\nJob Started for project %s successfully.\n", projectName);
	}

	public static void clickDeleteBtn(){
		manageProjectsPage.getElement(EIGHTTH_COL).findElement(By.cssSelector("a > img")).click();
		manageProjectsPage.waitForVisibilityOf(CONFIRM_BTN);
	}

	public static void deleteProject(){
		clickDeleteBtn();
		manageProjectsPage.tryClick(CONFIRM_BTN, DELET_PROJECT_ON_PROGRESS_WINDOW);
		manageProjectsPage.waitForInVisibilityOf(DELET_PROJECT_ON_PROGRESS_WINDOW);

		System.err.println("\nProject deleted successfully...\n");
	}


	public static void clickDeleteBtn(String projectName){
		manageProjectsPage.editText(searchBoxLocator, projectName);
		List<WebElement> projectList = manageProjectsPage.getElements(PROJECT_ITEM_LOCATOR);

		for(WebElement project : projectList){
			String name = project.findElement(By.tagName("td")).getText().trim();
			if(name.equals(projectName)){
				project.findElement(By.cssSelector("td:nth-of-type(8) a")).click();
			}
		}
	}

	public static void deleteProject(String projectName){
		clickDeleteBtn(projectName);
		manageProjectsPage.tryClick(CONFIRM_BTN, DELET_PROJECT_ON_PROGRESS_WINDOW);
		manageProjectsPage.waitForInVisibilityOf(DELET_PROJECT_ON_PROGRESS_WINDOW);

		System.err.printf("\nProject '%s' deleted successfully...\n", projectName);
	}
	
}
