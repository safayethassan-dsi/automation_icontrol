package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;

public class ManageUserProjectPage implements CommonActions{
	public static final ManageUserProjectPage mupp = new ManageUserProjectPage();
	
	public static final By PAGE_TITLE = By.className("configPageTitle");
	
	/**
	 * 		Project table header locators
	 */
	
	public static final By PROJECT_NAME_HEADER = By.cssSelector("#projectListTable thead th:nth-child(1)"); 
	public static final By WORKSPACE_HEADER =  By.cssSelector("#projectListTable thead th:nth-child(2)");
	public static final By STATUS_HEADER =  By.cssSelector("#projectListTable thead th:nth-child(3)");
	public static final By LAST_MODIFIED_HEADER =  By.cssSelector("#projectListTable thead th:nth-child(4)");
	
	/**
	 * 		Project table column locators
	 */
	
	public static final By PROJECT_NAME_COLUMN = By.cssSelector("#projectListTable tbody td:nth-child(1)");
	public static final By WORKSPACE_COLUMN = By.cssSelector("#projectListTable tbody td:nth-child(2)");
	public static final By STATUS_CLOUMN = By.cssSelector("#projectListTable tbody td:nth-child(3)");
	public static final By LAST_MODIFIED_COLUMN = By.cssSelector("#projectListTable tbody td:nth-child(4)");
	
	
	public static final By UPDATE_BTN = By.cssSelector("input[value='Update']");
	public static final By CANCEL_BTN = By.cssSelector("input[value='Cancel']");
	
	
	public static boolean isPageLoaded(){
		boolean isHeaderOk = mupp.getText(PAGE_TITLE).equals("Manage User Project");
		
		return isHeaderOk;
	} 
	
}
