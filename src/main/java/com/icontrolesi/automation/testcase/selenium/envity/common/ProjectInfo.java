package com.icontrolesi.automation.testcase.selenium.envity.common;

/**
 * ProjectInfo: provides project specific information loaded from properties files
 */
public interface ProjectInfo {
    String getProjectName();
    // String setProjectName(String projectName);
    String getEnvizeInstance();
    String getRelativityInstance();
    String getWorkspace();

    String getSavedSearchName();
    String getPredictionField();
    String getTruePositive();
    String getTrueNegative();


    default String getDocumentSet(){
        return null;
    }

    default String getBatchSetName(){
        return null;
    }

    default String getSyntheticDocument(){
        return null;
    }

    default String getDocumentIdentifier(){
        return null;
    }

    default String getBatchPrefix(){
        return null;
    }

    default String getBatchSize(){
        return null;
    }

    default String getJudgementalSampleName(){
        return null;
    }

    default String getSavedSearchNameForPopulatioin(){
        return null;
    }
}