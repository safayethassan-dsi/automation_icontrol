package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.util.AppConstant;

public class ProjectPage implements CommonActions{
	public static ProjectPage projectPage = new ProjectPage();

	public static final By LOGO = By.id("logo");
	public static final By ENABLE_MONITOR_JOB_BTN = By.xpath("//button[text()='Enable Monitoring Job']");
	
	/**
	 * 		User Profile menu & its items
	 */
	public static final By userProfileMenuLocator = By.cssSelector(".userProfileMenu");
	public static final By resetPasswordLinkLocator = By.xpath("//a[text()='Reset Password']");
	public static final By logoutLinkLocator = By.xpath("//a[text()='Logout']");
	public static final By helpLinkLocator = By.xpath("//a[text()='Help']");
	public static final By aboutLinkLocator = By.xpath("//a[text()='About']");
	
	/**
	 * 		Setting menu & its items
	 */
	public static final By settingMenuLocator = By.cssSelector("li[class='dropdown']");
	public static final By generalConfigurationLinkLocator = By.xpath("//a[text()='General Configuration']");
	public static final By userAdministrationLinkLocator = By.xpath("//a[text()='User Administration']");
	public static final By projectManagementLinkLocator = By.xpath("//a[text()='Project Management']");
	
	
	/**
	 * 		Create New Project dropdown & its items locators
	 */
	public static final By createNewProjectDropdown = By.xpath("//button[contains(text(), 'Create New Project')]");
	public static final By createSALProjectItemLocator = By.xpath("//li[text()='Create Standard SAL Project']");
	public static final By createCAL_SMPL_ProjectItemLocator = By.xpath("//li[text()='Create Simplified CAL Project']");
	public static final By createCAL_STD_ProjectItemLocator = By.xpath("//li[text()='Create Standard CAL Project']");
	
	
	public static final By searchFieldLocator = By.cssSelector("input[placeholder='Search']");
	public static final By projectListTableLocator = By.id("projectListTable");
	
	
	/**
	 * 		Project list table & its header locators
	 */
	
	public static final By headerLocator = By.xpath("//*[text()='Project']");
	public static final By workspaceHeaderLocator = By.xpath("//*[text()='Workspace']");
	public static final By typeHeaderLocator = By.xpath("//*[text()='Project Type']");
	public static final By createdHeaderLocator = By.xpath("//*[text()='Created']");
	public static final By lastModifiedHeaderLocator = By.xpath("//*[text()='Last Modified']");
	
	
	/**
	 * 		Project list table & its items locators
	 */
	
	public static final By projectNameLocator = By.cssSelector("#projectListTable tbody tr td:nth-child(1)");
	public static final By projectWorkspacLocator = By.cssSelector("#projectListTable tbody tr td:nth-child(2)");
	public static final By projectTypeLocator = By.cssSelector("#projectListTable tbody tr td:nth-child(3)");
	public static final By projectCreatedLocator = By.cssSelector("#projectListTable tbody tr td:nth-child(4)");
	public static final By projectLastModifiedLocator = By.cssSelector("#projectListTable tbody tr td:nth-child(5)");
	
	public static final By PROJECT_TABLE_INFO = By.id("projectListTable_info");
	public static final By PAGE_LINK = By.className("paginate_button");
	
	public static final By PREV_LINK = By.cssSelector("#projectListTable_previous a");
	public static final By NEXT_LINK = By.cssSelector("#projectListTable_next a");
	public static final By SELECTED_PAGE_LNK = By.cssSelector("li[class='paginate_button active']");
	
	
	public static class ResetPassword{
		public static final By oldPasswordLocator = By.id("password");
		public static final By newPasswordLocator = By.id("newPassword");
		public static final By confirmPasswordLocartor = By.id("confirmPassword");
		public static final By cancelBtn = By.cssSelector("input[value='Cancel']");
		public static final By changeBtn = By.cssSelector("input[value='Change Password']");
		
		public static void inputData(String oldPassword, String newPassword){
			projectPage.enterText(ResetPassword.oldPasswordLocator, oldPassword);
			projectPage.enterText(ResetPassword.newPasswordLocator, newPassword);
			projectPage.enterText(ResetPassword.confirmPasswordLocartor, newPassword);
		}
		
		public static void changePassword(String oldPassword, String newPassword){
			System.out.printf("Changing password from %s to %s...\n", oldPassword, newPassword);
			
			projectPage.tryClick(userProfileMenuLocator);
			projectPage.tryClick(resetPasswordLinkLocator, ResetPassword.oldPasswordLocator);
			
			inputData(oldPassword, newPassword);
			
			projectPage.tryClick(changeBtn);
			projectPage.waitForInVisibilityOf(changeBtn);
			
			System.out.printf("Password changed from %s to %s...\n", oldPassword, newPassword);
		}
	}
	
	/**
	 * 		Methods for performing action on the page
	 */
	
	public static void clickOnCreateProjectDropdown(){
		projectPage.tryClick(createNewProjectDropdown, 1);
		System.out.println("Create New Project dropdown clicked...");
	}	
	
	public static void gotoProjectCreationPage(String projectType) {
		clickOnCreateProjectDropdown();
		//projectPage.tryClick(By.xpath(String.format("//li[text()='%s']", projectType)), GeneralSettingsForProjectCreationPage.PROJECT_PAGE_LOADER);
		projectPage.tryClick(By.xpath(String.format("//li[text()='%s']", projectType)), 2);
		projectPage.waitForInVisibilityOf(GeneralSettingsForProjectCreationPage.PROJECT_PAGE_LOADER);
		
		System.out.printf("\n'%s' project creation loaded...\n", projectType);
	}
	
	public static boolean isUserLoggedIn(){
		return projectPage.getTotalElementCount(userProfileMenuLocator) == 1;
	}
	
	public static void performLogout(){
		System.out.println("Performing logout...");
		projectPage.tryClick(userProfileMenuLocator);
		projectPage.tryClick(logoutLinkLocator, EnvitySupport.loginBtnLocator);
		System.out.printf("'%s' is logged out of the system...\n", AppConstant.USER.toUpperCase());
	}
	
	public static boolean isProjectFound(String projectName) {
		projectPage.enterText(searchFieldLocator, projectName);
		System.err.println("Test: "+projectPage.getText(projectNameLocator).equals("No data available in table"));
		return projectPage.getText(projectNameLocator).equals("No data available in table") == false;
	}

	public static void openProject(String projectName){
		projectPage.enterText(searchFieldLocator, projectName);
		projectPage.getElements(projectNameLocator).stream()
					.filter(name -> name.getText().trim().equals(projectName))
					.findFirst()
					.get()
					.click();
		
		projectPage.waitForVisibilityOf(OverviewPage.DASHBOARD_LOCATOR);
		projectPage.waitFor(1);
		
		System.out.printf("Project %s opened for summary view...\n", projectName);
	}
	
	public static int getTotalProjectCount(){
		String projectStatus = projectPage.getText(PROJECT_TABLE_INFO);
		
		String [] temp = projectStatus.split("\\D+");
		
		return Integer.valueOf(temp[temp.length - 1].replaceAll("\\D+", ""));
	}
	
	public static int getTotalPageCount(){
		List<WebElement> pageLinks = projectPage.getElements(PAGE_LINK);
		
		return Integer.valueOf(pageLinks.get(pageLinks.size() - 2).getText().trim());
	}
	
	public static boolean isNextBtnEnabled(){
		return projectPage.getParentOf(projectPage.getElement(NEXT_LINK)).getAttribute("class").endsWith("disabled") == false;
	}
	
	public static boolean isPreviousBtnEnabled(){
		return projectPage.getParentOf(projectPage.getElement(PREV_LINK)).getAttribute("class").endsWith("disabled") == false;
	}
	
	public static void gotoNextPage(){
		projectPage.tryClick(NEXT_LINK);
	}
	
	public static void gotoPreviousPage(){
		projectPage.tryClick(PREV_LINK);
	}
	
	public static void gotoFirstPage() {
		int currentPage = getSelectedPageNumber();
		
		for(int i = currentPage; i >= 1; i--) {
			gotoNextPage();
		}
	}
	
	public static void gotoLastPage() {
		int totalPageCount = getTotalPageCount();
		int currentPage = getSelectedPageNumber();
		
		for(int i = currentPage; i <= totalPageCount; i++) {
			gotoNextPage();
		}
	}
	
	public static int getSelectedPageNumber(){
		return Integer.valueOf(projectPage.getText(SELECTED_PAGE_LNK));
	}
	
	public static void openSettingsMenu() {
		projectPage.tryClick(settingMenuLocator);
	}
	
	public static void gotoGeneralConfigurationPage() {
		openSettingsMenu();
		projectPage.tryClick(generalConfigurationLinkLocator);
		System.out.println("\nGeneral Configuration page loaded...");
	}
	
	public static void gotoUserAdministrationPage() {
		openSettingsMenu();
		projectPage.tryClick(userAdministrationLinkLocator, UsersListPage.PAGE_HEADER_LOCATOR);
		System.out.println("\nUser Administration page loaded...");
	}

	public static void gotoProjectAdministrationPage() {
		openSettingsMenu();
		projectPage.tryClick(projectManagementLinkLocator, ManageProjectsPage.PAGE_TITLE_LOCATOR);
		System.out.println("\nProject Management page loaded...");
	}

	public static void gotoHomePage() {
		projectPage.tryClick(LOGO, createNewProjectDropdown);
	}
	
	public static void main(String[] args) {
		System.out.println(Math.floorDiv(getTotalProjectCount(), 5));
	}
}
