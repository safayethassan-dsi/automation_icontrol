package com.icontrolesi.automation.testcase.selenium.envity.common;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;

public class SALGeneralConfigurationPage implements CommonActions{
	static SALGeneralConfigurationPage generalConfigurationPage = new SALGeneralConfigurationPage();
	
	/*
	 * General Configuration for SAL Project Creation
	 */
	public static final By SAL_PROJECT_NAME_FIELD_LOCATOR = By.name("name");
	public static final By SAL_RELATIVITY_INSTANCE_DROPDOWN_LOCATOR = By.name("relativityConnection");
	public static final By SAL_ENVIZE_INSTANCE_DROPDOWN_LOCATOR = By.name("envizeConnection");
	public static final By SAL_WORKSPACE_DROPDOWN_LOCATOR = By.id("workspace");
	
	
	/*
	 * Field Settings for SAL Project Creation
	 */
	
	public static final By SAL_PREDICTION_FIELD_LOCATOR = By.name("rankingField");
	public static final By SAL_TRUE_POSITIVE_DROPDOWN_LOCATOR = By.name("positiveValue");
	public static final By SAL_TRUE_NEGATIVE_DROPDOWN_LOCATOR = By.name("negativeValue");
	public static final By SAL_DOCUMENT_IDENTIFIER_FIELD_LOCATOR = By.name("documentIdentifier");
	
	
	/*
	 * Review Settings for SAL Project Creation
	 */
	
	public static final By SAL_SAVEDSEARCH_NAME_FIELD_LOCATOR = By.name("dataSourceSavedSearchName");
	public static final By SAL_BATCHSET_NAME_FIELD_LOCATOR = By.name("batchSetName");
	public static final By SAL_BATCH_PREFIX_FIELD_LOCATOR = By.name("batchPrefix");
	public static final By SAL_BATCH_SIZE_FIELD_LOCATOR = By.name("batchSize");
	
	public static final By SAL_CREATE_PROJECT_BTN_LOCATOR = By.cssSelector("input[value='Create']");
	public static final By SAL_CANCEL_PROJECT_BTN_LOCATOR = By.xpath("//button[text()='Cancel']");
	
	public static final By SAL_PROJECT_CREATION_WINDOW = By.className("ui-dialog-title");
	
	
	
	public static final By SAL_PROJECT_PAGE_LOADER = By.xpath("//*[text()='Retrieving fields...']");


	
	
	
	public static void clickOnCreateProjectDropdown(){
		generalConfigurationPage.tryClick(ProjectPage.createNewProjectDropdown, 1);
		System.out.println("Create New Project dropdown clicked...");
	}

	public static void gotoProjectCreationPage(){
		if(generalConfigurationPage.getTotalElementCount(GeneralSettingsForProjectCreationPage.PROJECT_NAME_DROPDOWN_LOCATOR) == 0){
			clickOnCreateProjectDropdown();
			// generalConfigurationPage.tryClick(ProjectPage.createSALProjectItemLocator,
			// SAL_PROJECT_PAGE_LOADER);
			generalConfigurationPage.tryClick(ProjectPage.createSALProjectItemLocator, SAL_CREATE_PROJECT_BTN_LOCATOR);
			// generalConfigurationPage.waitForInVisibilityOf(SAL_PROJECT_PAGE_LOADER);
			generalConfigurationPage.waitFor(10);
		}else{
			System.err.println("Already on the Project Creation page...\n");
		}		
	}

	public static void cancelProjectCreation() {
		generalConfigurationPage.tryClick(SAL_CANCEL_PROJECT_BTN_LOCATOR);
	}
	
	
	public static void createSALProject(String projectName){
		gotoProjectCreationPage();
		/*clickOnCreateProjectDropdown();
		generalConfigurationPage.tryClick(ProjectPage.createSALProjectItemLocator, SAL_PROJECT_PAGE_LOADER);
		generalConfigurationPage.waitForInVisibilityOf(SAL_PROJECT_PAGE_LOADER);*/
		
		//populateGeneralConfigurationForSAL(projectName, "Relativity Connection_DSI", "iControl SvP");
		populateGeneralConfigurationForSAL(projectName, "Envize default connection", "Relativity Instance", "iControl SvP");
		populateFieldSettingsForSAL("dsi_review_field_09", "Very Good", "Very Bad", "Control Number");
		populateReviewSettingForSAL("SS", "BS", "SAL_", "10");
		
		generalConfigurationPage.tryClick(SAL_CREATE_PROJECT_BTN_LOCATOR, SAL_PROJECT_CREATION_WINDOW);
		generalConfigurationPage.waitForInVisibilityOf(SAL_PROJECT_CREATION_WINDOW, 300);
		generalConfigurationPage.waitFor(2);
		
		System.out.println("\nSAL project created with name '" + projectName + "'...");
	}

	public static void createSALProject(ProjectInfo salInfo){
		gotoProjectCreationPage();
		populateGeneralConfigurationForSAL(salInfo.getProjectName(), salInfo.getEnvizeInstance(), salInfo.getRelativityInstance(), salInfo.getWorkspace());
		populateFieldSettingsForSAL(salInfo.getPredictionField(), salInfo.getTruePositive(), salInfo.getTrueNegative(), salInfo.getDocumentIdentifier());
		populateReviewSettingForSAL(salInfo.getSavedSearchName(), salInfo.getBatchSetName(), salInfo.getBatchPrefix(), salInfo.getBatchSize());

		generalConfigurationPage.tryClick(SAL_CREATE_PROJECT_BTN_LOCATOR, SAL_PROJECT_CREATION_WINDOW);
		generalConfigurationPage.waitForInVisibilityOf(SAL_PROJECT_CREATION_WINDOW, 300);
		generalConfigurationPage.waitFor(2);

		System.out.println("\nSAL project created with name '" + salInfo.getProjectName() + "'...");
	}

	public static boolean isPorjectCreated(){
		return TaskHistoryPage.isCurrentTaskFailed("Create Envity project") == false;
	}
	
	
	public static void addPopulation(String savedSearchNameFromRelativity){
		By SAVEDSEARCH_DROPDOWN_FOR_POPLATION = By.id("savedSearches");
		By ADD_BTN_FOR_POPULATION = By.xpath("//button/span[text()='Add']");
//		By LOADER_FOR_SAVEDSEARCH_FOR_SAL = By.xpath("//span[text()='Loading saved searches...']");
		
		generalConfigurationPage.tryClick(LeftPanelForProjectPage.MODIFY_POPULATION_LINK_LOCATOR, By.cssSelector(".blockUI.blockOverlay"));
		//generalConfigurationPage.selectFromDrowdownByText(SAVEDSEARCH_DROPDOWN_FOR_POPLATION, savedSearchNameFromRelativity);
		CALStandardGeneralConfigurationPage.selectSavedSearchFor(By.id("select2-savedSearches-container"), savedSearchNameFromRelativity);
		
		generalConfigurationPage.tryClick(ADD_BTN_FOR_POPULATION, TaskHistoryPage.TASK_PROGRESS_LOCATOR);
		
		TaskHistoryPage.waitforTaskToComplete();	//	For Adding Population
		System.err.println("Population Added...");
		TaskHistoryPage.waitforTaskToComplete();	//	For Indexing
		System.err.println("Indexing after adding population...");
		generalConfigurationPage.waitFor(2);
	}
	
	
	public static void addPopulation(String savedSearchNameFromRelativity, int waitTimeInMins){
		By SAVEDSEARCH_DROPDOWN_FOR_POPLATION = By.id("savedSearches");
		By ADD_BTN_FOR_POPULATION = By.xpath("//button/span[text()='Add']");
//		By LOADER_FOR_SAVEDSEARCH_FOR_SAL = By.xpath("//span[text()='Loading saved searches...']");
		
		generalConfigurationPage.tryClick(LeftPanelForProjectPage.MODIFY_POPULATION_LINK_LOCATOR, 5);
		generalConfigurationPage.selectFromDrowdownByText(SAVEDSEARCH_DROPDOWN_FOR_POPLATION, savedSearchNameFromRelativity);
		
		generalConfigurationPage.tryClick(ADD_BTN_FOR_POPULATION, TaskHistoryPage.TASK_PROGRESS_LOCATOR);
		
		TaskHistoryPage.waitforTaskToComplete();	//	For Adding Population
		System.err.println("Population Added...");
		TaskHistoryPage.waitforTaskToComplete();	//	For Indexing
		System.err.println("Indexing after adding population...");
	}

	public static void addJudgementalSample(final String savedSearchName){
		LeftPanelForProjectPage.addJudgementalSample(savedSearchName);
	}
	

	public static boolean isPopulationAdded(){
		return TaskHistoryPage.isCurrentTaskFailed("Add Population") == false;
	}
	
	public static boolean isControlSampleAdded(){
		return TaskHistoryPage.isCurrentTaskFailed("Create Control Samples") == false;
	}

	public static boolean isJudgementalSampleAdded(){
		return TaskHistoryPage.isCurrentTaskFailed("Create Judgemental Samples") == false;
	}

	public static boolean isActiveSampleAdded(){
		return TaskHistoryPage.isCurrentTaskFailed("Create Active Samples") == false;
	}

	public static void populateGeneralConfigurationForSAL(String projectName, String envizeInstance, String relativityInstance, String workspace){
//		generalConfigurationPage.enterText(SAL_PROJECT_NAME_FIELD_LOCATOR, projectName);
		generalConfigurationPage.getElement(SAL_PROJECT_NAME_FIELD_LOCATOR).sendKeys(projectName);
		generalConfigurationPage.selectFromDrowdownByText(SAL_ENVIZE_INSTANCE_DROPDOWN_LOCATOR, envizeInstance);
		
		String selectedRelativityInstance = generalConfigurationPage.getSelectedItemFroDropdown(SAL_RELATIVITY_INSTANCE_DROPDOWN_LOCATOR);
		if(relativityInstance.equals(selectedRelativityInstance)){
			System.out.println(relativityInstance + " already selected...");
		}else{
			generalConfigurationPage.selectFromDrowdownByText(SAL_RELATIVITY_INSTANCE_DROPDOWN_LOCATOR, relativityInstance);
			// waitforLoadingComplete();
			generalConfigurationPage.waitFor(5);
		}
		generalConfigurationPage.selectFromDrowdownByText(SAL_WORKSPACE_DROPDOWN_LOCATOR, workspace);
		try {
			waitforLoadingComplete();
		} catch (Exception e) {
			generalConfigurationPage.waitFor(10);
		}
	}
	
	public static void populateFieldSettingsForSAL(String predictionField, String truePositiveValue, String trueNegativeValue, String documentIdentifier){
		generalConfigurationPage.enterText(SAL_PREDICTION_FIELD_LOCATOR, predictionField);
		generalConfigurationPage.waitFor(2);
		generalConfigurationPage.tryClick(By.className("ui-menu-item"), 2);
		generalConfigurationPage.selectFromDrowdownByText(SAL_TRUE_POSITIVE_DROPDOWN_LOCATOR, truePositiveValue);
		generalConfigurationPage.waitFor(2);
		generalConfigurationPage.selectFromDrowdownByText(SAL_TRUE_NEGATIVE_DROPDOWN_LOCATOR, trueNegativeValue);
		generalConfigurationPage.enterText(SAL_DOCUMENT_IDENTIFIER_FIELD_LOCATOR, documentIdentifier);
	}
	
	public static void populateReviewSettingForSAL(String savedSearchName, String batchsetName, String batchPrefix, String batchSize){
		generalConfigurationPage.enterText(SAL_SAVEDSEARCH_NAME_FIELD_LOCATOR, savedSearchName);
		generalConfigurationPage.enterText(SAL_BATCHSET_NAME_FIELD_LOCATOR, batchsetName);
		generalConfigurationPage.enterText(SAL_BATCH_PREFIX_FIELD_LOCATOR, batchPrefix);
		generalConfigurationPage.enterText(SAL_BATCH_SIZE_FIELD_LOCATOR, batchSize);
	}
	
	public static void waitforLoadingComplete(){
//		generalConfigurationPage.switchToFrame(generalConfigurationPage.getElement(By.className("blockUI")));
		generalConfigurationPage.waitForVisibilityOf(By.cssSelector("body > div.blockUI.blockMsg.blockElement > span"), 300);
		generalConfigurationPage.waitForInVisibilityOf(By.cssSelector("body > div.blockUI.blockMsg.blockElement > span"), 300);
		generalConfigurationPage.waitFor(2);
//		generalConfigurationPage.switchToDefaultFrame();
	}
}
