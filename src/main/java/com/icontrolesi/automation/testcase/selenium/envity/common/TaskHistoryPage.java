package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

public class TaskHistoryPage implements CommonActions{
	public static final TaskHistoryPage taskHistoryPage = new TaskHistoryPage();
	
	public static final By TASK_HISTORY_ITEM = By.cssSelector("#taskHistoryTable tbody tr");
	
	public static final By TASK_PROGRESS_LOCATOR = By.cssSelector("table.runningTaskTable tbody tr td div");
	
	public static final By FAILED_TASK_LOCATOR = By.cssSelector("table.table.failedTaskTable tr");
	public static final By SEARCH_FIELD_LOCATOR = By.cssSelector("input[placeholder='Search']");
	
	/**
	 * 	Wait for a single task to be completed
	 */
	public static void waitforTaskToComplete(){
		taskHistoryPage.waitForNumberOfElementsToBeGreater(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1);
		taskHistoryPage.waitForNumberOfElementsToBePresent(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1, 1800);
	}

	public static long getTimeToTaskToComplete(long startTime){
		taskHistoryPage.waitForNumberOfElementsToBeGreater(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1);
		taskHistoryPage.waitForNumberOfElementsToBePresent(TaskHistoryPage.TASK_PROGRESS_LOCATOR, 1, 1800);
		long endTime = System.currentTimeMillis();

		return (endTime - startTime);
	}
	
	/**
	 * Wait for some tasks to be completed
	 * @param totalTasks
	 */
	public static void waitforTaskToComplete(int totalTasks, int waitTime){
		taskHistoryPage.waitForNumberOfElementsToBeGreater(TaskHistoryPage.TASK_PROGRESS_LOCATOR, totalTasks);
		taskHistoryPage.waitForNumberOfElementsToBePresent(TaskHistoryPage.TASK_PROGRESS_LOCATOR, totalTasks, waitTime * 60);
	}

	public static String getCurrentTaskStatus(String taskName){
		// taskHistoryPage.editText(SEARCH_FIELD_LOCATOR, taskName);
		List<WebElement> taskItemList = taskHistoryPage.getElements(TASK_HISTORY_ITEM);
		
		for(WebElement taskItem : taskItemList) {
			String taskItemName = taskItem.findElement(By.cssSelector("td:nth-child(2)")).getText().trim();
			String taskItemStatus = taskItem.findElement(By.cssSelector("td:nth-child(3)")).getText().trim();
			System.err.printf("\nTask Name, Status: %s, %s\n", taskItemName, taskItemStatus);
						
			if(taskItemName.equals(taskName)) {
				return taskItemStatus;				
			}
		}

		return "No Status :)";
	}

	public static boolean checkFailedTaskHasDownloadBtn(String taskName){
		// taskHistoryPage.editText(SEARCH_FIELD_LOCATOR, taskName);
		List<WebElement> taskItemList = taskHistoryPage.getElements(TASK_HISTORY_ITEM);
		
		for(WebElement taskItem : taskItemList) {
			String taskItemName = taskItem.findElement(By.cssSelector("td:nth-child(2)")).getText().trim();
			String taskItemStatus = taskItem.findElement(By.cssSelector("td:nth-child(3)")).getText().trim();
			String taskItemErrorLink = taskItem.findElement(By.cssSelector("td:nth-child(7)")).getAttribute("title").trim();

			System.err.printf("\nTask Name, Status: %s, %s\n", taskItemName, taskItemErrorLink);
						
			if(taskItemName.equals(taskName) && taskItemStatus.equals("ERROR")) {
				return taskItemErrorLink.equals("Download");
			}
		}

		return false;
	}

	public static boolean isCurrentTaskQueued(String taskName){
		String currentStatus = getCurrentTaskStatus(taskName);
		System.err.printf("\n%s\n", currentStatus);
		return currentStatus.equals("QUEUED");
	}
	
	public static boolean isCurrentTaskFailed(String taskName) {
		/* List<WebElement> taskItemList = taskHistoryPage.getElements(TASK_HISTORY_ITEM);
		
		boolean isTaskCreationListed = false;
		
		for(WebElement taskItem : taskItemList) {
			String taskItemName = taskItem.findElement(By.cssSelector("td:nth-child(2)")).getText().trim();
			String taskItemStatus = taskItem.findElement(By.cssSelector("td:nth-child(3)")).getText().trim();
			System.err.printf("Task Name, Status: %s, %s", taskItemName, taskItemStatus);
						
			if(taskItemName.equals(taskName)) {
				System.err.println("\nName Mathch to ho gaya!\n");
				isTaskCreationListed = true;
				if(taskItemStatus.equals("ERROR")) {
					System.err.println("\nStatus vi Mathch to ho gaya!\n");
					return true;
				}
			}
		}
		
		if(isTaskCreationListed == false) {
			System.err.println("\nName Mathch nehi hua!\n");
			return true;
		}
	
		return false; */

		return getCurrentTaskStatus(taskName).equals("ERROR");
	}
}
