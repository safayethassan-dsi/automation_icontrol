package com.icontrolesi.automation.testcase.selenium.envity.create_project;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSimpleGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.GeneralSettingsForProjectCreationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;

public class CancelBtnWorksProperly extends TestHelper{

	@Test(description="Check if Cancewl buttons work for all project types.")
	public void test_c384_CancelBtnWorksProperly() {
		checkCancelWorksForSAL();
		checkCancelWorksForCALSMPL();
		checkCancelWorksForCALSTD();
		
		softAssert.assertAll();
	}

    private void checkCancelWorksForSAL(){
		SALGeneralConfigurationPage.gotoProjectCreationPage();
		System.err.println(getText(GeneralSettingsForProjectCreationPage.PROJECT_NAME_LABEL_LOCATOR)+"********");
		softAssert.assertEquals(getText(GeneralSettingsForProjectCreationPage.PROJECT_NAME_LABEL_LOCATOR), "Create New Project", "Project Creation Page loaded:: ");
		SALGeneralConfigurationPage.cancelProjectCreation();
		String projectDropdownTxt = getText(ProjectPage.createNewProjectDropdown);
		softAssert.assertEquals(projectDropdownTxt, "Create New Project", "User redirected to landing page:: ");
	}
	
	private void checkCancelWorksForCALSMPL(){
		ProjectPage.gotoProjectCreationPage("Create Simplified CAL Project");
		softAssert.assertEquals(getText(GeneralSettingsForProjectCreationPage.PROJECT_NAME_LABEL_LOCATOR), "Create Simplified CAL Project", "Project Creation Page loaded:: ");
		CALSimpleGeneralConfigurationPage.cancelProjectCreation();
		String projectDropdownTxt = getText(ProjectPage.createNewProjectDropdown);
		softAssert.assertEquals(projectDropdownTxt, "Create New Project", "User redirected to landing page:: ");
	}
	
	private void checkCancelWorksForCALSTD(){
		ProjectPage.gotoProjectCreationPage("Create Standard CAL Project");
		softAssert.assertEquals(getText(GeneralSettingsForProjectCreationPage.PROJECT_NAME_LABEL_LOCATOR), "Create Standard CAL Project", "Project Creation Page loaded:: ");
		CALStandardGeneralConfigurationPage.cancelProjectCreation();
		String projectDropdownTxt = getText(ProjectPage.createNewProjectDropdown);
		softAssert.assertEquals(projectDropdownTxt, "Create New Project", "User redirected to landing page:: ");
    }
}