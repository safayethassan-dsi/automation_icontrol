package com.icontrolesi.automation.testcase.selenium.envity.create_project;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

public class CheckCALStandardProjectCreation extends TestHelper {

	@Test(description = "Check whether the full execution path for CAL Simple project creation is ok.")
	public void test_30000000_CheckCALStandardProjectCreation() {

		ProjectInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();

		CALStandardGeneralConfigurationPage.createProject(calStdInfo);
		
		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = TaskHistoryPage.isCurrentTaskFailed("Create Standard Envity CAL") == false;
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");
		
		softAssert.assertAll();
	} 
}
