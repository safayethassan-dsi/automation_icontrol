package com.icontrolesi.automation.testcase.selenium.envity.create_project;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

public class CheckSALProjectCreation extends TestHelper{
	
	@Test(description = "Check whether the full execution path for SAL project creation is ok.")
	public void test_10000000_CheckSALProjectCreation(){
		
		ProjectInfo projectInfo = new ProjectInfoLoader("sal").loadSALInfo();
		
		SALGeneralConfigurationPage.createSALProject(projectInfo);

		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = TaskHistoryPage.isCurrentTaskFailed("Create Envity project") == false;
		
		softAssert.assertTrue(isProjectCreationOk, "SAL project creation FAILED:: ");
				
		softAssert.assertAll();
	}
}
