package com.icontrolesi.automation.testcase.selenium.envity.create_project;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSimpleGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALSmplInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStdInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;

public class CreatedProjectAddedInTheListAfterClickingCreateButton extends TestHelper {

    @Test(description = "Created Projects added in the project list")
    public void test_c385_CreatedProjectAddedInTheListAfterClickingCreateButton() {
        checkCreatedSALProjectAddedInTheList();
        checkCreatedCALSMPLProjectAddedInTheList();
        checkCreatedCALSTDProjectAddedInTheList();

        softAssert.assertAll();
    }

    private void checkCreatedSALProjectAddedInTheList() {
        SALInfo salInfo = new ProjectInfoLoader("sal").loadSALInfo();

        SALGeneralConfigurationPage.createSALProject(salInfo);
        ProjectPage.gotoHomePage();
        boolean isProejctAdded = ProjectPage.isProjectFound(salInfo.getProjectName());
        softAssert.assertTrue(isProejctAdded, "SAL project added to project list:: ");
    }

    private void checkCreatedCALSMPLProjectAddedInTheList() {
        CALSmplInfo calSmplInfo = new ProjectInfoLoader("cal_smpl").loadCALSmplInfo();

        CALSimpleGeneralConfigurationPage.createProject(calSmplInfo);
        ProjectPage.gotoHomePage();
        boolean isProejctAdded = ProjectPage.isProjectFound(calSmplInfo.getProjectName());
        softAssert.assertTrue(isProejctAdded, "CAL Simple project added to project list:: ");
    }

    private void checkCreatedCALSTDProjectAddedInTheList() {
        CALStdInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();
        
        CALStandardGeneralConfigurationPage.createProject(calStdInfo);
        ProjectPage.gotoHomePage();
        boolean isProejctAdded = ProjectPage.isProjectFound(calStdInfo.getProjectName());
        softAssert.assertTrue(isProejctAdded, "CAL Standard project added to project list:: ");
    }
}