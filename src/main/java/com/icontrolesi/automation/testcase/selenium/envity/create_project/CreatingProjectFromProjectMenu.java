package com.icontrolesi.automation.testcase.selenium.envity.create_project;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.GeneralSettingsForProjectCreationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * CreatingProjectFromProjectMenu
 */
public class CreatingProjectFromProjectMenu extends TestHelper{
    @Test(description = "Checking whether projects get created from project menu")   
    public void test_c391_CreatingProjectFromProjectMenu(){
        ProjectPage.gotoProjectCreationPage("Create SAL Project");
        
        tryClick(GeneralSettingsForProjectCreationPage.PROJECT_NAME_DROPDOWN_LOCATOR);
        getElements(By.cssSelector(".dropdown-content > a"))
                    .stream()
                    .filter(l -> l.getText().trim().equals("Create SAL Project"))
                    .findFirst()
                    .get()
                    .click();
                    

        SALInfo projectInfo = new ProjectInfoLoader("sal").loadSALInfo();
        SALGeneralConfigurationPage.createSALProject(projectInfo);

        LeftPanelForProjectPage.gotoTaskQueuePage();

        boolean isProjectCreationOk = TaskHistoryPage.isCurrentTaskFailed("Create Envity project") == false;

        softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");

        softAssert.assertAll();
    }
}