package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

public class CheckForPasswordAndConfirmPasswordGivesProperMessage extends TestHelper{
	private String password = "Password length is-22";
	
	@Test(description="If 'Password' and 'Confirm Password' do not match, it gives an error.")
	public void test_c274_CheckForPasswordAndConfirmPasswordGivesProperMessage() {
		ProjectPage.gotoUserAdministrationPage();
		
		UsersListPage.openCreateUserWindow();
		
		UsersListPage.CreateUser.fillUserCreationForm("", "abdulawal", password, password+"1", "Abdul Awal", "Sazib", "Envize Administrator", "iControl");
		
		UsersListPage.CreateUser.clickCreateBtn();
		

		softAssert.assertEquals(getText(UsersListPage.CreateUser.CONFIRM_PASSWORD_ERR), "Password and Confirm password are not match", "***) Password and Confirm Password do not match:: ");
		
		softAssert.assertAll();
	}
}
