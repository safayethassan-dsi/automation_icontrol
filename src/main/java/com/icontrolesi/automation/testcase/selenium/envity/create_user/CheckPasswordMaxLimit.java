package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class CheckPasswordMaxLimit extends TestHelper{
	private String passwordLengthMoreThanExpected = String.format("%s_%d", "Password is of length-25!", getRandomNumberInRange(1, 10000000));
	
	@Test(description="If Password is less than min limit, it will give an error.")
	public void test_c273_CheckPasswordMaxLimit() {
		ProjectPage.gotoUserAdministrationPage();
		
		UsersListPage.openCreateUserWindow();
		
		UsersListPage.CreateUser.fillUserCreationForm("", "abdulawal", passwordLengthMoreThanExpected, "abdulawal", "Abdul Awal", "Sazib", "Envize Administrator", "iControl");
		
		UsersListPage.CreateUser.clickCreateBtn();
		
		softAssert.assertTrue(passwordLengthMoreThanExpected.length() > 24, String.format("Password lenght found %d but expected should be > 24 chars:: ", passwordLengthMoreThanExpected.length()));
		softAssert.assertEquals(getText(UsersListPage.CreateUser.PASSWORD_ERR), "Max 24 Characters", "***) Password lenght checking message not correct:: ");	
		
		softAssert.assertAll();
	}
}
