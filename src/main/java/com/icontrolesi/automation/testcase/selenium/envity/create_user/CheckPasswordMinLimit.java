package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class CheckPasswordMinLimit extends TestHelper{
	String passwordLestThanExpected = String.valueOf(getRandomNumberInRange(1, 10000000));
	
	@Test(description="If Password is less than min limit, it will give an error.")
	public void test_c272_CheckPasswordMinLimit() {
		ProjectPage.gotoUserAdministrationPage();

		UsersListPage.openCreateUserWindow();
		
		UsersListPage.CreateUser.fillUserCreationForm("", "abdulawal", passwordLestThanExpected, "abdulawal", "Abdul Awal", "Sazib", "Envize Administrator", "iControl");
		
		UsersListPage.CreateUser.clickCreateBtn();
		
		softAssert.assertTrue(passwordLestThanExpected.length() < 8, "***) Password length found at least 8 units:: ");
		softAssert.assertEquals(getText(UsersListPage.CreateUser.PASSWORD_ERR), "Min 8 Characters", "***) Password lenght checking message not correct:: ");	
		
		softAssert.assertAll();
	}
}
