package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class NoUserCreatedForAnyBlankField extends TestHelper{
	final String role = "Envize Administrator";
	
	@Test
	public void test_c270_NoUserCreatedForAnyBlankField(){
		ProjectPage.gotoUserAdministrationPage();
		
		UsersListPage.openCreateUserWindow();
		
		/*	Test for Blank E-mail	*/
		UsersListPage.CreateUser.fillUserCreationForm("", "abdulawal", "abdulawal", "abdulawal", "Abdul Awal", "Sazib", role, "iControl");
		UsersListPage.CreateUser.clickCreateBtn();
		
		String errorMsgForBlankEmail = getText(UsersListPage.CreateUser.EMAIL_ERR);
		
		softAssert.assertEquals(errorMsgForBlankEmail, "Please enter email address", "Error message displayed for blank Email:: ");
		
		
		
		/*	Test for Blank UserName	*/
		UsersListPage.CreateUser.fillUserCreationForm("abdulawal@gmail.com", "", "abdulawal", "abdulawal", "Abdul Awal", "Sazib", role, "iControl");
		UsersListPage.CreateUser.clickCreateBtn();
		
		String errorMsgForBlankUsername = getText(UsersListPage.CreateUser.USERNAME_ERR);
		
		softAssert.assertEquals(errorMsgForBlankUsername, "Please enter your username", "Error message displayed for blank Username:: ");
		
		
		/*	Test for Blank First Name	*/
		UsersListPage.CreateUser.fillUserCreationForm("abdulawal@gmail.com", "abdulawal", "abdulawal", "abdulawal", "", "Sazib", role, "iControl");
		UsersListPage.CreateUser.clickCreateBtn();
		
		String errorMsgForBlankFirstName = getText(UsersListPage.CreateUser.FIRSTNAME_ERR);
		
		softAssert.assertEquals(errorMsgForBlankFirstName, "Please enter first name", "Error message displayed for blank First Name:: ");
		
		
		/*	Test for Blank Last Name	*/
		UsersListPage.CreateUser.fillUserCreationForm("abdulawal@gmail.com", "abdulawal", "abdulawal", "abdulawal", "Abdul Awal", "", role, "iControl");
		UsersListPage.CreateUser.clickCreateBtn();
		
		String errorMsgForBlankLastName = getText(UsersListPage.CreateUser.LASTNAME_ERR);
		
		softAssert.assertEquals(errorMsgForBlankLastName, "Please enter last name", "Error message displayed for blank Last Name:: ");
		
		
		
		/*	Test for Blank Role	*/
		UsersListPage.CreateUser.fillUserCreationForm("abdulawal@gmail.com", "abdulawal", "abdulawal", "abdulawal", "Abdul Awal", "Sazib", "", "iControl");
		UsersListPage.CreateUser.clickCreateBtn();
		
		String errorMsgForBlankRole = getText(UsersListPage.CreateUser.ROLE_ERR);
		
		softAssert.assertEquals(errorMsgForBlankRole, "Please select a Role for this User", "Error message displayed for blank Last Name:: ");
		
		
		softAssert.assertAll();
		
	}
}
