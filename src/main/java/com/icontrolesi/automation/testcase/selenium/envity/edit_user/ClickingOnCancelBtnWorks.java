package com.icontrolesi.automation.testcase.selenium.envity.edit_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

import java.util.Date;

public class ClickingOnCancelBtnWorks extends TestHelper {
    private String possibleValidUserName = "AutoUser_" + new Date().getTime();
    private String email = String.format("%s%s", possibleValidUserName, "@automation.com");

    @Test(description = "When clicked on 'Cancel' button, it returns to User List.")
    public void test_c281_ClickingOnCancelBtnWorks(){
        ProjectPage.gotoUserAdministrationPage();
        UsersListPage.openCreateUserWindow();

        UsersListPage.CreateUser.perform(email, possibleValidUserName, "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

        UsersListPage.EditUser.openFor(possibleValidUserName);

        softAssert.assertEquals(getText(UsersListPage.EditUser.HEADER), "Edit User", "1) Edit User window open:: ");
        softAssert.assertEquals(getText(UsersListPage.EditUser.USERNAME_FIELD), possibleValidUserName, "2) Edit User window opened for expected user:: ");

        UsersListPage.EditUser.close();

        softAssert.assertEquals(getTotalElementCount(UsersListPage.EditUser.CLOSE_BTN), 0, "1) Edit User Window closed:: ");

        UsersListPage.deleteUser(possibleValidUserName);

        softAssert.assertAll();
    }
}
