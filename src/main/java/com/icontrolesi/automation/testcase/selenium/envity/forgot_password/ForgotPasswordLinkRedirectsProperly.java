package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class ForgotPasswordLinkRedirectsProperly extends TestHelper{
	@Test
	public void test_c342_ForgotPasswordLinkRedirectsProperly(){
		ProjectPage.performLogout();
		
		gotoForgotPasswordPage();
		
		String userNameLabel = getText(EnvitySupport.ResetPassword.USERNAME_LABEL);
		String userNameField = getAttribute(EnvitySupport.ResetPassword.USERNAME_FIELD, "placeholder");
		String resetPasswordBtn = getText(EnvitySupport.ResetPassword.RESET_BTN);
		String loginLnk = getText(EnvitySupport.ResetPassword.LOGIN_LNK);
		
		softAssert.assertEquals(userNameLabel, "User Name");
		softAssert.assertEquals(userNameField, "User Name");
		softAssert.assertEquals(resetPasswordBtn, "Reset Password");
		softAssert.assertEquals(loginLnk, "Log In Using Current Password");
		
		softAssert.assertAll();
	}
}
