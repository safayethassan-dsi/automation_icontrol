package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class LoginLinkRedirectsToLoginPage extends TestHelper{
	
	@Test
	public void test_c343_LoginLinkRedirectsToLoginPage(){
		ProjectPage.performLogout();
		
		gotoForgotPasswordPage();
		
		tryClick(EnvitySupport.ResetPassword.LOGIN_LNK, EnvitySupport.FORGOT_PASSWORD_LNK);
		
		String userNameLabel = getText(EnvitySupport.userNameLabel);
		String userPasswordLabel = getText(EnvitySupport.passwordLabel);
		String userNameField = getAttribute(EnvitySupport.userNameLocator, "placeholder");
		String passwordField = getAttribute(EnvitySupport.passwordLocator, "placeholder");
		String loginBtn = getText(EnvitySupport.loginBtnLocator);
		String forgotPasswordLnk = getText(EnvitySupport.FORGOT_PASSWORD_LNK);
		
		softAssert.assertEquals(userNameLabel, "User Name");
		softAssert.assertEquals(userNameField, "User Name");
		softAssert.assertEquals(userPasswordLabel, "Password");
		softAssert.assertEquals(passwordField, "Password");
		softAssert.assertEquals(loginBtn, "Sign In");
		softAssert.assertEquals(forgotPasswordLnk, "Forgot your password?");
		
		softAssert.assertAll();
	}
}
