package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class LoginUsingOldPasswordAfterChangingPassword extends TestHelper{
	private final String EXPECTED_ERROR_MSG = "Invalid username or password"; 
	
	@Test
	public void test_c348_LoginUsingOldPasswordAfterChangingPassword(){
		String newPassword = AppConstant.PASS2 + "_new";
		ProjectPage.performLogout();
		
		performLoginWith(AppConstant.USER2, AppConstant.PASS2);
		
		ProjectPage.ResetPassword.changePassword(AppConstant.PASS2, newPassword);
		
		ProjectPage.performLogout();
		
		inputLoginCredentials(AppConstant.USER2, AppConstant.PASS2);
		
		tryClick(EnvitySupport.loginBtnLocator);
		
		String loginErrorMsg = getText(EnvitySupport.loginErrorMsgLocator);
		
		performLoginWith(AppConstant.USER2, newPassword);
		
		ProjectPage.ResetPassword.changePassword(newPassword, AppConstant.PASS2);
				
		softAssert.assertEquals(loginErrorMsg, EXPECTED_ERROR_MSG);
		
		softAssert.assertAll();
	}
}
