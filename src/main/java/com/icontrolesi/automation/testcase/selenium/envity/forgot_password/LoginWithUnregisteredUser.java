package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class LoginWithUnregisteredUser extends TestHelper{
	private final String EXPECTED_ERROR_MSG = "Invalid username or password"; 
	
	@Test
	public void test_c349_LoginWithUnregisteredUser(){
		ProjectPage.performLogout();
		
		inputLoginCredentials(AppConstant.USER + "_Unregistered", "afasdfasfasdfasdfasd");
		
		tryClick(EnvitySupport.loginBtnLocator);
		
		String loginErrorMsg = getText(EnvitySupport.loginErrorMsgLocator);
		
		softAssert.assertEquals(loginErrorMsg, EXPECTED_ERROR_MSG);
		
		softAssert.assertAll();
	}
}
