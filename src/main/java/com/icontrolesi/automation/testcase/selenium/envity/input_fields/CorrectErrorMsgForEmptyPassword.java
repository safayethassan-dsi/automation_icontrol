package com.icontrolesi.automation.testcase.selenium.envity.input_fields;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class CorrectErrorMsgForEmptyPassword extends TestHelper{
	private final String EXPECTED_ALERT_MSG = "Please enter your password."; 
	@Test(description = "For empty 'Password', correct error message is shown.")
	public void test_c256_CorrectErrorMsgForEmptyPassword(){
		ProjectPage.performLogout();
		
		
		enterText(EnvitySupport.userNameLocator, AppConstant.USER);
		tryClick(EnvitySupport.loginBtnLocator);
		
		
		String alertTextForLoginError = getAlertMsg().trim();
		
		softAssert.assertEquals(alertTextForLoginError, EXPECTED_ALERT_MSG);
		
		softAssert.assertAll();
	}
}
