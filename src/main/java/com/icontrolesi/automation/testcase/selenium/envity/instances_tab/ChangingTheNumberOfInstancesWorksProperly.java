package com.icontrolesi.automation.testcase.selenium.envity.instances_tab;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.GeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.testng.annotations.Test;

public class ChangingTheNumberOfInstancesWorksProperly extends TestHelper {
    @Test(description = "Changing the number of instances loads data table properly")
    public void test_c1846_ChangingTheNumberOfInstancesWorksProperly(){
        ProjectPage.gotoGeneralConfigurationPage();
        GeneralConfigurationPage.Instances.gotoInstancePage();
        
        int currentlySelectedInstances = GeneralConfigurationPage.Instances.getSelectedNumberOfInstances();
        int totalDisplayedItems = GeneralConfigurationPage.Instances.getTotalDisplayedInstances();
        int totalItems = GeneralConfigurationPage.Instances.getTotalInstanceCount();

        int totalPage = ProjectPage.getTotalPageCount();

        if(currentlySelectedInstances >= totalItems){
            softAssert.assertEquals(totalPage, 1, "*** Page count matched:: ");
        }else{
            double calculatedPageCount = Math.ceil(totalItems / currentlySelectedInstances);
            softAssert.assertEquals(totalDisplayedItems, calculatedPageCount, "*** Page count matched:: ");
        }
        
        softAssert.assertTrue(totalDisplayedItems <= currentlySelectedInstances, "*** Number of Instances displayed as expected:: ");

        softAssert.assertAll();
    }
}