package com.icontrolesi.automation.testcase.selenium.envity.login_button;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class LoginWithDisabledUser extends TestHelper{
	private final String EXPECTED_ERROR_MSG = "User is disabled"; 
	
	@Test
	public void test_c1833_LoginWithDisabledUser(){
		ProjectPage.performLogout();
		
		inputLoginCredentials(AppConstant.DISABLED_USERNAME, AppConstant.DISABLED_USERPASS);
		
		tryClick(EnvitySupport.loginBtnLocator);
		
		String actualErrorMsg = getText(EnvitySupport.loginErrorMsgLocator); 
		
		softAssert.assertEquals(actualErrorMsg, EXPECTED_ERROR_MSG, String.format("***) %s:: ", EXPECTED_ERROR_MSG) );
		
		softAssert.assertAll();
	}
}
