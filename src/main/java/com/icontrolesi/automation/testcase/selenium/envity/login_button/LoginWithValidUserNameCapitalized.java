package com.icontrolesi.automation.testcase.selenium.envity.login_button;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class LoginWithValidUserNameCapitalized extends TestHelper{
	String assertMsg1 = "1) User Name should be changed to all lowercase letters while focus is removed from User Name:: ";
	@Test
	public void test_c1834_LoginWithValidUserNameCapitalized(){
		ProjectPage.performLogout();
		
		enterText(EnvitySupport.userNameLocator, AppConstant.USER.toUpperCase());
		
		String enteredUserNameWhenInFocus = getText(EnvitySupport.userNameLocator);
		
		enterText(EnvitySupport.passwordLocator, AppConstant.PASS);
		
		String enteredUserNameWhenOutOfFocus = getText(EnvitySupport.userNameLocator);
		
		tryClick(EnvitySupport.loginBtnLocator, 2);
		
		softAssert.assertEquals(enteredUserNameWhenInFocus, enteredUserNameWhenOutOfFocus.toUpperCase(), assertMsg1);
		
		boolean isLoginSuccessful = ProjectPage.isUserLoggedIn();
		
		softAssert.assertTrue(isLoginSuccessful, "***) Envity takes user to the landing page:: ");
		
		softAssert.assertAll();
	}
}
