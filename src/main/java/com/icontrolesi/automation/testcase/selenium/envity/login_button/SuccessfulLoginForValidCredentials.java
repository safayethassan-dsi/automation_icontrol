package com.icontrolesi.automation.testcase.selenium.envity.login_button;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class SuccessfulLoginForValidCredentials extends TestHelper{
	@Test
	public void test_c260_SuccessfulLoginForValidCredentials(){
		
		boolean isLoginSuccessful = ProjectPage.isUserLoggedIn();
		
		softAssert.assertTrue(isLoginSuccessful, "***) Envity takes user to the landing page:: ");
		
		softAssert.assertAll();
	}
}
