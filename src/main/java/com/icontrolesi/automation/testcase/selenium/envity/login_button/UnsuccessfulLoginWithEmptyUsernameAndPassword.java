package com.icontrolesi.automation.testcase.selenium.envity.login_button;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class UnsuccessfulLoginWithEmptyUsernameAndPassword extends TestHelper{
	private final String EXPECTED_ALERT_MSG = "Please enter your user name.";
	
	@Test
	public void test_c264_UnsuccessfulLoginWithEmptyUsernameValidPassword(){
		ProjectPage.performLogout();
		
		inputLoginCredentials("", "");
		
		tryClick(EnvitySupport.loginBtnLocator);
		
		String alertTextForLoginError = getAlertMsg().trim();
		
		softAssert.assertEquals(alertTextForLoginError, EXPECTED_ALERT_MSG);
		
		softAssert.assertAll();
	}
}
