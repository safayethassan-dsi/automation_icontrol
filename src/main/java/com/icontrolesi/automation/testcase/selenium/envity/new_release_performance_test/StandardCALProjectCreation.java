package com.icontrolesi.automation.testcase.selenium.envity.new_release_performance_test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

import org.testng.annotations.Test;

public class StandardCALProjectCreation extends TestHelper{

	@Test(description = "Check whether the full execution path for CAL Simple project creation is ok.")
	public void test_c1872_CheckCALStandardProjectCreation() {
		ProjectInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();
		
		CALStandardGeneralConfigurationPage.createProject(calStdInfo);

		long startTime = System.currentTimeMillis();

		double documentAddingDuration = TaskHistoryPage.getTimeToTaskToComplete(startTime) / 60000;

		softAssert.assertTrue(documentAddingDuration <= 4.30, "a. All documents added to project should complete at 4:30");

		double indexingDuration = TaskHistoryPage.getTimeToTaskToComplete(startTime) / 60000;

		softAssert.assertTrue(indexingDuration <= 6.15, "b. Indexing should Complete at 6:15");

		LeftPanelForProjectPage.gotoOverviewPage();

		softAssert.assertAll();
	} 
}
