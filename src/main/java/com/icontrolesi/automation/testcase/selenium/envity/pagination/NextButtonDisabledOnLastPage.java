package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

public class NextButtonDisabledOnLastPage extends TestHelper{
	
	@Test
	public void test_c356_NextButtonDisabledOnLastPage(){
		String urlBeforeClick = Driver.getDriver().getCurrentUrl();

		if(ProjectPage.getTotalPageCount() == 1){
			throw new SkipException("Not avaiable pages for performing this test.");
		}
		
		ProjectPage.gotoLastPage();
				
		boolean isNextButtonDisabled = ProjectPage.isNextBtnEnabled()  == false;
		
		ProjectPage.gotoNextPage();
		
		String urlAfterClick = Driver.getDriver().getCurrentUrl();
		
		softAssert.assertTrue(isNextButtonDisabled, "Next button disabled (UI):: ");
		softAssert.assertEquals(urlBeforeClick, urlAfterClick, "Clicking Next button navigates to nowhere:: ");
		
		softAssert.assertAll();
	}
}
