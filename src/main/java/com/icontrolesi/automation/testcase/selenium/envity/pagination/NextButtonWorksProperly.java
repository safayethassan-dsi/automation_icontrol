package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import java.util.List;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class NextButtonWorksProperly extends TestHelper{
	
	@Test
	public void test_c354_NextButtonWorksProperly(){
		int totalProjectCount = ProjectPage.getTotalProjectCount();
		
		int totalPagesCalculated = totalProjectCount / 5 + (totalProjectCount % 5 == 0 ? 0 : 1);
		int totalPages = ProjectPage.getTotalPageCount();
		
		softAssert.assertEquals(totalPages, totalPagesCalculated);
		
		
		
		
		if(totalProjectCount > 5){
			softAssert.assertEquals(ProjectPage.isPreviousBtnEnabled(), false, "*** Previous button is disabled:: ");
			softAssert.assertEquals(ProjectPage.isNextBtnEnabled(), true, "Next button is enabled:: ");
		
			boolean isNextBtnEnabled = ProjectPage.isNextBtnEnabled();
			
			Assert.assertTrue(isNextBtnEnabled, "Next link is enabled:: ");

			int pageNumberBefore = ProjectPage.getSelectedPageNumber();
			List<String> pageItemsBefore = getListOfItemsFrom(ProjectPage.projectNameLocator, project -> project.getText().trim());
			
			ProjectPage.gotoNextPage();
			
			int pageNumberAfter = ProjectPage.getSelectedPageNumber();
			List<String> pageItemsAfter = getListOfItemsFrom(ProjectPage.projectNameLocator, project -> project.getText().trim());
			
			softAssert.assertEquals(pageNumberAfter, pageNumberBefore + 1, "*** Page Number changed:: ");
			softAssert.assertNotEquals(pageItemsBefore, pageItemsAfter, "*** Page Items not same:: ");
			
		}else{
			throw new SkipException("No sufficient pages. Next button is disabled.");
		}
		
		softAssert.assertAll();
	}
}
