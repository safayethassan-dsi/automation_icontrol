package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class PaginationForLessThanFiveProjects extends TestHelper{
	
	@Test
	public void test_c351_PaginationForLessThanFiveProjects(){
		int totalProjectCount = ProjectPage.getTotalProjectCount();
		
		int totalPagesCalculated = totalProjectCount / 5 + (totalProjectCount % 5 == 0 ? 0 : 1);
		int totalPages = ProjectPage.getTotalPageCount();
		
		softAssert.assertEquals(totalPages, totalPagesCalculated);
		
		
		if(totalProjectCount < 5){
			boolean isPrevBtnDisabled = ProjectPage.isPreviousBtnEnabled() == false;
			boolean isNextBtnDisabled = ProjectPage.isNextBtnEnabled() == false;
			
			softAssert.assertTrue(isPrevBtnDisabled, "Previous button is disabled:: ");
			softAssert.assertTrue(isNextBtnDisabled, "Next button is disabled:: ");
		}else{
			for(int i = 0; i < totalPagesCalculated; i++){
				int totalProjectOnCurrentPage = getTotalElementCount(ProjectPage.projectNameLocator);
				
				if(i == totalPagesCalculated - 1){
					softAssert.assertTrue(totalProjectOnCurrentPage <= 5);
				}else{
					softAssert.assertEquals(totalProjectOnCurrentPage, 5);
				}
				
				tryClick(ProjectPage.NEXT_LINK, 1);
			}
		}
		
		softAssert.assertAll();
	}
}
