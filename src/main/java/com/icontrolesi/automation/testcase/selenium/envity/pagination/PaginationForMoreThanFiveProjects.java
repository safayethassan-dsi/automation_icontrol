package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class PaginationForMoreThanFiveProjects extends TestHelper{
	
	@Test
	public void test_c352_PaginationForMoreThanFiveProjects(){
		int totalProjectCount = ProjectPage.getTotalProjectCount();
		
		int totalPagesCalculated = totalProjectCount / 5 + (totalProjectCount % 5 == 0 ? 0 : 1);
		int totalPages = ProjectPage.getTotalPageCount();
		
		softAssert.assertEquals(totalPages, totalPagesCalculated);
		
		for(int i = 1; i < totalPagesCalculated; i++){
			int totalProjectOnCurrentPage = getTotalElementCount(ProjectPage.projectNameLocator);
			
			if(i == totalPagesCalculated){
				softAssert.assertTrue(totalProjectOnCurrentPage <= 5);
			}else{
				softAssert.assertEquals(totalProjectOnCurrentPage, 5);
			}
			
			ProjectPage.gotoNextPage();
		}
		
		softAssert.assertAll();
	}
}
