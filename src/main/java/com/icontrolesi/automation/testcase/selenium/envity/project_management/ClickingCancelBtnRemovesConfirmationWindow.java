package com.icontrolesi.automation.testcase.selenium.envity.project_management;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.testng.annotations.Test;

public class ClickingCancelBtnRemovesConfirmationWindow extends TestHelper {
    
    @Test(description = "Clicking cancel button removes up the pop-up window")
    public void test_c1885_ClickingCancelBtnRemovesConfirmationWindow() {
        ProjectInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();
        String projectName = calStdInfo.getProjectName();

        if(ProjectPage.getTotalProjectCount() == 0){
            CALStandardGeneralConfigurationPage.createProject(calStdInfo);
        }else{
            projectName = getText(ProjectPage.projectNameLocator);
        }

        ProjectPage.gotoProjectAdministrationPage();

        ManageProjectsPage.clickDeleteBtn(projectName);

        softAssert.assertEquals(getText(ManageProjectsPage.CANCEL_BTN), "Cancel", "Cancel button appeared:: ");
        
        tryClick(ManageProjectsPage.CANCEL_BTN, 1);

        softAssert.assertEquals(getTotalElementCount(ManageProjectsPage.CONFIRM_WINDOW_HEADER), 0, "Delete Project window removed:: ");
        softAssert.assertEquals(getTotalElementCount(ManageProjectsPage.CANCEL_BTN), 0, "Cancel button disappeared:: ");

        softAssert.assertAll();
    }
}