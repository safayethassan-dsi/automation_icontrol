package com.icontrolesi.automation.testcase.selenium.envity.project_management;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.testng.annotations.Test;

public class ClickingDeleteBtnShowsConfirmationBox extends TestHelper {
    @Test(description = "Clicking Delete button opens up a pop-up window")
    public void test_c1883_ClickingDeleteBtnShowsConfirmationBox() {
        ProjectInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();
        String projectName = calStdInfo.getProjectName();

        if(ProjectPage.getTotalProjectCount() == 0){
            CALStandardGeneralConfigurationPage.createProject(calStdInfo);
        }else{
            projectName = getText(ProjectPage.projectNameLocator);
        }
        ProjectPage.gotoProjectAdministrationPage();

        String pageTitle = getText(ManageProjectsPage.PAGE_TITLE_LOCATOR);

        softAssert.assertEquals(pageTitle, "Manage Projects", "*** Job status is ok::  ");

        ManageProjectsPage.clickDeleteBtn(projectName);

        checkConfirmWindowAppeared();

        softAssert.assertAll();
    }

    private void checkConfirmWindowAppeared(){
        String confirmWindowHeader = getText(ManageProjectsPage.CONFIRM_WINDOW_HEADER);
		int confirmBtnCount =  getTotalElementCount(ManageProjectsPage.CONFIRM_BTN);
        int cancelBtnCount =  getTotalElementCount(ManageProjectsPage.CANCEL_BTN);
        
        softAssert.assertEquals(confirmWindowHeader, "Delete Project", "Header is fine:: ");
        softAssert.assertEquals(confirmBtnCount, 1, "Confirm button present:: ");
        softAssert.assertEquals(cancelBtnCount, 1, "Cancel button present:: ");
    }
}