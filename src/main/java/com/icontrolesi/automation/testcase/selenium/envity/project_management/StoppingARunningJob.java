package com.icontrolesi.automation.testcase.selenium.envity.project_management;

import java.util.List;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ManageProjectsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.OverviewPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

import org.testng.annotations.Test;

public class StoppingARunningJob extends TestHelper {
    @Test(description = "Stopping a Running job works fine")
    public void test_c1881_StoppingARunningJob(){
        ProjectInfo calStdInfo = new ProjectInfoLoader("cal_std").loadCALStdInfo();
        CALStandardGeneralConfigurationPage.createProject(calStdInfo);

        OverviewPage.waitForBatchJobStart(300);

        ProjectPage.gotoProjectAdministrationPage();
        
        String pageTitle = getText(ManageProjectsPage.PAGE_TITLE_LOCATOR);

        softAssert.assertEquals(pageTitle, "Manage Projects", "1. Page Title displayed correctly:: ");

        String jobStatus = ManageProjectsPage.getJobStatus(calStdInfo.getProjectName());

        softAssert.assertEquals(jobStatus, "Stop Monitoring Job", "*** Job status is ok::  ");

        ManageProjectsPage.stopJob(calStdInfo.getProjectName());

        jobStatus = ManageProjectsPage.getJobStatus(calStdInfo.getProjectName());

        softAssert.assertEquals(jobStatus, "Start Monitoring Job", "*** Job status is ok::  ");

        ProjectPage.gotoHomePage();
        ProjectPage.openProject(calStdInfo.getProjectName());

        // LeftPanelForProjectPage.gotoOverviewPage();
        
        softAssert.assertEquals(OverviewPage.getJobStatus(), "Stopped", "*** Job Stopped is seen from Overview:: ");

        softAssert.assertAll();
    }
}