package com.icontrolesi.automation.testcase.selenium.envity.project_summary;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;

public class CreatingJudgmentalSampleAfterPopulation extends TestHelper {
	ProjectInfo salInfo = new ProjectInfoLoader("sal").loadSALInfo();


	@Test(description = "Add Judgmental Sample After adding Population to a new project")
	public void test_389_CreatingJudgmentalSampleAfterPopulation() {
		SALGeneralConfigurationPage.createSALProject(salInfo);;
		LeftPanelForProjectPage.gotoTaskQueuePage();

		checkPopulationAddingWorksForSAL();

		LeftPanelForProjectPage.syncProject();

		checkControlSampleCreationForSAL();
		
		softAssert.assertAll();
	} 

	void checkPopulationAddingWorksForSAL(){
		
		boolean isProjectCreationOk = SALGeneralConfigurationPage.isPorjectCreated();
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");

		SALGeneralConfigurationPage.addPopulation("1 of 11k");

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");
	}

	void checkControlSampleCreationForSAL(){
		LeftPanelForProjectPage.addJudgementalSample(salInfo.getJudgementalSampleName());
		boolean isControlSampleAdded = SALGeneralConfigurationPage.isControlSampleAdded();

		softAssert.assertTrue(isControlSampleAdded, "SAL Control Sample Adding FAILED:: ");
	}
}
