package com.icontrolesi.automation.testcase.selenium.envity.project_summary;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.OverviewPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;

public class DocumentCountAfterAddingJudgmentalSample extends TestHelper {

	SALInfo salInfo = null;

	@Test(description = "Add population for a new project")
	public void test_393_DocumentCountAfterAddingJudgmentalSample() {
		checkJudgmentalSampleWorksForSAL();

		LeftPanelForProjectPage.gotoOverviewPage();

		int totalDocumentsAdded = OverviewPage.getTotalDocumentsAdded();

		softAssert.assertEquals(totalDocumentsAdded, 200);

		

		String judgementalSmplMsg = getText(OverviewPage.TOTAL_TRAINING_SAMPLE_MSG_LOCATOR);

		softAssert.assertTrue(judgementalSmplMsg.matches("1 training samples containing \\d+ documents"), "Confirming Judgemental Sample Added:: ");
		
		softAssert.assertAll();
	} 

	void checkJudgmentalSampleWorksForSAL(){
		salInfo = new ProjectInfoLoader("sal").loadSALInfo();

		SALGeneralConfigurationPage.createSALProject(salInfo);;
		
		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = SALGeneralConfigurationPage.isPorjectCreated();
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");

		System.err.println("*********" + salInfo.getSavedSearchNameForPopulatioin());

		SALGeneralConfigurationPage.addPopulation(salInfo.getSavedSearchNameForPopulatioin());

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");

		System.err.println("*********" + salInfo.getJudgementalSampleName());

		SALGeneralConfigurationPage.addJudgementalSample(salInfo.getJudgementalSampleName());

		boolean isJudgementalSampleAdded = SALGeneralConfigurationPage.isJudgementalSampleAdded();

		softAssert.assertTrue(isJudgementalSampleAdded, "SAL Judgemental Sample Adding FAILED:: ");
	}
}
