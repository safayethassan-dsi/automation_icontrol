package com.icontrolesi.automation.testcase.selenium.envity.project_summary;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.OverviewPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;

public class DocumentCountAfterAddingMultiplePopulation extends TestHelper {

	SALInfo salInfo = null;
	private final String savedSearchName2 = "200 of 1k";

	@Test(description = "Add population for a new project")
	public void test_394_DocumentCountAfterAddingMultiplePopulation() {
		salInfo = new ProjectInfoLoader("sal").loadSALInfo();

		SALGeneralConfigurationPage.createSALProject(salInfo);

		checkMultiplePopulationAddingForDisjointSet();
		
		softAssert.assertAll();
	} 

	void checkMultiplePopulationAddingForDisjointSet(){
		SALGeneralConfigurationPage.addPopulation(salInfo.getSavedSearchNameForPopulatioin());

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");

		LeftPanelForProjectPage.gotoOverviewPage();

		int totalDocumentsAdded = OverviewPage.getTotalDocumentsAdded();

		softAssert.assertEquals(totalDocumentsAdded, 200, "Number of documents added matched:: ");

		SALGeneralConfigurationPage.addPopulation(savedSearchName2);

		isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding (2nd pahse) FAILED:: ");

		LeftPanelForProjectPage.gotoOverviewPage();

		totalDocumentsAdded = OverviewPage.getTotalDocumentsAdded();

		softAssert.assertEquals(totalDocumentsAdded, 200, "Number of documents added matched (Phase 2):: ");
	}
}
