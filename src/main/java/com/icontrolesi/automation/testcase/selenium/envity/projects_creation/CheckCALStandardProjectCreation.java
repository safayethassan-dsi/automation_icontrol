package com.icontrolesi.automation.testcase.selenium.envity.projects_creation;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

public class CheckCALStandardProjectCreation extends TestHelper{

	@Test(description = "Check whether the full execution path for CAL Simple project creation is ok.")
	public void test_0000003_CheckCALStandardProjectCreation() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYY_hhmm");
		String projectName = String.format("CL_STD_%s", sdf.format(new Date()));
		
		CALStandardGeneralConfigurationPage.createProject(projectName);
		
		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = TaskHistoryPage.isCurrentTaskFailed("Create Standard Envity CAL") == false;
		
		softAssert.assertTrue(isProjectCreationOk, "Standard CAL project creation FAILED:: ");
		
		softAssert.assertAll();
	} 

}
