package com.icontrolesi.automation.testcase.selenium.envity.task_queue;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

import org.testng.annotations.Test;

public class DownloadBtnAppearsForFailedTasks extends TestHelper {
    @Test(description = "After adding Judgemental Sample, a task is added in the Queued Task")   
    public void test_c409_DownloadBtnAppearsForFailedTasks(){
        SALInfo projectInfo = new ProjectInfoLoader("sal").loadSALInfo();
		
		SALGeneralConfigurationPage.createSALProject(projectInfo);

		LeftPanelForProjectPage.gotoTaskQueuePage();
		
        softAssert.assertEquals(TaskHistoryPage.getCurrentTaskStatus("Create Envity project"), "FINISHED", "SAL project creation FAILED:: ");
        
        LeftPanelForProjectPage.addJudgementalSample(projectInfo.getJudgementalSampleName());

        softAssert.assertEquals(TaskHistoryPage.getCurrentTaskStatus("Create Judgemental Samples"), "ERROR", "Status is correct for failed Task:: ");

        
        
        softAssert.assertAll();
    }
}