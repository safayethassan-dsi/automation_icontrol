package com.icontrolesi.automation.testcase.selenium.envity.task_queue;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

import org.testng.annotations.Test;

public class TaskQueueAddedAfterAddingJudgementalSample extends TestHelper {
    @Test(description = "After adding Judgemental Sample, a task is added in the Queued Task")   
    public void test_c398_TaskQueueAddedAfterAddingJudgementalSample(){
        SALInfo projectInfo = new ProjectInfoLoader("sal").loadSALInfo();
		
		SALGeneralConfigurationPage.createSALProject(projectInfo);

		LeftPanelForProjectPage.gotoTaskQueuePage();
		
        softAssert.assertEquals(TaskHistoryPage.getCurrentTaskStatus("Create Envity project"), "FINISHED", "SAL project creation FAILED:: ");
        
        SALGeneralConfigurationPage.addPopulation(projectInfo.getSavedSearchNameForPopulatioin());
  
        softAssert.assertEquals(TaskHistoryPage.getCurrentTaskStatus("Add Population"), "FINISHED", "SAL project creation FAILED:: ");

        LeftPanelForProjectPage.openSampleWindow("judgemental");
        selectFromDrowdownByText(LeftPanelForProjectPage.Samples.SAVEDSEARCH_DROPDOWN_FOR_JSAMPLE, projectInfo.getJudgementalSampleName());
        new TaskQueueAddedAfterAddingJudgementalSample().tryClick(LeftPanelForProjectPage.Samples.CREATE_BTN, TaskHistoryPage.TASK_PROGRESS_LOCATOR);
        waitFor(5);
		
        softAssert.assertEquals(TaskHistoryPage.getCurrentTaskStatus("Create Judgemental Samples"), "QUEUED", "SAL Judgemental Sample Adding FAILED:: ");
        
        softAssert.assertAll();
    }
}