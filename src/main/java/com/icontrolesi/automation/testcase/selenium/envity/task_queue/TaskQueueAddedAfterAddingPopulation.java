package com.icontrolesi.automation.testcase.selenium.envity.task_queue;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.TaskHistoryPage;

import org.testng.annotations.Test;

public class TaskQueueAddedAfterAddingPopulation extends TestHelper {
    @Test(description = "After adding a population, a task is added in the Queued Task")   
    public void test_c396_TaskQueueAddedAfterAddingPopulation(){
        SALInfo projectInfo = new ProjectInfoLoader("sal").loadSALInfo();
		
		SALGeneralConfigurationPage.createSALProject(projectInfo);

		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = TaskHistoryPage.isCurrentTaskFailed("Create Envity project") == false;
		
        softAssert.assertTrue(isProjectCreationOk, "SAL project creation FAILED:: ");
        
        SALGeneralConfigurationPage.addPopulation(projectInfo.getSavedSearchNameForPopulatioin());

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

        softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");
        
        softAssert.assertAll();
    }
}