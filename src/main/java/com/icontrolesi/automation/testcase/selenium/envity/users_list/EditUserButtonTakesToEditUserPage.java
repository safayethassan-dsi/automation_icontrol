package com.icontrolesi.automation.testcase.selenium.envity.users_list;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class EditUserButtonTakesToEditUserPage extends TestHelper{
	
	@Test(description = "Checks whether clicking the 'Edit User' button for a specific user opens up the 'Edit User' window accordingly.")
	public void test_c266_EditUserButtonTakesToEditUserPage(){
		ProjectPage.gotoUserAdministrationPage();
		
		/**
		 * 		Get all the information of the first user in the list
		 */
		
		enterText(UsersListPage.SEARCH_FIELD_LOCATOR, AppConstant.USER);

		String name = getText(UsersListPage.NAME_COLUMN);
		String username = getText(UsersListPage.USERNAME_COLUMN);
		String email = getText(UsersListPage.EMAIL_COLUMN);
		
		UsersListPage.EditUser.open();
		
		String emailnForm = getText(UsersListPage.EditUser.EMAIL_FIELD);
		String userNameInForm = getText(UsersListPage.EditUser.USERNAME_FIELD);
		String firstNameInForm = getText(UsersListPage.EditUser.FIRSTNAME_FIELD);
		String lastNameInForm = getText(UsersListPage.EditUser.LASTNAME_FIELD);
		
		
		String windowHeader = getText(UsersListPage.EditUser.HEADER);
		
		softAssert.assertEquals(windowHeader, "Edit User", "Edit User opened:: ");
		softAssert.assertEquals(username, userNameInForm, "Username matched:: ");
		softAssert.assertEquals(email, emailnForm, "Email matched:: ");
		softAssert.assertTrue(name.startsWith(firstNameInForm), "First Name matched:: ");
		softAssert.assertTrue(name.endsWith(lastNameInForm), "Last Name matched:: ");
		
		softAssert.assertAll();
	}
}
