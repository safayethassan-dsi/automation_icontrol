package com.icontrolesi.automation.testcase.selenium.envity.users_list;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class EnableOrDisableIconToggledAndActiveColumnIsUpdatedOnClicking extends TestHelper{
	static final String USER_TO_TEST = AppConstant.DISABLED_USERNAME;   
	
	@Test(description = "Checks whether clicking the 'Create User' button opens up the 'Create User' window.")
	public void test_c269_EnableOrDisableIconToggledAndActiveColumnIsUpdatedOnClicking(){
		ProjectPage.gotoUserAdministrationPage();
		
		boolean isUserEnabledBeforeClicking = UsersListPage.isUserEnabled(USER_TO_TEST);
		
		String userStatusIconTypeBefore = UsersListPage.getStatusIconType(USER_TO_TEST);
		
		UsersListPage.toggleUserStatus(USER_TO_TEST, isUserEnabledBeforeClicking);
		
		boolean isUserEnabledAfterClicking = UsersListPage.isUserEnabled(USER_TO_TEST);
		
		String userStatusIconTypeAfter = UsersListPage.getStatusIconType(USER_TO_TEST);
		
		System.err.println(userStatusIconTypeAfter+":" + userStatusIconTypeBefore);
		
		
		softAssert.assertNotEquals(isUserEnabledAfterClicking, isUserEnabledBeforeClicking, "User Enabled/Disabled status changed:: ");
		softAssert.assertNotEquals(userStatusIconTypeAfter, userStatusIconTypeBefore, "User Enabled/Disabled status icon changed:: ");
			
		softAssert.assertAll();
	}
}
