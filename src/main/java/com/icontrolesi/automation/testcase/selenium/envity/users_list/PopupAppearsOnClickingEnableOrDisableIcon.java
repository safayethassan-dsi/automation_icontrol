package com.icontrolesi.automation.testcase.selenium.envity.users_list;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;

public class PopupAppearsOnClickingEnableOrDisableIcon extends TestHelper{
	
	@Test(description = "Checks whether clicking the 'Create User' button opens up the 'Create User' window.")
	public void test_c268_PopupAppearsOnClickingEnableOrDisableIcon(){
		ProjectPage.gotoUserAdministrationPage();
		
		tryClick(UsersListPage.ENABLE_DISABLE_USER_ICON);
		
		String confirmationMsg = getText(UsersListPage.CONFIRMATION_BODY);
		
		boolean isConfirmBtnExist = getTotalElementCount(UsersListPage.CONFIRM_BTN) == 1;
		
		softAssert.assertEquals(confirmationMsg, "Are you sure to disable this user?", "Confirmation message appears properly:: ");	
		softAssert.assertTrue(isConfirmBtnExist, "Confirm button present:: ");
		
		softAssert.assertAll();
	}
}
