package com.icontrolesi.automation.testcase.selenium.page.accordion;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DomainAccordion extends ParentAccordion {
	@FindBy(how = How.ID, using = "sortByFreq")
	WebElement sortByFrequencyCheckBox;

	@FindBy(how = How.ID, using = "messageContainer")
	WebElement loadingMessage;

	@FindBy(how = How.ID, using = "domainList")
	WebElement domains;

	public DomainAccordion(WebDriver driver) {
		super(driver);
		driver.switchTo().frame("domainsFrame");
	}

	public int sortByFrequency() {
		while (loadingMessage.isDisplayed()) {
		}
		long start = System.nanoTime();
		long end = 0;
		sortByFrequencyCheckBox.click();
		while (loadingMessage.isDisplayed()) {
			end = System.nanoTime();
		}
		return (int) ((long) (end - start) / 1000000000);
	}

	public List<String> getDomainList(int counter) {
		while (loadingMessage.isDisplayed()) {
		}
		List<String> list = new ArrayList<String>();
		List<WebElement> domainList = domains.findElements(By.tagName("li"));
		for (WebElement element : domainList) {
			counter--;
			if(counter<=0){
				break;
			}
			element = element.findElement(By.cssSelector("span"));
			element = element.findElement(By.cssSelector("span"));
			String text = element.getText();
			list.add(text);
		}
		return list;
	}
}
