package com.icontrolesi.automation.testcase.selenium.page.accordion;

import org.openqa.selenium.WebDriver;

public class ParentAccordion {
	
	WebDriver driver;
	
	public ParentAccordion(WebDriver driver){
		this.driver = driver;
	}
	
	public void closeAccordion(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("MAIN");
	}

}
