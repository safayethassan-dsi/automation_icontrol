package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class ChangePassword implements CommonActions{

	
	
public void clickChangePassordButton(){
		List<WebElement> spanNodes= Driver.getDriver().findElements(By.cssSelector("span[class='ui-button-text']"));
		for(int i=0;i<spanNodes.size();i++){
			if(spanNodes.get(i).isDisplayed()){
				if(spanNodes.get(i).getText().equalsIgnoreCase("Change Password")){
				WebElement closeButton= SearchPage.getParent(Driver.getDriver(), spanNodes.get(i));
				click(closeButton);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}//end if
		}//end if
	}//end for
}//end function
	
	
	
public void performPasswordChange(String currentPass,String newPass){
	waitForFrameToLoad(Driver.getDriver(), Frame.MAIN_FRAME);
	
	//assign old pass
	SearchPage.waitForElement(Driver.getDriver(), "oldPassword", "name").sendKeys(currentPass);
	//assign new pass
	SearchPage.waitForElement(Driver.getDriver(), "newPassword1", "id").sendKeys(newPass);
    //confirm the new pass
	SearchPage.waitForElement(Driver.getDriver(), "newPassword2", "id").sendKeys(newPass);

	tryClick(By.id("submit"), 5);
	
	By lastPasswordChangeMsgLocator = By.cssSelector("li[style$='red']");
	
	boolean isLastPasswordMsgAppeared = getTotalElementCount(lastPasswordChangeMsgLocator) > 0;
	
	if(isLastPasswordMsgAppeared && getText(lastPasswordChangeMsgLocator).equals("Cannot reuse last 5 passwords")){
		new SkipException("Cannot reuse last 5 passwords");
	}
	
	System.out.println( Logger.getLineNumber() + "Password changed successfully to [ " + newPass + " ]...");
}		
}
