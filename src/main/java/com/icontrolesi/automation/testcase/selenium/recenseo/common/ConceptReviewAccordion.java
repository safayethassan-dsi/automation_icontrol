package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class ConceptReviewAccordion implements CommonActions{
	static WebDriver driver = Driver.getDriver(); 
	public static By tabLocation = By.id("cluster");
	public static By totalDocsInCluster = By.id("cluster-totalDocsInCluster");
	public static By totalDocsInState = By.id("cluster-docsInState");
	public static By addToSeachBtn = By.cssSelector("button[title='Add to the current search'] > span");
	public static By viewSelected = By.cssSelector("button[title='Open selected clusters to review']");
	public static By gridViewTableRow = By.cssSelector("#grid > tbody > tr > td");
	public static By clusterLocator = By.cssSelector("div.treeViewContainer.filetree.treeview > li > span");
	public static By clusterCheckboxLocator = By.cssSelector("div.treeViewContainer.filetree.treeview > li > span > input");
	
	private static ConceptReviewAccordion conceptReviewAccordion = new ConceptReviewAccordion();
	
	public static void open(){
		ConceptReviewAccordion conceptReviewAccordion = new ConceptReviewAccordion(); 
		driver = Driver.getDriver();
		//conceptReviewAccordion.waitForInVisibilityOf(tabLocation);
		driver.findElement(tabLocation).click();
		conceptReviewAccordion.waitForFrameToLoad("clusterFrame");
		conceptReviewAccordion.waitForElementToBeClickable(By.cssSelector("#cluster-actionList > button[title='Add to the current search']"));
		conceptReviewAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("body > div.treeViewContainer.filetree.treeview > li"), 0);
		conceptReviewAccordion.waitFor(5);
		
		System.out.println( Logger.getLineNumber() + "\nConceptReview accordion expanded...\n");
	}
	
	public static void selectFirstCluster(){
		conceptReviewAccordion.tryClick(clusterCheckboxLocator);
	}
	
	public static void openSelectedForView(){
		ConceptReviewAccordion conceptReviewAccordion = new ConceptReviewAccordion(); 
		conceptReviewAccordion.tryClick(viewSelected);
		conceptReviewAccordion.waitForPageLoad();
		conceptReviewAccordion.switchToDefaultFrame();
		conceptReviewAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
		conceptReviewAccordion.waitForNumberOfElementsToBeGreater(gridViewTableRow, 1);
		conceptReviewAccordion.waitFor(8);
	}
	
	
	public static void openSelectedForView(int position){
		ConceptReviewAccordion conceptReviewAccordion = new ConceptReviewAccordion(); 
		conceptReviewAccordion.tryClick(By.cssSelector("div[class='treeViewContainer filetree treeview'] > li:nth-child(" + position + ") > span > input"));
		conceptReviewAccordion.tryClick(viewSelected);
		conceptReviewAccordion.waitForPageLoad();
		conceptReviewAccordion.switchToDefaultFrame();
		conceptReviewAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
		conceptReviewAccordion.waitForNumberOfElementsToBeGreater(gridViewTableRow, 1);
		conceptReviewAccordion.waitFor(5);
	}
	
	public static void expandConceptReviewItem(int position){
		conceptReviewAccordion.tryClick(By.cssSelector("div[class='treeViewContainer filetree treeview'] > li:nth-child(" + position + ") > span"), totalDocsInState);
	}
	
	public static void addSelectedToSearch(){
		conceptReviewAccordion.tryClick(addToSeachBtn);
	}
	
	public static void closeClusterDetailsModal(){
		conceptReviewAccordion.tryClick(By.cssSelector("button[title='Close'] > span"));
	}
	
    public static int getTotalDocsInCluster(){
    	return Integer.parseInt(conceptReviewAccordion.getText(totalDocsInCluster));
    }
    
    public static int getTotalDocsInState(){
    	return Integer.parseInt(conceptReviewAccordion.getText(totalDocsInState));
    }
}
