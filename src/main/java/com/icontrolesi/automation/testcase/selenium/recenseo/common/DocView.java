package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;

import com.icontrolesi.automation.platform.util.Logger;

public class DocView implements CommonActions{
	public static DocView docView = new DocView();
	private static WebDriver driver;
	public static By totalDocsLocator = By.id("totalDocs");
	public static By actionMenuLocator = By.id("processmenu_btn");
	public static By documentId = By.cssSelector("#menuDocId > span:nth-child(2)");
	public static By loadingMsgLocator = By.id("load_grid");
	public static By pickListCheckboxLocator = By.cssSelector("#prefs > fieldset:nth-child(2) > div > input"); 
	public static By pickListUpdateMsg = By.id("picklistStatus");
	public static By checkAllCheckBox = By.id("checkAllCols");
	
	public static By nextDocument = By.id("btnNext");
	public static By previousDocument = By.id("btnPrev");
	public static By firstDocument = By.id("btnFirst");
	public static By lastDocument = By.id("btnLast");
	public static By selectDocument = By.id("selectDocument");
	public static By highlightBtn = By.id("toggleHighlightTerms");
	
	public static By commentInCodingTemplate = By.id("comm");
	
	public static By sideBarPanelLocator = By.id("sidebarPanel");
	public static By gridPanelLocator = By.id("grid_panel");
	public static By docPanelLocator = By.id("doc_panel");
	public static By selectAllDocumentLocator = By.id("cb_grid");
	public static By termLocator = By.cssSelector("span[id^='term']");
	
	
	public static By gridRowLocator = By.cssSelector("#grid > tbody > tr[id]");
	
	
	public static By codingTemplateFieldLocator = By.cssSelector("div[id^='field_']");
	
	public static By selectBoxForDocPerPage = By.cssSelector("#grid_toppager_center > table > tbody > tr > td:nth-child(8) > select");
	
	public static By nextBtnInWFS = By.cssSelector("#confirm > span");
	public static By goBtnInWFS = By.cssSelector("#changeWorkflowState > span");
	public static By cancelBtnInWFS = By.cssSelector("#cancelConfirmation > span");
	public static By goBackSummaryBtnLocator = By.xpath("//span[text()='Go Back To Summary Page']");
	public static By wfsSummaryRowLocator = By.cssSelector("#summary tbody tr");
	
	
	public static By docHistoryTime = By.cssSelector("td[aria-describedby='docHistory_createdTime']");
	public static By docHistoryState = By.cssSelector("td[aria-describedby='docHistory_state']");
	public static By docHistoryAction = By.cssSelector("td[aria-describedby='docHistory_action']");
	
	
	/*#
	 * 	Column headers
	 */
	
	public static By author = By.id("jqgh_grid_auth");
	public static By caseName = By.id("jqgh_grid_case");
	public static By confidentiality = By.id("jqgh_grid_conf");
	public static By documentID = By.id("jqgh_grid_inv_id");
	//public static By conceptRank = By.id("#jqgh_grid_ORDER\\\\:CONCEPT");
	public static By conceptRank = By.cssSelector("div[id='jqgh_grid_ORDER:CONCEPT']");
	
	/*#
	 * 	Column locators
	 */
	public static By documentIDColumnLocator = By.cssSelector("td[aria-describedby='grid_inv_id']");
	public static By documentfileNameColumnLocator = By.cssSelector("td[aria-describedby='grid_cfil']");
	public static By documentAuthorColumnLocator = By.cssSelector("td[aria-describedby='grid_auth']");
	public static By documentCustodianColumnLocator = By.cssSelector("td[aria-describedby='grid_srce']");
	public static By documentTitleColumnLocator = By.cssSelector("td[aria-describedby='grid_subj']");
	public static By documentTagColumnLocator = By.cssSelector("td[aria-describedby='grid_tags']");
	public static By documentDateColumnLocator = By.cssSelector("td[aria-describedby='grid_date']");
	public static By conceptRankColumnLocator = By.cssSelector("td[aria-describedby='grid_ORDER:CONCEPT']");
	public static By documentBatesColumnLocator = By.cssSelector("td[aria-describedby='grid_bates']");
	public static By documentConfidentialColumnLocator = By.cssSelector("td[aria-describedby='grid_conf']");
	public static By documentPrivilegedColumnLocator = By.cssSelector("td[aria-describedby='grid_priv']");
	public static By documentDateAddedLocator = By.cssSelector("td[aria-describedby='grid_date_added']");
	public static By documentAddresseeLocator = By.cssSelector("td[aria-describedby='grid_addr']");
	public static By documentFileTypeLocator = By.cssSelector("td[aria-describedby='grid_mimeType']");
	public static By documentPrivReasonColumnLocator = By.cssSelector("td[aria-describedby='grid_prvr']") ;
	public static By documentCheckboxColumnLocator = By.cssSelector("td[aria-describedby='grid_cb'] input") ;
	
	/*#
	 * 	View Locators
	 */
	
	public static By imageViewLocator = By.id("reviewImaged");
	public static By nativeViewLocator = By.id("reviewNative");
	public static By textViewLocator = By.id("reviewText");
	public static By historyViewLocator = By.id("reviewHistory");
	public static By findSimilarViewLocator = By.id("relatedDocs");
	public static By threadViewLocator = By.id("emailThread");
	public static By docFamilyViewLocator = By.id("documentSet");
	public static By translationViewLocator = By.id("reviewTranslation");
	
	
	/*#
	 * 		Locators for Global Edit section
	 */
	
	
	public static By editFieldForAttributes = By.id("editField");
	public static By nextBtnLocator = By.id("nextButton");
	public static By globalEditSummaryLocator = By.id("summary");
	public static By confirmGlobalEditLocator = By.cssSelector("#confirmOk span");
	public static By globalEditMsgHolderLocator = By.id("messagePlaceHolder");
	public static By editActionLocator = By.cssSelector("#fieldEditContainer > fieldset > div > select");
	
	
	/**
	 * 
	 * 		Locators for Preference window elements
	 */
	
	public static By defaultProductionSet = By.name("DEFAULT_PPSET");
	
	
	public void gridNavigation(WebDriver driver, SearchPage sp, String navigationType){
		if(navigationType.equalsIgnoreCase("previous")){
		    click(docView.getElement(By.cssSelector("span[class='ui-icon ui-icon-seek-prev']")));
		}else if(navigationType.equalsIgnoreCase("next")){
			click(docView.getElement(By.cssSelector("span[class='ui-icon ui-icon-seek-next']")));
		}else if(navigationType.equalsIgnoreCase("first")){
		      click(docView.getElement(By.cssSelector("span[class='ui-icon ui-icon-seek-first']")));
		}else if(navigationType.equalsIgnoreCase("last")){
		    click(docView.getElement(By.cssSelector("span[class='ui-icon ui-icon-seek-end']"))); 
		}
	}

	
	
	//This method is necessary for making script wait till an action is complete
		public static void waitForCompletion(WebDriver driver, SearchPage sp) throws InterruptedException{
			
			Thread.sleep(8000);
			
			try{
			
			WebElement message= Utils.waitForElement(driver, "messageContainer", "id");
			
			while(message.isDisplayed()){
	        	
	        	
	        	
	        	
	        }
			    }
			    catch(Exception e){
			    	
			    	
			    	
			    }
			
			
		}
	
	
	public static boolean isAllDocumentsOutOfState(){
		boolean isDocumentOutOfState = docView.getElements(gridRowLocator).stream()
											   .anyMatch(row -> row.getAttribute("class").contains("NotInState"));
		
		return isDocumentOutOfState;
	}
		
	public static int getHighlightedDocumentNumber(){
		String documentNumber = docView.getElements(DocView.gridRowLocator).stream()
				.filter(r -> docView.getAttribute(r, "class").endsWith("Highlight"))
				.findFirst().get().findElement(By.tagName("td")).getText().trim();
		
		return Integer.parseInt(documentNumber);
	}
	
	
	public String getTotalDocumentCount(WebDriver driver, SearchPage sp){
		String docCount=docView.getElement(By.id("totalDocs")).getText();
		return docCount ;
	}
	
	
	public static String getDocumentName(int position){
		List<WebElement> fileNameList = Driver.getDriver().findElements(documentfileNameColumnLocator);
		
		return fileNameList.get(position - 1).getText().trim();
	}
	
	
	public static String getTagsName(int position){
		List<WebElement> tagsName = Driver.getDriver().findElements(documentTagColumnLocator);
		
		return tagsName.get(position - 1).getText().trim();
	}
	
	public static List<String> getTagsNameSplitted(int position){
		String [] splittedTags = getTagsName(position).split("\\,");
		return Stream.of(splittedTags).collect(Collectors.toList());
	}
	
	
	public static Set<String> getTagsNameSplittedAsSet(int position){
		return Stream.of(getTagsName(position).split("\\,")).sorted().collect(Collectors.toSet());
	}
	
	public void clickActionMenuItem(WebDriver driver, SearchPage sp, String menuName){
		waitForPageLoad(driver);
		waitForElementToBeClickable(By.id("processmenu_btn"));
		
		docView.getElement(By.id("processmenu_btn")).click();
		
		waitFor(3);
		
		click(docView.getElement(By.linkText(menuName)));
		
		waitFor(10);
	}
	
	public void clickActionMenuItem(String menuName){
		if(Configuration.getConfig("selenium.browser").equals("ie11")){
			waitForPageLoad();
			waitFor(30);
		}else{
			waitForPageLoad();
			waitForVisibilityOf(By.id("processmenu_btn"));
		}
		
		tryClick(By.id("processmenu_btn"));
		
		tryClick(By.linkText(menuName), 25);
		//tryClick(By.linkText(menuName));
	}
	
	
	public static void openGlobalEditWindow(){
		docView.clickActionMenuItem("Global Edit");
		docView.switchToFrame(1);
		docView.waitForVisibilityOf(editFieldForAttributes);
		
		System.out.println( Logger.getLineNumber() + "'Global Edit' window opened...");
	}
	
	public static void openWFSManagerWindow(){
		docView.clickActionMenuItem("Workflow state manager");
		docView.switchToFrame(1);
		docView.waitForNumberOfElementsToBeGreater(wfsSummaryRowLocator, 0);
		
		System.out.println( Logger.getLineNumber() + "'Workflow state manager' window opened...");
	}
	
	public static void closeWFSManageerWindow(){
		docView.switchToDefaultFrame();
		docView.waitForFrameToLoad(Frame.MAIN_FRAME);
		List<WebElement> closeBtnsList = docView.getElements(By.xpath("//span[text()='Close']"));
		closeBtnsList.get(closeBtnsList.size() - 1).click();
		
		
		/*if(docView.getElement(By.id("loadingMessage")).isDisplayed()){
			docView.waitForInVisibilityOf(By.id("loadingMessage"));
			docView.waitFor(5);
		}*/
		
		docView.waitFor(20);
	}
	
	public static void openManageReviewAssignmentWindow(){
		docView.clickActionMenuItem("Manage Review Assignments");
		docView.switchToFrame(1);
		docView.waitForNumberOfElementsToBeGreater(ReviewAssignmentAccordion.visibleAssignmentLocator, 0);
		
		System.out.println( Logger.getLineNumber() + "'Manage Review Assignments' window opened...");
	}
	
	
	public static void openManageDocumentSecurityWindow(){
		docView.clickActionMenuItem("Manage Document Security");
		docView.switchToFrame(1);
		docView.waitForNumberOfElementsToBeGreater(ManageDocumentSecurity.teamListLocator, 0);
		
		System.out.println( Logger.getLineNumber() + "'Manage Document Security' window opened...");
	}
	

	public static int getDocumentsCountForWFState(String workflowState){
		List<WebElement> wfStateList = docView.getElements(wfsSummaryRowLocator);
		
		for(WebElement wfState : wfStateList){
			List<WebElement> wfs = wfState.findElements(By.tagName("td"));
			if(wfs.get(0).getText().equals(workflowState)){
				return Integer.parseInt(wfs.get(1).getText().trim().replaceAll("\\D+", ""));
			}
		}
		
		return Integer.MIN_VALUE;
	}
	
	public static void clickTags(){
		List<WebElement> girdTopPagerLeftMenuItems = Driver.getDriver().findElements(By.cssSelector("#grid_toppager_left > table > tbody > tr > td"));
		System.err.println("Size: " + girdTopPagerLeftMenuItems.size());
		for(WebElement menuItem : girdTopPagerLeftMenuItems){
			System.err.println(menuItem.getAttribute("title")+"****");
			if(menuItem.getAttribute("title").trim().equals("Tags")){
				menuItem.findElement(By.cssSelector("div > span")).click();
				/*Wait<WebDriver> wait = new FluentWait<>(driver)
										.withTimeout(120, TimeUnit.SECONDS)
										.pollingEvery(2, TimeUnit.SECONDS)
										.ignoring(NoSuchElementException.class);
				*/
				
				//wait.until(ExpectedConditions.textToBePresentInElement(By.cssSelector("#messageContainer > span"), "Loading tags..."));
				
				docView.waitFor(Driver.getDriver(), 20);
				System.out.println( Logger.getLineNumber() + "Tags menuItem clicked...");
				break;
			}
		}
	} 
	
	public static void clickOnUIButtonText(String buttonName){
		List<WebElement> list = docView.getElements(By.cssSelector("span[class='ui-button-text']"));
		for(WebElement btn : list){
			if(btn.getText().trim().equals(buttonName)){
				btn.click();
				System.out.println( Logger.getLineNumber() + "Create button for Tag clicked...");
				break;
			}
		}
	}
	
	public void selectDocuments(int num){
		SearchPage.waitForElement("input[class='cbox']", "css-selector");
		
		waitFor(8);
		
		List<WebElement> checkBoxes= docView.getElements(By.cssSelector("input[class='cbox']"));
		Actions actions = new Actions(driver);
		
		for(int i=1;i<=num;i++){
			actions.moveToElement(checkBoxes.get(i)).click().build().perform();
			waitFor(1);
		}
	}
	
	
   public void selectAllDocuments(WebDriver driver, SearchPage sp){
		//Wait for checkBoxes to become visible
	 	SearchPage.waitForElement("input[class='cbox']", "css-selector");
	   
	 	waitFor(8);
	 	 
		List<WebElement> checkBoxes= docView.getElements(By.cssSelector("input[class='cbox']"));
		click(checkBoxes.get(0));
		
	}//end function
	
   public static void checkDocumentBySpecificeNumber(int pos){
	   List<WebElement> checkboxList = docView.getElements(By.cssSelector(".cbox[id^='jqg_grid_']"));
	   
	   checkboxList.get(pos - 1).click();
   }
   
   public static void selectAllDocuments(){
	   if(docView.isElementChecked(selectAllDocumentLocator) == false)
		   docView.tryClick(selectAllDocumentLocator, 1);
	 	 
	   System.out.println( Logger.getLineNumber() + "All document selected on GridView...");
	}
   
   public void selectView(WebDriver driver, SearchPage sp, String viewName){
	   SearchPage.waitForElement(driver, "span[class=ui-button-text]", "css-selector");
	   
	   waitFor(driver, 15);
	   
	   List<WebElement> ViewPanelItems= docView.getElements(By.cssSelector("span[class=ui-button-text]"));
		if(ViewPanelItems.size()==0){
			  System.out.println( Logger.getLineNumber() + "List of nodes having" +  viewName + " not found");
		}
		
		for (int j=0;j<ViewPanelItems.size();j++){
			System.out.println( Logger.getLineNumber() + ViewPanelItems.get(j).getText());
			
			if(ViewPanelItems.get(j).getText().equalsIgnoreCase(viewName)){
				ViewPanelItems.get(j).click();
				break;
			}
		}
	   
		waitFor(15);
	}
	
   public static void selectView(String viewPanelLinkToClick){
		String cssForLink = "#review"+viewPanelLinkToClick+" > a > span[class=ui-button-text]";
		docView.switchToDefaultFrame();
		docView.waitForFrameToLoad(Frame.MAIN_FRAME);
		if(docView.getAttribute((By.cssSelector("#review"+viewPanelLinkToClick)), "class").contains("active") == false){
			new DocView().waitForPageLoad();
			new DocView().waitFor(40);
			docView.tryClick(By.cssSelector(cssForLink));
			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" clicked");
			(new DocView()).waitForPageLoad();
		}else{
			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" already selected...");
		}
		
		docView.waitForFrameToLoad(Frame.VIEWER_FRAME);
}
   
 public static void selectView(WebDriver driver, String viewPanelLinkToClick){
		String cssForLink = "#review"+viewPanelLinkToClick+" > a > span[class=ui-button-text]";
		//System.out.println( Logger.getLineNumber() + "\nSpan ***: "+docView.getElement(By.cssSelector("#review"+viewPanelLinkToClick)).getAttribute("class"));
		docView.switchToDefaultFrame();
		docView.waitForFrameToLoad(Frame.MAIN_FRAME);
		if(docView.getElement(By.cssSelector("#review"+viewPanelLinkToClick)).getAttribute("class").contains("active") == false){
			new DocView().waitForPageLoad(driver);
			new DocView().waitFor(40);
			docView.getElement(By.cssSelector(cssForLink)).click();
			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" clicked");
			//waitForAttributeIn(By.cssSelector("#review"+viewPanelLinkToClick), "class", "active");
			//waitForFrameToLoad("VIEWER");
			(new DocView()).waitForPageLoad(driver);
			//docView.waitForFrameToLoad(Frame.VIEWER_FRAME);
		}else{
			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" already selected...");
		}
		
		docView.waitForFrameToLoad(Frame.VIEWER_FRAME);
 }
 
 
 public static void selectViewByLink(String viewPanelLinkToClick){
	 	List<WebElement> bottomMenuItems = docView.getElements(By.cssSelector("#menu > li > a > span"));
	 	
	 	for(WebElement item : bottomMenuItems){
	 		if(docView.getText(item).equals(viewPanelLinkToClick)){
	 			item.click();
	 			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" clicked...");
	 			(new DocView()).waitForPageLoad();
	 			/*docView.waitForVisibilityOf(loadingMsgLocator);
	 			docView.waitForInVisibilityOf(loadingMsgLocator);*/
	 			docView.waitFor(30);
	 		}
	 	}
	 	
	 	docView.waitForFrameToLoad(Frame.VIEWER_FRAME);
	 	
	 	System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" view selected...");
	}
 
   public static void selectViewByLink(WebDriver driver, String viewPanelLinkToClick){
	 	List<WebElement> bottomMenuItems = docView.getElements(By.cssSelector("#menu > li > a > span"));
	 	
	 	for(WebElement item : bottomMenuItems){
	 		if(item.getText().equals(viewPanelLinkToClick)){
	 			item.click();
	 			System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" clicked...");
	 			(new DocView()).waitForPageLoad(driver);
	 			/*docView.waitForVisibilityOf(loadingMsgLocator);
	 			docView.waitForInVisibilityOf(loadingMsgLocator);*/
	 			docView.waitFor(30);
	 		}
	 	}
	 	
	 	docView.waitForFrameToLoad(Frame.VIEWER_FRAME);
	 	
	 	System.out.println( Logger.getLineNumber() + "\n"+viewPanelLinkToClick+" view selected...");
	}
   
   
    
   public static void searchTextInDocument(String text){
	   docView.editText(By.id("textSearchCriteria"), text + "\n");
		
	   docView.waitFor(5);
   }
   
   
   public static int getNumberOfMatches(){
	   String matchCountString= SearchPage.waitForElement("labelStatus", "id").getText();
	   String[] matchCountStringSplit= matchCountString.split("\\s+");
	   String matchCount= matchCountStringSplit[2];
	   return Integer.parseInt(matchCount);
   }
   
   
   public static void findNextTextMatch(){
	   docView.tryClick(By.cssSelector("button[title='Find next occurrence in page']"));
	   docView.waitFor(2);
   }
   
   
  public static void findPreviousTextMatch(){
	  docView.tryClick(By.cssSelector(("button[title='Find previous occurrence in page']")));
	  docView.waitFor(2);
   }
  
  
  public static void findPreviousHighlight(){
	  docView.tryClick(By.id("prevTerm"), 2);
  }
  
  public static void findNextHighlight(){
	  docView.tryClick(By.id("nextTerm"), 2);
  }
    
  public static void clickOnDocument(int docNum){
	  //By documentLocator = By.cssSelector("#grid > tbody > tr:nth-child(" + (docNum + 1) + ") > td:nth-child(3)");
	  By documentLocator = By.cssSelector("#grid > tbody > tr[id]:nth-child(" + (docNum + 1) + ") td:nth-child(3)");
  	  
	  docView.tryClick(documentLocator);
	  docView.waitForPageLoad();
	  docView.waitFor(10);
	  
	  System.out.println( Logger.getLineNumber() + "Document no. " + docNum + " openend...");
  }
  
  public static void clickOnDocumentByID(int docNum){
	  List<WebElement> docIDList = docView.getElements(documentIDColumnLocator);
	  
	  for(WebElement docID : docIDList){
		  if(docID.getText().trim().equals(docNum+"")){
			  docView.getParentOf(docID).findElement(By.tagName("td")).click();
			  break;
		  }
	  }
	  
	  docView.waitForPageLoad();
	  docView.waitFor(10);
	  
	  System.out.println( Logger.getLineNumber() + "Document no. " + docNum + " openend...");
  }
  
  
  public static void clickOnDocument(WebDriver driver, int docNum){
	  	  By documentLocator = By.cssSelector("#grid > tbody > tr:nth-child(" + (docNum + 1) + ") > td:nth-child(3)");
	  	  
		  docView.tryClick(documentLocator);
		  
		  docView.waitForPageLoad(driver);
		  docView.waitFor(10);
		  
		  System.out.println( Logger.getLineNumber() + "Document no. " + docNum + " openend...");
	  }
  
  public static void clickOnDocument(WebDriver driver, int docNum, int waitTime){
	  docView.getElement(By.cssSelector("#grid > tbody > tr:nth-child(" + (docNum + 1) + ") > td:nth-child(3)")).click();
	  
	  docView.waitFor(waitTime);
  }
  
   
  public void ImagePreviousPage(){
	  click(docView.getElement(By.id("prev_page")));
  }
  
  public void ImageNextPage(){
	  click(docView.getElement(By.id("next_page")));
	  waitFor(3);
  }
  
  public void ImageFirstPage(){
	  click(docView.getElement(By.id("first_page")));
  }
  
  public void ImageLastPage(){
	  click(docView.getElement(By.id("last_page")));
  } 
  
  public static String getPageBoxValue(){
	  List<WebElement> currentPageLocator = docView.getElements(By.tagName("input")) ;
	  
	  for(WebElement currentPage : currentPageLocator){
		  System.err.println(currentPage.getAttribute("id")+"*****");
		  if(currentPage.getAttribute("id").equals("pagepick")){
			  return currentPage.getAttribute("value");
		  }
	  }
	  
	  return "";
  }
  
  public String getPageBoxValue(WebDriver driver, SearchPage sp){
	  List<WebElement> currentPageLocator = docView.getElements(By.tagName("input")) ;
	  
	  for(WebElement currentPage : currentPageLocator){
		  System.err.println(currentPage.getAttribute("id")+"*****");
		  if(currentPage.getAttribute("id").equals("pagepick")){
			  return currentPage.getAttribute("value");
		  }
	  }
	  
	  //String value= docView.getElement(By.id("pagepick")).getAttribute("value");
	  return "";
  }
  
  public void zoomImageSlideBar(){
	  Actions action = new Actions(Driver.getDriver());
	  action.moveToElement(docView.getElement(By.id("zoom")));
	  waitFor(2);
	  WebElement slider= SearchPage.waitForElement("span[class*='ui-slider-handle']", "css-selector");
	  action.click(slider).build().perform();
	  waitFor(2);
	  for (int i = 0; i < 10; i++) {
	      action.sendKeys(Keys.ARROW_RIGHT).build().perform();
	      waitFor(1);
	  }
  }
  
  
  public static void waitWhileLoading(){
		 int maxLooping = 1;
		 docView.waitFor(1);  
		  
		try{  
			WebElement loadingMsg= docView.getElement(loadingMsgLocator);
		    while(loadingMsg.isDisplayed() && maxLooping++ <= 300){
		    	
		    }
		}catch(Exception e){
		}
	  }
  
  public static void waitWhileLoading(WebDriver driver){
	 int maxLooping = 1;
	 docView.waitFor(1);  
	  
	try{  
		WebElement loadingMsg= docView.getElement(loadingMsgLocator);
	    while(loadingMsg.isDisplayed() && maxLooping++ <= 300){
	    	
	    }
	   }catch(Exception e){
	   }
  }
  
  
 public static void clickUndockButton(){
	  docView.switchToDefaultFrame();
	  docView.waitForFrameToLoad(Frame.MAIN_FRAME);
	  docView.waitForVisibilityOf(By.id("btnDock"));
	  docView.getElement(By.id("btnDock")).click();
  }
  
  
  public void clickUndockButton(WebDriver driver, SearchPage sp){
	  
	  Driver.getDriver().switchTo().defaultContent();
	  //sp.waitForFrameLoad(Frame.MAIN_FRAME);
	  waitForFrameToLoad(Frame.MAIN_FRAME);
	  //click(SearchPage.waitForElement("btnDock", "id"));
	  waitForVisibilityOf(By.id("btnDock"));
	  docView.getElement(By.id("btnDock")).click();
  }
  
  
  
public void clickDockButtonOnUndockedWindow(WebDriver driver, SearchPage sp){
	  
	  click(SearchPage.waitForElement("btnDock", "id"));
	  
  }
  
  
  public void changeDocWorkFlowState(WebDriver driver, SearchPage sp){
	  
	  
	  
	  
	  
  }
  
  public int checkForError(WebDriver driver, SearchPage sp){
	  
	        int errorPresent=0;
	  
			List<WebElement>dialog= docView.getElements(By.cssSelector("span[class='ui-dialog-title']"));
			for(int i=0;i<dialog.size();i++){
				
				if(dialog.get(i).isDisplayed()){
					
					if(dialog.get(i).getText().equalsIgnoreCase("Error")){
						System.out.println( Logger.getLineNumber() + "Inside error block");
						errorPresent=1;
						
						
					}
					
				}
				
				
				
			}
			return errorPresent;
			
		
	  
  }
 
  
  public String[] getChildrenWindows(WebDriver driver, SearchPage sp){
	  
String windows[]= new String[10]; 
	  
	  //Get all the window handles in a set
      Set <String> handles =Driver.getDriver().getWindowHandles();
      Iterator<String> it = handles.iterator();
      //iterate through your windows
      
      int i = 0;
      while (it.hasNext()){
      
      windows[i]=it.next();
      i++;
      
      //Driver.getDriver().switchTo().window(windows[1]);
      }//end while
      
      return windows;
      
     
	  
	  
  }//end function
  
  
  public void clickCloseButton(WebDriver driver, SearchPage sp){
		
		
		List<WebElement> spanNodes= docView.getElements(By.cssSelector("span[class='ui-button-text']"));
		for(int i=0;i<spanNodes.size();i++){
			
			if(spanNodes.get(i).isDisplayed() && spanNodes.get(i).getText().equalsIgnoreCase("Close")){
				
				WebElement closeButton= SearchPage.getParent(spanNodes.get(i));
				click(closeButton);
				waitFor(5);
				break;
			}
		}
	}
  
  
public String checkViewMenuPermission(String menuName){
	  String permission="0"; //0 mean no permission and 1 mean has permission 
	  
	  switchToDefaultFrame();
	  waitForFrameToLoad(Frame.MAIN_FRAME);
	  
	  List<WebElement> viewMenuItems= docView.getElements(By.cssSelector("span[class='ui-button-text']"));
	  
	  for(int i=0;i<viewMenuItems.size();i++){
		  if(viewMenuItems.get(i).isDisplayed()){
			  if(viewMenuItems.get(i).getText().equalsIgnoreCase(menuName)){
				  WebElement parentNode= SearchPage.getParent(viewMenuItems.get(i));
				  if(parentNode.getAttribute("class").contains("ui-state-disabled")){
					  permission="0";
				  }else{
					 permission="1";
				  }
			  }
		  }
	  }

	  return permission;
  }
  
  
  
  public String checkViewMenuPermission(WebDriver driver, SearchPage sp, String menuName){
	  
	  //Check if user has permission to access particular view menu/tab
	  String permission="0"; //0 mean no permission and 1 mean has permission 
	  
	  Driver.getDriver().switchTo().defaultContent();
	  SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
	  
	  
	  List<WebElement> viewMenuItems= docView.getElements(By.cssSelector("span[class='ui-button-text']"));
	  
	  for(int i=0;i<viewMenuItems.size();i++){
		  
		  if(viewMenuItems.get(i).isDisplayed()){
			  
			  
			  if(viewMenuItems.get(i).getText().equalsIgnoreCase(menuName)){
				  
				  
				  WebElement parentNode= SearchPage.getParent(viewMenuItems.get(i));
				  if(parentNode.getAttribute("class").contains("ui-state-disabled")){
					  
					  permission="0";
					  
				  }
				  else{
					  
					  permission="1";
				  }
				  
			  }
			  
			  
		  }
		  
		  
	  }
	  
	  return permission;
	  
	  
  }
  
  

  public void turnKeywordHighlightingOnInTextView(){
		//clicking in highlight field
		click(SearchPage.waitForElement("toggleHighlightTerms", "id"));
				
		//finding checkbox for keywords
		WebElement allElements = docView.getElement(By.id("-2_anchor"));
		WebElement boxElement = allElements.findElement(By.tagName("i"));
		if(!allElements.getAttribute("class").contains("clicked")){
			click(boxElement);		
		}
		//clicking again in highlight field
		click(SearchPage.waitForElement("toggleHighlightTerms", "id"));
		waitFor(15);
	  }
  
  
  public void scrollToPageNumber(String pageNumber){
	  List<WebElement> elements = docView.getElements(By.tagName("button"));
		WebElement element = null;
		for(WebElement e : elements){
			if(e.getText().equalsIgnoreCase(pageNumber)){
				element = e;
				break;
			}
		}
		((JavascriptExecutor) Driver.getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		waitFor(15);
		//element.sendKeys(pageNumber+ "\n");
	  
	/*  List<WebElement> currentPageLocator = docView.getElements(By.tagName("input")) ;
	  for(WebElement currentPage : currentPageLocator){
		  System.err.println(currentPage.getAttribute("id")+"*****");
		  if(currentPage.getAttribute("id").equals("pagepick")){
			  currentPage.sendKeys(pageNumber+"\n");
			  docView.waitFor(5);
			  break;
		  }
	  }*/
	  
	  System.out.println( Logger.getLineNumber() + "Page scrolling performed...");
  }
  
  
  public void switchToViewer(){
	  	Driver.getDriver().switchTo().defaultContent();
		//SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		SearchPage.waitForFrameLoad(Frame.VIEWER_FRAME);
  }
  
  
  public void clickCompleteDocFamily(){
	  	driver = Driver.getDriver();
	  	Driver.getDriver().switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		WebElement elementToClick = null;
		try{
			elementToClick = docView.getElement(By.cssSelector("span[class='ui-icon ui-icon-circle-minus']"));
		}catch(NoSuchElementException e){
			elementToClick = docView.getElement(By.cssSelector("span[class='ui-icon ui-icon-circle-plus']"));
		}
	  	click(elementToClick);
	  	waitFor(15);
  }
  
  
  public static void clickBackToSearchResult(){
	  	docView.switchToDefaultFrame();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		docView.tryClick(By.id("prevDataset"));
		docView.waitForAttributeIn(ReviewTemplatePane.postBtnLocator, "class", "ui-button-disabled");
		docView.waitForAttributeEqualsIn(ReviewTemplatePane.postBtnLocator, "class", "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn-glow");
		docView.waitFor(10);
		System.out.println( Logger.getLineNumber() + "Back to search result page...");
  }
  
  public void clickPostButton(){
	  	Driver.getDriver().switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
	  	/*WebElement elementToClick = docView.getElement(By.id("btnDocDone"));
	  	elementToClick.click();*/
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("document.getElementById(\"btnDocDone\").click()");
	  	waitFor(15);
  }
  
  public void imageMenuClicker(String menuName){
	  WebElement imageElement = SearchPage.waitForElement("content", "id");
	  int viewWindowHeight = Integer.valueOf(docView.getElement(By.id("content")).getCssValue("height").replaceAll("\\D+", ""));
	  Actions actions = new Actions(Driver.getDriver());
	  actions.moveToElement(imageElement, 100, viewWindowHeight-10).contextClick().build().perform();
	  System.out.println( Logger.getLineNumber() + "Right click performed at (100, " + (viewWindowHeight-10) +")...");
	  //new Actions(driver).contextClick(imageElement).perform();
	  docView.waitFor(10);
	  
	  //List<WebElement> allMenuElements = docView.getElements(By.cssSelector("li.context-menu-item > span"));
	  List<WebElement> allMenuElements = docView.getElements(By.cssSelector("li > span"));
	  
	  for(WebElement e : allMenuElements){
		  System.err.println(e.getText() + "&&&&&&&&&");
		  if(e.getText().trim().equalsIgnoreCase(menuName)){
			  click(e);
			  docView.waitFor(5);
			  System.out.println( Logger.getLineNumber() + menuName + " menu Item clicked...");
			  return;
		  }
	  }
	  
	  System.out.println( Logger.getLineNumber() + "Could not click Menu Item.\nError at DocView:486");
  }    
  
  
  public void clickRedAct(){
	  List<WebElement> buttons= docView.getElements(By.cssSelector("span[class='ui-button-text']"));
	  
	  for(int i=0;i<buttons.size();i++){
			if(buttons.get(i).isDisplayed()){
				if(buttons.get(i).getText().equalsIgnoreCase("Redact")){
					WebElement closeButton= SearchPage.getParent(buttons.get(i));
					click(closeButton);
					waitFor(5);
				}
			}
		}
  }
  
  
  public String getDocumentId(){
	  return Driver.getDriver().findElement(By.id("menuDocId")).getText().replaceAll("\\D+", "");
  }
  
  
  //This method is used to draw Redacts on the image view of a doc
  public void performReadact(){
	    WebElement ImageWrapper= SearchPage.waitForElement("div[class='ImageWrapper ui-selectable']", "css-selector");
		clickRedAct();
		
		Utils.waitLong();

		Actions actions = new Actions(driver); 
		actions.dragAndDropBy(ImageWrapper,100, 100).perform(); 
		
		Utils.waitLong();
  }
  
  
  public static void openHighlightWindow(){
	  docView.tryClick(highlightBtn);
  }
  
  public void clickHighlightButton(){
	  click(SearchPage.waitForElement("toggleHighlightTerms", "id"));
	  waitFor(Driver.getDriver(), 15);
  }
  
  public static void sortByColumn(String columnName){
	  driver = Driver.getDriver();
	  List<WebElement> columnHeaders= docView.getElements(By.cssSelector("div[id*='jqgh_grid_']"));
	  for(int i=0;i<columnHeaders.size();i++){
		  if(columnHeaders.get(i).getText().equals(columnName)){
			  docView.click(columnHeaders.get(i));
			  waitWhileLoading(driver);
			  break;
		  }
	  }
  }
  
  
  public void clickCrossButton(){
	  driver = Driver.getDriver();
	  Driver.getDriver().switchTo().defaultContent();
	  List<WebElement> allButtons = docView.getElements(By.tagName("button"));
	  for(WebElement e : allButtons){
		  
		 if(e.isDisplayed()){ 
		  
		  if(e.getAttribute("title").equalsIgnoreCase("Close")){
			  click(e);
			  Utils.waitLong();
			  break;
		  }
		  
		 }
	  }
	  
	  //Utils.waitShort();
  }
  
  public static void clickCrossBtn(){
	  driver = Driver.getDriver();
	  Driver.getDriver().switchTo().defaultContent();
	  List<WebElement> allButtons = docView.getElements(By.tagName("button"));
	  for(WebElement e : allButtons){
		 if(e.isDisplayed()){ 
		  if(e.getAttribute("title").equalsIgnoreCase("Close")){
			  e.click();
			  System.out.println( Logger.getLineNumber() + "Window closed...");
			  docView.waitFor(10);
			  break;
		  }
		 }
	  }
  }
  
  
  public static void closeFindAnnotationWindow(){
	  List<WebElement> allButtons = docView.getElements(By.tagName("button"));
	  for(WebElement e : allButtons){
		 if(e.isDisplayed()){ 
		  if(e.getAttribute("title").equalsIgnoreCase("Close")){
			  e.click();
			  System.out.println( Logger.getLineNumber() + "Window closed...");
			  break;
		  }
		 }
	  }
  }
  
  
  public void clickDownload(){
	  WebElement setWorkFLow = (new WebDriverWait(driver, 40)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span[class*='menu-icomoon']")));

	  //Set Search Workflow
	  setWorkFLow.click();
	  click(Utils.waitForElement(driver , "transfer", "id"));
	  Utils.waitShort();
	  //Driver.getDriver().switchTo().frame(docView.getElement(By.cssSelector("iframe[src='https://recenseotest.icontrolesi.com/QuickReviewWeb/download_manager.jsp']")));
	  Utils.setCurrentURL();
	  String url= "iframe[src='" + Utils.getCurrentURL() + "download_manager.jsp']";
	  System.out.println( Logger.getLineNumber() + "String in selector: "+url);
	  Driver.getDriver().switchTo().frame(docView.getElement(By.cssSelector(url)));
  }
  
  
  public void clickShareOnDownloadFrame(){
	  WebElement divElement = Utils.waitForElement(driver, "viewPref", "id");
	  List<WebElement> allButtons = divElement.findElements(By.tagName("button"));
	  click(allButtons.get(1));
  }
  
 
  public void performAnnotate(){
	    WebElement ImageWrapper= SearchPage.waitForElement("div[class='ImageWrapper ui-selectable']", "css-selector");
		Utils.waitLong();

		Actions actions = new Actions(driver); 
		actions.dragAndDropBy(ImageWrapper,100, 100).perform(); 
		
		Utils.waitLong();
	  }
  
  public void clickSearchOnTop(){
		Driver.getDriver().switchTo().defaultContent();
		click(Utils.waitForElement(Driver.getDriver(), "search", "id"));
	}
	
	
	public void clickAnnotate(){
		List<WebElement> allElements = docView.getElements(By.cssSelector("ui-button-text"));
		for(WebElement e : allElements){
			if(e.getText().equals("Annotate")){
				click(e);
				break;
			}
		}
	}
	
	
	
	
	public void clickSubmitButton(){
	 // click(SearchPage.waitForElement(Driver.getDriver(), "input[value='Submit'][onclick='validate();'][type='button']", "css-selector"));
		docView.tryClick(By.cssSelector("input[value='Submit'][onclick='validate();'][type='button']"), By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"));
	}
	
	
	public static void addDocsToWorkflow(String workflowName){
		if(selecteAddOptionFor(workflowName)){
			clickNextBtnToAddOrRemoveDocsToWF();
			System.out.println( Logger.getLineNumber() + "Documents Added to '" + workflowName + "' ...");
		}else{
			System.out.println( Logger.getLineNumber() + "Radio button not enabled for workflow: " + workflowName + "...");
			System.out.println( Logger.getLineNumber() + "Documents adding is cancelled...");
		}
	}
	
	
	public static boolean selecteAddOptionFor(String workflowName){
		List<WebElement> workflowList = docView.getElements(wfsSummaryRowLocator);
		
		for(WebElement workflow : workflowList){
			if(docView.getText(workflow.findElement(By.tagName("td"))).equals(workflowName)){
				WebElement radioBtnForToBeAddedWF = workflow.findElement(By.cssSelector("td > input"));
				if(radioBtnForToBeAddedWF.isEnabled()){
					radioBtnForToBeAddedWF.click();
					return true;
				}
			}
		}
		
		return false;
	}
	

	public static void removeDocsFromeWorkflow(String workflowName){
		if(selecteRemoveOptionFor(workflowName)){
			clickNextBtnToAddOrRemoveDocsToWF();
			System.out.println( Logger.getLineNumber() + "Documents Removed from '" + workflowName + "' ...");
		}else{
			System.out.println( Logger.getLineNumber() + "Radio button not enabled for workflow: " + workflowName + "...");
			System.out.println( Logger.getLineNumber() + "Documents removing is cancelled...");
		}
	}
	
	
	public static boolean selecteRemoveOptionFor(String workflowName){
		List<WebElement> workflowList = docView.getElements(wfsSummaryRowLocator);
		
		for(WebElement workflow : workflowList){
			if(docView.getText(workflow.findElement(By.tagName("td"))).equals(workflowName)){
				System.out.println( Logger.getLineNumber() + "Workflow matched!!");
				WebElement radioBtnForToBeRemovedWF = workflow.findElement(By.cssSelector("td:nth-child(4) input"));
				if(radioBtnForToBeRemovedWF.isEnabled()){
					radioBtnForToBeRemovedWF.click();
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	public static int getDocumentCountForWFS(String workflowName){
		List<WebElement> workflowList = docView.getElements(wfsSummaryRowLocator);
		
		for(WebElement workflow : workflowList){
			if(docView.getText(workflow.findElement(By.tagName("td"))).equals(workflowName)){
				System.out.println( Logger.getLineNumber() + "Workflow matched!!");
				WebElement documentCount = workflow.findElement(By.cssSelector("td:nth-child(2)"));
				
				return Integer.parseInt(docView.getText(documentCount));
			}
		}
		
		return 0;
	}
	
	public static void clickNextBtnToAddOrRemoveDocsToWF(){
		docView.tryClick(nextBtnInWFS);
		docView.tryClick(goBtnInWFS, goBackSummaryBtnLocator);
	
	}
	
	
	public static int getDocmentId(){
    	return Integer.parseInt(docView.getText(documentId).replaceAll("\\D+", ""));
    }
	
    public static int getDocmentId(WebDriver driver){
    	return Integer.parseInt(docView.getElement(documentId).getText());
    }
    
    public static int getDocmentIdFromCodingTemplate(){
    	int id = Integer.MIN_VALUE;
    	List<WebElement> attributeListInCodingTemplate = docView.getElements(codingTemplateFieldLocator);
    	
    	for(WebElement attribute : attributeListInCodingTemplate){
    		String attributeString = attribute.getText().trim();
    		System.out.println( Logger.getLineNumber() + attributeString + "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
    		if(attributeString.startsWith("Document ID:")){
    			 return Integer.parseInt(attributeString.replaceAll("\\D+", ""));
    		}
    	}
    	
    	return id;
    	//return Integer.parseInt(attributeListInCodingTemplate.stream().filter(s -> s.getText().contains("Document ID: ")).findFirst().get().getText().replaceAll("\\D+", ""));
    }
    
    public static int getDocmentCount(){
    	return Integer.parseInt(docView.getText(totalDocsLocator).replaceAll("\\D+", ""));
    }
    
    public static int getDocmentCount(WebDriver driver){
    	return Integer.parseInt(docView.getText(totalDocsLocator).replaceAll("\\D+", ""));
    }
    
    
    public static void clickPageNavigationButton(String navButtonName){
    	  By navigateBtnLocator = By.cssSelector("#" + navButtonName + "_t_grid_toppager > span");
    	  //docView.waitForElementToBeClickable(navigateBtnLocator);
    	 //docView.waitForElementToBeClickable(navigateBtnLocator);
    	  docView.tryClick(navigateBtnLocator);
    	  waitForLoad();
    }
    
    public static void clickPageNavigationButton(WebDriver driver, String navButtonName){
  	  By navigateBtnLocator = By.cssSelector("#" + navButtonName + "_t_grid_toppager > span");
  	  docView.waitForElementToBeClickable(navigateBtnLocator);
  	 //docView.waitForElementToBeClickable(navigateBtnLocator);
  	  docView.getElement(navigateBtnLocator).click();
  	  waitForLoad();
  }
    
    
    public static String getComment(){
    	return docView.getText(commentInCodingTemplate);
    }
    
    public static void gotoPreviousPage(){
    	clickPageNavigationButton("prev");
    }
    
    public static void gotoNextPage(){
    	clickPageNavigationButton("next");
    }
    
    public static void gotoFirstPage(){
    	clickPageNavigationButton("first");
    }
    
    public static void gotoLastPage(){
    	clickPageNavigationButton("last");
    }
    
    public static void gotoPageNumber(int pageNumberToGo){
    	WebElement pageNumber= docView.getElement(By.cssSelector("#grid_toppager_center > table > tbody > tr > td:nth-child(4) > input"));
    	pageNumber.clear();
	    pageNumber.sendKeys(Integer.toString(pageNumberToGo));  
	    pageNumber.sendKeys(Keys.ENTER);
	    waitForLoad();
  }
  
    public static void gotoPageNumber(WebDriver driver, int pageNumberToGo){
    	WebElement pageNumber= docView.getElement(By.cssSelector("#grid_toppager_center > table > tbody > tr > td:nth-child(4) > input"));
    	pageNumber.clear();
	    pageNumber.sendKeys(Integer.toString(pageNumberToGo));  
	    pageNumber.sendKeys(Keys.ENTER);
	    waitForLoad();
  }
    
    public static int getCurrentPageNumber(){
    	By pageNumberLocator = By.cssSelector("#grid_toppager_center  input");
    	return Integer.parseInt(docView.getText(pageNumberLocator));
    }
    
    
    public static int getNumberOfDocumentsPerPage(){
    	return Integer.parseInt(docView.getSelectedItemFroDropdown(selectBoxForDocPerPage).replaceAll("\\D+", ""));
    }
    
    public static void setNumberOfDocumentPerPage(int numberOfDocuments){
    	docView.selectFromDrowdownByText(selectBoxForDocPerPage, Integer.toString(numberOfDocuments));
    	docView.waitFor(20);
    }
    
    public static void waitForLoad(){
    	if(Configuration.getConfig("selenium.browser").equals("ie11")){
    		docView.waitFor(30);
    	}else{
    		docView.waitForAttributeIn(loadingMsgLocator, "style", "display: block;");
    		docView.waitForAttributeIn(loadingMsgLocator, "style", "display: none;");
    	}
    }
    
    
    public static void openPreferenceWindow(){
    	/*if(Configuration.getConfig("selenium.browser").equals("chrome")){
    		Driver.getDriver().get("https://recenseotest.icontrolesi.com/QuickReviewWeb/mvc/preferences/preference");
    	}else{*/
    		docView.switchToDefaultFrame();
	    	SearchPage.clickUserMenu();
	    	//#secondaryMenu > li > ul
	    	List<WebElement> menuItems = docView.getElements(By.cssSelector("#secondaryMenu > li > ul > li"));
	    	
	    	for(WebElement menuItem : menuItems){
	    		if(menuItem.getText().trim().equals("Preferences")){
	    			menuItem.click();
	    			List<WebElement> frameList = docView.getElements(By.tagName("iframe"));
	    			WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
	    			
	    			for(WebElement frame : frameList){
	    				if(frame.getAttribute("src").endsWith("preference")){
	    					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
	    					break;
	    				}
	    			}
	    			
	    			docView.waitForVisibilityOf(By.id("docPrefView"));
	    			
	    			break;
	    		}
	    	}
    	
     	System.out.println( Logger.getLineNumber() + "Preferences window appeared...");
    }
    
    public static void switchPreferenceTab(String tabName){
    	By tabLocation = By.cssSelector("#preferences a");
    	List<WebElement> tabs = docView.getElements(tabLocation);
    	
    	for(WebElement tab : tabs){
    		if(tab.getText().trim().equals(tabName)){
    			tab.click();
    			if(tab.getText().trim().equals("File Type")){
    				docView.waitFor(40);
    			}else{
    				docView.waitFor(5);
    			}
    			System.out.println( Logger.getLineNumber() + "Preference Tab switched to " + tabName + "...");
    			break;
    		}
    	}
    } 
    
    
    public static boolean isBatesArchiveOptionChecked(){
    	return docView.isElementChecked(By.name("BATES_ARCHIVE"));
    }
    
    public static void checkBatesArchiveOption(){
    	if(isBatesArchiveOptionChecked()){
    		System.out.println( Logger.getLineNumber() + "Bates archive option is already checked in Preference window...");
    	}else{
    		docView.tryClick(By.name("BATES_ARCHIVE"), 2);
    		System.out.println( Logger.getLineNumber() + "Bates archive option is checked in Preference window...");
    	}
    }
    
    
    public static void unCheckBatesArchiveOption(){
    	if(isBatesArchiveOptionChecked()){
    		docView.tryClick(By.name("BATES_ARCHIVE"), 2);
    		System.out.println( Logger.getLineNumber() + "Bates archive option is un-checked in Preference window...");
    	}else{
    		System.out.println( Logger.getLineNumber() + "Bates archive option is already un-checked in Preference window...");    		
    	}
    }
    
    public static void checkAllColumns(){
    	docView.waitForVisibilityOf(checkAllCheckBox);
    	if(docView.isElementChecked(checkAllCheckBox) == false){
    		docView.tryClick(checkAllCheckBox);
    	}
    	
    	System.out.println( Logger.getLineNumber() + "Check/Unceck All clicked. All columns selected...");
    }
    
    public static void checkSelectedColumns(String ... columns){
    	deselectAllDocument();
    	List<WebElement> columnsList = docView.getElements(By.cssSelector("#columns > li > label"));
    	for(String column : columns){
    		for(WebElement columnInList : columnsList){
    			//System.out.println( Logger.getLineNumber() + columnInList.getText()+"**");
    			if(columnInList.getText().trim().equals(column) && columnInList.findElement(By.tagName("input")).isSelected() == false){
    				columnInList.click();
    				System.out.println( Logger.getLineNumber() + "***" + column + " selected. ***");
    			}
    		}
    	}
    }
    
    public static void enableExpandPickList(){
    	WebElement pickListCheckbox = docView.getElement(pickListCheckboxLocator);
    	if(pickListCheckbox.isSelected() == false){
    		pickListCheckbox.click();
    		docView.waitForVisibilityOf(pickListUpdateMsg);
    		docView.waitForInVisibilityOf(pickListUpdateMsg);
    		System.out.println( Logger.getLineNumber() + "Expanding picklist enabled...");
    	}
    }
    
    public static void disableExpandPickList(){
    	WebElement pickListCheckbox = docView.getElement(pickListCheckboxLocator);
    	if(pickListCheckbox.isSelected()){
    		pickListCheckbox.click();
    		docView.waitForVisibilityOf(pickListUpdateMsg);
    		docView.waitForInVisibilityOf(pickListUpdateMsg);
    		System.out.println( Logger.getLineNumber() + "Expanding picklist disabled...");
    	}
    }
    
    
    public static boolean isColumnSelectedInPreference(String columnName){
    	List<WebElement> columnLabels = docView.getElements(By.cssSelector("#columns li label"));
    	
    	for(WebElement columnLabel : columnLabels){
    		if(docView.getText(columnLabel).equals(columnName)){
    			return columnLabel.findElement(By.tagName("input")).isSelected();
    		}
    	}
    	
    	return false;
    }
    
    public static void closePreferenceWindow(){
    	clickCrossBtn();
    	System.out.println( Logger.getLineNumber() + "Preference window closed...");
    }
    
    
    public static int getNumberOfSelectedDocuments(){
    	return Integer.parseInt(Driver.getDriver().findElement(By.id("selectedDocCount")).getText());
    }
    
    public static int getCurrentDocumentNumber(){
    	return Integer.valueOf(docView.getText(selectDocument).replaceAll("\\D+", ""));
    }
    
    
    public static void deselectAllDocument(){
    	if(isAllDocumentSelected()){
    		docView.getElement(checkAllCheckBox).click();
    	}else{
    		docView.getElement(checkAllCheckBox).click(); // first select
    		docView.getElement(checkAllCheckBox).click(); // now deselect
    	}
    }
    
    public static boolean isAllDocumentSelected(){
    	return docView.getElement(checkAllCheckBox).isSelected();
    }
    
    public static void selectDocumentByID(String documentID){
    	List<WebElement> documentList = Driver.getDriver().findElements(documentIDColumnLocator);
    	for(int i = 0; i < documentList.size(); i++){
    		if(documentList.get(i).getText().trim().equals(documentID)){
    			Driver.getDriver().findElement(By.cssSelector("#grid > tbody > tr:nth-child(" + (i + 2) + ") > td:nth-child(2)")).click();
    			System.out.println( Logger.getLineNumber() + "Document with ID " + documentID + " selected...");
    			break;
    		}
    	}
    }   
    
    
    public static void selectDocumentByID(int documentID){
    	List<WebElement> documentList = Driver.getDriver().findElements(documentIDColumnLocator);
    	for(int i = 0; i < documentList.size(); i++){
    		if(documentList.get(i).getText().trim().equals(documentID+"")){
    			//Driver.getDriver().findElement(By.cssSelector("#grid > tbody > tr:nth-child(" + (i + 2) + ") > td:nth-child(2)")).click();
    			docView.getElements(documentCheckboxColumnLocator).get(i).click();
    			System.out.println( Logger.getLineNumber() + "Document with ID " + documentID + " selected...");
    			break;
    		}
    	}
    }   
    
    public static void selectPageLayout(String layOut){
    	List<WebElement> layouts = Driver.getDriver().findElements(By.cssSelector("#viewPage > li"));
    	
    	for(WebElement l : layouts){
    		if(l.getText().trim().equals(layOut)){
    			l.findElement(By.name("viewName")).click();
    			System.out.println( Logger.getLineNumber() + layOut + " view selected...");
    			docView.waitFor(5);
    			break;
    		}
    	}
    }
    
    
    public static void selectDocumentsWitIDs(String ... ids){
    	for(String id : ids){
    		selectDocumentByID(id);
    	}
    }
    
    public static boolean isDocumentIDFound(String documentID){
    	List<WebElement> documentList = Driver.getDriver().findElements(documentIDColumnLocator);
    	for(WebElement docID : documentList){
    		if(docID.getText().trim().equals(documentID)){
    			return true;
    		}
    	}
    	return false;
    }
    
    
    public static boolean isColumnContains(By columnLocator, String valueToCheckFor){
    	valueToCheckFor = valueToCheckFor.toLowerCase();
    	List<String> columnValueList = docView.getListOfItemsFrom(columnLocator, v -> docView.getText(v).toLowerCase());
    	int i = 1;
    	for(String columnValue : columnValueList){
    		if(!columnValue.contains(valueToCheckFor)){
    			System.out.println( Logger.getLineNumber() + "Stopped at: " + i);
    			return false;
    		}
    		
    		i++;
    	}
    	
    	return true;
    }
    
    public static boolean isDocumentIDFound(int documentID){
    	List<WebElement> documentList = Driver.getDriver().findElements(documentIDColumnLocator);
    	for(WebElement docID : documentList){
    		System.out.println( Logger.getLineNumber() + "DocID: " + docID.getText() + "**********");
    		if(docID.getText().trim().equals(documentID+"")){
    			return true;
    		}
    	}
    	return false;
    }
    
	public static void clickOnCompleteDocFamily(String navItemName){
		int documentCount = getDocmentCount();
		List<WebElement> navItems = docView.getElements(By.cssSelector("#grid_toppager_left > table > tbody > tr > td"));
		
		for(WebElement element : navItems){
			if(element.getAttribute("title").equals(navItemName)){
				element.click();
				break;
			}
		}
		
		
		if(documentCount >= 5000){
			docView.acceptAlert();
		}
		
		docView.waitForVisibilityOf(loadingMsgLocator);
		docView.waitForInVisibilityOf(loadingMsgLocator);
		docView.waitFor(5);
	} 

	public static void clickOnCompleteDocFamily(){
		List<WebElement> navItems = docView.getElements(By.cssSelector("#grid_toppager_left > table > tbody > tr > td"));
		
		for(WebElement element : navItems){
			String linkText = element.getAttribute("title").trim();
			if(linkText.equals("Complete Doc Family") || linkText.equals("Exclude Added Family")){
				element.click();
				break;
			}
		}
		
		docView.waitForVisibilityOf(loadingMsgLocator);
		docView.waitForInVisibilityOf(loadingMsgLocator);
		docView.waitFor(5);
	} 
	
	public static void clickOnCompleteDocFamily(WebDriver driver){
		List<WebElement> navItems = docView.getElements(By.cssSelector("#grid_toppager_left > table > tbody > tr > td"));
		
		for(WebElement element : navItems){
			String linkText = element.getAttribute("title").trim();
			if(linkText.equals("Complete Doc Family") || linkText.equals("Exclude Added Family")){
				element.click();
				break;
			}
		}
		
		docView.waitForVisibilityOf(loadingMsgLocator);
		docView.waitForInVisibilityOf(loadingMsgLocator);
		docView.waitFor(5);
	} 

	public static void sortSearchResultBy(By element){
		docView.tryClick(element);
		/*docView.waitForVisibilityOf(Driver.getDriver(), loadingMsgLocator);
		docView.waitForInVisibilityOf(Driver.getDriver(), loadingMsgLocator);*/
	}
	
	public static void clickNextDocumentBtn(){
		docView.tryClick(nextDocument);
	}
	
	public static void clickPreviousDocumentBtn(){
		docView.tryClick(previousDocument);
	}
	
	public static void clickFirstDocumentBtn(){
		docView.tryClick(firstDocument);
	}
	
	public static void clickLastDocumentBtn(){
		docView.tryClick(lastDocument);
	}
	
	public static void selectDocumentBySpecificNumber(String documentNumber){
		docView.editText(selectDocument, documentNumber);
		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.ENTER).perform();
		docView.waitFor(1);
	}
	
	public static void selectDocumentBySpecificNumber(int documentNumber){
		WebElement documentNumberBox = docView.getElement(selectDocument);
		
		Actions actions = new Actions(Driver.getDriver());
		
		actions.sendKeys(documentNumberBox, Keys.chord(Keys.CONTROL, "a")).build().perform();
		actions.sendKeys(documentNumberBox, Keys.chord(Keys.CONTROL, "a"), documentNumber+"").build().perform();
		//actions.sendKeys(Keys.ENTER).perform();
		actions.moveToElement(documentNumberBox).sendKeys(Keys.ENTER).build().perform();
		
		docView.waitFor(10);
	}
	
	public static void gotoNextDocument(){
		clickNextDocumentBtn();
	}
	
	public static void gotoPreviousDocument(){
		clickPreviousDocumentBtn();
	}

	public static void gotoFirstDocument(){
		clickFirstDocumentBtn();
	}

	public static void gotoLastDocument(){
		clickLastDocumentBtn();
		docView.waitFor(10);
	}
	
	public static void openQueryInfo(){
		By queryInfoBtn = By.cssSelector("#grid_toppager_left > table > tbody > tr > td[title='Query Info']");
		docView.tryClick(queryInfoBtn);
		docView.waitFor(1);
	}
	
	public static void closeQueryInfo(){
		clickCrossBtn();
		docView.waitForFrameToLoad(Frame.MAIN_FRAME);
	}
	
	
	public static class GlobalEdit{
		
		public static void open(){
			DocView.openGlobalEditWindow();
		}
		
		public static void close(){
		 	  List<WebElement> allButtons = docView.getElements(By.tagName("button"));
			  for(WebElement e : allButtons){
				 if(e.isDisplayed()){ 
				  if(e.getAttribute("title").equalsIgnoreCase("Close")){
					  e.click();
					  System.out.println( Logger.getLineNumber() + "Window closed...");
					  docView.waitFor(10);
					  break;
				  }
				 }
			  }
		}
		
		public static void addAttributesForGlobalEdit(String attributeName){
			docView.selectFromDrowdownByText(editFieldForAttributes, attributeName);
			System.out.println( Logger.getLineNumber() + "Attribute added for Global Edit ...");
		}
		
		public static void selectEditAction(String actionName){
			docView.selectFromDrowdownByText(editActionLocator, actionName);
			System.out.println( Logger.getLineNumber() + "Edit Action selected ...");
		}
		
		public static void clickNextButton(){
			docView.tryClick(nextBtnLocator, globalEditSummaryLocator);
			docView.waitFor(2);
			System.out.println( Logger.getLineNumber() + "Next button clicked...");
		}
		
		public static void clickConfirmButton(){
			docView.tryClick(confirmGlobalEditLocator, globalEditMsgHolderLocator);
			System.out.println( Logger.getLineNumber() + "Ok button clicked...");
			/*docView.waitForVisibilityOf(By.id("waitImage"));
			docView.waitForInVisibilityOf(By.id("waitImage"));*/
			docView.waitForVisibilityOf(globalEditMsgHolderLocator);
			System.out.println( Logger.getLineNumber() + "Global Edit performed...");
		}
	}
}
