package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;

import com.icontrolesi.automation.platform.util.Logger;

public class DomainsAccordion implements CommonActions{
	static WebDriver driver = Driver.getDriver(); 
	private static By domainLocation = By.id("domains");
	public static By addBtnLocator = By.cssSelector("button[title='Add domain(s) to search']");
	static DomainsAccordion domainAccordion = new DomainsAccordion(); 
	
	public static void open(){
		//domainAccordion.waitForInVisibilityOf(domainLocation);
		domainAccordion.tryClick(domainLocation);
		domainAccordion.waitForFrameToLoad(Frame.DOMAINS_FRAME);
		if(Configuration.getConfig("selenium.browser").equals("firefox")){
			domainAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("#domainList > li"), 0);
		}
		else
			domainAccordion.waitFor(30);
		
		//domainAccordion.waitFor(5);
		//driver.switchTo().defaultContent();
		
		System.out.println( Logger.getLineNumber() + "\nTag accordion expanded...\n");
	}
	
	
	public static void toggleSortByFrequency(){
		WebElement sortByFrequencyToggleButton = domainAccordion.getElement(By.id("sortByFreq")); 
		boolean isSortedByFrequency = sortByFrequencyToggleButton.isSelected();
		System.out.println( Logger.getLineNumber() + "Is Selected: " + isSortedByFrequency);
		sortByFrequencyToggleButton.click();
		domainAccordion.waitForVisibilityOf(By.id("messageContainer"));
		domainAccordion.waitForInVisibilityOf(By.id("messageContainer"));
		if(isSortedByFrequency)
			System.out.println( Logger.getLineNumber() + "'Sort by frequency' toggle button selected...");
		else
			System.out.println( Logger.getLineNumber() + "'Sort by frequency' toggle button de-selected...");
	}
	
	
	public static void selectFirstDomain(){
		domainAccordion.tryClick(By.name("chkDomain"), 1);
	}
	
	
	public static void addSelectedToSearchBuilder(){
		domainAccordion.tryClick(addBtnLocator, 1);
	}

}
