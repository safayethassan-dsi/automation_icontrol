package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class Download implements CommonActions{
	public static final By fileTypeSettingLocator = By.id("fTypeDialog");
    public static final By deleteBtnLocator = By.xpath("//span[text()='Delete']");
    public static final By shareBtnLocator = By.xpath("//span[text()='Share']");
    public static final By uncheckBtnLocator = By.xpath("//span[text()='Uncheck']");
    public static final By successMsgLocator = By.id("messageContainer");
    public static final By downloadableLinkLocator = By.className("BlueAnchor");
    public static final By fileTypePrefWindowLocator = By.id("fileTypePrefs");
    
    
    
    private static Download download =  new Download();
    
    public static void deleteSelectDownloads(){
    	//int totalLinkBeforeDelete = download.getTotalElementCount(downloadableLinkLocator);
    	download.tryClick(deleteBtnLocator);
    	download.acceptAlert(Driver.getDriver());
    	//int totalLinkAfterDelete = download.getTotalElementCount(downloadableLinkLocator);
    	download.waitForVisibilityOf(successMsgLocator);
    	
    	System.out.println( Logger.getLineNumber() + "Selected downloads deleted successfully...");
    }
	
    public static void shareSelectedDownloads(){
    	download.tryClick(shareBtnLocator);
    	download.acceptAlert();
    	download.waitForVisibilityOf(successMsgLocator);
    	System.out.println( Logger.getLineNumber() + "Selected downloads shared successfully...");
    }
    
    public static void uncheckSelectedDownloads(){
    	download.tryClick(uncheckBtnLocator);
    	System.out.println( Logger.getLineNumber() + "Selected downloads deselected successfully...");
    }
    
    public static boolean isDownloadExist(String downloadFileName){
    	boolean isFound = download.getElements(downloadableLinkLocator).stream()
    			.filter(d -> download.getText(d).equals(downloadFileName))
    			.findAny().isPresent();
    	
    	return isFound;
    }
    
	public static void selectAllCheckBox(int toCheck[]){
		
		//This method is intended to mark all checkboxes for whom the value of toCheck[i] is 1, if it is 0 then keep the checkbox unchecked
		//"Separator sheets" can only be checked if "Print All Docs in One PDF" is selected
		
		List<WebElement> checkBoxes= Driver.getDriver().findElements(By.cssSelector("input[type='checkbox']"));
		for(int i=0;i<(checkBoxes.size()-1);i++){
			
			if(toCheck[i]==1){
				
				 if(!checkBoxes.get(i).isSelected()){
					 
					 checkBoxes.get(i).click();
				 }
				
				
			}
			else{
				
				if(checkBoxes.get(i).isSelected()){
					 
					 checkBoxes.get(i).click();
				 }
				
				
				
			}//end else
			
		}
		
		
	}

	
	public void clickContinueButton(){
		
	    tryClick(By.id("submitJob"));

		
		
	}
	
	
	/*
	 * 	This method wait for a file to be downloaded with a maximum waiting period.
	 */
	public static void waitForFileDownloadToComplete(String fileName, int waitTime){
		int currentTime = 0;
		while(true){
			if(currentTime >= waitTime){
				System.err.println("File download not completed. Estimated time has over.");
				break;
			}
			File file = new File(fileName);
			File partFile = new File(fileName+".part");
			if(file.exists() && partFile.exists() == false){
				System.err.println(file.getName() + " downloaded completely...");
				break;
			}else{
				download.waitFor(5);
			}
			
			
			currentTime += 5;
		}
	}
	
	
	/*
	 * 	This method wait for a file to be downloaded with a maximum waiting period.
	 */
	public static void waitForFileDownloadToComplete(Pattern filePattern, int waitTime){
		int currentTime = 0;
		File filePath = new File(filePattern.toString());
		while(true){
			if(currentTime >= waitTime){
				System.err.println("File download not completed. Estimated time has over.");
				break;
			}
				File [] fileList = filePath.getParentFile().listFiles();
			
			for(File file : fileList)			{
				//if file pattern found and .part file removed, file is downloaded completely
			}
			
			
			
			
			currentTime += 5;
		}
	}
	
	
	 public static void closeDownloadWindow(){
		 download.switchToDefaultFrame();
		 download.waitForFrameToLoad(Frame.MAIN_FRAME);
		 List<WebElement> allButtons = download.getElements(By.tagName("button"));
		  for(WebElement e : allButtons){
			 if(e.isDisplayed()){ 
			  if(e.getAttribute("title").equalsIgnoreCase("Close")){
				  e.click();
				  System.out.println( Logger.getLineNumber() + "Window closed...");
				  download.waitFor(10);
				  break;
			  }
			 }
		  }
	  }	
	 
	 
	 public static void openFileTypePrefs(){
		download.tryClick(fileTypeSettingLocator);
		//Driver.getDriver().switchTo().frame(download.getElement(By.id("fileDownloadPrefFrame")));
		download.switchToFrame("src", "prepareDownload=true");
		download.waitForVisibilityOf(fileTypePrefWindowLocator);
		System.out.println( Logger.getLineNumber() + "File Type Preference opened from Actions Menu ...");
	}	
	
	public static void closeFileTypePrefs(){
		//download.switchToDefaultFrame();
		Driver.getDriver().switchTo().parentFrame();
			
		//download.tryClick(By.xpath("//span[text()='Close']"), fileTypeSettingLocator);
		download.getElements(By.xpath("//span[text()='Close']")).get(1).click();
		download.waitForVisibilityOf(fileTypeSettingLocator);
		//download.waitForInVisibilityOf(By.cssSelector("input[value='Submit'][onclick='validate();'][type='button']"));
		System.out.println( Logger.getLineNumber() + "File Type Preference closed from Actions Menu pane...");
	}
	
	
	public static void setDownloadPreferenceAllTo(String preferenceType){
		ReviewTemplatePane.setDownloadPreferenceAllTo(preferenceType);
	}
}
