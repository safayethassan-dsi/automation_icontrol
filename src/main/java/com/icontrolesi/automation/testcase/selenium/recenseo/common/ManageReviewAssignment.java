package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.util.AutomationDataCleaner;
import com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase.GridActionAddingDocs2;
import com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase.GridActionAddingDocs3;

import com.icontrolesi.automation.platform.util.Logger;

public class ManageReviewAssignment  implements CommonActions{
	public static int multipleAssignmentAlert=0;
	public static String alertString="";
	
	
	
	public static By assignmentNameLocator = By.name("assignmentName");
	public static By assignmentWorkflowLocator = By.name("assignmentWorkflow");
	public static By assigneeLocator = By.name("assignmentAssignee");
	public static By assignmentPriorityLocator = By.name("assignmentPriority");
	public static By createAssignmentLocator = By.name("btnAdminUpdate");
	
	
	
	public static By addSelectedDocumentsToAssignemntLocator = By.cssSelector("button[title='Add selected document(s) to selected assignment']");
	public static By resolutionChoiceReassignNone = By.cssSelector("#conflictContainer input[name$='Choice'][value='1']");
	public static By resolutionChoiceReassignAll = By.cssSelector("#conflictContainer input[name$='Choice'][value='2']");
	public static By continueBtnForAddingSelectedDocumentsLocator = By.xpath("//button[text()='Continue']");
	
	public static By removeSelectedDocumentsFromAssignemntLocator = By.cssSelector("button[title='Remove selected document(s) from the selected assignment']");

	public static By editBtn = By.cssSelector("button[title='Update edited assignment']"); 
	public static By cloneBtn = By.cssSelector("button[onclick*='CLONE']");
	public static By deleteBtn = By.cssSelector("button[title='Delete selected assignment']");
	public static By newBtn = By.cssSelector("button[onclick*=CREATE]");
	
	public static By findBtnLocator = By.xpath("//span[text()='Find']");
	public static By clearBtnLocator = By.xpath("//span[text()='Clear']");
	public static By addFilterLocator = By.id("filterOptions");
	public static By addFilterCriteriaOptionLocator = By.className("filterCriteriaOption");
	public static By addFilterCriteriaTextLocator = By.className("filterCriteriaText");
	

	private static ManageReviewAssignment manageReviewAssignment = new ManageReviewAssignment();
	
	public static void switchTab(String tabMarkup){
		manageReviewAssignment.click(SearchPage.waitForElement(tabMarkup, "css-selector"));
		manageReviewAssignment.waitFor(2);
	}
	
	
	public void switchTab(WebDriver driver, String tabMarkup, SearchPage sp){
		click(SearchPage.waitForElement(driver, tabMarkup, "css-selector"));
		waitFor(driver, 2);
	}
	
	
	
	public void deleteAssignment(WebDriver driver, SearchPage sp){
		//Click the Delete button
		click(SearchPage.waitForElement(driver, "button[title='Delete selected assignment']", "css-selector"));
		
		waitFor(driver, 2);
		
		acceptAlert(driver);
		
		SearchPage.waitForCompletion(driver, sp);
	}
	
	
	
	
	public void changeSortOrder(WebDriver driver, SearchPage sp, String order){
		//sp.setSelectBox(driver, "sortAssignments", "id", order);
		
		SearchPage.setSelectBox(driver, By.id("sortAssignments"), order);
		SearchPage.waitForCompletion(driver, sp);
	    
	    waitFor(driver, 6);
	}
	
/*	public void createAssignment(String name, String workflow, String assignee,SearchPage sp){
		manageReviewAssignment.waitForElementToBeClickable(By.cssSelector("button[onclick*=CREATE]"));
		//driver.findElement(By.cssSelector("button[onclick*=CREATE]")).click();
		manageReviewAssignment.tryClick(createAssignmentLocator);
		
		System.out.println( Logger.getLineNumber() + name);
		
		waitFor(4);
		
        SearchPage.waitForElement("input[name='assignmentName']", "css-selector").sendKeys(name);
	    
        waitFor(4);
	    
	    //Assign Workflow
	    SearchPage.setSelectBox(By.cssSelector("select[name='assignmentWorkflow']"), workflow);
	   
	    waitFor(5);
    	
	    //Set Assignee
	    //sp.setSelectBox("select[name='assignmentAssignee']", "css-selector", assignee);
    	SearchPage.setSelectBox(By.cssSelector("select[name='assignmentAssignee']"), assignee);
	    
	    //Press Create button
	    click(SearchPage.waitForElement("button[name='btnAdminUpdate']", "css-selector"));
	    
	    //Thread.sleep(5000);
	    SearchPage.waitForCompletion(sp);
	}*/
	
	/*public void createAssignment(WebDriver driver, String name, String workflow, String assignee,SearchPage sp){
		manageReviewAssignment.waitForElementToBeClickable(driver, By.cssSelector("button[onclick*=CREATE]"));
		driver.findElement(By.cssSelector("button[onclick*=CREATE]")).click();
		
		System.out.println( Logger.getLineNumber() + name);
		
		waitFor(driver, 4);
		
        SearchPage.waitForElement(driver, "input[name='assignmentName']", "css-selector").sendKeys(name);
	    
        waitFor(driver, 4);
	    
	    //Assign Workflow
	    SearchPage.setSelectBox(driver, By.cssSelector("select[name='assignmentWorkflow']"), workflow);
	   
	    waitFor(driver, 5);
    	
	    //Set Assignee
	    //sp.setSelectBox(driver, "select[name='assignmentAssignee']", "css-selector", assignee);
    	SearchPage.setSelectBox(driver, By.cssSelector("select[name='assignmentAssignee']"), assignee);
	    
	    //Press Create button
	    click(SearchPage.waitForElement(driver, "button[name='btnAdminUpdate']", "css-selector"));
	    
	    //Thread.sleep(5000);
	    SearchPage.waitForCompletion(driver, sp);
	}*/
	
	
	public static void createAssignment(String assignmentName, String assignmentWorkflow, String assignee, String assignmentPriority){
		//manageReviewAssignment.waitForVisibilityOf(newBtn);
		gotoAdminPanel();
		manageReviewAssignment.waitForElementToBeClickable(newBtn);
		manageReviewAssignment.waitFor(2);
		manageReviewAssignment.tryClick(newBtn);
		manageReviewAssignment.editText(assignmentNameLocator, assignmentName);
		manageReviewAssignment.waitFor(5);
		manageReviewAssignment.selectFromDrowdownByText(assignmentWorkflowLocator, assignmentWorkflow);
		manageReviewAssignment.waitFor(3);
		manageReviewAssignment.selectFromDrowdownByText(assigneeLocator, assignee);
		manageReviewAssignment.selectFromDrowdownByText(assignmentPriorityLocator, assignmentPriority);
		
		manageReviewAssignment.tryClick(createAssignmentLocator);
		
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		AutomationDataCleaner.listOfAssignmentsCreated.add(assignmentName);
		
		System.out.println( Logger.getLineNumber() + "Assignments created with the name: " + assignmentName + "...");
	}
	
public void createAssignment(WebDriver driver, String name, int workflowPosition, String assignee,SearchPage sp){
		//Click the New button
		click(SearchPage.waitForElement(driver, "button[onclick*=CREATE]", "css-selector"));
		
		System.out.println( Logger.getLineNumber() + name);
		
		waitFor(driver, 4);
		
		SearchPage.waitForElement(driver, "input[name='assignmentName']", "css-selector").sendKeys(name);
	    
        waitFor(driver, 4);
	    
	    //Assign Workflow
        SearchPage.setSelectBox(driver, "select[name='assignmentWorkflow']", "css-selector", workflowPosition);
	    
	    waitFor(driver, 5);
    	
	    
	    //Set Assignee
	    SearchPage.setSelectBox(driver, "select[name='assignmentAssignee']", "css-selector", assignee);
	    
	    
	    //Press Create button
	    click(SearchPage.waitForElement(driver, "button[name='btnAdminUpdate']", "css-selector"));
	    
	    //Thread.sleep(5000);
	    SearchPage.waitForCompletion(driver, sp);
	}
	
	
	public void editAssignment(WebDriver driver, String assignmentName){
		
		
	}
	
	
	public void findAssignment(WebDriver driver, String assignmentName, SearchPage sp){
		SearchPage.waitForElement(driver, "findAssignment", "id").clear();
		waitFor(driver, 4);
		SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
        waitFor(driver, 4);
      
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	    
	   	//Thread.sleep(6000);
	    SearchPage.waitForCompletion(driver, sp);
	    waitFor(driver, 8);
	}
	
	
	public static boolean isAssignmentDeleted(String assignmentName){
		List<WebElement> assignmentList = Driver.getDriver().findElements(By.cssSelector("span[title='" + assignmentName + "']"));
		
		return assignmentList.size() == 0;
	}
	
	
	public static int getAssignmentCount(String assignmentName){
		List<WebElement> assignmentList = Driver.getDriver().findElements(By.cssSelector("span[title='" + assignmentName + "']"));
		
		return assignmentList.size();
	}
	
	public void selectAllAssignments(WebDriver driver, SearchPage sp){
		List<WebElement> selectAll= driver.findElements(By.id("toggleAllAssignments"));
		click(selectAll.get(0));
	}
	
	@SuppressWarnings("static-access")
	public void addDocsToAssignment(WebDriver driver,SearchPage sp,String reAssignType){
		switchTab(driver, "a[href='#actionManageAssignments']", sp);
		waitFor(driver, 2);
		
		//Click the check-box beside the assignment
        List<WebElement> checkboxes= driver.findElements(By.cssSelector("input[class='chkAssignment']"));
        for(int i=0;i<checkboxes.size();i++){
      	  if(checkboxes.get(i).isDisplayed()){
      		  click(checkboxes.get(i));
      		  break;
      	  }
      	}
		
        waitFor(driver, 8);
		//Attempt to add all documents to assignments
        click(sp.waitForElement(driver, "button[onclick='validateAndAddDocument()']", "css-selector"));
        waitFor(driver, 20);
        //Utils.waitForAlertAndAccept();
        Utils.waitShort();
        
        WebElement conflictContainer= sp.waitForElement(driver, "conflictContainer", "id");
        System.out.println( Logger.getLineNumber() + conflictContainer.getTagName());
        waitFor(driver, 2);
        
        //int proceedWithAssignment=1;
        
        try{
       	 	conflictContainer.findElement(By.cssSelector("input"));
       	 
       	 	List<WebElement> radioButtons= conflictContainer.findElements(By.cssSelector("input"));
       	 	
       	 	if(radioButtons.size() == 0) return;
       	 	
       	    GridActionAddingDocs2.warningPresent=1;
       	    GridActionAddingDocs3.warningPresent=1;
       	 
       	 	if(reAssignType.equalsIgnoreCase("all")){
       	 		//Click ReAssign all radio button
       	 		radioButtons.get(1).click();
    	
       	 		waitFor(driver, 2);
    	
       	 		//Click Continue
       	 		click(sp.waitForElement(driver, "button[onclick*='resolveConflict']", "css-selector"));

       	 		waitFor(driver, 2);
        
       	 		//Switch to alert and accept it
       	 		try {
       	 			WebDriverWait wait = new WebDriverWait(driver, 15);
       	 			wait.until(ExpectedConditions.alertIsPresent());
            
       	 			Alert alert = driver.switchTo().alert();
            
       	 			System.out.println( Logger.getLineNumber() + alert.getText());
                  
       	 			alert.accept();
       	 			}//end inner try 
       	 			catch (Exception e) {}
         
       	 	          
       	 	       	}//end if
       	    
       	 	else if(reAssignType.equalsIgnoreCase("none")){
       	 						radioButtons.get(0).click();
       	 					    //Click Continue
       	 	       	 		    click(sp.waitForElement(driver, "button[onclick*='resolveConflict']", "css-selector"));
       	 	       	 		    
       	 	       	 		    //Switch to alert and accept it
       	 	       	 		    try {
       	 	       	 		    	WebDriverWait wait = new WebDriverWait(driver, 15);
       	 	       	 		    	wait.until(ExpectedConditions.alertIsPresent());
       	 	            
       	 	       	 		    	Alert alert = driver.switchTo().alert();
       	 	            
       	 	       	 		    	System.out.println( Logger.getLineNumber() + alert.getText());
       	 	                  
       	 	       	 		    	alert.accept();
       	 	       	 				}//end inner try 
       	 	       	 				catch (Exception e) {}
       	 	       	 		    		//do nothing
       	 							}
        		}//end outer try
        
        
        		catch(NoSuchElementException ef){
       	 	         //Click Continue
            		// click(sp.waitForElement(driver, "button[onclick*='resolveConflict']", "css-selector"));
        			driver.findElement(By.cssSelector("button[onclick*='resolveConflict']")).click();
            		 waitFor(driver, 5);
       	            //proceedWithAssignment=1;
       	        }
              
        
        //List<WebElement> pNodes= conflictContainer.findElements(By.cssSelector("p"));
        //List<WebElement> children= sp.getAllChildren(driver, conflictContainer);
        
        //Thread.sleep(2000);
        sp.waitForCompletion(driver, sp);
 	}//end function
	
	
	
	/*public void removeDocumentsAdminTab(WebDriver driver, SearchPage sp){
		
		//Go to "Admin" tab and check the box beside it
       
        switchTab(driver, "a[href='#actionAdminAssignments']", sp);
        waitFor(driver, 2);
        
        List<WebElement>checkboxes= driver.findElements(By.cssSelector("input[class='chkAssignment']"));
        for(int i=0;i<checkboxes.size();i++){
      	  
      	  if(checkboxes.get(i).isDisplayed()){
      		  
      		  click(checkboxes.get(i));
      		  break;
      		  
      	  }//end if
      	  
        }//end for
		
		//Click "Remove Documents button"
        click(SearchPage.waitForElement(driver, "button[onclick='removeAllDocuments()']", "css-selector"));
        
        String alertText = getAlertMsg(driver);
        
        GridActionRemoveDocuments.alertPresent = (alertText == null || alertText == "") ? 0 : 1;
        
        SearchPage.waitForCompletion(driver, sp);
	}*/
	
	
	/*public void removeSelectedDocsManageTab(WebDriver driver, SearchPage sp){
		//From Manage tab click the "Remove Selected Documents button"
	    switchTab(driver, "a[href='#actionManageAssignments']", sp);
	    waitFor(driver, 2);
	    
	    List<WebElement>checkboxes= driver.findElements(By.cssSelector("input[class='chkAssignment']"));
        for(int i=0;i<checkboxes.size();i++){
      	  if(checkboxes.get(i).isDisplayed()){
      		  click(checkboxes.get(i));
      		  break;
      	  }//end if
      	}//end for
        
        //Click "Remove Selected Documents" button
        click(SearchPage.waitForElement(driver, "button[onclick='removeDocument()']", "css-selector"));
		
        String alertText = getAlertMsg(driver);
        
        GridActionRemoveDocuments.alertPresent = (alertText == null || alertText == "") ? 0 : 1; 
     
        SearchPage.waitForCompletion(driver, sp);
	}*/
	
	
	public void cloneAssignment(WebDriver driver, String name, String assignee,String workflow,SearchPage sp){
		//Click the clone button
		click(SearchPage.waitForElement(driver, "button[onclick*=CLONE]", "css-selector"));
		waitFor(driver, 4);
		SearchPage.waitForElement(driver, "input[name='assignmentName']", "css-selector").sendKeys(name);
	    
        waitFor(driver, 5);
    	
    	//Assign Workflow
        SearchPage.setSelectBox(driver, "select[name='assignmentWorkflow']", "css-selector", workflow);

	    waitFor(driver, 5);
    	
	    //Set Assignee
	    SearchPage.setSelectBox(driver, "select[name='assignmentAssignee']", "css-selector", assignee);
	    //Press Clone button
	    click(SearchPage.waitForElement(driver, "button[name='btnAdminUpdate']", "css-selector"));
	    
	    waitFor(driver, 8);
	    //sp.waitForCompletion(driver, sp);
	    System.out.println( Logger.getLineNumber() + "Assignment cloned with the name: " + name);
	}
	
	/*private void makeDocsReadyForAssignment(WebDriver driver, String assignmentName){
		SearchPage sp= new SearchPage();
		WebElement conflictContainer= SearchPage.waitForElement(driver, "conflictContainer", "id");
		List<WebElement> radioButtons= conflictContainer.findElements(By.cssSelector("input"));
		//Click ReAssign all radio button
		radioButtons.get(1).click();
		
		waitFor(driver, 2);
		
		//Click Continue
	    click(SearchPage.waitForElement(driver, "button[onclick*='resolveConflict']", "css-selector"));
	
	    waitFor(driver, 5);
	    
	    //Switch to alert and accept it
	    acceptAlert(driver);
	    waitFor(driver, 10);
	    
		//Check for the same assignment again
	  //Go to the Find tab and search for the same assignment again
	    //click(sp.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	    
	    driver.findElement(By.cssSelector("body > header > ul > li:nth-child(5) > a")).click();
	    SearchPage.waitForElement(driver, "findAssignment", "id").clear();
	    waitFor(driver, 2);
	    SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	  
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	    
	    waitFor(driver, 5);
	    
	    //Go to "Manage" tab and check the box beside it
	    click(SearchPage.waitForElement(driver, "a[href='#actionManageAssignments']", "css-selector"));
	
	    List<WebElement> checkboxes= driver.findElements(By.cssSelector("input[class='chkAssignment']"));
	    for(int i=0;i<checkboxes.size();i++){
	  	  if(checkboxes.get(i).isDisplayed()){
	  		  click(checkboxes.get(i));
	  		  break;
	  	  }
	  	}
	    
	    //Click the "Remove Selected Documents" button
	    click(SearchPage.waitForElement(driver, "button[onclick='removeDocument()']", "css-selector"));
	    
	    
	    //Switch to alert and accept it
	    acceptAlert(driver);
	    waitFor(driver, 10);
	    
	  //Go to the Find tab and search for the newly created assignment
	    click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	      
	    waitFor(driver, 2);
	      
		SearchPage.waitForElement(driver, "findAssignment", "id").clear(); 	
	    SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	  
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	    
	   	//Thread.sleep(5000);
	    SearchPage.waitForCompletion(driver, sp);          	
	    
	     //Go back to Manage tab
	      click(SearchPage.waitForElement(driver, "a[href='#actionManageAssignments']", "css-selector"));
	
	      //Click the check-box beside the assignment
	      checkboxes= driver.findElements(By.cssSelector("input[class='chkAssignment']"));
	      for(int i=0;i<checkboxes.size();i++){
	    	  if(checkboxes.get(i).isDisplayed()){
	    		  click(checkboxes.get(i));
	    		  break;
	    	  }
	      }
	
	     //Attempt to add all documents to assignments
	     click(SearchPage.waitForElement(driver, "button[onclick='validateAndAddDocument()']", "css-selector"));
	     waitFor(driver, 5);
	}	
	*/
	
	public static void gotoAdminPanel(){
		if(Driver.getDriver().findElement(By.cssSelector("body > header >ul[role='tablist'] > li")).getAttribute("aria-expanded").equals("false")){
			manageReviewAssignment.tryClick(By.cssSelector("a[href='#actionAdminAssignments']"));
			manageReviewAssignment.waitFor(1);
		}else{
			System.out.println( Logger.getLineNumber() + "Admin panel already expanded...");
		}
	}
	
	public static void gotoManagePanel(){
		if(Driver.getDriver().findElement(By.cssSelector("body > header >ul[role='tablist'] > li:nth-child(2)")).getAttribute("aria-expanded").equals("false")){
			manageReviewAssignment.tryClick(By.cssSelector("a[href='#actionManageAssignments']"));
			manageReviewAssignment.waitFor(1);
		}else{
			System.out.println( Logger.getLineNumber() + "Manage panel already expanded...");
		}
	}
	
	public static void gotoFiltersPanel(){
		manageReviewAssignment.waitForVisibilityOf(By.cssSelector("li[aria-controls='actionFilterAssignments']"));
		
		if(Driver.getDriver().findElement(By.cssSelector("li[aria-controls='actionFilterAssignments']")).getAttribute("aria-expanded").equals("false")){
			manageReviewAssignment.tryClick(By.cssSelector("a[href='#actionFilterAssignments']"));
			manageReviewAssignment.waitFor(1);
		}else{
			System.out.println( Logger.getLineNumber() + "Filters panel already expanded...");
		}
	}
	
	
	public static void gotoFindPanel(){
		manageReviewAssignment.waitForVisibilityOf(By.cssSelector("li[aria-controls='actionFindAssignments']"));
		
		if(Driver.getDriver().findElement(By.cssSelector("li[aria-controls='actionFindAssignments']")).getAttribute("aria-expanded").equals("false")){
			manageReviewAssignment.tryClick(By.cssSelector("a[href='#actionFindAssignments']"));
			manageReviewAssignment.waitFor(1);
		}else{
			System.out.println( Logger.getLineNumber() + "Find panel already expanded...");
		}
	}
	
	public static void addFilter(String filterName){
		manageReviewAssignment.selectFromDrowdownByText(addFilterLocator, filterName);
	}
	
	
	public static void addFilterCriteriaOption(int position, String criteriaOption){
		List<WebElement> criteriaOptionList = manageReviewAssignment.getElements(addFilterCriteriaOptionLocator);
		manageReviewAssignment.selectFromDrowdownByText(criteriaOptionList.get(position - 1), criteriaOption);
	}
	
	public static void addFilterCriteriaText(int position, String criteriaText){
		List<WebElement> criteriaOptionList = manageReviewAssignment.getElements(addFilterCriteriaTextLocator);
		manageReviewAssignment.editText(criteriaOptionList.get(position - 1), criteriaText);
	}
	
	public static void clickEditAssignment(){
		gotoAdminPanel();
		manageReviewAssignment.tryClick(editBtn);
		manageReviewAssignment.waitFor(2);
	}
	
	public static void clickCloneAssignment(){
		gotoAdminPanel();
		manageReviewAssignment.tryClick(cloneBtn);
		manageReviewAssignment.waitFor(2);
	}
	
	public static String clickDeleteAssignment(){
		gotoAdminPanel();
		manageReviewAssignment.tryClick(deleteBtn);
		manageReviewAssignment.waitFor(2);
		String alertMsg = manageReviewAssignment.getAlertMsg(Driver.getDriver());
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		manageReviewAssignment.waitFor(10);
		
		return alertMsg;
	}
	
	
	public static void clickFindAssignment(){
		gotoFindPanel();
		manageReviewAssignment.tryClick(findBtnLocator);
		manageReviewAssignment.waitFor(20);
	}
	
	public static void clickClearFilteredAssignment(){
		gotoFindPanel();
		manageReviewAssignment.tryClick(clearBtnLocator);
		manageReviewAssignment.waitFor(20);
	}
	
	public static void clickAddSelectedDocumentsToAssignment(){
		gotoManagePanel();
		manageReviewAssignment.tryClick(addSelectedDocumentsToAssignemntLocator, continueBtnForAddingSelectedDocumentsLocator);
		manageReviewAssignment.waitForVisibilityOf(continueBtnForAddingSelectedDocumentsLocator);
	}
	
	public static void resolveConflict(){
		manageReviewAssignment.tryClick(resolutionChoiceReassignAll);
		manageReviewAssignment.tryClick(continueBtnForAddingSelectedDocumentsLocator);
		manageReviewAssignment.acceptAlert(Driver.getDriver());
		//ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		manageReviewAssignment.waitFor(15);
	}
	
	public static void addDocumentsToSelectedAssignment(){
		clickAddSelectedDocumentsToAssignment();
		resolveConflict();
	}
	
	public static void clickRemoveSelectedDocumentsFromAssignment(){
		gotoManagePanel();
		manageReviewAssignment.tryClick(removeSelectedDocumentsFromAssignemntLocator);
		manageReviewAssignment.acceptAlert(Driver.getDriver());
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
	}
	
	/**
	 * 
	 * @param newAssignmentName keep blank if want it to be unchanged
	 * @param newAssignee keep blank if want it to be unchanged
	 * @param newPriority keep blank if want it to be unchanged
	 */
	public static void editAssignment(String newAssignmentName, String newAssignee, String newPriority){
		//gotoAdminPanel();
		clickEditAssignment();
		if(newAssignmentName.equals("") == false)
			manageReviewAssignment.editText(assignmentNameLocator, newAssignmentName);
		if(newAssignee.equals("") == false)
			manageReviewAssignment.selectFromDrowdownByText(assigneeLocator, newAssignee);
		if(newPriority.equals("") == false)
			manageReviewAssignment.selectFromDrowdownByText(assignmentPriorityLocator, newPriority);
		
		manageReviewAssignment.tryClick(createAssignmentLocator);
		
		manageReviewAssignment.waitFor(2);
		
		
		AutomationDataCleaner.popAssignmentFromList();
		AutomationDataCleaner.listOfAssignmentsCreated.add(newAssignmentName);
		
		
		System.out.println( Logger.getLineNumber() + "Assignment name updated to '" + newAssignmentName + "'...");
	}
	
	
	public static void cloneAssignment(String newAssignmentName, String workflow, String newAssignee, String newPriority){
		clickCloneAssignment();
		if(newAssignmentName.equals("") == false)
			manageReviewAssignment.editText(By.name("assignmentName"), newAssignmentName);
		if(newAssignee.equals("") == false)
			manageReviewAssignment.selectFromDrowdownByText(By.name("assignmentAssignee"), newAssignee);
		if(newPriority.equals("") == false)
			manageReviewAssignment.selectFromDrowdownByText(By.name("assignmentPriority"), newPriority);
		
		manageReviewAssignment.selectFromDrowdownByText(assignmentWorkflowLocator, workflow);
		
		manageReviewAssignment.tryClick(createAssignmentLocator);
		
		manageReviewAssignment.waitFor(2);
		
		AutomationDataCleaner.listOfAssignmentsCreated.add(newAssignmentName);
		
		System.out.println( Logger.getLineNumber() + "Assignment cloned with name '" + newAssignmentName + "'...");
	}
	
	
	public static String deleteAssignment(String assignmentName){
		 ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	     ReviewAssignmentAccordion.selectAssignment(assignmentName);
	     gotoAdminPanel();
	     String alertMsg = clickDeleteAssignment();	
	     
	     AutomationDataCleaner.popAssignmentFromList();
	     
	     System.out.println( Logger.getLineNumber() + "Assignment '" + assignmentName + "' deleted...");
	     
	     return alertMsg;
	}
	
}