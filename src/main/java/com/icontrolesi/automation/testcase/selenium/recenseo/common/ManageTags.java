package com.icontrolesi.automation.testcase.selenium.recenseo.common;



import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.icontrolesi.automation.platform.util.AutomationDataCleaner;

import com.icontrolesi.automation.platform.util.Logger;

@SuppressWarnings("static-access")
public class ManageTags extends TagManagerInGridView{
	static WebDriver driver = Driver.getDriver(); 
	public static String editAlertPresent="1";	
	static ManageTags manageTags = new ManageTags();
	public static By tagNameLocator = By.id("tag-editOrSubName");
	public static By tagPermissionLocator = By.id("tags-editOrSubPermission");
	public static By cancelTagWindowLocator = By.id("cancel");
	public static By tagCreateLocator = By.id("create");
	public static By clearSelectionLocator = By.cssSelector("button[title='Clear selection']");
	public static By deleteSelectedBtnLocator = By.cssSelector("button[title='Delete selected tag(s)']");
	
	
	
	public static void open(){
		manageTags.tryClick(TagAccordion.manageTagBtnLocator);
		
		manageTags.switchToDefaultFrame();
		manageTags.waitForFrameToLoad(Frame.MANAGE_TAG_FRAME);
		manageTags.waitForNumberOfElementsToBeGreater(TagManagerInGridView.tagItemLocator, 0);
		
		System.out.println( Logger.getLineNumber() + "Manage Tags opened from Tags Accordion...");
	}
	
	
	public static void createTopLevelTag(String tagName, String permission){
		manageTags.tryClick(createTagBtnLocator, createBtnLocator);
		manageTags.editText(tagNameFieldLocator, tagName);
		if(permission.equals("") == false)
			manageTags.selectFromDrowdownByText(tagPermissionFieldLocator, permission);
		manageTags.tryClick(createBtnLocator, 5);
		
		AutomationDataCleaner.listOfTagsCreated.add(tagName);
		
		System.out.println( Logger.getLineNumber() + "Top level tag created with name '" + tagName + "'...");
		
		Actions actions = new Actions(Driver.getDriver());
		actions.sendKeys(Keys.chord(Keys.CONTROL, Keys.SHIFT, "v")).build().perform();
	}
	
	public static void createSubTagFor(String parentTagName, String subTagName, String subTagsPermission){
		openUpSubtagCreationWindowFor(parentTagName);
		fillUpWithNewData(subTagName, subTagName, subTagsPermission);
		
		manageTags.tryClick(createBtnLocator, 5);
		
		AutomationDataCleaner.listOfTagsCreated.add(subTagName);
		
		System.out.println( Logger.getLineNumber() + "Subtag created with name '" + subTagName + "' under Top level tag '" + parentTagName + "'...");
	}
		
	
	public void switchTab(String tabMarkup){
			
			
			click(SearchPage.waitForElement( tabMarkup, "css-selector"));
			waitFor( 2);
			
		}
		
	
	public void createTag(WebDriver  driver, String tagName, String accessibleBy, SearchPage sp){
		
		//Assign name
		WebElement name= sp.waitForElement( "tags-editName", "id");  
		name.clear();
		waitFor( 2);
		name.sendKeys(tagName);
		
		//Select accessibility type
		sp.setSelectBox(By.id("tags-editPermission"), accessibleBy);
		
		//Click the Create button
		click(sp.waitForElement( "tags-create", "id"));
		sp.waitForCompletion();
	
		
		waitFor( 5);
	}
	
	
	public static void createTag(String tagName, String tagPermission){
		//DocView.clickTags();
		
		//manageTags.waitForFrameToLoad(Driver.getDriver(), Frame.MANAGE_TAG_FRAME);
		manageTags.editText(tagNameLocator, tagName);
		manageTags.selectFromDrowdownByText(tagPermissionLocator, tagPermission);
		manageTags.tryClick(tagCreateLocator, 1);
		
		System.out.println( Logger.getLineNumber() + "Tag created with name '" + tagName + "' ...");
	}
	
	
	public void findTag(String tagName){
		
		//Provide tag name
		SearchPage.waitForElement( "findTag", "id").clear();
		waitFor( 2);
		SearchPage.waitForElement( "findTag", "id").sendKeys(tagName);
	    
	    waitFor( 2);
	    
	    //Click Find button
	    click(SearchPage.waitForElement( "tags-search", "id"));
	    
	    waitFor( 3);
	   	
	   	//Wait for loading to be over
	   	WebElement tagContainer= SearchPage.waitForElement( "tagContainer", "id");
	   	WebElement loadingMessageContainer= tagContainer.findElement(By.id("messageContainer"));
		while(loadingMessageContainer.isDisplayed()){
			
		}
		
		waitFor( 3);
	}
	
		
	public void addDocumentsToTag(WebDriver driver, SearchPage sp){
		click(manageTags.getElement(By.cssSelector("button[title='Add document(s) to selected tag(s)']")));
		waitFor( 5);
	}

	
	public void selectVisibleTag(){
		List<WebElement>checkboxes= manageTags.getElements(By.cssSelector("input[name='chkTag']"));
	    
	    //Mark the checkBoxes for both tags
	    for(int i=0;i<checkboxes.size();i++){
	  	  
	  	  if(checkboxes.get(i).isDisplayed()){
	  		  click(checkboxes.get(i));
	  		  waitFor( 3);
	  	  }
	  	}
	}


	public void deleteTag(WebDriver driver, SearchPage sp){
		
		//Click the Delete button
		click(sp.waitForElement( "tags-delete", "id"));
		
		//Switch to alert
		try {
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.alertIsPresent());
	        
	        
	        Alert alert = driver.switchTo().alert();
	        
	        
	        System.out.println( Logger.getLineNumber() + alert.getText());
	        
	        alert.accept();
	    } catch (Exception e) {
	    	
	        //exception handling
	    }
		
	}

	public static void addDocumentsToTag(String tagName){
		Actions actions = new Actions(Driver.getDriver());
		
		By tagsLocator = By.cssSelector("#tagManage-picklist > ul > li");
		
		List<WebElement> tags = manageTags.getElements(tagsLocator);
		
		System.out.println( Logger.getLineNumber() + tags.size() + "*********************************");
		
		for(WebElement tag : tags){
			if(tag.getAttribute("title").trim().equals(tagName)){
				System.out.println( Logger.getLineNumber() + "Tag found...");
				actions.moveToElement(tag).perform();
				manageTags.getElement(By.id("tag_addDoc")).click();
				manageTags.acceptAlert(driver);
				System.out.println( Logger.getLineNumber() + "Document(s) added to selected [" + tagName + "] tag...");
				break;
			}
		}
	}


}
