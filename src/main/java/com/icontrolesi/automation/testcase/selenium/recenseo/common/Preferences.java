package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class Preferences implements CommonActions{
	public static final WebDriver driver = Driver.getDriver();
	public static final int EXPAND_ALL_PICKLISTS = 0;
	public static final int SHOW_ALL_TAGS_ON_CODING_PANEL = 1;
	
	public static final String VIEW = "view";
	public static final String FILE = "file";
	public static final String BATES = "bates";
	
	//variables to set View Page Layout Preference
		public static final String DEFAULT_RIGHT = "all";
		public static final String DEFAULT_LEFT = "all_left";
		public static final String GRID_ONLY = "grid_only";
		public static final String SPLIT_GRID_ONLY = "no_sidebar";
		public static final String HIDE_GRID_RIGHT = "no_grid";
		public static final String HIDE_GRID_LEFT = "no_grid_left";
	
		static Preferences preferences = new Preferences();
		
	public void switchPreferenceTab(String tabName){
		
		waitFor(15);
		
		/*
		  tabName:
		   "view" for View preference
		   "file" for file type preference
		   "bates" for bates preference
		      
		*/
		
        if(tabName.equalsIgnoreCase("view")){
			//WebElement liNode= SearchPage.waitForElement(driver, "viewTab", "id");
			//WebElement tabNode= liNode.findElement(By.cssSelector("a"));
	        WebElement tabNode= SearchPage.waitForElement(driver, "a[href='#docPrefView']", "css-selector");	
			click(tabNode);
			waitFor(12);
        }else if(tabName.equalsIgnoreCase("file")){
        	//WebElement liNode= SearchPage.waitForElement(driver, "fileTab", "id");
        	//WebElement tabNode= SearchPage.waitForElement(driver, "a[href='/QuickReviewWeb/mvc/preferences/fileTypePreference']", "css-selector");
        	WebElement tabNode= SearchPage.waitForElement(driver, "a[href*='mvc/preferences/fileTypePreference']", "css-selector");
    		click(tabNode);
    		waitFor(10);
        }else if(tabName.equalsIgnoreCase("bates")){
        	//WebElement liNode= SearchPage.waitForElement(driver, "batesTab", "id");
        	WebElement tabNode= SearchPage.waitForElement(driver, "a[href='#batesViewDiv']", "css-selector");
    		click(tabNode);
    		waitFor(10);
        }//end else
		
	}//end function
	
	
	public void codingTemplatePreferences(WebDriver driver,int which, boolean check){
		Utils.waitForElement(driver, "expandOpts", "name");
		WebElement expandCheckBox = driver.findElement(By.name("expandOpts"));
		WebElement showAllTagsCheckBox = driver.findElement(By.name("tagWidgetShowAll"));
		try {
			Thread.sleep(Utils.WAIT_MEDIUM);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(which == EXPAND_ALL_PICKLISTS) {
			if(check){
				if(!expandCheckBox.isSelected()) expandCheckBox.click();
			}else{
				if(expandCheckBox.isSelected()) expandCheckBox.click();
			}
		}
		else if(which == SHOW_ALL_TAGS_ON_CODING_PANEL) {
			if(check){
				if(!showAllTagsCheckBox.isSelected()) showAllTagsCheckBox.click();
			}else{
				if(showAllTagsCheckBox.isSelected()) showAllTagsCheckBox.click();
			}
		}
		else
			try {
				throw new Exception("Invalid Item id");
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		try {
			Thread.sleep(Utils.WAIT_MEDIUM);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void selectPreferenceTab(String tabName){
		By preferenceTablocator = By.cssSelector("#preferences > ul > li > a");
		List<WebElement> tabs = driver.findElements(preferenceTablocator);
		for(WebElement tab : tabs){
			if(tab.getText().trim().equalsIgnoreCase(tabName)){
				tab.click();
				
				if(tabName.equalsIgnoreCase("File Types")){
					preferences.waitForNumberOfElementsToBeGreater(By.cssSelector("#jqg1 > tbody > tr"), 0);
				}else if(tabName.trim().equalsIgnoreCase("Bates")){
					preferences.waitForVisibilityOf(driver, By.id("prefs"));
				}
				
				System.out.println( Logger.getLineNumber() + tabName + " selected...");
				
				break;
			}
		}
	}	
	
	public void selectFileTypeViewerPreferences(WebDriver driver, int[] viewerPref, SearchPage sp){
		/*
		  viewerPref and downloadPref are arrays that contains 1's and/or 0's 2's
		  0 means native and 1 means images and 2 means Text
		  
		*/
		
		waitFor(driver, 20);
		
		driver.switchTo().defaultContent();
		
		SearchPage.waitForElement(driver, "iframe[src*='/QuickReviewWeb/mvc/preferences']", "css-selector");
        WebElement frameElement= driver.findElement(By.cssSelector("iframe[src*='/QuickReviewWeb/mvc/preferences']"));
		
		//WebElement div= driver.findElement(By.id("ui-id-1"));
		//WebElement frame= div.findElement(By.tagName("iframe"));
        waitFor(driver, 6);
		driver.switchTo().frame(frameElement);
		
		SearchPage.waitForElement(driver, "td[aria-describedby='jqg1_viewPrefName']", "css-selector");
		List<WebElement> viewPrefNodes= driver.findElements(By.cssSelector("td[aria-describedby='jqg1_viewPrefName']"));
		
		//Click to make select box visible
		
		
		for(int i=0;i<6;i++){
			click(viewPrefNodes.get(i));
			waitFor(driver, 2);
			
			if(viewerPref[i]==0){
			new Select(viewPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Native");
			waitFor(driver, 2);
			/*saveRowInPreferences(driver, sp);
			Thread.sleep(2000);*/
			}
			else if (viewerPref[i]==1){
				new Select(viewPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Imaged");
				waitFor(driver, 2);
				/*saveRowInPreferences(driver, sp);
				Thread.sleep(2000);*/
			}
			else if(viewerPref[i]==2){
				new Select(viewPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Text");
				waitFor(driver, 2);
				/*saveRowInPreferences(driver, sp);
				Thread.sleep(2000);*/
			}
		}//end for
		//saveRowInPreferences(driver, sp);
	}//end function
	
	
	public void selectFileTypeDownloadPreferences(WebDriver driver, int[] prefs, SearchPage sp){
			
		/*
		  prefs are arrays that contains 1's and/or 0's
		  0 means native and 1 means imaged
		  
		*/
		
		waitFor(driver, 40);
		
		
		driver.switchTo().defaultContent();
		//driver.switchTo().frame("MAIN");
		SearchPage.waitForFrameLoad(driver, "MAIN");
		Utils.waitMedium();
		
		try{
		    SearchPage.waitForElement(driver, "iframe[height='100%'][width='100%'][style='width:100%; height:100%; border:0; margin:0']", "css-selector");
			WebElement iframe= driver.findElement(By.cssSelector("iframe[height='100%'][width='100%'][style='width:100%; height:100%; border:0; margin:0']"));
			driver.switchTo().frame(iframe);
			Thread.sleep(2000);
			System.out.println( Logger.getLineNumber() + "Swtiched to First iframe");
		}
		catch(Exception e){
			
			
			
		}
		
		waitFor(driver, 5);
		
		try{
			
			//SearchPage.waitForFrameLoad(driver, "fileDownloadPrefFrame");
			WebElement fileDownloadFrame= driver.findElement(By.id("fileDownloadPrefFrame"));
			if(fileDownloadFrame.isDisplayed()){
				
				driver.switchTo().frame("fileDownloadPrefFrame");
				
				
				
				
				Thread.sleep(2000);
				System.out.println( Logger.getLineNumber() + "Swtiched to second iframe");
				
			}
			
		}
	    catch(Exception e1){
	    	
	    	System.out.println( Logger.getLineNumber() + "Pref frame is hidden");
	    	
	    }
		
		//SearchPage.waitForFrameLoad(driver, "fileDownloadPrefFrame");
		//switchToFrame(1);
		//WebElement frameElement= driver.findElement(By.cssSelector("iframe[src='/QuickReviewWeb/mvc/preferences/fileTypePreference?prepareDownload=true']"));
		
		try{
			SearchPage.waitForElement(driver, "iframe[src*='/QuickReviewWeb/mvc/preferences']", "css-selector");
			WebElement frameElement= driver.findElement(By.cssSelector("iframe[src*='/QuickReviewWeb/mvc/preferences']"));
			driver.switchTo().frame(frameElement);
			Thread.sleep(2000);
		}
		catch(Exception e){
			
			
			
		}
		
		//WebElement div= driver.findElement(By.id("ui-id-1"));
		//WebElement frame= div.findElement(By.tagName("iframe"));
		
		waitFor(driver, 5);
		
		SearchPage.waitForElement(driver, "td[aria-describedby='jqg1_downloadPrefName']", "css-selector");
		//List<WebElement> downloadPrefNodes= driver.findElements(By.cssSelector("td[aria-describedby='fileTypePreferenceList_fileHostingTypeNameDownload']"));
		List<WebElement> downloadPrefNodes= driver.findElements(By.cssSelector("td[aria-describedby='jqg1_downloadPrefName']"));

		
		//Click to make select box visible
		
		for(int i=0;i<4;i++){
			Utils.waitMedium();
			click(downloadPrefNodes.get(i));
			waitFor(driver, 2);
			
			if(prefs[i]==0){
			new Select(downloadPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Native");
			waitFor(driver, 2);
			saveRowInPreferences(driver, sp);
			waitFor(driver, 2);
			}
			else if (prefs[i]==1){
				
				new Select(downloadPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Imaged");
				waitFor(driver, 5);
//				saveRowInPreferences(driver, sp);
//				Thread.sleep(2000);
				
			}
			
		}//end for
		
//		saveRowInPreferences(driver, sp);
					
	}//end function
	
	/*
	public void selectFileTypeDownloadPreferencesFromSearch(WebDriver driver, int[] prefs, SearchPage sp) throws InterruptedException{
		
		
		 // prefs are arrays that contains 1's and/or 0's
		 // 0 means native and 1 means imaged
		  
		
		Utils.waitMedium();
		
		//SearchPage.waitForFrameLoad(driver, "fileDownloadPrefFrame");
		switchToFrame(1);
		//WebElement frameElement= driver.findElement(By.cssSelector("iframe[src='/QuickReviewWeb/mvc/preferences/fileTypePreference?prepareDownload=true']"));
		
		WebElement div= driver.findElement(By.id("ui-id-1"));
		WebElement frame= div.findElement(By.tagName("iframe"));
		driver.switchTo().frame(frame);
		
		
		
		//List<WebElement> downloadPrefNodes= driver.findElements(By.cssSelector("td[aria-describedby='fileTypePreferenceList_fileHostingTypeNameDownload']"));
		List<WebElement> downloadPrefNodes= driver.findElements(By.cssSelector("td[aria-describedby='jqg1_downloadPrefName']"));

		
		//Click to make select box visible
		
		for(int i=0;i<4;i++){
			Utils.waitMedium();
			click(downloadPrefNodes.get(i));
			Thread.sleep(2000);
			
			if(prefs[i]==0){
			new Select(downloadPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Native");
			Thread.sleep(2000);
			saveRowInPreferences(driver, sp);
			Thread.sleep(2000);
			}
			else if (prefs[i]==1){
				
				new Select(downloadPrefNodes.get(i).findElement(By.cssSelector("select"))).selectByVisibleText("Imaged");
				Thread.sleep(2000);
				saveRowInPreferences(driver, sp);
				Thread.sleep(2000);
				
			}
			
		}//end for
		
		saveRowInPreferences(driver, sp);
					
	}//end function

	*/
	
	
	
	public void clickBackButton(){
		
		Driver.getDriver().switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");
		WebElement frame= SearchPage.waitForElement(Driver.getDriver(), "iframe[src='interceptorSingleDoc.jsp']", "css-selector");
		Driver.getDriver().switchTo().frame(frame);
		
		click(Driver.getDriver().findElement(By.id("prefToggle")));
		
		
		
	}
	
	//added by Shaheed Sagar
	public void saveRowInPreferences(WebDriver driver, SearchPage sp){
		//click(SearchPage.waitForElement(driver, "span[class='ui-icon ui-icon-disk']", "css-selector"));
		waitForElementToBeClickable(By.cssSelector("span[class='ui-icon ui-icon-disk']"));
		driver.findElement(By.cssSelector("span[class='ui-icon ui-icon-disk']"));
		
	}
	
	
	public void viewPageLayoutPreference(String pref){
		Utils.waitForElement(Driver.getDriver(), "input[value='all']", "css-selector");
		String radio= "input[value='"+pref+"']";
		click(Utils.waitForElement(Driver.getDriver(), radio, "css-selector"));
	}
	
	public void setSort(int which,String sortName,boolean asc) throws InterruptedException{
		WebElement selectElement = Utils.waitForElement(Driver.getDriver(), "SRT"+which, "id");
		Select select = new Select(selectElement);
		select.selectByVisibleText(sortName);
		if(asc){
			WebElement inputElement = SearchPage.getParent(Driver.getDriver(), selectElement).findElements(By.tagName("input")).get(0);
			click(inputElement);
		}else{
			WebElement inputElement = SearchPage.getParent(Driver.getDriver(), selectElement).findElements(By.tagName("input")).get(1);
			click(inputElement);
		}
		Utils.waitShort();
	}
	
	
	public static void setViewerPreferenceTo(String fileType, String viewerPreference){
		preferences.tryClick(By.cssSelector("tr[id='" + fileType + "']" + " > td:nth-child(2)"));
		preferences.selectFromDrowdownByText(By.name("viewPrefName"), viewerPreference);
		preferences.waitFor(driver, 2);
	}
	
}//end class
