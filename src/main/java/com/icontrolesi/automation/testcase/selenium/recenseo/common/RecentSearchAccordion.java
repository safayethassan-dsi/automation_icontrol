package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;

import com.icontrolesi.automation.platform.util.Logger;

public class RecentSearchAccordion implements CommonActions{
	static WebDriver driver = Driver.getDriver(); 
	private static By tabLocation = By.id("recent");
	private static By historyItemsLocation = By.cssSelector("#recentSearches > ul > li");
	
	public static void open(){
		//driver.switchTo().frame("MAIN");
		(new RecentSearchAccordion()).waitForElementToBeClickable(tabLocation);
		driver.findElement(tabLocation).click();
		(new RecentSearchAccordion()).waitForAttributeEqualsIn(driver, tabLocation, "aria-expanded", "true");
		(new RecentSearchAccordion()).waitForNumberOfElementsToBeGreater(historyItemsLocation, 0);
		//driver.switchTo().defaultContent();
		
		System.out.println( Logger.getLineNumber() + "\nRecent Search accordion expanded...\n");
	}
	
	
	public static boolean isRecentSearchSaved(){
		return driver.findElements(historyItemsLocation).size() > 0;
	}
	
	public static boolean isRecentSearchSaved(String lastSearchFromHistory){
		(new RecentSearchAccordion()).waitForNumberOfElementsToBeGreater(historyItemsLocation, 0);
		String lastSearchDate = lastSearchFromHistory.replace("Created", "").trim();
		String currentSearchDate = getLastSearchFromHistory().getText().replace("Created", "").trim();
		String currentSearchTitle = getLastSearchFromHistory().getAttribute("title");
		String [] splittedTitle = currentSearchTitle.split("\\s+");
		String currentSearchOwner = splittedTitle[splittedTitle.length - 1];
		
		DateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.ENGLISH);
		Date searchDate01 = null;
		Date searchDate02 = null;
		try {
			searchDate01 = format.parse(lastSearchDate);
			searchDate02 = format.parse(currentSearchDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		boolean isOwnerOk = currentSearchOwner.equals(Configuration.getConfig("recenseo.username"));
		boolean isSearchDateOk = searchDate02.compareTo(searchDate01) > 0;
		
		return isOwnerOk && isSearchDateOk;
	}
	
	public static WebElement getLastSearchFromHistory(){
		return driver.findElements(historyItemsLocation).get(0);
	}
	
	public static void addRecentSearchCriteriaFromHistory(){
		//By searchCriteria = By.cssSelector("#searchForm > div > div:nth-child(2) > div");
		//int numberOfCriterionPresent = driver.findElements(searchCriteria).size();
		getLastSearchFromHistory().click();
		/*(new RecentSearchAccordion()).switchToDefaultFrame();
		(new RecentSearchAccordion()).waitForFrameToLoad(driver, "MAIN");
		(new RecentSearchAccordion()).waitForNumberOfElementsToBeGreater(driver, searchCriteria, numberOfCriterionPresent);*/
		(new RecentSearchAccordion()).waitFor(driver, 5);
	}
	
}
