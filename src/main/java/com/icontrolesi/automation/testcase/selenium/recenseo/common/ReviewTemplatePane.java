package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.TestHelper;

import com.icontrolesi.automation.platform.util.Logger;

public class ReviewTemplatePane implements CommonActions{
	public static WebDriver driver = Driver.getDriver();
	static ReviewTemplatePane reviewTemplate = new ReviewTemplatePane();
	
	public static By saveToDiskBtnLocator = By.id("btnSaveToDB");
	public static By postBtnLocator = By.id("btnDocDone");
	public static By printBtnLocator = By.id("btnPrint");
	public static By tagItemLocator = By.cssSelector("div.pickListContainer li");
	public static By tagItemAnchorLocator = By.cssSelector("div.pickListContainer li > a");
	public static By tagEditBtnLocator = By.cssSelector("button[title='Edit tag']");
	public static By tagWindowCancelBtnLocator = By.id("cancel");
	public static By tagItemPermissionLocator = By.className("tagPermissionSelectFromDialog");
	public static By tagWidgetLocator = By.className("coding-template-tag");
	public static By fileTypeSettingLocator = By.id("prefToggle");
	public static By availableFieldLocator = By.cssSelector("div[id*=field_] > label");
	
	
	/**
	 * 		Print window related locators
	 */
	
	public static By printBatesDropdownLocator = By.id("use_pbat_data");

	public void editDateField(WebDriver driver,SearchPage sp, String dateValue){
		 SearchPage.waitForElement("date", "id");
		 SearchPage.waitForElement("date", "id").clear();
		 SearchPage.waitForElement("date", "id").sendKeys(dateValue);
	}
	
	public static void saveDocument(){
		reviewTemplate.tryClick(saveToDiskBtnLocator);
		reviewTemplate.waitFor(10);
	}
	
	public static void postDocument(){
		reviewTemplate.tryClick(postBtnLocator);
		reviewTemplate.waitFor(10);
	}
	
	public static void openPrintingWindow(){
		reviewTemplate.waitFor(10);
		reviewTemplate.tryClick(printBtnLocator, printBatesDropdownLocator);
	}
	
	
	public void editTextArea(WebDriver driver, SearchPage sp, String textAreaName, String text){
		
		driver.switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		List<WebElement> textAreaNames= reviewTemplate.getElements(By.cssSelector("label"));
		for(int i=0;i<textAreaNames.size();i++){
			if(textAreaNames.get(i).getText().contains(textAreaName)){
				SearchPage.getFollowingSibling(textAreaNames.get(i)).clear();
				Utils.waitShort();
				SearchPage.getFollowingSibling(textAreaNames.get(i)).sendKeys(text);;
			}
		}
	}
	
	
	public static boolean isTagWidgetDisplayed(){
		reviewTemplate.waitForVisibilityOf(tagWidgetLocator);
		String displayProerty = reviewTemplate.getCssValue(tagWidgetLocator, "display");
		
		return displayProerty.equals("block");
	}
	
	public String getValueTextArea(WebDriver driver, SearchPage sp, String textAreaName){
		
		driver.switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		List<WebElement> textAreaNames= reviewTemplate.getElements(By.cssSelector("label"));
		String value="";
		for(int i=0;i<textAreaNames.size();i++){
			
			if(textAreaNames.get(i).getText().contains(textAreaName)){
				
				 value= SearchPage.getFollowingSibling(textAreaNames.get(i)).getAttribute("value");
				 
				
			}
			
			
		}
		
		return value;
		
	}
	
	public void switchTabReviewPane(String tabName){
		reviewTemplate.switchToDefaultFrame();
		reviewTemplate.waitForFrameToLoad(Frame.MAIN_FRAME);  
		
		  List<WebElement> spanParent= reviewTemplate.getElements(By.cssSelector("a[class='ui-tabs-anchor']"));
		  for(int j=0;j<spanParent.size();j++){
			  WebElement spanNode= spanParent.get(j).findElement(By.cssSelector("span"));
			  if(spanNode.getText().equalsIgnoreCase(tabName)){
				  click(spanNode);
			  }
		  }
	  }
	
	
	
	//This method to be used to create a new tag and add that tag to a doc
	public void TagActionsReviewTemplate(WebDriver driver, SearchPage sp) throws InterruptedException{
		  
		  
		  
		//Create and Add a new tag to the document
		  
		  
			click(SearchPage.waitForElement("button[title='New Tag']", "css-selector"));
			//Assign tag name
			Calendar cal = Calendar.getInstance();
			Date currentTime = cal.getTime();
			SearchPage.waitForElement("input[name='tagName']", "css-selector").sendKeys("AutomatedTag"+currentTime);
			
			List<WebElement> spanNodes= reviewTemplate.getElements(By.cssSelector("span[class='ui-button-text']"));
			Thread.sleep(3000);
			
			for(int i=0;i<spanNodes.size();i++){
				
				spanNodes= reviewTemplate.getElements(By.cssSelector("span[class='ui-button-text']"));
				
				if(spanNodes.get(i).isDisplayed()){
					
					if(spanNodes.get(i).getText().equalsIgnoreCase("Create")){
						
						click(spanNodes.get(i));
						break;
						
					}
					
					
				}
				
				
				
			}
			
			Thread.sleep(6000);
			
			//Check the box for the tag
			List<WebElement> tagCheckBox= reviewTemplate.getElements(By.name("chkTag"));
			
			for(int i=0;i<tagCheckBox.size();i++){
				
				
				WebElement tagName= SearchPage.getFollowingSibling(tagCheckBox.get(i));
				System.out.println( Logger.getLineNumber() + tagName.getText());
				if(tagName.getText().equalsIgnoreCase("AutomatedTag"+currentTime)){
					
					click(tagCheckBox.get(i));
					
				}
				
			} 
			
			Thread.sleep(5000);
			
			//Click the Save button
				//click(SearchPage.waitForElement("btnSaveToDB", "id"));
			ReviewTemplatePane.saveDocument();
		  	  
	  }//end function
	
	
	public String[] getMetaDataInfo(){
		waitFor(5);
		String[] metaDataValues=new String[50];
		
		reviewTemplate.switchToDefaultFrame();
		reviewTemplate.waitForFrameToLoad(Frame.MAIN_FRAME); 
		
		WebElement tabsPanel= reviewTemplate.getElement(By.cssSelector("tr[class='ui-widget-header']"));
			List<WebElement> fieldNames= tabsPanel.findElements(By.cssSelector("th"));
			for(int z=0;z<fieldNames.size();z++){
				System.out.println( Logger.getLineNumber() + fieldNames.get(z).getText());
				metaDataValues[z]=fieldNames.get(z).getText();
			}		
		   
		return metaDataValues;
	}
	
	
	
	public void showTags(WebDriver driver, String action){
		//"all" to show all tags
		//"selected" to show only selected tags
		
		if(action.equalsIgnoreCase("all")){
			try{
				reviewTemplate.getElement(By.cssSelector("button[title='Show All Tags']"));
				//Click to show all tags
				click(SearchPage.waitForElement("button[title='Show All Tags']", "css-selector"));
				waitFor(4);
			}catch(NoSuchElementException e){
				//do nothing
			}//end catch
		
		
		}else if(action.equalsIgnoreCase("selected")){
		   try{
				reviewTemplate.getElement(By.cssSelector("button[title='Show Selected Tags']"));
				//Click to show all tags
				click(SearchPage.waitForElement("button[title='Show Selected Tags']", "css-selector"));
				waitFor(4);
			}catch(NoSuchElementException e){
				//do nothing
			}//end catch
		}//end if
	}//end function
	
	
	public static void selectTagWidgetFilter(String tagFilterName){
		By tagFilterLocator = By.className("tagPermissionSelect");
		reviewTemplate.selectFromDrowdownByText(tagFilterLocator, tagFilterName);
		/*reviewTemplate.waitForVisibilityOf(Driver.getDriver(), loadTagMsgLocaor);
		reviewTemplate.waitForInVisibilityOf(Driver.getDriver(), loadTagMsgLocaor);*/
		reviewTemplate.waitFor(Driver.getDriver(), 5);
		
		System.out.println( Logger.getLineNumber() + "Tag loaded for filter '" + tagFilterName + "'... ");		
	}
	
	
	public static List<String> getAppliedTags(){
		List<WebElement> appliedTagList = reviewTemplate.getElements(tagItemAnchorLocator);
		
		return appliedTagList.stream().filter(t -> t.getAttribute("class").contains("checked") && reviewTemplate.getParentOf(t).getAttribute("class").endsWith("parentNode") == false)
						.map(t -> t.getText().trim())
						.collect(Collectors.toList());
	}
	
	public static Set<String> getAppliedTagsAsSet(){
		Set<String> tagSet = new LinkedHashSet<>();
		List<WebElement> appliedTagList = reviewTemplate.getElements(tagItemAnchorLocator);
		
		/*return appliedTagList.stream().filter(t -> t.getAttribute("class").contains("checked") && reviewTemplate.getParentOf(t).getAttribute("class").endsWith("parentNode") == false)
						.sorted().map(t -> t.getText().trim())
						.collect(Collectors.toSet());*/
		
		
		for(WebElement tag : appliedTagList){
			String tagClass = tag.getAttribute("class");
			if(tagClass.contains("jstree-checked")){
				tagSet.add(reviewTemplate.getText(tag));
			}
		}
		
		/*return appliedTagList.stream().filter(t -> t.getAttribute("class").contains("checked"))
				.map(t -> t.getText().trim())
				.collect(Collectors.toSet());*/
		tagSet.remove("");
		return tagSet.stream().sorted().collect(Collectors.toSet());
	}
	
	
	public static void removeTagsFromSelectedDocument(String ... tags){
		for(int i = 0; i < tags.length; i++){
			List<WebElement> tagElems = reviewTemplate.getElements(tagItemAnchorLocator);
			
			for(WebElement tagElem: tagElems){
				if(tagElem.getText().trim().equals(tags[i])){
					tagElem.findElement(By.tagName("i")).click();
					System.out.println( Logger.getLineNumber() + "Tag '" + tags[i] + "' selected...");
					break;
				}
			}
		}
	}
	
	public boolean checkTagSelection(WebDriver driver){
		//This method is to check whether all tags are shown or only selected tags are shown
		
		boolean onlySelected = false;
		//onlySelected=1 means only selected tags are shown and 0 means both selected and unselected tags are shown
		List<WebElement> tagCheckBoxes= reviewTemplate.getElements(By.name("chkTag"));
		for(int i=0;i<tagCheckBoxes.size();i++){
			if(!tagCheckBoxes.get(i).isSelected()){
				System.out.println( Logger.getLineNumber() + "Not selected tag found");
				onlySelected = true;
				break;
			}
		}
		
		return onlySelected;
	}
	
	
	public void addRemoveTag(WebDriver driver, String action, SearchPage sp) throws InterruptedException{
		
		//action can be "add" or "remove"
		
		if(action.equalsIgnoreCase("add")){
			
			
			List<WebElement> tagCheckBoxes= reviewTemplate.getElements(By.name("chkTag"));
			for(int i=0;i<tagCheckBoxes.size();i++){
				
				if(!tagCheckBoxes.get(i).isSelected()){
			        click(tagCheckBoxes.get(i));
					break;
					
				}
				
			}
			ReviewTemplatePane.saveDocument();
			
			
			
		}
		else if(action.equalsIgnoreCase("remove")){
			
			
			List<WebElement> tagCheckBoxes= reviewTemplate.getElements(By.name("chkTag"));
			for(int i=0;i<tagCheckBoxes.size();i++){
				
				if(tagCheckBoxes.get(i).isSelected()){
			        click(tagCheckBoxes.get(i));
					break;
					
				}
				
			}//end for
			
			ReviewTemplatePane.saveDocument();

			
		}//end if
		
		
	}//end function
	
	/**
	 * 
	 * @param driver
	 * @param sp
	 * @param tName Name of tag that needs to be created
	 * @param subTagName Name of sub tag that needs to be created
	 * @param addDocument true if the checkbox needs to be checked
	 * @throws InterruptedException
	 */
	
	//This method to be used when one needs to create a tag and a sub tag under it
	public void createTagAndSubTag(WebDriver driver, SearchPage sp,String tName,String subTagName,boolean addDocument){
		//Create and Add a new tag to the document
		tryClick(By.cssSelector("#grid_toppager_left > table > tbody > tr > td:nth-child(4)"), 20);
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MANAGE_TAG_FRAME);
		new ReviewTemplatePane().waitFor(30);
		clickOnCreateTagFroWidget();
		//Assign tag name
		SearchPage.waitForElement("input[id='tag-editOrSubName']", "css-selector").sendKeys(tName);
		
		tryClick(By.cssSelector("#tag-addEditDialog ~ div > div > button[id='create']"), 2);
		
		hoverOverElement(By.cssSelector("#tagManage-picklist > ul > li > a"));
		tryClick(By.cssSelector("div.hoverDiv[style$='inline-block;'] > button[title='Create sub tag']"), 5);
		
		SearchPage.waitForElement("input[id='tag-editOrSubName']", "css-selector").sendKeys(subTagName);
		
		tryClick(By.cssSelector("#tag-addEditDialog ~ div > div > button[id='create']"), 5);
		
		tryClick(By.id("done"), 10);
		
	}
	
	
	public WebElement renameTag(String tagName){
		List<WebElement> tagCheckBox= Driver.getDriver().findElements(By.name("chkTag"));
		
		for(int i=0;i<tagCheckBox.size();i++){
			WebElement tagElements= SearchPage.getFollowingSibling(Driver.getDriver(), tagCheckBox.get(i));
			System.out.println( Logger.getLineNumber() + tagElements.getText());
			if(tagElements.getText().equalsIgnoreCase(tagName)){
				click(SearchPage.getFollowingSibling(Driver.getDriver(), tagCheckBox.get(i)));
			}
		} 
		
		List<WebElement> buttons = Driver.getDriver().findElements(By.tagName("button"));
		WebElement renameButton = null;
		for(WebElement e : buttons){
			if(e.getAttribute("title").trim().equalsIgnoreCase("Rename Tag")){
				renameButton = e;
				break;
			}
		}
		click(renameButton);
		
		List<WebElement> inputs = Driver.getDriver().findElements(By.tagName("input"));
		WebElement input = null;
		for(WebElement e : inputs){
			if(e.getAttribute("value").trim().equalsIgnoreCase(tagName)){
				input = e;
				break;
			}
		}
		input.clear();
		input.sendKeys("edited");
		input.sendKeys("\n");
		Utils.waitShort();
		
		List<WebElement> allspans = Driver.getDriver().findElements(By.className("tagName"));
		WebElement returnElement = null;
		for(WebElement e : allspans){
			if(e.getText().trim().equalsIgnoreCase("edited")){
				returnElement = e;
				break;
			}
		}
		return returnElement;
	}
	
	
	public void clickOnTag(String tagName){
		//Check the box for the tag
		List<WebElement> tagCheckBox= Driver.getDriver().findElements(By.name("chkTag"));
		
		for(int i=0;i<tagCheckBox.size();i++){
			
			
			WebElement tagElements= SearchPage.getFollowingSibling(Driver.getDriver(), tagCheckBox.get(i));
			System.out.println( Logger.getLineNumber() + tagElements.getText());
			if(tagElements.getText().equalsIgnoreCase(tagName)){
				//clicking on name
				click(SearchPage.getFollowingSibling(Driver.getDriver(), tagCheckBox.get(i)));
				break;
			}
			
		} 
		
		Utils.waitMedium();
	}
	
	
	public void clickTagPermissionButton(){
		WebElement tagPermissionButton = Driver.getDriver().findElement(By.xpath("//button[@title='Tag Permission']"));
		click(tagPermissionButton);
	}


	public void clickTagDeleteButton(){
		WebElement deleteButton = Driver.getDriver().findElement(By.xpath("//button[@title='Delete Tag']"));
		click(deleteButton);
		waitFor(5);
	}

	public boolean tagExists(String tagName){
		  //Check the box for the tag
		  List<WebElement> tagCheckBox= Driver.getDriver().findElements(By.name("chkTag"));
		  
		  for(int i=0;i<tagCheckBox.size();i++){
		   WebElement tagElements= SearchPage.getFollowingSibling(Driver.getDriver(), tagCheckBox.get(i));
		   
		   if(tagElements.getText().equalsIgnoreCase(tagName)){
			   System.out.println( Logger.getLineNumber() + tagElements.getText()); 
			   return true;
		   }
		  } 
		  //Utils.waitMedium();
		  return false;
		 }
	
	/*public void clickDownload() throws InterruptedException{
		Driver.getDriver().switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Driver.getDriver(), Frame.MAIN_FRAME);
		click(SearchPage.waitForElement(Driver.getDriver(), "btnDownload", "id"));
		Utils.waitMedium();
	}*/
	
	
	public static void clickDownload(){
		driver = Driver.getDriver();
		//click(SearchPage.waitForElement(Driver.getDriver(), "btnDownload", "id"));
		reviewTemplate.waitForVisibilityOf(By.id("btnDownload"));
		reviewTemplate.getElement(By.id("btnDownload")).click();
		
		driver.switchTo().frame(Frame.DOWNLOAD_PARENT_FRAME);
		
		//reviewTemplate.waitFor(10);
		
		reviewTemplate.waitForVisibilityOf(By.id("saveToDiskContainer"));
		
		//driver.switchTo().defaultContent();
		//driver.switchTo().frame(Frame.MAIN_FRAME);
	}
	
	public static void openDownloadWindow(){
		reviewTemplate.tryClick(By.id("btnDownload"), 15);
		//Driver.getDriver().switchTo().frame(reviewTemplate.getElement(By.cssSelector("iframe[src='interceptorSingleDoc.jsp']")));
		reviewTemplate.waitForFrameToLoad(reviewTemplate.getElement(By.cssSelector("iframe[src='interceptorSingleDoc.jsp']")));
		reviewTemplate.waitForVisibilityOf(By.id("saveToDiskContainer"));
		reviewTemplate.waitForVisibilityOf(fileTypeSettingLocator);
		System.out.println( Logger.getLineNumber() + "Download window opened from ReviewTemplate pane...");
	}
	
	public static void openFileTypePrefs(){
		reviewTemplate.tryClick(fileTypeSettingLocator);
		Driver.getDriver().switchTo().frame(reviewTemplate.getElement(By.id("fileDownloadPrefFrame")));
		//reviewTemplate.switchToFrame("src", "prepareDownload=true");
		reviewTemplate.waitForVisibilityOf(By.id("fileTypePrefs"));
		System.out.println( Logger.getLineNumber() + "File Type Preference opened from ReviewTemplate pane...");
	}	
	
	public static void closeFileTypePrefs(){
		//reviewTemplate.switchToDefaultFrame();
		Driver.getDriver().switchTo().parentFrame();
			
		reviewTemplate.tryClick(By.id("prefToggle"), fileTypeSettingLocator);
		//reviewTemplate.waitForInVisibilityOf(By.cssSelector("input[value='Submit'][onclick='validate();'][type='button']"));
	}
	
	public static void setDownloadPreferenceAllTo(String preferenceType){
		List<WebElement> downloadPreferenceList = reviewTemplate.getElements(By.cssSelector("#jqg1  tbody  tr[id]"));
		
		System.out.println( Logger.getLineNumber() + "Total: "+downloadPreferenceList.size());
		
		int i = 1;
		for(WebElement pref : downloadPreferenceList){
			WebElement preference = pref.findElement(By.cssSelector("td:nth-child(3)"));
			System.out.println( Logger.getLineNumber() + reviewTemplate.getText(pref.findElement(By.cssSelector("td:nth-child(3)")))+"%$%%$$");
			if(reviewTemplate.getText(preference).equals(preferenceType) == false){
				try{
					preference.click();
				}catch (Exception e) {
					if(e.getMessage().contains("element not visible")){
						JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
						jse.executeScript("arguments[0].click();", preference);
					}
				}
				reviewTemplate.selectFromDrowdownByText(By.id(i + "_downloadPrefName"), preferenceType);
				//reviewTemplate.waitForVisibilityOf(By.id("load_jqg1"));
				reviewTemplate.waitFor(2);
			}
			
			i++;
		}
	}
	
	public static void clickIncludeCompleteDocFamily(){
		reviewTemplate.getElement(By.id("doc_set")).click();
	}
	
	public void clickLastDocumentInCurrentState(){
		click(Utils.waitForElement(Driver.getDriver(), "btnLast", "id"));
	}
	
	//added by Shaheed Sagar
	public void clickNextDocumentInCurrentState(){
		click(Utils.waitForElement(Driver.getDriver(), "btnNext", "id"));
		waitFor(8);
	}
	
	//added by Shaheed Sagar
	public void clickPreviousDocumentInCurrentState(){
		click(Utils.waitForElement(Driver.getDriver(), "btnPrev", "id"));
	}
	
	//added by Shaheed Sagar
	public void clickFirstDocumentInCurrentState(){
		click(Utils.waitForElement(Driver.getDriver(), "btnFirst", "id"));
	}
	
	//added by Shaheed Sagar
	public void switchToDocument(String docId){
		waitFor(10);
		WebElement boxElement = Utils.waitForElement(Driver.getDriver(), "selectDocument", "id");
		
		while(!boxElement.isEnabled()){
			//do nothing
		}
		
		waitFor(5);
		boxElement.clear();
		waitFor(5);
		System.out.println( Logger.getLineNumber() + "Document id to load: "+docId);
		boxElement.sendKeys(docId);
		waitFor(5);
		boxElement.clear();
		waitFor(5);
		boxElement.sendKeys(docId);
		Utils.waitMedium();
		boxElement.sendKeys("\n");
		Utils.waitMedium();
	}
	
	
	
/*	public static void switchToFrameEndsWith(String name){
		List<WebElement> frameList = reviewTemplate.getElements(By.tagName("iframe"));
		WebDriverWait wait = new WebDriverWait(120);
		
		for(WebElement frame : frameList){
			System.out.println( Logger.getLineNumber() + frame.getAttribute("src")+"***************");
			if(frame.getAttribute("src").endsWith(name)){
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "Switched to download frame....");
	}	*/
	
	public void clickOnCreateTagFroWidget(){
		waitForVisibilityOf(By.cssSelector("button[title='Create top level tag']"));
		reviewTemplate.getElement(By.cssSelector("button[title='Create top level tag']")).click();
		waitFor(2);
	}
	
	public static void closePopupWindow(){
		  reviewTemplate.switchToDefaultFrame();
		  reviewTemplate.waitForFrameToLoad(Frame.MAIN_FRAME);
	      List<WebElement> allButtons = reviewTemplate.getElements(By.tagName("button"));
	 	  for(WebElement e : allButtons){
	 		 if(e.isDisplayed()){ 
	 		  if(e.getAttribute("title").equalsIgnoreCase("Close")){
	 			  e.click();
	 			  System.out.println( Logger.getLineNumber() + "Window closed...");
	 			  reviewTemplate.waitFor(2);
	 			  break;
	 		  }
	 		 }
	 	  }
	}
	
	public static void clickEditBtnForTag(){
		if(Configuration.getConfig("selenium.browser").equals("chrome")){
			driver = Driver.getDriver();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			WebElement tagEdit = reviewTemplate.getElement(tagEditBtnLocator);
			jse.executeScript("arguments[0].click();", tagEdit);
			System.out.println( Logger.getLineNumber() + "Element [ " + tagEditBtnLocator.toString() + " ] clicked...");
			reviewTemplate.waitFor(5);
		}else{
			reviewTemplate.tryClick(tagEditBtnLocator, 1);
		}
	}
	
	public static void clickEditBtnForTag(WebElement tagEdit){
		if(Configuration.getConfig("selenium.browser").equals("chrome")){
			driver = Driver.getDriver();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			//WebElement tagEdit = reviewTemplate.getElement(tagEditBtnLocator);
			jse.executeScript("arguments[0].click();", tagEdit);
			System.out.println( Logger.getLineNumber() + "Element [ " + tagEditBtnLocator.toString() + " ] clicked...");
			reviewTemplate.waitFor(5);
		}else{
			reviewTemplate.tryClick(tagEditBtnLocator, 1);
		}
	}
	
	public static void closeTagWidgetEditWindow(){
		reviewTemplate.tryClick(tagWindowCancelBtnLocator, 2);
	}
	
	public static void openEditTagWindowFor(WebElement element){
		reviewTemplate.hoverOverElement(element);
		clickEditBtnForTag(element);
	}
	
	public static void isTagFilterWorkProperly(String expectedPermission){
		List<WebElement> tagList = reviewTemplate.getElements(tagItemLocator);
		
		openEditTagWindowFor(tagList.get(0));
		
		String tagPermissionForFirstElement = reviewTemplate.getSelectedItemFroDropdown(tagItemPermissionLocator);
		
		new TestHelper().softAssert.assertEquals(tagPermissionForFirstElement, expectedPermission);
		
		closeTagWidgetEditWindow();
		
		openEditTagWindowFor(tagList.get(tagList.size() - 1));
		
		String tagPermissionForLastElement = reviewTemplate.getSelectedItemFroDropdown(tagItemPermissionLocator);
		
		new TestHelper().softAssert.assertEquals(tagPermissionForLastElement, expectedPermission);
		
		closeTagWidgetEditWindow();
	}
	
	public static void expandPicklistFor(String picklistName){
		List<WebElement> fieldList = reviewTemplate.getElements(DocView.codingTemplateFieldLocator);
		
		for(WebElement field : fieldList){
			if(field.findElement(By.tagName("label")).getText().equals(picklistName+":")){
				WebElement lessOrMoreBtn = field.findElement(By.cssSelector("a[class*='moreless']"));
				
				if(lessOrMoreBtn.getText().equals("+ Show All")){
					lessOrMoreBtn.click();
					System.out.println( Logger.getLineNumber() + picklistName + " expanded...");
				}else{
					System.out.println( Logger.getLineNumber() + picklistName + " already expanded...");
				}
				
				break;
			}
		}
	}
}
