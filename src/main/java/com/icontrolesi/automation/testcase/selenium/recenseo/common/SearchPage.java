package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AutomationDataCleaner;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

import com.icontrolesi.automation.platform.util.Logger;

public class SearchPage implements CommonActions {
	static WebDriver driver;
	public final static By runSearchButton = By.id("topSearchButton");
	public final static By addListDropdown = By.id("addList");
	public final static By viewDocumentsLink = By.cssSelector("#review > a") ;
	public final static By clearSearchCriteria = By.id("clearButton");
	public final static By userProfileLocator = By.cssSelector("#secondaryMenu > li > span");
	public final static By repoMenuLocator = By.id("repo-menu-header");
	public final static By repoMenuItemLocator = By.cssSelector("#repositories > div > div > div > div > a");
	public final static By currentWorkFlow = By.id("flow-name");
	public final static By sidebarAccordionWrapper = By.id("sidebarAccordion");
	public final static By repoMenu = By.cssSelector("#secondaryMenu > li:nth-child(2) > span");
	public final static By workflowList = By.cssSelector("#workflows > div > div > div > div > a");
	public final static By saveSearchBtnLocator = By.cssSelector("button[title='Save search']");
	public final static By searchBoxLocator = By.cssSelector("#searchForm > div");
	public final static By saveSearchNameLocator = By.id("saveSearchName");
	public final static By saveSearchDialogHeaderLocator = By.cssSelector("div[class$='dialogTitleBar'] > span");
	public final static By savedSearchSelectGroupLocator = By.id("selectGroup");
	public final static By savedSearchCheckGroupLocator = By.id("checkGroup");
	public final static By savedSearchContinueBtnLocator = By.cssSelector("#saveSearchDialog ~ div > div > button:nth-child(2)");
	public final static By sortingCriterionLocator = By.cssSelector("#sortPanel  div select");
	public final static By sortingOrderBtnLocator = By.id("addSortOrder");
	public final static By selectedWorkFlowNameLocator = By.id("flow-name");
	public final static By criterionGroupLocator = By.className("GroupCriterion");
	
	
	public static By userProfileMenuItemLocator = By.cssSelector("#secondaryMenu li:nth-child(1) li a");
	
	public final static By queryDescriptLocator = By.id("queryDescription");
	
	
	public String currentUrl="";
	
	private static final SearchPage searchPage = new SearchPage();

	
	public static void selectRepo(WebDriver driver, String repoName){
		searchPage.switchToDefaultFrame();
		searchPage.getElement(repoMenuLocator).click();
		List<WebElement> repoList = searchPage.getElements(repoMenuItemLocator);
		
		for(WebElement repo : repoList){
			if(repo.getText().trim().startsWith(repoName)){
				repo.click();
				//searchPage.waitUntilPageLoadComplete(driver);
				searchPage.waitForInVisibilityOf(By.id("topSearchButton"));
				searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
				searchPage.waitForVisibilityOf(By.id("topSearchButton"));
				searchPage.switchToDefaultFrame();
				System.out.println( Logger.getLineNumber() + "Repository " + repoName + " selected...");
				break;
			}
		}
	}
	
	public static void  setWorkflow(String workflow){
		SearchPage searchPage = new SearchPage();
		searchPage.waitForVisibilityOf(selectedWorkFlowNameLocator);
		String currentWorkflowName = searchPage.getText(selectedWorkFlowNameLocator);
		if(currentWorkflowName.equalsIgnoreCase(workflow)){
			System.out.println( Logger.getLineNumber() + "\n" + workflow + " already selected...\n");
		}else{
			openUserMenu();
			searchPage.waitFor(1);
			List<WebElement> workflowElements = searchPage.getElements(workflowList);
			for(WebElement workflowElem : workflowElements){
				System.err.println(workflowElem.getText().trim()+"*****");
				if(searchPage.getText(workflowElem).equalsIgnoreCase(workflow)){
					workflowElem.click();
					Driver.getDriver().manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
					System.out.println( Logger.getLineNumber() + "\nWorkflow " + workflow + " selected...\n");
					break; 
				}
			}
		}
		
		searchPage.waitForInVisibilityOf(runSearchButton);
		searchPage.waitForInVisibilityOf(criterionGroupLocator);
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		searchPage.waitForVisibilityOf(runSearchButton);
		searchPage.waitForVisibilityOf(criterionGroupLocator);
		searchPage.waitFor(15);
	}
	
	public static void  setWorkflow(WebDriver driver, String workflow){
		SearchPage searchPage = new SearchPage();
		searchPage.waitForVisibilityOf(selectedWorkFlowNameLocator);
		String currentWorkflowName = searchPage.getElement(selectedWorkFlowNameLocator).getText().trim();
		if(currentWorkflowName.equalsIgnoreCase(workflow)){
			System.out.println( Logger.getLineNumber() + "\n" + workflow + " already selected...\n");
		}else{
			//Click user profile icon
			//searchPage.getElement(userProfileLocator).click();
			openUserMenu();
			searchPage.waitFor(driver, 1);
			List<WebElement> workflowElements = searchPage.getElements(workflowList);
			for(WebElement workflowElem : workflowElements){
				System.err.println(workflowElem.getText().trim()+"*****");
				if(workflowElem.getText().trim().equalsIgnoreCase(workflow)){
					workflowElem.click();
					driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
					System.out.println( Logger.getLineNumber() + "\nWorkflow " + workflow + " selected...\n");
					break; 
				}
			}
		}
		
		searchPage.waitForInVisibilityOf(runSearchButton);
		searchPage.waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		//Frame.setCurrentFrame(Frame.MAIN_FRAME);
		searchPage.waitForVisibilityOf(driver, runSearchButton);
		//searchPage.waitForPageLoad(driver);
		searchPage.waitFor(driver, 15);
	}
	
	
	public static void openDownloadWindow(){
		searchPage.switchToDefaultFrame();
		SearchPage.clickUserMenu();
		searchPage.getElements(userProfileMenuItemLocator).stream()
				  .filter(item -> searchPage.getText(item).equals("Downloads"))
				  .findFirst().get().click();
		
		//Driver.getDriver().switchTo().frame(3);
		searchPage.switchToFrame("src", "download_manager.jsp");
		
		//searchPage.waitForNumberOfElementsToBeGreater(Download.downloadableLinkLocator, 0);
		searchPage.waitForVisibilityOf(By.id("downloadList"));
		searchPage.waitFor(5);
		
		System.out.println( Logger.getLineNumber() + "Download window opened in Search page...");
	}
	
	
	public static void  setWorkflowFromGridView(String workflow){
		searchPage.switchToDefaultFrame();
		SearchPage searchPage = new SearchPage();
		searchPage.waitForVisibilityOf(selectedWorkFlowNameLocator);
		String currentWorkflowName = getCurrentWorkFlow();
		if(currentWorkflowName.equalsIgnoreCase(workflow)){
			System.out.println( Logger.getLineNumber() + "\n" + workflow + " already selected...\n");
		}else{
			openUserMenu();
			List<WebElement> workflowElements = searchPage.getElements(workflowList);
			for(WebElement workflowElem : workflowElements){
				if(workflowElem.getText().trim().equalsIgnoreCase(workflow)){
					workflowElem.click();
					Driver.getDriver().manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
					System.out.println( Logger.getLineNumber() + "\nWorkflow " + workflow + " selected...\n");
					break; 
				}
			}
			
			currentWorkflowName = getCurrentWorkFlow();
			
			if(!currentWorkflowName.equalsIgnoreCase((workflow))){
				System.out.println( Logger.getLineNumber() + "No workflow found with name: " + workflow +"...");
			}
			
			closeUserMenu();
		}
		
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		searchPage.waitForVisibilityOf(CommonActions.bottomMenuInGridView);
		searchPage.waitForNumberOfElementsToBeGreater(DocView.gridRowLocator, 0);
	}
	
	
	public static void  setWorkflowFromGridView(WebDriver driver, String workflow){
		SearchPage searchPage = new SearchPage();
		searchPage.waitForVisibilityOf(selectedWorkFlowNameLocator);
		String currentWorkflowName = getCurrentWorkFlow();
		if(currentWorkflowName.equalsIgnoreCase(workflow)){
			System.out.println( Logger.getLineNumber() + "\n" + workflow + " already selected...\n");
		}else{
			//Click user profile icon
			//searchPage.getElement(userProfileLocator).click();
			openUserMenu();
			List<WebElement> workflowElements = searchPage.getElements(workflowList);
			for(WebElement workflowElem : workflowElements){
				//System.err.println(workflowElem.getText().trim()+"*****");
				if(workflowElem.getText().trim().equalsIgnoreCase(workflow)){
					workflowElem.click();
					driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
					System.out.println( Logger.getLineNumber() + "\nWorkflow " + workflow + " selected...\n");
					break; 
				}
			}
			
			closeUserMenu();
		}
		
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		//Frame.setCurrentFrame(Frame.MAIN_FRAME);
		searchPage.waitForVisibilityOf(CommonActions.bottomMenuInGridView);
	}
	
	public static String getWorkFlow(){
		String workflow="";
		clickUserMenu();
		WebElement divParent= Utils.waitForElement(Driver.getDriver(), "div[class='scrollable-item workflow-item selected'", "css-selector");
		workflow= divParent.findElement(By.cssSelector("a")).getText();
		
		return workflow;
	}
	
	public static String getCurrentWorkFlow(){
		searchPage.switchToDefaultFrame();
		return Driver.getDriver().findElement(currentWorkFlow).getText().trim();
	}
	
	public static void clickUserMenu(){
		searchPage.waitFor(2);
		searchPage.tryClick(userProfileLocator, 1);
	}
	
	public static void openUserMenu(){
		if(searchPage.getCssValue((By.cssSelector("#secondaryMenu > li > ul")), "display").equals("none"))
			searchPage.tryClick(userProfileLocator, 1);
	}
	
	
	public static void closeUserMenu(){
		if(searchPage.getCssValue((By.cssSelector("#secondaryMenu > li > ul")), "display").equals("none") == false)
			searchPage.tryClick(By.tagName("body"));
	}
	
	
	public static void logOut(){
		searchPage.switchToDefaultFrame();
		
		String projectName = Configuration.getConfig(Configuration.PROJECT_NAME);
      
		String appUrl= Configuration.getConfig(projectName + ".url");
		System.out.println( Logger.getLineNumber() + appUrl);
		String stringUrl= appUrl+"mvc/user/logout";
		System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
		Driver.getDriver().get(stringUrl);
	}
	
	public static void logOut(WebDriver driver){
		searchPage.switchToDefaultFrame();
		//driver.get("https://recenseotest.icontrolesi.com/QuickReviewWeb/mvc/user/logout");
		
		String projectName = Configuration.getConfig(Configuration.PROJECT_NAME);
      
		String appUrl= Configuration.getConfig(projectName + ".url");
		//driver.get(appUrl);
		System.out.println( Logger.getLineNumber() + appUrl);
		String stringUrl= appUrl+"mvc/user/logout";
		System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
		driver.get(stringUrl);
	}
	
	
	
	public static void clickAccordion(WebDriver driver, String AccordionName) {
		new SearchPage().waitForPageLoad(driver);
		searchPage.getElement(By.id(AccordionName)).click();
		new SearchPage().waitForAttributeIn(By.id(AccordionName), "aria-expanded", "true");
		new SearchPage().waitFor(10);
	}

	
	public String checkValueOfSelectBox(String elementMarkUpName) {
		return searchPage.getSelectedItemFroDropdown(By.id(elementMarkUpName));
	}
	
	public String checkValueOfSelectBox(WebDriver driver, String elementMarkUpName) {

		// searchPage.switchToDefaultFrame();
		Select comboBox = new Select(searchPage.getElement(By.id(elementMarkUpName)));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		System.out.println( Logger.getLineNumber() + "Selected combo value: " + selectedComboValue);

		return selectedComboValue;
	}
	
	
	public String getValueOfSelectedTeam(WebDriver driver, By element) {
		Select comboBox = new Select(searchPage.getElement(element));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText();
		System.out.println( Logger.getLineNumber() + "Selected combo value: " + selectedComboValue);

		return selectedComboValue;
	}
	
	public static void waitForFrameLoad(String frameName) {
		WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 120);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		System.out.println( Logger.getLineNumber() + "Frame switched to " + frameName + "...");
	}

	public static void waitForFrameLoad(WebDriver driver, String frameName) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		System.out.println( Logger.getLineNumber() + "Frame switched to " + frameName + "...");
	}
	
	public static WebElement waitForElement(String markUp, String selectorType) {
		try {
			switch (selectorType) {
			case "id":
				WebElement element=  ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.id(markUp))));
				while(!element.isEnabled()){

				}
				return element;
			case "class-name":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.className(markUp))));
			case "css-selector":
				/*return ((new WebDriverWait(driver, 40))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
								*/
				element=  ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
				return element;
			 case "x-path":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.xpath(markUp))));
			case "name":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.name(markUp))));
			
			case "tag":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.tagName(markUp))));
			}
			System.out.println( Logger.getLineNumber() + "Invalid selectorType");
			return null;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + markUp + " not found");
			return null;
		}

	}

	public static WebElement waitForElement(WebDriver driver, String markUp,
			String selectorType) {
		try {
			switch (selectorType) {
			case "id":
				WebElement element=  ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.id(markUp))));
				while(!element.isEnabled()){

				}
				return element;
			case "class-name":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.className(markUp))));
			case "css-selector":
				/*return ((new WebDriverWait(driver, 40))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
								*/
				element=  ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
				return element;
			 case "x-path":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.xpath(markUp))));
			case "name":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.name(markUp))));
			
			case "tag":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.tagName(markUp))));
			}
			System.out.println( Logger.getLineNumber() + "Invalid selectorType");
			return null;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + markUp + " not found");
			return null;
		}

	}
	
	
	public static void waitForCompletion(){
		searchPage.waitFor(12);
		
		try{
			WebElement messageContainer= SearchPage.waitForElement("messageContainer", "id");
			while(messageContainer.isDisplayed()){
        	}
		}catch(Exception e){
		}
	}
	
	
	//This method is necessary for making script wait till an action is complete
	public static void waitForCompletion(WebDriver driver, SearchPage sp){
		searchPage.waitFor(12);
		
		try{
			WebElement messageContainer= SearchPage.waitForElement(driver, "messageContainer", "id");
			while(messageContainer.isDisplayed()){
        	}
		}catch(Exception e){
		}
	}
	
	
	public static void addSearchCriteria(String searchCriteria){
		searchPage.selectFromDrowdownByText(addListDropdown, searchCriteria);
		System.out.println( Logger.getLineNumber() + "Criterion '" + searchCriteria + "' selected...");
	}
	
	
	/*public static void setSelectBox(String elementMarkUpName, String selectorType, String text) {
		WebElement selectbox = searchPage.getElement(By.cssSelector(elementMarkUpName));
			
		Select select = new Select(selectbox);
		select.selectByVisibleText(text);
	}*/
	
	
	public static void setSelectBox(By element, String text) {
		searchPage.selectFromDrowdownByText(element, text);
	}

	public static void setSelectBox(WebDriver driver, String elementMarkUpName, String selectorType, String text) {
		WebElement selectbox = searchPage.getElement(By.cssSelector(elementMarkUpName));
			
		Select select = new Select(selectbox);
		select.selectByVisibleText(text);
	}
	
	public static void setSelectBox(WebDriver driver, By element, String text) {
		new SearchPage().waitForVisibilityOf(driver, element);
		Select select = new Select(searchPage.getElement(element));
		select.selectByVisibleText(text);
	}	
	
	public static void setSelectBox(WebDriver driver, String elementMarkUpName, String selectorType, int position) {
		WebElement selectbox = searchPage.getElement(By.cssSelector(elementMarkUpName));
					
		Select select = new Select(selectbox);
		//System.out.println( Logger.getLineNumber() + select.getOptions());
		select.selectByIndex(position);
	}

	public String[] getGridCountString(WebDriver driver) {

		// Get string containing doc count on page 5
		String docCountString = searchPage.getElement(
				By.cssSelector("div[class='ui-paging-info']")).getText();
		String splitDocCountString[] = docCountString.split("\\s+");
		return splitDocCountString;

	}
	
	public static WebElement getParent(WebElement element){
		WebElement parentNode = element.findElement(By.xpath("parent::*"));
		
		return parentNode;
		
	}
	
	public static WebElement getParent(WebDriver driver, WebElement element){
		WebElement parentNode= element.findElement(By.xpath("parent::*"));
		
		return parentNode;
		
	}
	
	
	public static WebElement getPrecedingSibling(WebElement element){
		WebElement precedingSibling= element.findElement(By.xpath("preceding-sibling::*"));
		
		return precedingSibling;
	}
	
	
	public static WebElement getPrecedingSibling(WebDriver driver, WebElement element){
		
		WebElement precedingSibling= element.findElement(By.xpath("preceding-sibling::*"));
		
		return precedingSibling;
		
	}
	
	
	 public static WebElement getFollowingSibling(WebElement element){
			WebElement followingSibling= element.findElement(By.xpath("following-sibling::*"));
			return followingSibling;
		}
	
	
    public static WebElement getFollowingSibling(WebDriver driver, WebElement element){
		
    	
		WebElement followingSibling= element.findElement(By.xpath("following-sibling::*"));
		
		return followingSibling;
		
	}
	
	
	
	public List<WebElement> getAllChildren(WebDriver driver, WebElement element){
		List<WebElement> children= element.findElements(By.xpath(".//*"));
		
		return children;
	}
	
	
	public WebElement findAncestor(WebDriver driver, String markup){
        WebElement liParent= searchPage.getElement(By.xpath("//span[@class='open']/ancestor::li[contains(@class,'assignment visible')]"));
		return liParent;
	}
	
	
	public void OpenReviewAssignment(WebDriver driver){
		searchPage.getElement(By.cssSelector("button[onclick='openAssignments()']")).click();
		waitFor(driver, 15);
	}
	
	
	public void tabSwitch(WebDriver driver, String tabMarkup, SearchPage sp){
		click(SearchPage.waitForElement(driver, tabMarkup, "css-selector"));
		waitFor(driver, 2);
	}
	
	
public void clickOnTag(WebDriver driver,String tagName){
	click(Utils.waitForElement(driver, "category", "id"));
	
	List<WebElement>Tags= searchPage.getElements(By.cssSelector("span[class='tagName']"));
	
	for(int i=0;i<Tags.size();i++){
		
		WebElement parentSpan = SearchPage.getParent(driver, Tags.get(i));
		WebElement parentLi = SearchPage.getParent(driver, parentSpan);
		try{
			
			WebElement divElement = parentLi.findElement(By.tagName("div"));
			click(divElement);
		}
		catch(Exception e){
			
			
			
		}
		
		if(Tags.get(i).getText().equalsIgnoreCase(tagName)){
			
			click(Tags.get(i));
			waitFor(driver, 3);
			break;
		}
		
		
	}
}

	public static String getCriterionField(int fieldPosition){
		List<WebElement> fields = searchPage.getElements(By.cssSelector(".CriterionField span"));
		return searchPage.getText(fields.get(fieldPosition));
	}

	public static String getOperatorValue(int OperatorNumber){
		List<WebElement> operators = searchPage.getElements(By.cssSelector("select.CriterionOperator:not([style='display: none;'])"));
		return searchPage.getSelectedItemFroDropdown(operators.get(OperatorNumber));
	}
	
	
	public static String getCriterionValue(int criterionPosition){
		List<WebElement> operators = searchPage.getElements(By.cssSelector("input:not([style$='none;'])[class*='CriterionValue']"));
		return searchPage.getText(operators.get(criterionPosition));
	}

	public static void setOperatorValue(int OperatorNumber, String selectionText){
		List<WebElement> operators = searchPage.getElements(By.cssSelector("select.CriterionOperator:not([style='display: none;'])"));
		new Select(operators.get(OperatorNumber)).selectByVisibleText(selectionText);
		searchPage.waitFor(1);
	}

	
	public static void setCriterionValue(int criterionNum, String Text){
		List<WebElement> criterionFields = searchPage.getElements(By.cssSelector("input:not([style$='none;'])[class*='CriterionValue']"));
		criterionFields.get(criterionNum).clear();
		searchPage.editText(criterionFields.get(criterionNum), Text);
	}
	
	
     public static void setCriterionValueTextArea(int criterionNum, String Text){
		List<WebElement> criterionFields= searchPage.getElements(By.cssSelector("textarea[class*='CriterionValue']"));
		criterionFields.get(criterionNum).clear();
		criterionFields.get(criterionNum).sendKeys(Text);
	}
     
     
    public static void setCriterionValueTextArea(int criterionNum, String [] Text){
 		List<WebElement> criterionFields= searchPage.getElements(By.cssSelector("textarea[class*='CriterionValue']"));
 		criterionFields.get(criterionNum).clear();
 		criterionFields.get(criterionNum).sendKeys(Text);
 	}
    
    
    //This method clicks the link to open the interface for changing password
    public static void openChangePassword(){
   	 	searchPage.switchToDefaultFrame();
        clickUserMenu();
        searchPage.getElement(By.cssSelector("a[href='mvc/changePassword']")).click();
     }	
	
     //This method clicks the link to open the interface for changing password
     public static void openChangePassword(WebDriver driver){
    	 searchPage.switchToDefaultFrame();
         clickUserMenu();
         searchPage.getElement(By.cssSelector("a[href='mvc/changePassword']")).click();
      }
     
     
     public static void setSortCriterion(){
    	 searchPage.tryClick(sortingOrderBtnLocator);
    	 System.out.println( Logger.getLineNumber() + "Sort criterion added...");
     }
     
     public static void setSortCriterionValue(int criterionPosition, String criterionValue){
    	 int pos = 2 * criterionPosition - 2;
    	 searchPage.selectFromDrowdownByText(searchPage.getElements(sortingCriterionLocator).get(pos), criterionValue);
    	 System.out.println( Logger.getLineNumber() + "Sort Criterion values is set to '" + criterionValue +"'...");
     }
     
     public static void setSortCriterionOrder(int criterionPosition, String criterionSortingOrder){
    	 int pos = 2 * criterionPosition - 1;
    	 searchPage.selectFromDrowdownByText(searchPage.getElements(sortingCriterionLocator).get(pos), criterionSortingOrder);
    	 System.out.println( Logger.getLineNumber() + "Sort Criterion order is set to '" + criterionSortingOrder +"'...");
     }
     
     
     public static String getSortCriterionOperatorValue(int criterionPosition){
    	 int pos = 2 * criterionPosition - 2;
    	 return searchPage.getSelectedItemFroDropdown(searchPage.getElements(sortingCriterionLocator).get(pos));
     }
     
     public static String getSortCriterionOrderingValue(int criterionPosition){
    	 int pos = 2 * criterionPosition - 1;
    	 return searchPage.getSelectedItemFroDropdown(searchPage.getElements(sortingCriterionLocator).get(pos));
     }
	
	public void clickCloseButton(WebDriver driver, SearchPage sp){
		
		
		List<WebElement> spanNodes= searchPage.getElements(By.cssSelector("span[class='ui-button-text']"));
		for(int i=0;i<spanNodes.size();i++){
			
			if(spanNodes.get(i).isDisplayed()){
				
				if(spanNodes.get(i).getText().equalsIgnoreCase("Close")){
				
				WebElement closeButton= SearchPage.getParent(driver, spanNodes.get(i));
				click(closeButton);
				waitFor(driver, 5);
				}
				
			}
			
			
		}
		
	}
	
	
	public void addSortCondition(String sortName, String type){
		searchPage.switchToDefaultFrame();
		SearchPage.waitForFrameLoad(driver, Frame.MAIN_FRAME);
		
		WebElement sortButton = Utils.waitForElement(driver, "addSortOrder", "id");
		click(sortButton);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    
		WebElement divElement = Utils.waitForElement(driver, "div[class='ui-helper-reset ui-widget-content ui-corner-bottom']", "css-selector");
		//List<WebElement> allDivs = divElement.findElements(By.tagName("div"));
		List<WebElement> allDivs = divElement.findElements(By.cssSelector("div"));
		
		//WebElement lastSortElement = allDivs.get((allDivs.size()-1));
		//List<WebElement> allSelects = lastSortElement.findElements(By.tagName("select"));
		
		List<WebElement> selectNodes= allDivs.get(0).findElements(By.cssSelector("select"));
		
		//Select firstSelect = new Select(allSelects.get(0));
		//Select secondSelect = new Select(allSelects.get(1));
		
		new Select(selectNodes.get(0)).selectByVisibleText(sortName);
		new Select(selectNodes.get(1)).selectByVisibleText(type);
		
		//firstSelect.selectByVisibleText(sortName);
		//secondSelect.selectByValue(type);
	}
	
	//added By Shaheed Sagar
	public void clickIncludeDocFamilyCheckBox(){
		click(Utils.waitForElement(driver, "includeDocSet", "id"));
	}
	
	
	/**
	 * To open Review Activity call like openReports(0)
	 * indexing starts from 0
	 * @param subMenuNumber
	 * @throws InterruptedException
	 */
	public void openReports(int subMenuNumber){
		searchPage.switchToDefaultFrame();
		click(Utils.waitForElement(driver, "reports", "id"));
		WebElement ulElement = Utils.waitForElement(driver, "ul", "tag");
		List<WebElement> allLis = ulElement.findElements(By.tagName("li"));
		click(allLis.get(subMenuNumber));
		Utils.waitMedium();
		//driver.switchTo().frame(searchPage.getElement(By.cssSelector("iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/mvc/reports']")));
	
		Utils.setCurrentURL();
		String string= "iframe[src*='" + Utils.getCurrentURL() + "reports']";
		System.out.println( Logger.getLineNumber() + "String in selector: "+string);
		driver.switchTo().frame(searchPage.getElement(By.cssSelector(string)));

	}
	
	
	
	/*public void logout(){
		//click(Utils.waitForElement(driver,"ui-id-21" ,"id"));
		//click(Utils.waitForElement(driver,"secondaryMenu" ,"id"));
		driver = Driver.getDriver();
		searchPage.switchToDefaultFrame();
		clickUserMenu();
		waitFor(driver, 10);
		click(Utils.waitForElement(driver,"logout" ,"id"));
		Utils.waitShort();
	}*/
	
	
	public static void logout(){
		driver = Driver.getDriver();
		searchPage.switchToDefaultFrame();
		clickUserMenu();
		searchPage.tryClick(By.id("logout"), By.id("l_button"));
		System.out.println( Logger.getLineNumber() + "System is Logged out from Recenseo...");
	}
	
	
	public void clickOnReviewAssignments(){
		click(Utils.waitForElement(driver, "queue", "id"));
	}
	
	
	/**
	 * 
	 * @param assignmentName
	 * @param filter ->send empty string if not needed
	 * @throws InterruptedException 
	 */
	public void findReviewAssignments(String assignmentName,String filter){
		click(Utils.waitForElement(driver, "ui-id-3", "id"));
		Utils.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);
		if(!filter.equals("")){
			Select select = new Select(Utils.waitForElement(driver, "filterOptions", "id"));
			select.selectByVisibleText(filter);
		}
		WebElement divElement = Utils.waitForElement(driver, "actionFindAssignments", "id");
		click(divElement.findElement(By.tagName("button")));
		Utils.waitShort();
	}
	
	
	/**
	 * 
	 * @param number -> send the number where the assignment is located.
	 * 					Starts from 0.
	 */
	public void checkReviewAssignment(int number){
		WebElement ulElement = Utils.waitForElement(driver, "assignmentList", "id");
		List<WebElement> allAssignments = ulElement.findElements(By.tagName("li"));
		allAssignments.get(number).findElement(By.tagName("input")).click();
	}
	
	
	public void clickReviewAssignmentOpen(){
		click(Utils.waitForElement(driver, "ui-id-1", "id"));
		WebElement divElement = Utils.waitForElement(driver, "actionReview", "id");
		click(divElement.findElements(By.tagName("button")).get(1));
		Utils.waitMedium();
	}
	
	
	public void clickConceptReview(){
		click(Utils.waitForElement(driver, "cluster", "id"));
		Utils.waitShort();
		driver.switchTo().frame(Utils.waitForElement(driver, "clusterFrame", "id"));
	}
	

	/**
	 * 
	 * @param number -> starts from 0
	 * Must call clickConceptReview Method before this for frame switching
	 */
	public void checkConceptReview(int number){
		
		WebElement divElement = Utils.waitForElement(driver, "treeViewContainer filetree treeview", "css-selector");
		List<WebElement> allLis = divElement.findElements(By.tagName("li"));
		
		allLis.get(number).findElement(By.tagName("span")).findElement(By.tagName("input")).click();
	}
	
	
	public void clickOpenOnConceptReview(){
		WebElement divElement = searchPage.getElement(By.id("cluster-actionList"));
		List<WebElement> allButtons = divElement.findElements(By.tagName("button"));
		click(allButtons.get(1));
	}
	
	public void clickTags(){
		click(Utils.waitForElement(driver, "category", "id"));
		Utils.waitShort();
		driver.switchTo().frame(Utils.waitForElement(driver, "tagFrame", "id"));
		Utils.waitMedium();
	}
	
	
	public void findTag(String tagName){
		click(Utils.waitForElement(driver, "ui-id-1", "id"));
		Utils.waitForElement(driver, "findTag", "id").sendKeys(tagName);
		click(Utils.waitForElement(driver, "tags-search", "id"));
		SearchPage.waitForCompletion(driver, new SearchPage());
	}
	
	
	public void checkTag(String tagName){
		List<WebElement> allTags = searchPage.getElements(By.className("tagName"));
		for(WebElement e : allTags){
			if(e.getText().equals(tagName)){
				SearchPage.getPrecedingSibling(driver, e).click();
				break;
			}
		}
		Utils.waitShort();
	}

	public void clickOpenOnTags(){
		click(Utils.waitForElement(driver, "ui-id-4", "id"));
		click(Utils.waitForElement(driver, "tags-open", "id"));
	}
	
	
	public void editTag(String tagName,String accesibleBy){
		click(Utils.waitForElement(driver, "ui-id-3", "id"));
		click(Utils.waitForElement(driver, "tags-edit", "id"));
		waitForCompletion(driver, this);
		Utils.waitForElement(driver, "tags-editName", "id").sendKeys(tagName);
		Select accecibleSelect = new Select(Utils.waitForElement(driver, "tags-editPermission", "id"));
		if(!accesibleBy.equals("")){
			accecibleSelect.selectByVisibleText(accesibleBy);
		}
		click(Utils.waitForElement(driver, "tags-rename", "id"));
		waitForCompletion(driver, this);
		Utils.waitForAlertAndAccept();
	}
	
	
	public void clickClearButton(){
		click(Utils.waitForElement(driver, "clearButton", "id"));
		waitFor(driver, 5);
	}
	
	
	public void saveSearch(String searchName,String accecibleBy){
		WebElement andElement = Utils.waitForElement(driver, "MatchOperator", "class-name");
        new Actions(driver).moveToElement(andElement).perform();
        List<WebElement> allButtons = searchPage.getElements(By.cssSelector("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only"));
        for(WebElement e : allButtons){
        	if(e.getAttribute("title").equals("Save search")){
        		click(e);
        		break;
        	}
        }
        Utils.waitMedium();
		Utils.waitForElement(driver, "saveSearchName", "id").sendKeys(searchName);;
        if(accecibleBy.equalsIgnoreCase("Private")){
        	click(Utils.waitForElement(driver, "privateSaveSearchPermission", "id"));
        }else if(accecibleBy.equalsIgnoreCase("Team members")){
        	click(Utils.waitForElement(driver, "teamSaveSearchPermission", "id"));
        }else if(accecibleBy.equalsIgnoreCase("Everyone")){
        	click(Utils.waitForElement(driver, "everyoneSaveSearchPermission", "id"));
        }
        
        List<WebElement> allSpans = searchPage.getElements(By.cssSelector("ui-button-text"));
        for(WebElement e : allSpans){
        	if(e.getText().equals("Continue")){
        		click(e);
        		break;
        	}
        }
	}
	
	
	public boolean doesSavedSearchExists(String searchName){
		WebElement divElement = Utils.waitForElement(driver, "savedSearches", "id");
		WebElement ulElement = divElement.findElement(By.tagName("ul"));
		List<WebElement> allSearches = ulElement.findElements(By.tagName("li"));
		for(WebElement e : allSearches){
			if(e.getText().equals(searchName)){
				return true;
			}
		}
		return false;
	}
	
	
	public void clickSavedSearch(String searchName){
		Utils.waitMedium();
		WebElement divElement = Utils.waitForElement(driver, "savedSearches", "id");
		WebElement ulElement = divElement.findElement(By.tagName("ul"));
		List<WebElement> allSearches = ulElement.findElements(By.tagName("li"));
		for(WebElement e : allSearches){
			System.out.println( Logger.getLineNumber() + e.getText());
			if(e.getText().equals(searchName)){
				click(e);
				break;
			}
		}
	}
	

	public void clickOnSavedSearch(){
		click(Utils.waitForElement(driver, "history", "id"));
	}
	
	
	public void clickSelectedSavedSearch(String savedSearchName){
		
		searchPage.switchToDefaultFrame();
		Utils.waitForFrameLoad(driver, Frame.MAIN_FRAME);
		clickOnSavedSearch();
		Utils.waitMedium();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	   
	   //Get the title of the latest search made
	   	//find the div element
	   WebElement divSavedSearches= searchPage.getElement(By.id("savedSearches"));
	   	//find the ul element under the div
	   WebElement ulSavedSearches= divSavedSearches.findElement(By.cssSelector("ul"));
	    //find the li elements under the ul element
	   List<WebElement>liElements= ulSavedSearches.findElements(By.cssSelector("li"));
	
	   
	   
	   for (int i=0;i<liElements.size();i++){
		   
		   if(liElements.get(i).getText().equalsIgnoreCase(savedSearchName)){
			   
			   click(liElements.get(i));
			   break;
			   
		   }//end if
		   
	   }//end for
	   
	   Utils.waitMedium();
	}
	

	public void clickOpenInReviewAssignment(WebDriver driver){
		WebElement divElement = Utils.waitForElement(driver, "actionReview", "id");
		click(divElement.findElements(By.tagName("button")).get(1));
	}
	
	
	//This method is meant for checking all check-boxes under the "Filters" tab for Review Assignments 
	public void checkAllFilterOptions(){
		//Check if filter tab is already selected
		driver = Driver.getDriver();
		WebElement filterTab= searchPage.getElement(By.cssSelector("a[href='#actionFilterAssignments']"));
		WebElement parentNode=getParent(driver, filterTab);
		
		if(parentNode.getAttribute("aria-selected").equalsIgnoreCase("false")){
			switchTab(driver, "a[href='#actionFilterAssignments']");
		}
		
		
		//Uncheck all checkboxes under "Users"
		WebElement checkBoxMine= waitForElement(driver, "input[value='USER']","css-selector");
		WebElement checkBoxTeam= waitForElement(driver, "input[value='TEAM']","css-selector");
		WebElement checkBoxUnassign= waitForElement(driver, "input[value='UNASSIGNED']","css-selector");

		
      if(!checkBoxMine.isSelected()){
			checkBoxMine.click();
			searchPage.waitFor(driver, 3);
      }
		
		
       if(!checkBoxTeam.isSelected()){
			
    	   checkBoxTeam.click();
    	   waitFor(driver, 3);

			
		}
       
       
       if(!checkBoxUnassign.isSelected()){
		   checkBoxUnassign.click();
		   waitFor(driver, 3);
		}
	}
	
		
	public void switchTab(WebDriver driver, String tabMarkup){
			click(waitForElement(driver, tabMarkup, "css-selector"));
			waitFor(driver, 2);
	}
		
	
	public void removeSortByItem() throws InterruptedException{
		  searchPage.switchToDefaultFrame();
		  //List<WebElement> allButtons = searchPage.getElements(By.tagName("button"));
		  List<WebElement> allCloseButtons = searchPage.getElements(By.cssSelector("button[title='Close']"));
		  
		  for(WebElement e : allCloseButtons){
			  if(e.isDisplayed()){
				  click(e);
				  Utils.waitLong();
				  break;
			  }
		  }
		  
		  Utils.waitShort();
	}
	
	public String getDocCountOfAssignment(){
		
		//This method will return the number of docs in the assignment that has just been searched for
		List<WebElement> spanNodes= searchPage.getElements(By.cssSelector("span[class='open']"));
		String docCount="";
		
		for(int i=0;i<spanNodes.size();i++){
			
			if(spanNodes.get(i).isDisplayed()){
				
				//get the children span nodes of the element
				List<WebElement> spanChildren= spanNodes.get(i).findElements(By.cssSelector("span"));
				//The second child has the total number of docs in the assignment
				docCount= spanChildren.get(1).getText();
				
				
			}
			
		}
		
		return docCount;
	}
	
	
	@Override
	public void click(WebElement element) {
		// TODO Auto-generated method stub
		CommonActions.super.click(element);
	}

	public static void goToDashboard(){
		//SearchPage searchPage = new SearchPage();
		driver = Driver.getDriver();
		searchPage.switchToDefaultFrame();
		searchPage.waitForVisibilityOf(By.cssSelector("a[href='search']"));
		searchPage.tryClick(By.cssSelector("a[href='search']"));
		
		searchPage.waitForPageLoad();
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		searchPage.waitForVisibilityOf(criterionGroupLocator);
		searchPage.switchToDefaultFrame();
		System.out.println( Logger.getLineNumber() + "\nDashboard loaded...\n");
	}
	
	public static void goToDashboard(WebDriver driver){
		searchPage.switchToDefaultFrame();
		WebElement dashboard= Utils.waitForElement("a[href='search']", "css-selector");
		dashboard.click();
		
		new SearchPage().waitForPageLoad();
	}
	
	
	public static void gotoViewDocuments(){
		searchPage.switchToDefaultFrame();
		searchPage.tryClick(viewDocumentsLink);
		searchPage.waitForPageLoad();
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		searchPage.waitForVisibilityOf(By.id("menu"));
		searchPage.waitForNumberOfElementsToBeGreater(DocView.gridRowLocator, 0);
		
		System.out.println( Logger.getLineNumber() + "\nView Documents page loaded...");
	}


	public static void hideSearchGroup(int groupNum){
		searchPage.switchToDefaultFrame();
		Utils.waitForFrameLoad(Frame.MAIN_FRAME);;
		List<WebElement> hideButtonList= searchPage.getElements(By.cssSelector("button[role='button'][title='Hide']"));
		hideButtonList.get(groupNum).click();
	}

	
	public static void clearSearchCriteria(){
		searchPage.tryClick(clearSearchCriteria);
		searchPage.waitFor(1);
	}
	
	public static boolean isSearchPage(){
		return (searchPage.getElements(runSearchButton).size() == 1) &&  searchPage.getElements(sidebarAccordionWrapper).size() == 1;
	}
	
	public static boolean isDocViewPage(){
		return true;
	}
	
	
	public static void clickRepoMenu(){
		searchPage.hoverOverElement(repoMenu);
	}
	
	public static void clickRepo(String repoName){
		List<WebElement> repoList = searchPage.getElements(By.cssSelector("#repositories > div > div > div > div > a"));
		for(WebElement repo : repoList){
			if(repo.getText().trim().startsWith(repoName)){
				repo.click();
				searchPage.waitForInVisibilityOf(runSearchButton);
				searchPage.waitForInVisibilityOf(runSearchButton);
				searchPage.waitForPageLoad();
				System.out.println( Logger.getLineNumber() + "Repository '" + repoName + "' selected...");
				break;
			}
		}
	}
	
	/**
	 * 	Create a Saved Search with a specified name while there will be a search criterion added
	 */
	public static void createSavedSearch(String savedSearchName, String savedSearchPermission, String groupForSavedSearch){
		System.out.println( Logger.getLineNumber() + "Creating SavedSearch...");
        searchPage.hoverOverElement(searchBoxLocator);
        searchPage.tryClick(saveSearchBtnLocator, saveSearchNameLocator);
        searchPage.editText(saveSearchNameLocator, savedSearchName);
        
        if(savedSearchPermission.equals("") == false)
        	selectPermissionForSavedSearchInPopup(savedSearchPermission);
        if(groupForSavedSearch.equals("") == false){
        	WebElement selectGroupCheckBox = searchPage.getElement(savedSearchCheckGroupLocator);
        	if(selectGroupCheckBox.isSelected()){
        		System.out.println( Logger.getLineNumber() + "Checkbox for 'Save the search in a group' is already selected...");
        	}else{
        		selectGroupCheckBox.click();
        		System.out.println( Logger.getLineNumber() + "Checkbox for 'Save the search in a group' is selected...");
        	}
        	searchPage.waitFor(2);
        	searchPage.selectFromDrowdownByText(savedSearchSelectGroupLocator, groupForSavedSearch);
        }
        	
        
        searchPage.tryClick(savedSearchContinueBtnLocator, 30);
        
        AutomationDataCleaner.listOfSavedSearchesCreated.add(savedSearchName);
        
        System.out.println( Logger.getLineNumber() + "SavedSearch created with the name '" + savedSearchName + "' ...");
	}
	
	public static void selectPermissionForSavedSearchInPopup(String savedSearchPermission){
		List<WebElement> permissionList = searchPage.getElements(By.cssSelector("#saveSearchPermissionWarning ~ div:nth-child(4) > ul > li"));
		
		for(WebElement permission : permissionList){
			if(permission.getText().trim().startsWith(savedSearchPermission)){
				permission.findElement(By.tagName("input")).click();
				System.out.println( Logger.getLineNumber() + "Permission '" + savedSearchPermission + "' selected for SavedSearch...");
				break;
			}
		}
	}
	
	public static void selectCheckGroupForSavedSearch(){
		if(searchPage.isElementChecked(savedSearchCheckGroupLocator)){
			System.out.println( Logger.getLineNumber() + "The check box 'Save the search in a group' already selected...");
		}else{
			searchPage.tryClick(savedSearchCheckGroupLocator);
			System.out.println( Logger.getLineNumber() + "The check box 'Save the search in a group' selected...");
		}
	}
	
	public static int getTotalGadgetsCount(){
		return searchPage.getTotalElementCount(By.cssSelector("#gadgets > div.gadgets > div"));
	}
	
	public static boolean isGadgetContainerContainsGadgets(){
		return getTotalGadgetsCount() > 0;
	}
	
	public static boolean isGadgetExistWithName(String gadgetName){
		searchPage.switchToDefaultFrame();
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		
		List<WebElement> gadgetsNameList = searchPage.getElements(By.cssSelector(".title"));
		
		for(WebElement gadget : gadgetsNameList){
			System.out.println( Logger.getLineNumber() + gadget.getText()+"*&*&*");
			if(gadget.getText().trim().equals(gadgetName))
				return true;
		}
		
		return false;
	}
	
	public static void createGadgetByName(String gadgetName){
		if(isGadgetExistWithName(gadgetName)){
			System.out.println( Logger.getLineNumber() + "Gadget with name '" + gadgetName + "' already exists...");
		}else{
			GeneralReport.openFileTypesReport();
			searchPage.tryClick(GeneralReport.exportOrSaveBtnLocator, 1);
			searchPage.tryClick(By.xpath("//*[text()='Save to Dashboard']"), By.id("gadgetTitle"));
			searchPage.editText(By.id("gadgetTitle"), gadgetName);
			searchPage.tryClick(By.xpath("//*[text()='Save']"), 1);
			goToDashboard();
		}
	}
	
	public static void openGadgetByName(String gadgetName){
		searchPage.switchToDefaultFrame();
		searchPage.waitForFrameToLoad(Frame.MAIN_FRAME);
		if(isGadgetContainerContainsGadgets()){
			searchPage.getElements(By.cssSelector("div[id^='gadget-']"))
			.stream().filter(gadget -> searchPage.getText(gadget.findElement(By.cssSelector(" span.title"))).equals(gadgetName))
			.findFirst().get().findElement(By.cssSelector("div div.header-right a span[class*='enlarge']")).click();
			
			searchPage.waitForVisibilityOf(GeneralReport.chartContainerLocator);
			
			System.out.println( Logger.getLineNumber() + "Gadget with name '" + gadgetName + "' opened in GridView...");
		}else{
			System.out.println( Logger.getLineNumber() + "No Gadgets present on Dashboard...");
			throw new SkipException("No Gadgets present with name '" + gadgetName + "' on Dashboard...");
		}
	}
	
	
	public static class Gadgets{
		public static By gadgetsMenuLocator = By.cssSelector("#gadgets i");
		public static By gadgetMenuItemLocator = By.cssSelector("#gadgets ul li");
		public static By gadgetLocator = By.cssSelector("#main-panel div[id^='gadget-']");
		public static By gadgetTitleLocator = By.cssSelector(".header-left span.title");
		public static By gadgetTitlFieldLocator = By.id("gadgetTitle");
		public static By gadgetUpdateBtnLocator = By.xpath("//span[text()='Update']");
		public static By gadgetSpecificMenuIconLocator = By.cssSelector(".icon.icon-menu7");
		public static By gadgetEditOptionLocator = By.cssSelector(".options.ui-menu-item");
		public static By gadgetRemoveOptionLocator = By.cssSelector(".remove.ui-menu-item");
		public static By gadgetShareOptionLocator = By.cssSelector(".share.ui-menu-item");
		public static By gadgetShareBtnLocator = By.xpath("//span[text()='Share']");
		
		public static void addGadgetFromDashboard(){
			searchPage.tryClick(gadgetsMenuLocator);
			searchPage.tryClick(gadgetMenuItemLocator);
			System.out.println( Logger.getLineNumber() + "Gadget added to dashboard...");
		}
		
		public static int getGadgetCount(){
			return searchPage.getTotalElementCount(gadgetLocator);
		}
		
		public static int getGadgetCountFor(String gadgetName){
			int gadgetCountFor = (int)searchPage.getElements(gadgetTitleLocator).stream()
					  .filter(g -> g.getText().trim().equals(gadgetName)).count();
			
			return gadgetCountFor;
		}
		
		public static WebElement getGadgetElement(String gadgetName){
			WebElement gadgetElement = searchPage.getElements(gadgetTitleLocator).stream()
					  .filter(g -> g.getText().trim().equals(gadgetName))
					  .findFirst().get();
			
			return gadgetElement;
		}
		
		public static boolean isGadgetExist(String gadgetName){
			return getGadgetElement(gadgetName) != null;
		}
		
		public static void editGadgetName(String gadgetName, String gadgetNewName){
			List<WebElement> gadgets = searchPage.getElements(gadgetTitleLocator);
			if(gadgets.size() != 0){
				for(int i = 0; i < gadgets.size(); i++){
					if(gadgets.get(i).getText().trim().equals(gadgetName)){
						searchPage.hoverOverElement(searchPage.getElements(gadgetSpecificMenuIconLocator).get(i));
						searchPage.waitFor(1);
						searchPage.getElements(gadgetEditOptionLocator).get(i).click();
						searchPage.waitForVisibilityOf(gadgetTitlFieldLocator);
						searchPage.editText(gadgetTitlFieldLocator, gadgetNewName);
						searchPage.tryClick(gadgetUpdateBtnLocator, 3);
						
						System.out.println( Logger.getLineNumber() + "Gadget '" + gadgetName + "' is renamed to '" + gadgetNewName + "'...");
						
						break;
					}
				}
			}else{
				System.out.println( Logger.getLineNumber() + "No such gadget with name '" + gadgetName + "' ...");
			}
		}
		
		
		public static void removeGadgetWithName(String gadgetName){
			List<WebElement> gadgets = searchPage.getElements(gadgetTitleLocator);
			if(gadgets.size() != 0){
				for(int i = 0; i < gadgets.size(); i++){
					if(gadgets.get(i).getText().trim().equals(gadgetName)){
						//searchPage.getElements(gadgetSpecificMenuIconLocator).get(i).click();
						searchPage.hoverOverElement(searchPage.getElements(gadgetSpecificMenuIconLocator).get(i));
						searchPage.waitFor(1);
						searchPage.getElements(gadgetRemoveOptionLocator).get(i).click();
						
						System.out.println( Logger.getLineNumber() + "Gadget '" + gadgetName + "' is removed...'");
						
						break;
					}
				}
			}else{
				System.out.println( Logger.getLineNumber() + "No such gadget with name '" + gadgetName + "' ...");
			}
		}
		
		
		public static void shareGadgetWithName(String gadgetName, String ... sharedWith){
			List<WebElement> gadgets = searchPage.getElements(gadgetTitleLocator);
			if(gadgets.size() != 0){
				for(int i = 0; i < gadgets.size(); i++){
					if(gadgets.get(i).getText().trim().equals(gadgetName)){
						searchPage.hoverOverElement(searchPage.getElements(gadgetSpecificMenuIconLocator).get(i));
						searchPage.waitFor(1);
						searchPage.getElements(gadgetShareOptionLocator).get(i).click();
						searchPage.waitForVisibilityOf(gadgetShareBtnLocator);
						
						for(String user : sharedWith){
							try {
								System.out.println( Logger.getLineNumber() + "input[value='" + user + "']");
								searchPage.tryClick(By.cssSelector("input[value='" + user + "']"));
								System.out.println( Logger.getLineNumber() + "User '" + user + "' is selcted for sharing...");
							} catch (Exception e) {
								System.err.println("*********************************************************");
								System.err.println("User '" + user + "' is not valid.");
								System.err.println("*********************************************************");
							}
						}
						
						searchPage.tryClick(gadgetShareBtnLocator);
						searchPage.waitForInVisibilityOf(gadgetShareBtnLocator);
						
						System.out.println( Logger.getLineNumber() + "Gadget '" + gadgetName + "' is shared with " + Arrays.asList(sharedWith));
						
						break;
					}
				}
			}else{
				System.out.println( Logger.getLineNumber() + "No such gadget with name '" + gadgetName + "' to share...");
			}
		}
	}
}
