package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AutomationDataCleaner;

import com.icontrolesi.automation.platform.util.Logger;

public class SearchesAccordion implements CommonActions{
	static WebDriver driver = Driver.getDriver(); 
	public static By tabLocation = By.id("history");
	public static By searchTabsLocation = By.cssSelector("ul[role='tablist'] > li");
	public static By savedTabLocation = By.id("savedSearch");
	public static By searchHistoryTabLocation = By.id("historySearch");
	public static By savedSearchShowFilterLocator = By.id("saved-showFilter");
	public static By historyItemsLocation = By.cssSelector("#searches-picklist > ul > li");
	public static By historyTabLocation = By.cssSelector("li[aria-labelledby='historySearch']");
	public static By historyCategoryLocator = By.cssSelector("#searches-picklist li");
	public static By searchFieldLocator = By.id("saved-find");
	public static By searchBtnLocator = By.id("saved-search");
	public static By queryDescriptionLocator = By.id("queryDescription");
	public static By addToSearchBtnLocator = By.cssSelector("button[title='Add selected to search builder']");
	public static By viewSelectedInGridView = By.cssSelector("button[title='View documents in selected']");
	public static By savedSearchDelete = By.id("savedSearches_dlt");
	public static By createTopLevelGroupBtnLocator = By.cssSelector("#saved-addGroup > span > span");
	public static By createGroupBtnLocator = By.id("create");
	public static By savedGroupNameLocator = By.id("saved-groupName");
	public static By savedGroupPermissionLocator = By.id("saved-groupPermission");
	public static By savedSearchGroupEditBtnLocator = By.cssSelector("#savedGroup_edit > span");
	public static By savedSearchOrGroupWindowHeaderLocator = By.cssSelector("div[aria-describedby='saved-addGroupDialog'] > div > span");
	public static By todayItemLocator = By.cssSelector("#today li");
	
	public static By savedSearchEditBtnLocator = By.cssSelector("#savedSearches_edit > span");
	public static By savedSearchAddBtnLocator = By.cssSelector("#savedSearches_add > span");
	
	
	public static By savedSearchInfoDetailLocator = By.cssSelector("#savedSearches_detail > span");
	public static By savedSearchDeleteBtnLocator = By.cssSelector("#savedSearches_dlt > span");
	public static By savedPermissionInfoLocator = By.id("savedPermission");
	public static By savedSearchCloseWindowLocator = By.id("dialogClose");
	
	public static By autoCompleteListLocator = By.cssSelector("ul[id^='ui-id'][class^='ui-autocomplete']");
	public static By autoCompleteItemsLocator = By.cssSelector("ul[id^='ui-id'][class^='ui-autocomplete'] > li");
	
	public static By savedSearchUnderAGroupLocator = By.cssSelector("ul[class='jstree-children'] > li");
	//public static By savedSearchCheckboxUnderAGroupLocator = By.cssSelector("ul[class='jstree-children'],[class*='jstree-children'] > li > a > i");
	public static By savedSearchCheckboxUnderAGroupLocator = By.cssSelector("#searches-picklist li[class$='childNode'] i[class$='checkbox']");
	
	public static By savedSearchItemLocator = By.cssSelector("#searches-picklist li > a");
	public static By savedSearchItemCheckboxLocator = By.cssSelector("#searches-picklist li > a > i");
	
	private static SearchesAccordion searchesAccordion = new SearchesAccordion();
	
	public static void open(){
		driver = Driver.getDriver();
		//searchesAccordion.waitForElementToBeClickable(driver, tabLocation);
		WebElement searchesAccordionTab = searchesAccordion.getElement(tabLocation);
		
		if(searchesAccordionTab.getAttribute("aria-expanded").equals("false")){
			searchesAccordionTab.click();
			
			if(Configuration.getConfig("selenium.browser").equals("ie11")){
				new SearchesAccordion().waitFor(30);
				searchesAccordion.waitForFrameToLoad(Frame.SAVED_SEARCHES_FRAME);
			}else{
				new SearchesAccordion().waitForAttributeEqualsIn(driver, tabLocation, "aria-expanded", "true");
				searchesAccordion.waitForFrameToLoad(Frame.SAVED_SEARCHES_FRAME);
				new SearchesAccordion().waitForNumberOfElementsToBeGreater(historyItemsLocation, 0);
			}
			
			
			System.out.println( Logger.getLineNumber() + "\nSaved Searches accordion expanded...\n");
		}else{
			searchesAccordion.waitForFrameToLoad(Frame.SAVED_SEARCHES_FRAME);
			searchesAccordion.waitForNumberOfElementsToBeGreater(historyItemsLocation, 0);
			System.out.println( Logger.getLineNumber() + "\nSavedSearch accordion already expanded...\n");
		}
	}
	
	
	public static void openSavedTab(){
		boolean isTabExpanded = searchesAccordion.getAttribute(savedTabLocation, "aria-expanded").equals("true");
		
		if(isTabExpanded){
			System.out.println( Logger.getLineNumber() + "Saved tab for Searches accordion already expanded...");
		}else{
			searchesAccordion.tryClick(savedTabLocation);
			
			if(Configuration.getConfig("selenium.browser").equals("ie11")){
				new SearchesAccordion().waitFor(30);
			}else{
				new SearchesAccordion().waitForAttributeEqualsIn(tabLocation, "aria-expanded", "true");
				new SearchesAccordion().waitForNumberOfElementsToBeGreater(historyItemsLocation, 0);
			}
			
			System.out.println( Logger.getLineNumber() + "Saved tab for Searches accordion expanded...");
		}
	}
	
	
	public static void openSearchHistoryTab(){
		boolean isTabExpanded = searchesAccordion.getAttribute(historyTabLocation, "aria-expanded").equals("true");
		
		if(isTabExpanded){
			new SearchesAccordion().waitForNumberOfElementsToBeGreater(historyCategoryLocator, 0);
			searchesAccordion.waitFor(5);
			System.out.println( Logger.getLineNumber() + "Search History tab for Searches accordion already expanded...");
		}else{
			searchesAccordion.tryClick(searchHistoryTabLocation);
			new SearchesAccordion().waitForAttributeEqualsIn(historyTabLocation, "aria-expanded", "true");
			new SearchesAccordion().waitForNumberOfElementsToBeGreater(historyCategoryLocator, 0);
			searchesAccordion.waitFor(5);
			
			System.out.println( Logger.getLineNumber() + "Search History tab for Searches accordion expanded...");
		}
	}
	
	
	public static void selectFilter(String filterName){
		if(searchesAccordion.getSelectedItemFroDropdown(savedSearchShowFilterLocator).equals(filterName) == false){
			searchesAccordion.selectFromDrowdownByText(savedSearchShowFilterLocator, filterName);
			searchesAccordion.waitFor(5);
		}
		System.out.println( Logger.getLineNumber() + "Filter '" + filterName + "' selected...");
	}
	
	public static void searchForSavedSearch(String savedSearchName){
		searchesAccordion.editText(searchFieldLocator, savedSearchName);
		searchesAccordion.tryClick(searchBtnLocator);
		searchesAccordion.waitFor(5);
	}
	
	public static void selectSavedSearch(String savedSearchName){
		searchForSavedSearch(savedSearchName);
		List<WebElement> savedSearchList = searchesAccordion.getElements(historyItemsLocation);
		for(WebElement savedSearchItem : savedSearchList){
			if(savedSearchItem.getText().trim().equals(savedSearchName)){
				savedSearchItem.findElement(By.cssSelector("a > i")).click();
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "SavedSearch '" + savedSearchName +"' selected...");
	}
	
	
	public static void openSavedSearchForView(){
		//searchesAccordion.tryClick(By.cssSelector("#searches-picklist > ul > li"));
		searchesAccordion.tryClick(historyItemsLocation);
		searchesAccordion.switchToDefaultFrame();
		searchesAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
		searchesAccordion.waitForVisibilityOf(SearchPage.bottomMenuInGridView);
		searchesAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("#grid > tbody > tr"), 0);
	}
	
	public static void openSavedSearchForView(String savedSearchName){
		selectFilter("All");
		searchForSavedSearch(savedSearchName);
		openSavedSearchForView();
		System.out.println( Logger.getLineNumber() + "SavedSearch " + savedSearchName + " opened in GridView...");
	}
	
	public static void addSelectedToSearch(){
		searchesAccordion.tryClick(addToSearchBtnLocator);
		searchesAccordion.waitFor(Driver.getDriver(), 2);
	} 
	
	public static void addSelectedToSearch(String savedSearchName){
		searchForSavedSearch(savedSearchName);
		searchesAccordion.tryClick(SearchesAccordion.savedSearchCheckboxUnderAGroupLocator);
		
		searchesAccordion.tryClick(addToSearchBtnLocator);
		searchesAccordion.waitFor(2);
	}
	
	public static void navigateSavedSearch(String savedSearchName){
		selectSavedSearch(savedSearchName);
		Driver.getDriver().findElement(addToSearchBtnLocator).click();
		searchesAccordion.waitFor(Driver.getDriver(), 1);
		System.out.println( Logger.getLineNumber() + "Selected SavedSearch added to search builder...");
	}
	
	public static boolean isSearchSaved(){
		(new RecentSearchAccordion()).waitForNumberOfElementsToBeGreater(historyItemsLocation, 0);
		return Driver.getDriver().findElements(historyItemsLocation).size() > 0;
	}
	
	public static WebElement getLatestSearchFromHistory(){
		return Driver.getDriver().findElements(historyItemsLocation).get(0);
	}
	
	public static void openLatestSavedSearch(){
		By searchCriteria = By.cssSelector("#savedSearches > ul > li");
		int numberOfCriterionPresent = searchesAccordion.getTotalElementCount(searchCriteria);
		getLatestSearchFromHistory().click();
		(new RecentSearchAccordion()).waitForNumberOfElementsToBeGreater(searchCriteria, numberOfCriterionPresent);
	}
	
	public static void openLatestSavedSearch(String searchName){
		By searchCriteria = By.cssSelector("#savedSearches > ul > li");
		List<WebElement> savedSearchList = searchesAccordion.getElements(historyItemsLocation);
		int numberOfCriterionPresent = savedSearchList.size();
		for(WebElement savedSearchItem : savedSearchList){
			if(savedSearchItem.getText().trim().equals(searchName)){
				savedSearchItem.click();
				break;
			}
		}
		
		if(Configuration.getConfig("selenium.browser").equals("firefox")){
			(new RecentSearchAccordion()).waitForNumberOfElementsToBeGreater(searchCriteria, numberOfCriterionPresent);
		}else{
			new RecentSearchAccordion().waitFor(20);
		}
	}
	
	public static void openSavedSearchWithName(String savedSearchName){
		List<WebElement> savedSearchItems = searchesAccordion.getElements(historyItemsLocation);
		System.out.println( Logger.getLineNumber() + "Total items: " + savedSearchItems.size());
		for(WebElement savedItem : savedSearchItems){
			if(savedItem.findElement(By.tagName("a")).getText().trim().equals(savedSearchName)){
				savedItem.click();
				searchesAccordion.switchToDefaultFrame();
				searchesAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
				new SearchesAccordion().waitForVisibilityOf(queryDescriptionLocator);
				new SearchesAccordion().waitFor(15);
				System.err.println(savedSearchName + " selected...");
				break;
			}
		}
		
	}
	
	public static int getSavedSearchCount(String savedSearchName){
		int count = 0;
		List<WebElement> savedSearchItems = searchesAccordion.getElements(By.cssSelector("#searches-picklist li a"));
		for(WebElement savedItem : savedSearchItems){
			if(searchesAccordion.getText(savedItem).equals(savedSearchName)){
				count++;
			}
		}
		
		return count;
	}
	
	public static void deleteSavedSearch(String savedSearchName){
		SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
        
		List<WebElement> savedSearchList = searchesAccordion.getElements(savedSearchItemLocator);
		
		if(savedSearchList.size() == 0){
			System.err.println("No SavedSearch '" + savedSearchName + "' to delete...");
		}else{
			for(WebElement savedSearchItem : savedSearchList){
				if(savedSearchItem.getText().trim().equals(savedSearchName)){
					Actions actions = new Actions(Driver.getDriver());
					actions.moveToElement(savedSearchItem).build().perform();
					searchesAccordion.waitFor(1);
				
					WebElement deleteSavedSearchButton = searchesAccordion.getElement(savedSearchDeleteBtnLocator);
					
					jse.executeScript("arguments[0].click();", deleteSavedSearchButton);
					
					searchesAccordion.acceptAlert();
					searchesAccordion.acceptAlert();
					searchesAccordion.waitFor(5);
					
					AutomationDataCleaner.popSavedSearchFromList();
					
					System.out.println( Logger.getLineNumber() + "Saved search '" + savedSearchName + "' deleted...");
					
					break;
				}
			}
		}
	}
	
	public static void createTopLevelGroup(String groupName, String groupPermission){
		if(isTopLevelGroupExists(groupName) == false){
			searchesAccordion.tryClick(createTopLevelGroupBtnLocator, createGroupBtnLocator);
			searchesAccordion.editText(savedGroupNameLocator, groupName);
			
			if(groupPermission.equals("") == false)
				searchesAccordion.selectFromDrowdownByText(savedGroupPermissionLocator, groupPermission);
			
			searchesAccordion.tryClick(createGroupBtnLocator);
			searchesAccordion.acceptAlert(Driver.getDriver());
			searchesAccordion.waitFor(2);
			System.out.println( Logger.getLineNumber() + "Group '" + groupName + "' created for SavedSearch...");
		}else{
			System.out.println( Logger.getLineNumber() + groupName + " already exists...");
		}
	}
	
	public static boolean isSavedSearchExists(String savedSearchName){
		selectFilter("All");
		searchForSavedSearch(savedSearchName);
		//List<WebElement> savedSearchList = searchesAccordion.getElements(historyItemsLocation);
		List<WebElement> savedSearchList = searchesAccordion.getElements(By.cssSelector("li[role='treeitem']"));
		
		for(WebElement item : savedSearchList){
			String itemName = item.getText().trim();
			String fileIconDisplayProperty = item.findElement(By.cssSelector("a > i")).getCssValue("display").trim();
			String folderIconDisplayProperty = item.findElement(By.cssSelector("a > i[class*='icon-folder-plus']")).getCssValue("display").trim();
			System.out.println( Logger.getLineNumber() + itemName+", " + fileIconDisplayProperty + ", " + folderIconDisplayProperty);
			if(itemName.equals(savedSearchName) && fileIconDisplayProperty.equals("inline-block") && folderIconDisplayProperty.equals("none"))
				return true;
		}
		
		return false;
	} 
	
	public static boolean isTopLevelGroupExists(String savedSearchName){
		selectFilter("All");
		searchForSavedSearch(savedSearchName);
		List<WebElement> savedSearchList = searchesAccordion.getElements(historyItemsLocation);
		
		for(WebElement item : savedSearchList){
			String itemName = item.getText().trim();
			String fileIconDisplayProperty = item.findElement(By.cssSelector("a > i")).getCssValue("display").trim();
			String folderIconDisplayProperty = item.findElement(By.cssSelector("a > i[class*='icon-folder-plus']")).getCssValue("display").trim();
			System.out.println( Logger.getLineNumber() + itemName+", " + fileIconDisplayProperty + ", " + folderIconDisplayProperty);
			if(itemName.equals(savedSearchName) && fileIconDisplayProperty.equals("none") && folderIconDisplayProperty.equals("inline-block"))
				return true;
		}
		
		return false;
	} 
	
	
	public static void openSavedSearchInfoDetail(String savedSearchName){
		//driver = Driver.getDriver();
		//Actions actions = new Actions(driver);
		List<WebElement> savedSearchList = searchesAccordion.getElements(By.cssSelector("li[role='treeitem']"));
		
		for(WebElement savedSearch : savedSearchList){
			System.err.println(savedSearch.findElement(By.tagName("a")).getText()+"****");
			if(savedSearch.findElement(By.tagName("a")).getText().trim().equals(savedSearchName)){
				//actions.moveToElement(savedSearch).build().perform();
				searchesAccordion.hoverOverElement(savedSearch);
				searchesAccordion.waitFor(1);
				System.out.println( Logger.getLineNumber() + searchesAccordion.getTotalElementCount(savedSearchInfoDetailLocator)+"^^^^");
				searchesAccordion.tryClick(savedSearchInfoDetailLocator);
				WebElement infoLink = searchesAccordion.getElement(savedSearchInfoDetailLocator);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				jse.executeScript("arguments[0].click();", infoLink);
				searchesAccordion.waitFor(5);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "\nSavedSearch info modal opened ...");
	}
	
	public static void openSavedSearchInfoDetail(By element){
		searchesAccordion.hoverOverElement(element);
		JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
		WebElement details = searchesAccordion.getElement(savedSearchInfoDetailLocator);
		
		jse.executeScript("arguments[0].click();", details);
		searchesAccordion.waitFor(5);
	}
	
	
	public static void openSavedSearchInfoDetail(WebElement element){
		searchesAccordion.hoverOverElement(element);
		JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
		WebElement details = searchesAccordion.getElement(savedSearchInfoDetailLocator);
		
		jse.executeScript("arguments[0].click();", details);
		searchesAccordion.waitFor(5);
	}
	
	
	public static void closeSavedSearchInfoDetail(){
		WebElement closeBtn = Driver.getDriver().findElement(savedSearchCloseWindowLocator);
		JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
		jse.executeScript("arguments[0].click();", closeBtn);
		searchesAccordion.waitFor(2);
		//searchesAccordion.tryClick((savedSearchCloseWindowLocator), 1);	
		System.out.println( Logger.getLineNumber() + "\nSavedSearch info modal closed...");
	}
	
	public static void openEditSearchGroupWindow(String groupName){
		/*WebElement groupdEdit = Driver.getDriver().findElement(savedSearchGroupEditBtnLocator);
		jse.executeScript("arguments[0].click;", groupdEdit);
		searchesAccordion.waitFor(driver, 2);*/
		
		
		driver = Driver.getDriver();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		
		Actions actions = new Actions(driver);
		List<WebElement> savedSearchList = searchesAccordion.getElements(By.cssSelector("li[role='treeitem']"));
		
		for(WebElement savedSearch : savedSearchList){
			System.err.println(savedSearch.findElement(By.tagName("a")).getText()+"****");
			if(savedSearch.findElement(By.tagName("a")).getText().trim().equals(groupName)){
				actions.moveToElement(savedSearch).build().perform();
				searchesAccordion.waitFor(driver, 1);
				System.out.println( Logger.getLineNumber() + searchesAccordion.getTotalElementCount(savedSearchGroupEditBtnLocator)+"^^^^");
				WebElement groupdEdit = searchesAccordion.getElement(savedSearchGroupEditBtnLocator);
				jse.executeScript("arguments[0].click();", groupdEdit);
				//savedSearch.findElement(By.cssSelector("div.hoverDiv > button > span"));
				searchesAccordion.waitFor(driver, 2);
				break;
			}
		}
		
		//searchesAccordion.waitForVisibilityOf(driver, savedPermissionInfoLocator);
		
		System.out.println( Logger.getLineNumber() + "\nEdit Group window modal opened ...");
	}
	
	public static void openEditSearchHistoryWindow(String savedSearchName){
		/*WebElement searchdEdit = Driver.getDriver().findElement(savedSearchEditBtnLocator);
		jse.executeScript("arguments[0].click;", searchdEdit);
		searchesAccordion.waitFor(driver, 2);
		*/
		
		driver = Driver.getDriver();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		
		Actions actions = new Actions(driver);
		List<WebElement> savedSearchList = searchesAccordion.getElements(By.cssSelector("li[role='treeitem']"));
		
		for(WebElement savedSearch : savedSearchList){
			System.err.println(savedSearch.findElement(By.tagName("a")).getText()+"****");
			if(savedSearch.findElement(By.tagName("a")).getText().trim().equals(savedSearchName)){
				actions.moveToElement(savedSearch).build().perform();
				searchesAccordion.waitFor(driver, 1);
				System.out.println( Logger.getLineNumber() + searchesAccordion.getTotalElementCount(savedSearchInfoDetailLocator)+"^^^^");
				WebElement searchdEdit = searchesAccordion.getElement(savedSearchEditBtnLocator);
				jse.executeScript("arguments[0].click();", searchdEdit);
				//savedSearch.findElement(By.cssSelector("div.hoverDiv > button > span"));
				searchesAccordion.waitFor(driver, 2);
				break;
			}
		}
		
		//searchesAccordion.waitForVisibilityOf(driver, savedPermissionInfoLocator);
		
		System.out.println( Logger.getLineNumber() + "\nEdit Searches window modal opened ...");
	}
	
	
	public static void clickEditBtnForSavedSearch(){
		if(Configuration.getConfig("selenium.browser").equals("chrome")){
			driver = Driver.getDriver();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			WebElement searchEdit = searchesAccordion.getElement(savedSearchEditBtnLocator);
			jse.executeScript("arguments[0].click();", searchEdit);
			System.out.println( Logger.getLineNumber() + "Element [ " + savedSearchEditBtnLocator.toString() + " ] clicked...");
			searchesAccordion.waitFor(driver, 1);
		}else{
			searchesAccordion.tryClick(savedSearchEditBtnLocator, 1);
		}
	}
	
	public static void clickDeleteBtnForSavedSearch(){
		if(Configuration.getConfig("selenium.browser").equals("chrome")){
			driver = Driver.getDriver();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			WebElement searchDelete = searchesAccordion.getElement(savedSearchDeleteBtnLocator);
			jse.executeScript("arguments[0].click();", searchDelete);
			System.out.println( Logger.getLineNumber() + "Element [ " + savedSearchDeleteBtnLocator.toString() + " ] clicked...");
			searchesAccordion.waitFor(driver, 1);
		}else{
			searchesAccordion.tryClick(savedSearchDeleteBtnLocator, 1);
		}
	}
	
	
	public static void clickAddBtnForSavedSearch(){
		if(Configuration.getConfig("selenium.browser").equals("chrome")){
			driver = Driver.getDriver();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			WebElement searchAdd = searchesAccordion.getElement(savedSearchAddBtnLocator);
			jse.executeScript("arguments[0].click();", searchAdd);
			System.out.println( Logger.getLineNumber() + "Element [ " + savedSearchAddBtnLocator.toString() + " ] clicked...");
			searchesAccordion.waitFor(driver, 1);
		}else{
			searchesAccordion.tryClick(savedSearchAddBtnLocator, 1);
		}
	}
	
	
	public static void clickInfoBtnForSavedSearch(){
		if(Configuration.getConfig("selenium.browser").equals("chrome")){
			driver = Driver.getDriver();
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			WebElement searchInfo = searchesAccordion.getElement(savedSearchInfoDetailLocator);
			jse.executeScript("arguments[0].click();", searchInfo);
			System.out.println( Logger.getLineNumber() + "Element [ " + savedSearchInfoDetailLocator.toString() + " ] clicked...");
			searchesAccordion.waitFor(driver, 1);
		}else{
			searchesAccordion.tryClick(savedSearchInfoDetailLocator, 1);
		}
	}
	
	public static boolean isAutoSuggestionAppeared(){
		WebElement autoSuggestion = Driver.getDriver().findElement(autoCompleteListLocator);
		boolean isListAppeared = autoSuggestion.getCssValue("display").equals("block") 
									&& autoSuggestion.findElements(By.tagName("li")).size() > 0;
									
		System.out.println( Logger.getLineNumber() + autoSuggestion.findElements(By.tagName("li")).size() +"&*");
		return isListAppeared;
	}
	
	public static void clickOpenBtnForViewSavedSearch(){
		searchesAccordion.tryClick(viewSelectedInGridView);
		searchesAccordion.switchToDefaultFrame();
		searchesAccordion.waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		new SearchesAccordion().waitForVisibilityOf(queryDescriptionLocator);
		//new SearchesAccordion().waitFor(Driver.getDriver(), 15);
		searchesAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("#grid > tbody > tr"), 0);
	}
	
	
	public static String getSearchCriteriaNameToSearch(){
		int totalCountBeforeRunningSearch = searchesAccordion.getTotalElementCount(todayItemLocator);
		System.out.println( Logger.getLineNumber() + "Total count: " + totalCountBeforeRunningSearch);
		
		String searchName = "";
		
		if(totalCountBeforeRunningSearch > 0){
			String [] searchFullName = searchesAccordion.getText(By.cssSelector("#today > ul > li > a")).split("\\,");
			searchName = searchFullName[searchFullName.length - 1].trim();
		}else{
			searchName = "Addressee";
		}
		
		return searchName;
	}
	
	public static int getTodaysHistoryCount(){
		return searchesAccordion.getTotalElementCount(todayItemLocator);
	}
	
	public static int getSavedSearchesInstanceCount(){
		return searchesAccordion.getTotalElementCount(By.cssSelector("#recentInfoDialog > div")) - 2;
	}
}
