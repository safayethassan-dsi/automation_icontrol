package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AutomationDataCleaner;

import com.icontrolesi.automation.platform.util.Logger;

public class TagAccordion implements CommonActions{
	private static TagAccordion tagAccordion = new TagAccordion();
	
	static WebDriver driver = Driver.getDriver(); 
	public static By tabLocation = By.id("category");
	public static By topAddToSearchBtn = By.cssSelector("button[title='Add selected to search builder']");
	public static By tagItemLocator = By.cssSelector("#tag-picklist ul li > a");
	public static By firstTagLocatorCheckbox = By.cssSelector("#tag-picklist > ul > li > a > i");
	public static By filterSelectionLocator = By.id("tags-showPermission");
	public static By tagInfoDetail = By.cssSelector("#tag_detail > span");
	public static By editTagLocator = By.cssSelector("#tag_edit > span");
	public static By createSubtagLocator = By.cssSelector("#tag_newSub > span");
	public static By tagSearchFieldLocator = By.id("tag-find");
	public static By tagFindBtnLocator = By.id("tag-search");
	public static By totalDocCountForTag = By.id("totalCount");
	public static By tagInfoColseBtn = By.id("dialogClose");
	public static By viewSelectedBtn = By.cssSelector("button[title='View documents in selected']");
	public static By addNewTagBtnLocator = By.id("tag-new");
	public static By editFieldLocator = By.id("tag-editOrSubName");
	public static By permissionLocator = By.id("tags-editOrSubPermission");
	public static By createTagLocator = By.id("create");
	public static By manageTagBtnLocator = By.id("tag-managing");
	public static By messageContainer = By.id("messageContainer");

	public static void open(){
		TagAccordion categoryAccordion = new TagAccordion(); 
		driver = Driver.getDriver();  
		driver.findElement(tabLocation).click();
		categoryAccordion.waitForFrameToLoad(Frame.TAGS_FRAME);
		if(Configuration.getConfig("selenium.browser").equals("ie11")){
			categoryAccordion.waitFor(30);
		}
		else
			categoryAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("#tag-picklist > ul > li"), 0);
		
		categoryAccordion.waitFor(5);
		//driver.switchTo().defaultContent();
		
		System.out.println( Logger.getLineNumber() + "\nTag accordion expanded...\n");
	}
	
	
	public static void selectFilter(String filterName){
		if(tagAccordion.getSelectedItemFroDropdown(filterSelectionLocator).equals(filterName)){
			System.out.println( Logger.getLineNumber() + "Filter '" + filterName + "'  is already selected...");
		}else{
			tagAccordion.selectFromDrowdownByText(filterSelectionLocator, filterName);
			/*tagAccordion.waitForVisibilityOf(By.id("j4_loading"));
			tagAccordion.waitForInVisibilityOf(By.id("j4_loading"));*/
			tagAccordion.waitFor(Driver.getDriver(), 10);
			
			System.out.println( Logger.getLineNumber() + "Filter '" + filterName + "'  is selected...");
		}
	}
	
	public static void searchForTag(String tagName){
		tagAccordion.editText(tagSearchFieldLocator, tagName);
		tagAccordion.tryClick(tagFindBtnLocator);
		/*tagAccordion.waitForVisibilityOf(By.id("j7_loading"));
		tagAccordion.waitForInVisibilityOf(By.id("j7_loading"));*/
		tagAccordion.waitFor(Driver.getDriver(), 10);
		
		System.out.println( Logger.getLineNumber() + "Search for '" + tagName + "' performed...");
	}
	
	public static void openTagForView(String tagName){
		selectFilter("All");
		searchForTag(tagName);
		tagAccordion.getElements(TagAccordion.tagItemLocator)
			.stream().filter(tag -> tagAccordion.getText(tag).equals(tagName))
			.findFirst().get().click();
		
		tagAccordion.switchToDefaultFrame();
		tagAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
		
		tagAccordion.waitForVisibilityOf(SearchPage.bottomMenuInGridView);
		tagAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("#grid > tbody > tr"), 0);
		
		System.out.println( Logger.getLineNumber() + "Tag '" + tagName + "' opened for view...");
	}
	
	public static void selectTag(String tagName){
		By tagLocator = By.cssSelector("#tag-picklist > ul > li > a");
		List<WebElement> listOfTags = tagAccordion.getElements(tagLocator);
		System.out.println( Logger.getLineNumber() + "Number of tags: " + listOfTags.size());
		for(WebElement tag : listOfTags){
			System.err.println("Tag: "+tag.getText()+"****");
			if(tag.getText().trim().equals(tagName)){
			System.err.println(tag.getAttribute("class")+"*****");
				tag.findElement(By.tagName("i")).click();
				new SearchPage().waitFor(2);
				System.out.println( Logger.getLineNumber() + tagName + " tag selected...");
				break;
			}
		}
	}
	
	public static void selectTag(WebDriver driver, String tagName){
		By tagLocator = By.cssSelector("#tag-picklist > ul > li > a");
		List<WebElement> listOfTags = driver.findElements(tagLocator);
		System.out.println( Logger.getLineNumber() + "Number of tags: " + listOfTags.size());
		for(WebElement tag : listOfTags){
			System.err.println("Tag: "+tag.getText()+"****");
			if(tag.getText().trim().equals(tagName)){
			System.err.println(tag.getAttribute("class")+"*****");
				tag.findElement(By.tagName("i")).click();
				new SearchPage().waitFor(2);
				System.out.println( Logger.getLineNumber() + tagName + " tag selected...");
				break;
			}
		}
	}
	
	public static void selectTag(String tagName, boolean isLeafNode){
		By tagLocator = By.cssSelector("ul[role='group'] > li[role='treeitem'] > a");
		List<WebElement> listOfTags = tagAccordion.getElements(tagLocator);
		System.out.println( Logger.getLineNumber() + "Number of tags: " + listOfTags.size());
		for(WebElement tag : listOfTags){
			if(tag.getText().trim().equals(tagName)){
				WebElement parentOfTagLink = SearchPage.getParent(tag);
				if(parentOfTagLink.getAttribute("class").contains("jstree-leaf") && isLeafNode){
					tag.findElement(By.tagName("i")).click();;
					new SearchPage().waitFor(2);
					System.out.println( Logger.getLineNumber() + tagName + " leaf tag selected...");
					break;
				}else if(parentOfTagLink.getAttribute("class").contains("jstree-leaf") == false && isLeafNode == false){
					tag.findElement(By.tagName("i")).click();;
					new SearchPage().waitFor(2);
					System.out.println( Logger.getLineNumber() + tagName + " non-leaf tag selected...");
					break;
				}
			}
		}
	}
	
	
	public static void selectTag(WebDriver driver, String tagName, boolean isLeafNode){
		By tagLocator = By.cssSelector("ul[role='group'] > li[role='treeitem'] > a");
		List<WebElement> listOfTags = driver.findElements(tagLocator);
		System.out.println( Logger.getLineNumber() + "Number of tags: " + listOfTags.size());
		for(WebElement tag : listOfTags){
			
			if(tag.getText().trim().equals(tagName)){
				WebElement parentOfTagLink = SearchPage.getParent(tag);
				//System.err.println(parentOfTagLink.getAttribute("class").contains("jstree-leaf"));
				if(parentOfTagLink.getAttribute("class").contains("jstree-leaf") && isLeafNode){
					tag.findElement(By.tagName("i")).click();;
					new SearchPage().waitFor(2);
					System.out.println( Logger.getLineNumber() + tagName + " leaf tag selected...");
					break;
				}else if(parentOfTagLink.getAttribute("class").contains("jstree-leaf") == false && isLeafNode == false){
					tag.findElement(By.tagName("i")).click();;
					new SearchPage().waitFor(2);
					System.out.println( Logger.getLineNumber() + tagName + " non-leaf tag selected...");
					break;
				}
			}
		}
	}
	
	
	public static void expandTag(String tagName){
		By tagLocator = By.cssSelector("ul[role='group'] > li[role='treeitem'] > a");
		List<WebElement> listOfTags = tagAccordion.getElements(tagLocator);
		
		for(WebElement tag : listOfTags){
			if(tag.getText().trim().equals(tagName)){
				new SearchPage().getParentOf(tag).findElement(By.tagName("i")).click();;
				new SearchPage().waitFor(10);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + tagName + " tag expanded...");
	}
	
	public static void expandTag(WebDriver driver, String tagName){
		By tagLocator = By.cssSelector("ul[role='group'] > li[role='treeitem'] > a");
		List<WebElement> listOfTags = driver.findElements(tagLocator);
		
		for(WebElement tag : listOfTags){
			if(tag.getText().trim().equals(tagName)){
				//folder.findElement(By.tagName("i")).click();
				new SearchPage().getParentOf(tag).findElement(By.tagName("i")).click();;
				new SearchPage().waitFor(10);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + tagName + " tag expanded...");
	}
	
	public static void createTag(String tagName, String permission){
		clickOnAddTag();
		tagAccordion.editText(editFieldLocator, tagName);
		if(!permission.equals(""))
			tagAccordion.selectFromDrowdownByText(permissionLocator, permission);
		tagAccordion.getElements(createTagLocator).get(1).click();
		tagAccordion.waitFor(2);
		
		System.out.println( Logger.getLineNumber() + "A toplevel tag with name '" + tagName + "' has been created...");
		
		AutomationDataCleaner.listOfTagsCreated.add(tagName);
	}
	
	public static void createSubTagFor(String parentTagName, String subTagName, String subtagPermission){
		openSubtagCreationWindowFor(parentTagName);
				
		if(!subTagName.equals("")){
			tagAccordion.editText(editFieldLocator, subTagName);
		}
		
		if(!subtagPermission.equals("")){
			tagAccordion.selectFromDrowdownByText(permissionLocator, subtagPermission);
		}
		
		tagAccordion.getElements(createTagLocator).get(1).click();
		tagAccordion.waitFor(5);
		
		AutomationDataCleaner.listOfTagsCreated.add(subTagName);
		
		System.out.println( Logger.getLineNumber() + "A subtag with name '" + subTagName + "' has been created under Top Level tag '" + parentTagName + "'...");
	}
	
	public static String editTagWithName(String tagNameToEdit, String newTagName, String newTagPermission){
		openEditWindowFor(tagNameToEdit);
		
		if(!newTagName.equals("")){
			tagAccordion.editText(editFieldLocator, newTagName);
		}
		
		if(!newTagPermission.equals("")){
			tagAccordion.selectFromDrowdownByText(permissionLocator, newTagPermission);
		}
		
		tagAccordion.getElements(createTagLocator).get(1).click();
		String aertMsg = tagAccordion.getAlertMsg();
		AutomationDataCleaner.listOfTagsCreated.remove(tagNameToEdit);
		AutomationDataCleaner.listOfTagsCreated.add(newTagName);
		
		System.out.println( Logger.getLineNumber() + "Tag with name '" + tagNameToEdit + "' has beed edited with new name: '" + newTagName + "'...");
		
		return aertMsg;
	}
	
	public static void clickOnAddTag(){
		//Driver.getDriver().findElement(addNewTagBtnLocator).click();
		//tagAccordion.waitFor(driver);
		//tagAccordion.tryClick(addNewTagBtnLocator, createTagLocator);
		tagAccordion.tryClick(addNewTagBtnLocator, 2);
	}
	
	public static void addSelectedToSearch(){
		Driver.getDriver().findElement(topAddToSearchBtn).click();
		
		System.out.println( Logger.getLineNumber() + "\nSelected tag(s) added to search...");
	} 
	
	public static void selectFirstTag(){
		Driver.getDriver().findElement(firstTagLocatorCheckbox).click();
		new FolderAccordion().waitFor(8);
		
		System.out.println( Logger.getLineNumber() + "\nFirst Tag item selected...");
	}
	
	public static void openTagInfoDetail(){
		driver = Driver.getDriver();
		
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By.cssSelector("#tag-picklist > ul > li > a"))).build().perform();
		/*driver.findElement(tagInfoDetail).click();
		//tagAccordion.waitForVisibilityOf(totalDocCountForTag);
		tagAccordion.waitFor(10);*/
		
		WebElement infoLink = tagAccordion.getElement(tagInfoDetail);
		JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
		jse.executeScript("arguments[0].click();", infoLink);
		//tagAccordion.waitForVisibilityOf(totalDocCountForTag);
		tagAccordion.waitFor(10);
		
		System.out.println( Logger.getLineNumber() + "\nTag info modal opened ...");
	}
	
	
	public static void openTagInfoDetail(String tagName){
		selectFilter("All");
		searchForTag(tagName);
		
		/*driver = Driver.getDriver();
		Actions actions = new Actions(driver);*/
		List<WebElement> tagList = tagAccordion.getElements(tagItemLocator);
		
		for(WebElement tag : tagList){
			if(tagAccordion.getText(tag).equals(tagName)){
				//actions.moveToElement(tagInfo).build().perform();
				tagAccordion.hoverOverElement(tag);
				tagAccordion.waitFor(1);
				System.err.println(tagAccordion.getTotalElementCount(tagInfoDetail)+"^^^^");
				//tagAccordion.tryClick(tagInfoDetail);
				WebElement infoLink = tagAccordion.getElement(tagInfoDetail);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				jse.executeScript("arguments[0].click();", infoLink);
				tagAccordion.waitFor(5);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "\nTag info modal opened ...");
	}
	
	
	public static void openSubtagCreationWindowFor(String parentTagName){
		selectFilter("All");
		searchForTag(parentTagName);
		
		List<WebElement> tagList = tagAccordion.getElements(tagItemLocator);
		
		for(WebElement tag : tagList){
			if(tagAccordion.getText(tag).equals(parentTagName)){
				//actions.moveToElement(tagInfo).build().perform();
				tagAccordion.hoverOverElement(tag);
				tagAccordion.waitFor(1);
				WebElement infoLink = tagAccordion.getElement(createSubtagLocator);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				jse.executeScript("arguments[0].click();", infoLink);
				tagAccordion.waitFor(1);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "\nSubtag creation modal opened ...");
	}
	
	public static void openEditWindowFor(String parentTagName){
		selectFilter("All");
		searchForTag(parentTagName);
		
		List<WebElement> tagList = tagAccordion.getElements(tagItemLocator);
		
		for(WebElement tag : tagList){
			if(tagAccordion.getText(tag).equals(parentTagName)){
				tagAccordion.hoverOverElement(tag);
				tagAccordion.waitFor(1);
				WebElement infoLink = tagAccordion.getElement(editTagLocator);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				jse.executeScript("arguments[0].click();", infoLink);
				tagAccordion.waitFor(1);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "\nEdit modal opened ...");
	}
	
	public static void closeTagInfoDetail(){
		System.err.println(tagAccordion.getElements(tagInfoColseBtn).size()+"******");
		tagAccordion.getElements(tagInfoColseBtn).get(0).click();
		System.out.println( Logger.getLineNumber() + "\nTag info modal closed...");
	}
	
	public static int getTotalDocsCount(){
    	return Integer.parseInt(driver.findElement(totalDocCountForTag).getText().replaceAll("\\D+", ""));
    }	
	
	public static void openSelected(){
		driver = Driver.getDriver();
		driver.findElement(viewSelectedBtn).click();
		tagAccordion.waitForPageLoad(driver);
		tagAccordion.switchToDefaultFrame();
		tagAccordion.waitForFrameToLoad(Frame.MAIN_FRAME);
		
		if(Configuration.getConfig("selenium.browser").equals("ie11"))
			tagAccordion.waitFor(30);
		else
			tagAccordion.waitForVisibilityOf(DocView.totalDocsLocator);
		
		System.out.println( Logger.getLineNumber() + "\nSelected tag(s) Open for view...");
	}
	
	public static boolean isTagFound(String tagName){
		selectFilter("All");
		searchForTag(tagName);
		List<String> tags = tagAccordion.getListOfItemsFrom(tagItemLocator, item -> tagAccordion.getText(item));
		
		return tags.stream().anyMatch(tag -> tag.equals(tagName));
	}
}
