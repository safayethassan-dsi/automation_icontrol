package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.util.AutomationDataCleaner;

import com.icontrolesi.automation.platform.util.Logger;


public class TagManagerInGridView implements CommonActions{
	public static By girdTopPagerLeftMenuItemLocator = By.cssSelector("#grid_toppager_left > table > tbody > tr > td");
	
	public static By addDocumentsBtnOnTopLocator = By.cssSelector("button[title='Add selected documents to selected tags'] > span");
	public static By removeDocumentsBtnOnTopLocator = By.cssSelector("button[title='Remove selected documents from selected tags'] > span");
	//public static By createTagBtnLocator = By.cssSelector("#tagManage-new > span > span");
	public static By createTagBtnLocator = By.cssSelector("button[title='Create top level tag']");
	//public static By tagNameFieldLocator = By.id("tag-editOrSubName");
	public static By tagNameFieldLocator = By.cssSelector("input[id='tag-editOrSubName'],[class='tag-addEdit']");
	//public static By tagPermissionFieldLocator = By.id("tags-editOrSubPermission");
	public static By tagPermissionFieldLocator = By.cssSelector("select[class='tagPermissionSelectFromDialog'],[id='tags-editOrSubPermission']");
	public static By createBtnLocator = By.xpath("//div[contains(@style, 'block')]//*[text()='Create']");
	public static By updateBtnLocator = By.xpath("//div[contains(@style, 'block')]//*[text()='Update']");
	public static By cancelBtnLocator = By.xpath("//div[contains(@style, 'block')]//*[text()='Cancel']");
	//public static By tagSearchInputFieldLocator = By.id("tag-findManage");
	public static By tagSearchInputFieldLocator = By.cssSelector("input[id='tag-findManage'],[class='tag-find']");
	public static By tagSearchBtnLocator = By.cssSelector("#tag-search > span > span");
	public static By tagFilterLocator = By.id("tagsManage-showPermission");
//	public static By tagFilterLocator = By.cssSelector("select[class='tagPermissionSelect'],[id='tagsManage-showPermission']");
	public static By tagItemLocator = By.cssSelector("#tagManage-picklist a");
	public static By tagItemCheckboxLocator = By.cssSelector("#tagManage-picklist a i[class$='checkbox']");
	//public static By doneBtnLocator = By.xpath("//span[text()='Done']");
	public static By doneBtnLocator = By.id("done");
	public static By tagWindowLocator = By.cssSelector("body > div[aria-describedby='tag-Dialog']");
	public static By tagWiseDocCountLocator = By.cssSelector("span.tag-docNum");
		
	//public static By tagWindowLoadingMsgLocator = By.cssSelector("#messageContainer:not([style='display: none;']) > span");
	public static By tagWindowLoadingMsgLocator = By.cssSelector("#tag-Dialog > div > span");
	public static By tagLoadingMsgLocator = By.xpath("//a[contains(text(), 'Loading ...')]");
	
	public static By addDocumentsToTagLocator = By.cssSelector("div[class='hoverDiv'][style$='block;'] > button[title='Add selected document(s) to this tag']");
	public static By removeDocumentsToTagLocator = By.cssSelector("div[class='hoverDiv'][style$='block;'] > button[title='Remove selected document(s) from this tag']");
	//public static By createSubTagLocator = By.cssSelector("div[class='hoverDiv'][style$='block;'] > button[title='Create sub tag']");
	//public static By createSubTagLocator = By.id("tag_newSub");
	public static By createSubTagLocator = By.cssSelector("button[title='Create sub tag']");
	public static By editTagLocator = By.id("tag_edit");
	public static By deleteTagLocator = By.id("tag_dlt");
	public static By tagInfoDetailLocator = By.id("tag_detail");
	public static By removeDocFromTagocator = By.id("tag_remDoc");
	
	public static By tagSearchMsgContainerLocator = By.cssSelector("#messageContainer[style$='block;'] > span");
	
	private static TagManagerInGridView tagManagerInGridView = new TagManagerInGridView();
	
	public static void open(){
		List<WebElement> gridTopPagerLeftMenuItems = tagManagerInGridView.getElements(girdTopPagerLeftMenuItemLocator);
		
		gridTopPagerLeftMenuItems.stream()
			.filter(s -> s.getAttribute("title").trim().equals("Tags"))
			.findFirst()
			.ifPresent(item -> item.click());
		
		tagManagerInGridView.switchToDefaultFrame();
		/*tagManagerInGridView.waitForVisibilityOf(By.xpath("//div[@id='tag-Dialog']//span[text()='Loading...']"));
		tagManagerInGridView.waitForInVisibilityOf(By.xpath("//div[@id='tag-Dialog']//span[text()='Loading...']"));
		*///setHeight(600);
		setWidth(600);
		tagManagerInGridView.waitForFrameToLoad(Frame.MANAGE_TAG_FRAME);
		System.out.println( Logger.getLineNumber() + "Tag manager window opened...");
	}
	
	
	public static void close(){
		tagManagerInGridView.switchToDefaultFrame();
		
		tagManagerInGridView.tryClick(doneBtnLocator);
		tagManagerInGridView.waitFor(20);
		
		System.out.println( Logger.getLineNumber() + "Tag manager window closed...");
	}
	
	
	public static int getTotalDocCount(){
		return Integer.parseInt(tagManagerInGridView.getText(By.cssSelector("#tag-manage span.totalDoc")).replaceAll("\\D+", ""));
	}
	
	public static void setHeight(int height){
		JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
		
		WebElement tagDialogWindow = tagManagerInGridView.getElement(tagWindowLocator);
		jse.executeScript("arguments[0].setAttribute('style', 'height:" + height + "px');", tagDialogWindow);
		tagManagerInGridView.waitFor(1);
	}
	
	public static void setWidth(int width){
		JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
		
		WebElement tagDialogWindow = tagManagerInGridView.getElement(tagWindowLocator);
		jse.executeScript("arguments[0].setAttribute('style', 'width:" + width + "px');", tagDialogWindow);
		tagManagerInGridView.waitFor(1);
	}
	
	public static void createTopLevelTag(String tagName, String permission){
		tagManagerInGridView.tryClick(createTagBtnLocator, createBtnLocator);
		tagManagerInGridView.editText(tagNameFieldLocator, tagName);
		if(permission.equals("") == false)
			tagManagerInGridView.selectFromDrowdownByText(tagPermissionFieldLocator, permission);
		tagManagerInGridView.tryClick(createBtnLocator, 5);
		
		AutomationDataCleaner.listOfTagsCreated.add(tagName);
		
		System.out.println( Logger.getLineNumber() + "Top level tag created with name '" + tagName + "'...");
	}
	
	public static void createSubTagFor(String parentTagName, String subTagName, String subTagsPermission){
		openUpSubtagCreationWindowFor(parentTagName);
		fillUpWithNewData(subTagName, subTagName, subTagsPermission);
		
		tagManagerInGridView.tryClick(createBtnLocator, 5);
		
		AutomationDataCleaner.listOfTagsCreated.add(subTagName);
		
		System.out.println( Logger.getLineNumber() + "Subtag created with name '" + subTagName + "' under Top level tag '" + parentTagName + "'...");
	}
	
	public static void openUpSubtagCreationWindowFor(String parentTagName){
		selectTagFilter("All");
		searchForTag(parentTagName);
		List<WebElement> tagList = tagManagerInGridView.getElements(tagItemLocator);
		
		tagList.stream().filter(tag -> tag.getText().trim().equals(parentTagName))
				.findFirst().ifPresent(tag -> tagManagerInGridView.hoverOverElement(tag));
		
		//tagManagerInGridView.hoverOverElement(tagItemLocator);	
		//tagManagerInGridView.tryClick(createSubTagLocator, createBtnLocator);
		WebElement subtagElement = tagManagerInGridView.getElement(createSubTagLocator);
		JavascriptExecutor jse = (JavascriptExecutor) Driver.getDriver();
		jse.executeScript("arguments[0].click();", subtagElement);
	
		System.out.println( Logger.getLineNumber() + "Subtag window is opened for Parent Tag: " + parentTagName + "...");
	}
	
	public static void selectTagFilter(String filterName){
		if(tagManagerInGridView.getSelectedItemFroDropdown(tagFilterLocator).equals(filterName)){
			System.out.println( Logger.getLineNumber() + "Filter '" + filterName + "' already selected...");
		}else{
			tagManagerInGridView.selectFromDrowdownByText(tagFilterLocator, filterName);
			waitForTagLoadingMsg();
			//System.out.println( Logger.getLineNumber() + "Tags loaded...");
			System.out.println( Logger.getLineNumber() + "'" + filterName + "' selected...");
		}
	}
	
	public static void waitForTagLoadingMsg(){
		try{
			tagManagerInGridView.waitForVisibilityOf(tagLoadingMsgLocator);
			tagManagerInGridView.waitForInVisibilityOf(tagLoadingMsgLocator);
		}catch(Exception ex){
			System.err.println("Error:: " + ex.getMessage() + "\n Waiting some more time...");
			tagManagerInGridView.waitFor(10);
		}
	}
	
	public static void searchForTag(String tagName){
		tagManagerInGridView.editText(tagSearchInputFieldLocator, tagName);
		tagManagerInGridView.tryClick(tagSearchBtnLocator);
		waitForTagLoadingMsg();
	}	
	
	
	public static void openTagInfoDetail(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
		
		List<WebElement> tagList = tagManagerInGridView.getElements(tagItemLocator);
		
		for(WebElement tag : tagList){
			if(tagManagerInGridView.getText(tag).equals(tagName)){
				//actions.moveToElement(tagInfo).build().perform();
				tagManagerInGridView.hoverOverElement(tag);
				tagManagerInGridView.waitFor(1);
				System.err.println(tagManagerInGridView.getTotalElementCount(tagInfoDetailLocator)+"^^^^");
				//tagManagerInGridView.tryClick(tagInfoDetail);
				WebElement infoLink = tagManagerInGridView.getElement(tagInfoDetailLocator);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				jse.executeScript("arguments[0].click();", infoLink);
				tagManagerInGridView.waitFor(5);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "\nTag info modal opened ...");
	}
	
	
	public static void closeTagInfoDetail(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
		
		List<WebElement> tagList = tagManagerInGridView.getElements(tagItemLocator);
		
		for(WebElement tag : tagList){
			if(tagManagerInGridView.getText(tag).equals(tagName)){
				//actions.moveToElement(tagInfo).build().perform();
				tagManagerInGridView.hoverOverElement(tag);
				tagManagerInGridView.waitFor(1);
				System.err.println(tagManagerInGridView.getTotalElementCount(tagInfoDetailLocator)+"^^^^");
				//tagManagerInGridView.tryClick(tagInfoDetail);
				WebElement infoLink = tagManagerInGridView.getElement(tagInfoDetailLocator);
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				jse.executeScript("arguments[0].click();", infoLink);
				tagManagerInGridView.waitFor(5);
				break;
			}
		}
		
		System.out.println( Logger.getLineNumber() + "\nTag info modal opened ...");
	}
	
	public static String addDocumentsForTag(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
		//tagManagerInGridView.hoverOverElement(tagItemCheckboxLocator);
		//TagAccordion.selectTag(tagName, true);
		tagManagerInGridView.tryClick(tagItemCheckboxLocator);
		tagManagerInGridView.tryClick(addDocumentsBtnOnTopLocator);
		
		String alertMsg = tagManagerInGridView.getAlertMsg();
		
		System.out.println( Logger.getLineNumber() + "Documents added to '" + tagName + "' tag...");
		
		return alertMsg;
	}
	
	
	public static String removeDocumentsForTag(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
		/*tagManagerInGridView.hoverOverElement(tagItemLocator);
		//TagAccordion.selectTag(tagName, true);
		//tagManagerInGridView.tryClick(tagItemCheckboxLocator);
		tagManagerInGridView.tryClick(removeDocumentsToTagLocator, 1);*/
		
		tagManagerInGridView.hoverOverElement(tagItemLocator);
		
		tagManagerInGridView.waitFor(1);
		tagManagerInGridView.waitForVisibilityOf(removeDocFromTagocator);
		
		WebElement removeTagElement = tagManagerInGridView.getElement(removeDocFromTagocator);
		JavascriptExecutor jse = (JavascriptExecutor) Driver.getDriver();
		jse.executeScript("arguments[0].click();", removeTagElement);
		
		String removeDocsMsg = tagManagerInGridView.getAlertMsg();
		
		/*tagManagerInGridView.waitForAttributeIn(By.id("messageContainer"), "style", "block");
		tagManagerInGridView.waitForAttributeIn(By.id("messageContainer"), "style", "none");*/
		
		tagManagerInGridView.waitFor(10);
		System.out.println( Logger.getLineNumber() + "Documents removed from '" + tagName + "' tag...");
		
		return removeDocsMsg;
	}
	
	
	public static String editTag(String tagName, String newTagName, String newPermission){
		openUpEditWindowFor(tagName);
		fillUpWithNewData(tagName, newTagName, newPermission);
		
		tagManagerInGridView.tryClick(updateBtnLocator);
		
		String alertText = tagManagerInGridView.getAlertMsg();
		
		//tagManagerInGridView.waitFor(10);
		
		System.out.println( Logger.getLineNumber() + "Tag edited with new name '" + newTagName + "' from '" + tagName + "'...");
		
		return alertText;
	}
	
	
	public static void openUpEditWindowFor(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
		List<WebElement> tagList = tagManagerInGridView.getElements(tagItemLocator);
		
		WebElement tagItem = tagList.stream()
									.filter(tag -> tag.getText().trim().equals(tagName))
									.findFirst()
									.get();
		
		tagManagerInGridView.hoverOverElement(tagItem);
		
		WebElement editTagElement = tagManagerInGridView.getElement(editTagLocator);
		JavascriptExecutor jse = (JavascriptExecutor) Driver.getDriver();
		jse.executeScript("arguments[0].click();", editTagElement);
		
		System.out.println( Logger.getLineNumber() + "Edit window opened for tag: "  + tagName);
	}
	
	
	public static void fillUpWithNewData(String oldTagName, String newTagName, String newPermission){
		tagManagerInGridView.waitFor(5);
		tagManagerInGridView.editText(tagNameFieldLocator, newTagName);
		if(newPermission.equals("") == false)
			tagManagerInGridView.selectFromDrowdownByText(tagPermissionFieldLocator, newPermission);
	
		System.out.println( Logger.getLineNumber() + "New data entered for tag to be created/edited: " + oldTagName);
	}
	
	
	public static String deleteTag(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
			
		tagManagerInGridView.hoverOverElement(tagItemLocator);
		
		tagManagerInGridView.waitFor(1);
		tagManagerInGridView.waitForVisibilityOf(deleteTagLocator);
		
		WebElement deleteTagElement = tagManagerInGridView.getElement(deleteTagLocator);
		JavascriptExecutor jse = (JavascriptExecutor) Driver.getDriver();
		jse.executeScript("arguments[0].click();", deleteTagElement);
		
		String deleteConfirmationMsg = tagManagerInGridView.getAlertMsg();
		tagManagerInGridView.waitFor(5);
		
		//AutomationDataCleaner.popTagFromList();
		
		System.out.println( Logger.getLineNumber() + "Tag deleted with name '" + tagName + "' from taglist...");
		
		return deleteConfirmationMsg;
	}
	
	
	public static int getDocumentCountForTag(String tagName){
		/*selectTagFilter("All");
		searchForTag(tagName);*/
		List<WebElement> tagList = tagManagerInGridView.getElements(tagItemLocator);
		
		WebElement docCountElem = tagList.stream().filter(t -> t.getText().trim().equals(tagName)).findFirst().get();
		System.out.println( Logger.getLineNumber() + docCountElem.getText() + "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		
		return Integer.parseInt(docCountElem.findElement(By.className("tag-docNum")).getText().replaceAll("\\D+", ""));
	}
	
	public static boolean isTagFound(String tagName){
		selectTagFilter("All");
		searchForTag(tagName);
		List<String> tags = tagManagerInGridView.getListOfItemsFrom(tagItemLocator, item -> tagManagerInGridView.getText(item));
		
		return tags.stream().anyMatch(tag -> tag.equals(tagName));
	}
}
