package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.util.TestHelper;

import com.icontrolesi.automation.platform.util.Logger;

public class UserManager implements CommonActions{

	static WebDriver driver = Driver.getDriver();
	
	public static final String ISSUE_REVIEW = "Issue Review";
	public static final String OBJECTIVE_CODING = "Objective Coding";
	public static final String PRIVILEGE_REVIEW = "Privilege Review";
	public static final String QC_OBJECTIVE_CODING = "QC Objective Coding";
	public static final String SEARCH = "Search";
	
	public static final String NO_ACCESS = "no_access";
	public static final String USE = "use";
	public static final String ASSIGN = "assign";
	
	public static final String SORT_USERNAME="";
	public static int countTabSwitch=0;
	
	public static By tabsLocator = By.cssSelector("#tabs li a");
	public static By usersTabbtnLocatorsUnder = By.cssSelector("#tabs span.ui-button-text");
	public static By usersTableHeaderLocator = By.cssSelector("#tabs th[id^='availableUsers_'] div[class$='sortable']");
	public static By wfTableHeaderLocator = By.cssSelector("#tabs th[id^='workflowStateAccesses_'] div[class$='sortable']");
	public static By generalAccessTableHeaderLocator = By.cssSelector("div[id^='jqgh_generalAccesses_']");
	
	public static By usersRowLocator = By.cssSelector("#availableUsers tr[id]");
	public static By generalAccessRowLocator = By.cssSelector("#generalAccesses tr[id]");
	public static By generalAccessNameLocator = By.cssSelector("td[aria-describedby='generalAccesses_access']");
	
	public static By availableUserSelectBoxLocator = By.cssSelector("td[aria-describedby='availableUsers_cb'] input");
	public static By availableUserTeamColumnLocator = By.cssSelector("td[aria-describedby='availableUsers_teamName']");
	public static By availableUserFullNameColumnLocator = By.cssSelector("td[aria-describedby='availableUsers_userFullName']");
	public static By availableUserIDColumnLocator = By.cssSelector("td[aria-describedby='availableUsers_userID']");
	public static By availableUserCompanyColumnLocator = By.cssSelector("td[aria-describedby='availableUsers_userCompany']");
	public static By userTableSearchFieldLocator = By.cssSelector("#users_tab input[id^='gs_']");
	
	public static By userTeamFilterLocator = By.id("gs_teamName");
	
	public static By loadSettingBtnLocator = By.id("loadAccesses"); 
	public static By createTeamBtnLocator = By.cssSelector("#createTeam > span");
	public static By updateTeamBtnLocator = By.id("updateTeam");
	public static By teamSelectDropdownLocator = By.id("teamSelect"); 
	public static By allUsersInUsersCaseLocator = By.cssSelector("#availableUsers > tbody > tr[style$='table-row;'] > td[aria-describedby='availableUsers_teamName']");
	public static By allUsersInTeamUsersLocator = By.cssSelector("#teamUsers > tbody > tr[class^='ui-widget-content'] > td");
	public static By confirmAddUserDialogLocator = By.cssSelector("div[aria-describedby='confirmAddUsersDialog']");
	public static By performAssignmentExcludingLocator = By.xpath("/html/body/div[7]/div[3]/div/button[1]/span");
	public static By performAssignmentIncludingLocator = By.xpath("/html/body/div[7]/div[3]/div/button[2]/span");
	public static By cancelTeamAssignmentLocator = By.xpath("/html/body/div[7]/div[3]/div/button[3]/span");
	public static By selectAllAvailableUsersInUsersCase = By.id("cb_availableUsers");
	public static By selectAllAvailableUsersInTeamUsers = By.id("cb_teamUsers");
	public static By infoBtnLocator = By.id("btnShowSettingsInfo");
	public static By cancelLoadedSettingsLocator = By.id("cancel");
	public static By updateLoadedSettingsLocator = By.id("updateAccesses");
	
	
	public static By loadedUserIDColumn = By.cssSelector("td[aria-describedby='loadedUsersOriginal_userID']");
	
	public static UserManager userManager = new UserManager();
	
	public static void open(){
		SprocketMenu.openUserManager();
		userManager.waitForNumberOfElementsToBeGreater(usersRowLocator, 1);
	}	
	
	public void loadUserSettings(WebDriver driver,SearchPage sp, String userName){
		acceptAlert(driver);
		
		checkUser(userName);
		
		waitFor(3);
		//Click the "Load setting" button
		click(SearchPage.waitForElement("loadAccesses", "id"));
		waitFor(5);
	}
	
	
	public static boolean isUserManagerPage(){
		userManager.switchToDefaultFrame();
		By reportTab = By.id("reportTab");
		
		return userManager.getCssValue(reportTab, "display").endsWith("block") && userManager.getText(reportTab).equals("User Manager");
	}
	
	public static void selectUsers(String ...usersList){
		for(String user : usersList){
			checkUser(user);
		}
	}
	
	public static void loadUsersSettings(String ... usersList){
		selectUsers(usersList);
		loadAllSelectedUserSettings();
	}
	
	public static void loadAllSelectedUserSettings(){
		userManager.click(SearchPage.waitForElement("loadAccesses", "id"));
		userManager.tryClick(By.id("loadAccesses"));
		userManager.waitForAttributeIn(By.id("generalAccesses"), "style", "table");
		userManager.waitForAttributeIn(By.id("workflowStateAccesses"), "style", "table");
		System.out.println( Logger.getLineNumber() + "User(s) settings loaded...");
	}
	
	public static void loadUserSettings(String userName){
		checkUser(userName);
		loadAllSelectedUserSettings();
	}
	
	
	public static void cancelLoadedSettings(){
		userManager.tryClick(cancelLoadedSettingsLocator);
		System.out.println( Logger.getLineNumber() + "Loaded settings for selected users have been cancelled...");
	}
	
	
	public static void filterUserTable(By locator, String filterValue){
		userManager.editText(locator, filterValue);
	}
	
	public static void filterByTeam(String teamName){
		filterUserTable(userTeamFilterLocator, teamName);
	}
	
	public static void checkTabEementsCleanlyVisible(){
		List<String> _generalAccessTableHeaderList = Arrays.asList("", "Access", "Description", "");
		List<String> _wfTableHeaderList = Arrays.asList("No Access", "Use", "Assign", "Keep Current", "Workflow State", "");
		List<String> _usersTableHeaderList = Arrays.asList("Team Name", "User Full Name", "User ID", "User Company");
		List<String> _btnTxtList = Arrays.asList("Load Settings", "Copy Settings", "Help", "Reset All", "Update Settings", "Cancel", "Settings Info", "Reset Team");
		
		List<String> tabsNameList = userManager.getListOfItemsFrom(tabsLocator, tab -> userManager.getText(tab));
		List<String> userBtnsTextList = userManager.getListOfItemsFrom(usersTabbtnLocatorsUnder, btnTxt -> userManager.getText(btnTxt));
		List<String> usersTableHeaderList = userManager.getListOfItemsFrom(usersTableHeaderLocator, header -> userManager.getText(header));
		List<String> wfTableHeaderList = userManager.getListOfItemsFrom(wfTableHeaderLocator, header -> userManager.getText(header));
		List<String> generalAccessTableHeaderList = userManager.getListOfItemsFrom(generalAccessTableHeaderLocator, header -> userManager.getText(header));
		
		System.err.println(userBtnsTextList);
		System.err.println(userBtnsTextList.toString());
		
		System.err.println(usersTableHeaderList);
		System.err.println(usersTableHeaderList.toString());
		
		new TestHelper().softAssert.assertEquals(tabsNameList.get(0), "Users", "***) Tab displayed (" + tabsNameList.get(0) + "):: ");
		new TestHelper().softAssert.assertEquals(tabsNameList.get(1), "Teams", "***) Tab displayed (" + tabsNameList.get(1) + "):: ");
		
		new TestHelper().softAssert.assertEquals(userBtnsTextList, _btnTxtList, "***) All buttons present for Users tab:: ");
		new TestHelper().softAssert.assertEquals(usersTableHeaderList, _usersTableHeaderList, "***) All headers match for the User table:: ");
		new TestHelper().softAssert.assertEquals(wfTableHeaderList, _wfTableHeaderList, "***) All headers match for the Workflow State Access table:: ");
		new TestHelper().softAssert.assertEquals(generalAccessTableHeaderList, _generalAccessTableHeaderList, "***) All headers match for the General Access table:: ");
	}
	
	public static void checkSortingForUserHeadersWorks(){
		List<WebElement> userHeaderElementList = userManager.getElements(usersTableHeaderLocator);
		
		int columnPosition = 2;
		for(WebElement header : userHeaderElementList){
			header.click();
			
			List<String> headerListBeforeSorting = userManager.getListOfItemsFrom(By.cssSelector("#availableUsers tbody tr[id] td:nth-child(" + (columnPosition) + ")"), item -> userManager.getText(item));
			
			header.click();
			
			List<String> headerListAfterSorting = userManager.getListOfItemsFrom(By.cssSelector("#availableUsers tbody tr[id] td:nth-child(" + (columnPosition) + ")"), item -> userManager.getText(item));
			
			new TestHelper().softAssert.assertNotEquals(headerListAfterSorting, headerListBeforeSorting, "***) Sorting works for column header: " + header.getText() + "(order reversed):: ");
			
			header.click();	
			
			List<String> headerListAfterSorting2 = userManager.getListOfItemsFrom(By.cssSelector("#availableUsers tbody tr[id] td:nth-child(" + (columnPosition) + ")"), item -> userManager.getText(item));
			
			new TestHelper().softAssert.assertEquals(headerListAfterSorting2, headerListBeforeSorting, "***) Sorting works for column header: " + header.getText() + "(order preserved):: ");
			
			columnPosition++;
		}
	}
	
	public static void checkFilteringForUserHeaderWorks(){
		List<WebElement> searchFieldList = userManager.getElements(userTableSearchFieldLocator);
		
		List<String> searchValues = Arrays.asList("DEM", "Test", "test", "icontrol");
		int cellPos = 2;
		for(WebElement searchField : searchFieldList){
			String columnLocator = "#availableUsers tr[id] td:nth-child(" + cellPos + ")";
			String filteringType = searchField.getAttribute("name") ;
			String searchVal = searchValues.get(cellPos-2).toLowerCase();
			System.out.println( Logger.getLineNumber() + "\nChecking filtering for '" + filteringType + "'...");
			List<String> columnElems = userManager.getListOfItemsFrom(By.cssSelector(columnLocator), item -> userManager.getText(item));
			userManager.editText(searchField, searchVal);
			List<WebElement> columnElemsAfterFiltering = userManager.getElements(By.cssSelector(columnLocator));
			boolean isFilteringOk = columnElemsAfterFiltering.stream().allMatch(item -> userManager.getText(item).toLowerCase().contains(searchVal));
			
			for(int i = 0; i < searchVal.length(); i++){
				searchField.sendKeys(Keys.BACK_SPACE);
			}
			
			userManager.waitFor(1);
			List<String> columnElemsAfterFiltering2 = userManager.getListOfItemsFrom(By.cssSelector(columnLocator), item -> userManager.getText(item));

			new TestHelper().softAssert.assertTrue(columnElems.size() >= columnElemsAfterFiltering.size(), "***) Size change for filtering '" + filteringType + "':: ");
			new TestHelper().softAssert.assertTrue(isFilteringOk, "***) Filtering works for '" + filteringType + "':: ");
			new TestHelper().softAssert.assertEquals(columnElems, columnElemsAfterFiltering2, "***) Filtering works for '" + filteringType + "' (Clearing Filter):: ");
			
			cellPos++;
		}
	}
	
	
	public static void setGeneralAccess(WebDriver driver,SearchPage sp,String which, boolean check){
		List<WebElement> generalAccessItems= userManager.getElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
		for(int i=0;i<generalAccessItems.size();i++){
			
			if(generalAccessItems.get(i).getAttribute("title").equalsIgnoreCase(which)){
				WebElement precedingSibling= SearchPage.getPrecedingSibling(generalAccessItems.get(i));
				WebElement checkBox= precedingSibling.findElement(By.cssSelector("input"));
				
				if(check){
					  if(!checkBox.isSelected()){
							checkBox.click();
							userManager.tryClick(updateLoadedSettingsLocator);
							userManager.tryClick(By.id("saveAccesses"));
							
							userManager.acceptAlert();
						}
				}else {
				
					if(checkBox.isSelected()){
							checkBox.click();
							userManager.tryClick(updateLoadedSettingsLocator);
							userManager.tryClick(By.id("saveAccesses"));
							
							userManager.acceptAlert();
						}
				}
			}
		}
	}
	
	
	public static boolean getGeneralAccess(String generalAccessName){
		List<WebElement> generalAccessItems = userManager.getElements(generalAccessNameLocator);
		
		for(WebElement generalAccessItem : generalAccessItems){
			if(userManager.getText(generalAccessItem).equals(generalAccessName)){
				return userManager.getParentOf(generalAccessItem).findElement(By.cssSelector("td > input")).isSelected();
			}
		}
		
		return false;
	}

	public static void setGeneralAccess(String which, boolean check){
		List<WebElement> generalAccessItems= userManager.getElements(generalAccessNameLocator);
		for(int i=0;i<generalAccessItems.size();i++){
			
			if(generalAccessItems.get(i).getAttribute("title").equalsIgnoreCase(which)){
				WebElement precedingSibling= SearchPage.getPrecedingSibling(generalAccessItems.get(i));
				WebElement checkBox= precedingSibling.findElement(By.cssSelector("input"));
				
				if(check){
					  if(!checkBox.isSelected()){
							checkBox.click();
							userManager.tryClick(updateLoadedSettingsLocator);
							userManager.tryClick(By.id("saveAccesses"));
							
							userManager.acceptAlert();
						}
				}else {
				
					if(checkBox.isSelected()){
							checkBox.click();
							userManager.tryClick(updateLoadedSettingsLocator);
							userManager.tryClick(By.id("saveAccesses"));
							
							userManager.acceptAlert();
						}
				}
			}
		}
	}

	
	//Should have proper access to Driver.getDriver();
public static void setWorkflowStateAccess(String workflowState,String type){
	
	if(workflowState.equalsIgnoreCase(ISSUE_REVIEW)){
		List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_0"));
		WebElement element = getProperElement(radioElements, type);
		setState(element);
	}else if(workflowState.equalsIgnoreCase(OBJECTIVE_CODING)){
		List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_1"));
		WebElement element = getProperElement(radioElements, type);
		setState(element);
	}else if(workflowState.equalsIgnoreCase(PRIVILEGE_REVIEW)){
		List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_2"));
		WebElement element = getProperElement(radioElements, type);
		setState(element);
	}else if(workflowState.equalsIgnoreCase(QC_OBJECTIVE_CODING)){
		List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_3"));
		WebElement element = getProperElement(radioElements, type);
		setState(element);
	}else if(workflowState.equalsIgnoreCase(SEARCH)){
		List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_4"));
		WebElement element = getProperElement(radioElements, type);
		setState(element);
	}
}		
		
		
		public static void getWorkflowStateAccess(String workflowState,String type) throws InterruptedException{
			loadUserSettings("ice");
			Thread.sleep(Utils.WAIT_MEDIUM);
			
			if(workflowState.equalsIgnoreCase(ISSUE_REVIEW)){
				List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_0"));
				WebElement element = getProperElement(radioElements, type);
				getState(element);
			}else if(workflowState.equalsIgnoreCase(OBJECTIVE_CODING)){
				List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_1"));
				WebElement element = getProperElement(radioElements, type);
				getState(element);
			}else if(workflowState.equalsIgnoreCase(PRIVILEGE_REVIEW)){
				List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_2"));
				WebElement element = getProperElement(radioElements, type);
				getState(element);
			}else if(workflowState.equalsIgnoreCase(QC_OBJECTIVE_CODING)){
				List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_3"));
				WebElement element = getProperElement(radioElements, type);
				getState(element);
			}else if(workflowState.equalsIgnoreCase(SEARCH)){
				List<WebElement> radioElements = userManager.getElements(By.id("wfs_access_4"));
				WebElement element = getProperElement(radioElements, type);
				getState(element);
			}
			
			
			
		}
		
		
		public static WebElement getProperElement(List<WebElement> elements,String condition){
			for(WebElement element : elements){
				if(element.getAttribute("value").equalsIgnoreCase(condition)){
					return element;
				}
			}
			return null;
		}
		
		//clicking on radio button
		public static void setState(WebElement element){
			userManager.click(element);
		}
		
		
		public static boolean getState(WebElement element){
			
			boolean status=false;
			
			if(element.isSelected()){
				
				status=true;
				
			}
			
			
			
			return status;
			
		}
		
		
		//clicking update and save
		public void clickUpdateAndSave(){
			try{
				WebElement el = Driver.getDriver().findElement(By.id("updateAccesses"));
				click(el);
				
				click(SearchPage.waitForElement("saveAccesses", "id"));
				
				//Utils.waitForAlertAndAccept();
				acceptAlert(Driver.getDriver());
			}catch(Exception e){
				//no changes were made
				clickCancelButton();
			}
		}
		
	
		public static void updateLoadedSettings(){
			userManager.tryClick(By.id("updateAccesses"));
			userManager.tryClick(By.id("saveAccesses"));
			userManager.acceptAlert();
			System.out.println( Logger.getLineNumber() + "Loaded Settings updated...");
		}

		
	public static void switchTab(String tabName){
		userManager.switchToDefaultFrame();
		userManager.waitForFrameToLoad(Frame.MAIN_FRAME);
		driver = Driver.getDriver();
		By tabLocation = By.cssSelector("#tabs > ul > li[aria-controls='" + tabName.toLowerCase() + "_tab']");
		if(userManager.getAttribute(tabLocation, "aria-selected").equals("true")){
			System.out.println( Logger.getLineNumber() + tabName.toUpperCase() + " tab already seleceted...");
		}else{
			userManager.tryClick(tabLocation, 5);
			//userManager.waitForVisibilityOf(createTeamBtnLocator );
			System.out.println( Logger.getLineNumber() + "Switched to " + tabName.toUpperCase() + " tab...");
		}
		
		if(tabName.equalsIgnoreCase("teams")){
			userManager.waitForFrameToLoad(Frame.TEAMS_FRAME);
		}
	}
		
	
	public static void selectUserFromAllUsersInCase(boolean isAlreadyAssignedGToAnotherTeam){
		driver = Driver.getDriver();
		List<WebElement> userList = userManager.getElements(allUsersInUsersCaseLocator);
		for(WebElement user : userList){
			if(isAlreadyAssignedGToAnotherTeam && user.getAttribute("title").trim().equals("") == false){
				user.click();
				userManager.waitFor(2);
				//userManager.switchToDefaultFrame();
				//userManager.waitForFrameToLoad(Frame.TEAMS_FRAME);
				userManager.waitForVisibilityOf(confirmAddUserDialogLocator);
				System.out.println( Logger.getLineNumber() + "User selected from All Users In CASE already assigned to a Team selected...");
				break;
			}else if(isAlreadyAssignedGToAnotherTeam == false && user.getAttribute("title").trim().equals("")){
				user.click();
				//userManager.waitFor(5);
				userManager.waitForNumberOfElementsToBePresent(allUsersInUsersCaseLocator, userList.size() - 1);
				System.out.println( Logger.getLineNumber() + "User selected from All Users In CASE not assigned to a Team yet...");
				break;
			}
		}
	}
	
	public static int getUserCountFor(By element){
		return userManager.getTotalElementCount(element);
	}
	
	public static void deselectAllUsersFromTeamUsers(){
		List<WebElement> userList = userManager.getElements(allUsersInTeamUsersLocator);
		for(; ;){
			if(userList.size() == 0)
				break;
			userList.get(0).click();
			//userManager.waitForNumberOfElementsToBePresent(allUsersInTeamUsersLocator, userCount - 1);
			userManager.waitFor(3);
			userList = userManager.getElements(allUsersInTeamUsersLocator);
		}
	}
	
	
	public static void deselectUnassignedUserFromAllUsersInCase(){
		List<WebElement> userList = userManager.getElements(By.cssSelector("#availableUsers > tbody > tr > td[aria-describedby='availableUsers_teamName']"));
		for(WebElement user : userList){
			if(user.getAttribute("title").trim().equals("")){
				user.click();
				userManager.waitFor(2);
				System.out.println( Logger.getLineNumber() + "User selected from All Users In CASE...");
				break;
			}
		}
	}
	
	
	public static void closeConfirmAddUserDialog(){
		userManager.tryClick(cancelTeamAssignmentLocator);
		userManager.waitFor(1);
	} 



/*public void createTeam(String name, String description, String role){
	if(countTabSwitch==0){
		switchTab("teams");
	}
	
	//Assign team name
	SearchPage.waitForElement("teamName", "id").sendKeys(name);;
	//Assign description
	SearchPage.waitForElement("teamDesc", "id").sendKeys(description);;
    //Assign Role
	SearchPage.setSelectBox(By.id("teamRole_createTeam"), role);
	
	//Press Create button
	click(SearchPage.waitForElement("createTeam", "id"));
	
	acceptAlert();
	
	countTabSwitch++;
}	*/	
	
	
	public static void createTeam(String name, String description, String role){
		System.out.println( Logger.getLineNumber() + "Creating team: " + name + "...");
		switchTab("teams");
		
		userManager.editText(By.id("teamName"), name);
		userManager.editText(By.id("teamDesc"), description);
		userManager.selectFromDrowdownByText(By.id("teamRole_createTeam"), role);
		
		userManager.tryClick(By.id("createTeam"));
		System.out.println( Logger.getLineNumber() + "Team created: " + name + "...");
		
		userManager.acceptAlert();
	}	
		
	public static String updateTeam(String teamName, String editedName, String description, String role){
		System.out.println( Logger.getLineNumber() + "Updating team: " + teamName + "...");
		switchTab("teams");
		selectTeam(teamName);
		
		userManager.editText(By.id("teamNameUpdate"), editedName);
		userManager.editText(By.id("teamDescUpdate"), description);
		userManager.selectFromDrowdownByText(By.id("teamRoleUpdate"), role);
		
		userManager.tryClick(By.id("updateTeam"));
		
		String updateMsg = userManager.getAlertMsg();
		
		System.out.println( Logger.getLineNumber() + "Team updated: " + teamName + "...");
		
		return updateMsg;
	}		
		  
	  public static void selectTeam(int teamPosition){
		   userManager.selectFromDrowdownByIndex(teamSelectDropdownLocator, teamPosition);
		   userManager.waitForVisibilityOf(updateTeamBtnLocator);
		   userManager.waitFor(5);
		   System.out.println( Logger.getLineNumber() + "Team selected at positiion: " + (teamPosition + 1));
	   }
	   
	   public static void selectTeam(String teamName){
		   userManager.selectFromDrowdownByText(teamSelectDropdownLocator, teamName);
		   userManager.waitForVisibilityOf(updateTeamBtnLocator);
		   userManager.waitFor(5);
		   System.out.println( Logger.getLineNumber() + "Team '" + teamName + "'  selected...");
	   }
	   
	   public void selectTeamWithoutSwitching(String teamName){
		   SearchPage.setSelectBox(By.id("teamSelect"), teamName);
		   Utils.waitShort();
	   }
	   
	   
	   
	   public static String[] teamProperties(String teamName){
		   String[] properties= new String[10];
		   
		   selectTeam(teamName);
		   
		   properties[0]=SearchPage.waitForElement("teamNameUpdate", "id").getAttribute("value");
		   properties[1]=SearchPage.waitForElement("teamDescUpdate", "id").getAttribute("value");
		   properties[2]=SearchPage.waitForElement("teamRoleUpdate", "id").getAttribute("value");

		   return properties;
	   }
	   
	   
	   public static String deleteTeam(String teamName){
		   	System.out.println( Logger.getLineNumber() + "Deleting team: " + teamName + "...");
		   	selectTeam(teamName);
			userManager.tryClick(By.id("deleteTeamButton"));
			userManager.acceptAlert();
			String deleteMsg = userManager.getAlertMsg();
			System.out.println( Logger.getLineNumber() + "Team deleted: " + teamName + "...");
			return deleteMsg;
		}	   
	   
	   
	   
	/*   public void deleteTeam(String teamName){
		    //switchTab("teams");
			selectTeam(teamName);
			Utils.waitMedium();
			//Press Delete button
			click(SearchPage.waitForElement("deleteTeamButton", "id"));
			//Accept the alert visible
			try {
		        WebDriverWait wait = new WebDriverWait(15);
		        wait.until(ExpectedConditions.alertIsPresent());
		        
		        Alert alert = Driver.getDriver().switchTo().alert();
		        
		        System.out.println( Logger.getLineNumber() + alert.getText());
		 
		        alert.accept();
		    } catch (Exception e) {
		        //exception handling
		    }
			
			acceptAlert(Driver.getDriver());
			
			//Accept the second alert
			try {
		        WebDriverWait wait = new WebDriverWait(15);
		        wait.until(ExpectedConditions.alertIsPresent());
		        
		        Alert alert = Driver.getDriver().switchTo().alert();
		        
		        System.out.println( Logger.getLineNumber() + alert.getText());
		    
		        alert.accept();
		    } catch (Exception e) {
		        //exception handling
		    }
			
			acceptAlert(Driver.getDriver());
			
			//Utils.waitShort();
		}	   
	   */
		
	
	public static boolean teamExists(String teamName){
		
		userManager.getItemsFromDropdown(By.id("teamSelect")).stream().forEach(t -> userManager.getText(t));
		
		boolean teamPresent = userManager.getItemsFromDropdown(By.id("teamSelect")).stream().anyMatch(team -> userManager.getText(team).equals(teamName));
			   
		return teamPresent;
	}
		
		
public void selectUserCaseTable(String userFullName,String teamName,String userID,String userCompany, int conflictResolveOption){
	  
	  //Send null for an argument if it is not needed
	  /*
	   * To resolve conflict:
	   * To mean "Perform team assignment ,excluding conflicted users [conflictResolveOption] will be 0"
	   * To mean "Perform team assignment ,including conflicted users [conflictResolveOption] will be 1"
	   * To Cancel assignment it will be 2
	   * To mean nothing give any other integer value
	   */
	  
	  
	  //Assign team name
	  SearchPage.waitForElement("gs_teamName", "id").sendKeys(teamName);
	  //Assign full name
	  SearchPage.waitForElement("gs_userFullName", "id").sendKeys(userFullName);
    //Assign user ID
	  SearchPage.waitForElement("gs_userID", "id").sendKeys(userID);
    //Assign company name
	  SearchPage.waitForElement("gs_userCompany", "id").sendKeys(userCompany);

	  //Mark the check-box
	  SearchPage.waitForElement("input[id*='jqg_availableUsers']", "css-selector").click();

	  Utils.waitShort();
	  
	  //Handle pop-up if present  
	  try{
		  
		  Driver.getDriver().findElement(By.id("confirmAddUsersDialog_Message"));
		  if(conflictResolveOption==0){
			  
			  
			    List<WebElement> spanNodes= userManager.getElements(By.cssSelector("span[class='ui-button-text']"));
				for(int i=0;i<spanNodes.size();i++){
					
					if(spanNodes.get(i).isDisplayed()){
						
						if(spanNodes.get(i).getText().equalsIgnoreCase("Perform team assignment(s), EXCLUDING conflicted users")){
						//result.nl().record("Button for EXCLUDING conflicted users is present:pass");
						WebElement Button= SearchPage.getParent(spanNodes.get(i));
						click(Button);
						waitFor(5);
						}
						
					}
					
					
				}
			  
			  
			  
		  }
		  else if(conflictResolveOption==1){
			  
			  
			    List<WebElement> spanNodes= userManager.getElements(By.cssSelector("span[class='ui-button-text']"));
				for(int i=0;i<spanNodes.size();i++){
					
					if(spanNodes.get(i).isDisplayed()){
						
						if(spanNodes.get(i).getText().equalsIgnoreCase("Perform team assignment(s). INCLUDING conflicted users")){
						//result.nl().record("Button for INCLUDING conflicted users is present:pass");
						WebElement Button= SearchPage.getParent(spanNodes.get(i));
						click(Button);
						waitFor(5);
						}
						
					}
					
					
				}
			  
			  
			  
		  }
		  else if(conflictResolveOption==2){
			  
			  List<WebElement> spanNodes= userManager.getElements(By.cssSelector("span[class='ui-button-text']"));
				for(int i=0;i<spanNodes.size();i++){
					
					if(spanNodes.get(i).isDisplayed()){
						
						if(spanNodes.get(i).getText().equalsIgnoreCase("Cancel team assignment(s)")){
						
						WebElement Button= SearchPage.getParent(spanNodes.get(i));
						click(Button);
						waitFor(5);
						}
						
					}
					
					
				}
			  
		  }
		  
		  Utils.waitShort();
		  
		  
		  //Handle alert message if present
		acceptAlert(Driver.getDriver());
			
		  
		  
	  }//end try
	  
	  catch(NoSuchElementException e){
		  
		  System.out.println( Logger.getLineNumber() + "No conflict during assignment");
		  
	  }//end catch
	  
}	   


public static boolean isUserExists(String userID){
	List<WebElement> userIDs= userManager.getElements(By.cssSelector("td[aria-describedby='teamUsers_userID']"));
    for(WebElement uID : userIDs){
    	if(uID.getText().trim().equals(userID))
    		return true;
    }
    return false;
}	
	  
	  
	  public boolean userExistsInLoadedTable(String userFullName,String userID,String userCompany){
		  	waitFor(10);
		  	boolean userPresent=false;
			
			List<WebElement> userIDs= userManager.getElements(By.cssSelector("td[aria-describedby='availableUsers_userID']"));
		    for(int i=0;i<userIDs.size();i++){
		    	if(userIDs.get(i).getText().contains(userID)){
		    		userPresent=true;
		    	    break;	
		    	}
		    }
		  
		return userPresent;
	}	  
	
	 //This method removes a specific user
	  public void removeUserFromLoadedTable(String userFullName,String userID,String userCompany){
	 		 
			 
			 //Assign user full name
			//  SearchPage.waitForElement("gs_userFullName", "id").sendKeys(userFullName);
	         //Assign user ID
			//  SearchPage.waitForElement("gs_userID", "id").sendKeys(userID);
	         //Assign company name
			//  SearchPage.waitForElement("gs_userCompany", "id").sendKeys(userCompany);

		  /*
		   List<WebElement> userIDs= userManager.getElements(By.id("gs_userID"));
		   
		   for(int i=0;i<userIDs.size();i++){
			   
			   if(userIDs.get(i).isDisplayed()){
				   
				   SearchPage.waitForElement("gs_userID", "id").sendKeys(userID);
				   
			   }
			   
		   }
		  
		  */
		  
		  WebElement teamUsers= Driver.getDriver().findElement(By.id("gview_teamUsers"));
		  WebElement userIDLoadedTable= teamUsers.findElement(By.id("gs_userID"));
		  userIDLoadedTable.sendKeys(userID);
		  
		  
		  
		     Utils.waitShort();

			  
			 //Mark the check-box for selecting all users from the table(this will be used to remove the desired user) 
			 WebElement checkBox= SearchPage.waitForElement("input[id='cb_teamUsers']", "css-selector") ;
			 checkBox.click();
			 Utils.waitShort();
			 
		 }	 
	 
	 
	 //Remove all users
	  public void removeAllUsersFromLoadedTable(){
			 SearchPage.waitForElement("input[id='cb_teamUsers']", "css-selector") ;
			 Utils.waitShort();
		 }
	 
	 
	 public static void checkUser(String user){
			//acceptAlert(Driver.getDriver());

			//waitFor(20);
			List<WebElement> allUserIds = userManager.getElements(By.cssSelector("td[aria-describedby='availableUsers_userID']"));
			for(WebElement e : allUserIds){
				if(e.getText().equalsIgnoreCase(user)){
					WebElement trEl = SearchPage.getParent(e);
					WebElement chkEl = trEl.findElement(By.cssSelector("td[aria-describedby='availableUsers_cb']"));
					userManager.click(chkEl.findElement(By.tagName("input")));
					break;
				}
			}
		}
		
		
		public void clickLoadSettings(){
			//Click the "Load setting" button
			//Thread.sleep(10000);
			click(SearchPage.waitForElement("loadAccesses", "id"));
			//Utils.waitMedium();
			waitForAttributeIn(By.id("generalAccesses"), "style", "display: table;");
		}
	 
	 
		public void clickCancelButton(){
			click(Utils.waitForElement("cancel", "id"));
			waitFor(8);
		}
		
		
		public static void clickInfoButton() {
			userManager.tryClick(infoBtnLocator);
		}
		
		
		public static void clickBackButton() {
			userManager.tryClick(By.id("backOriginal"));
		}
		
		
		public static void selectAllAvailableUserFromUsersCase(){
			userManager.tryClick(selectAllAvailableUsersInUsersCase);
			userManager.waitFor(1);
		}
		
		public static void selectAllAvailableUserFromTeamUsers(){
			userManager.tryClick(selectAllAvailableUsersInTeamUsers);
			userManager.waitFor(1);
		}
	 
		public void setGeneralAccessWithoutSave(WebDriver driver,SearchPage sp,String which){
			
			List<WebElement> generalAccessItems= userManager.getElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
			for(int i=0;i<generalAccessItems.size();i++){
				
				
				if(generalAccessItems.get(i).getAttribute("title").equalsIgnoreCase(which)){
					
					
					WebElement precedingSibling= SearchPage.getPrecedingSibling(generalAccessItems.get(i));
					WebElement checkBox= precedingSibling.findElement(By.cssSelector("input"));
		
					click(checkBox);
				
					acceptAlert(Driver.getDriver());
						
					}
					
				}
				
		}
		
		
		
		//added by Shaheed Sagar
		public void clickResetAll(){
			click(Driver.getDriver().findElement(By.id("resetAll")));
		}
		
		public static void copySettingsForSelectedUsers(){
			userManager.tryClick(By.id("copyApplyAccesses"));
			//pass
		}
		
		public static void clickCopySettingsButton() {
			userManager.tryClick(By.id("copyApplyAccesses"));
		}
		
		
	 	public void clickCloseButton(){
	 		List<WebElement> spanNodes= userManager.getElements(By.cssSelector("span[class='ui-button-text']"));
			for(int i=0;i<spanNodes.size();i++){
				if(spanNodes.get(i).isDisplayed()){
					if(spanNodes.get(i).getText().equalsIgnoreCase("Close")){
						WebElement closeButton= SearchPage.getParent(spanNodes.get(i));
						click(closeButton);
						waitFor(5);
					}
				}
			}
	 	}

	 	
	  public void sortUserTable(String columnName){
		  
		  
		  
		  
		  
	  }
	 	
	  public static void enableFieldRestriction(){
		  if(userManager.isElementChecked(By.id("permission64"))){
			  System.out.println( Logger.getLineNumber() + "Field Restriction for the User is already enabled...");
		  }else{
			  userManager.tryClick(By.id("permission64"), 1);
			  System.out.println( Logger.getLineNumber() + "Field Restriction for the User is enabled...");
		  }
	  }
	  
	  public static void selectTeamRestrictedFields(String ... fields ){
		  By fieldDescriptionLocator = By.cssSelector("td[aria-describedby='teamRestrictedFields_fieldDescription']");
		  
		  List<String> fieldDescriptionList = userManager.getListOfItemsFrom(fieldDescriptionLocator, item -> userManager.getText(item));
		  
		  for(String field : fields){
			  int i = 2;
			  for(String fd : fieldDescriptionList){
				  By fieldSelectionLocator = By.cssSelector("#teamRestrictedFields tr:nth-child(" + (i++) + ") td input");
				  boolean isChecked = userManager.isElementChecked(fieldSelectionLocator);
				  if(field.equals(fd) && isChecked){
					  System.out.println( Logger.getLineNumber() + field + " is already selected...");
				  }else if(field.equals(fd) && !isChecked){
					  userManager.tryClick(fieldSelectionLocator, 1);
					  System.out.println( Logger.getLineNumber() + field + " is selected...");
				  }
			  }
		  }
	  }
}
