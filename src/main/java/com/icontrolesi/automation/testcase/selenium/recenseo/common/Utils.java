package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.icontrolesi.automation.platform.io.Result;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Shaheed Sagar
 *
 */

public class Utils {
	
	//Constant values for using in Thread Sleep Method.
	public static final int WAIT_SHORT = 8000;
	public static final int WAIT_MEDIUM = 15000;
	public static final int WAIT_LONG = 20000;
	private static String baseURL;
	
	//Output handlers
	public static void handleResult(String message, Result result, boolean isPassed){
		if(isPassed){
			System.out.println( Logger.getLineNumber() + message + " state:pass");
			result.nl().record(message + " state:pass");
		}else{
			System.out.println( Logger.getLineNumber() + message + " state:fail");
			result.nl().record(message + " state:fail");
			result.fail();
		}
	}
	
	
	public static WebElement waitForElement(String markUp, String selectorType) {
		try {
			switch (selectorType) {
			case "id":
				WebElement element=  ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By.id(markUp))));
				while(!element.isEnabled()){
				      
				}
				return element;
			case "class-name":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.className(markUp))));
			case "css-selector":
				/*return ((new WebDriverWait(driver, 40))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
								*/
				element=  ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
				return element;
			 case "x-path":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.xpath(markUp))));
			case "name":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.name(markUp))));
			
			case "tag":
				return ((new WebDriverWait(Driver.getDriver(), 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.tagName(markUp))));
			}
			System.out.println( Logger.getLineNumber() + "Invalid selectorType");
			return null;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + markUp + " not found");
			return null;
		}

	}
	
	//element wait handler
	public static WebElement waitForElement(WebDriver driver, String markUp,
			String selectorType) {
		try {
			switch (selectorType) {
			case "id":
				WebElement element=  ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.id(markUp))));
				while(!element.isEnabled()){
				      
				}
				return element;
			case "class-name":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.className(markUp))));
			case "css-selector":
				/*return ((new WebDriverWait(driver, 40))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
								*/
				element=  ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector(markUp))));
				return element;
			 case "x-path":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.xpath(markUp))));
			case "name":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.name(markUp))));
			
			case "tag":
				return ((new WebDriverWait(driver, 120))
						.until(ExpectedConditions.presenceOfElementLocated(By
								.tagName(markUp))));
			}
			System.out.println( Logger.getLineNumber() + "Invalid selectorType");
			return null;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + markUp + " not found");
			return null;
		}

	}
	
	
	//wait till an element is click-able
	public void waitTillClickable(String markup){
		
		
		new WebDriverWait(Driver.getDriver(), 40).until(ExpectedConditions.elementToBeClickable(By.id(markup)));
		
		
		
	}
	
	
		public static boolean isSameColor(WebElement element,String colorToMatch){
			String color = element.getCssValue("background-color");
			String hex = "";
			if(color.startsWith("#") == false){
				if(color.equalsIgnoreCase("transparent")){
					color = "rgba(0, 0, 0, 0)";
				}
				String[] rgb = color.replaceAll("(rgba)|(rgb)|(\\()|(\\s)|(\\))","").split(",");
				hex = String.format("#%s%s%s", toHexValue(Integer.parseInt(rgb[0])), toHexValue(Integer.parseInt(rgb[1])), toHexValue(Integer.parseInt(rgb[2])));
			}
			
			System.out.println( Logger.getLineNumber() + "color is : " + hex);
			
			return hex.equalsIgnoreCase(colorToMatch);
		}
		
		
		public static String toHexValue(int number) {
	        StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
	        while (builder.length() < 2) {
	            builder.append("0");
	        }
	        return builder.toString().toUpperCase();
	    }
	
		
		
		public static void waitShort(){
			try {
				Thread.sleep(Utils.WAIT_SHORT);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		public static void waitMedium(){
			try {
				Thread.sleep(Utils.WAIT_MEDIUM);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		public static void waitLong(){
			try {
				Thread.sleep(Utils.WAIT_LONG);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	

		
		public static void waitForAlertAndAccept(){
			try {
		        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 30);
		        wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = Driver.getDriver().switchTo().alert();
		        alert.accept();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
		
		public static boolean doesDropDownListContains(Select list,String which){
			List<WebElement> options = list.getOptions();
			for(WebElement e : options){
				if(e.getText().equalsIgnoreCase(which)){
					return true;
				}
			}
			return false;
		}
		
		//method for returning a list of items
		public static List<WebElement> returnList(WebDriver driver, String markUp,
				String selectorType) {
			try {
				
				switch (selectorType) {
				case "id":
					Utils.waitForElement(driver, markUp, selectorType);
					List<WebElement> element=  driver.findElements(By.id(markUp));
					return element;
					
				case "class-name":
					
					Utils.waitForElement(driver, markUp, selectorType);
					element=  driver.findElements(By.className(markUp));
					return element;
					
				case "css-selector":
					
					Utils.waitForElement(driver, markUp, selectorType);
					element=  driver.findElements(By.cssSelector(markUp));
					return element;
					
					
				 case "x-path":
					
					 Utils.waitForElement(driver, markUp, selectorType);
					 element=  driver.findElements(By.xpath(markUp));
					 return element;
					 
					 
				case "name":
					
					Utils.waitForElement(driver, markUp, selectorType);
					element=  driver.findElements(By.name(markUp));
					return element;
					
					
				case "tag":
					
					Utils.waitForElement(driver, markUp, selectorType);
					element=  driver.findElements(By.tagName(markUp));
					return element;
					
				}
				System.out.println( Logger.getLineNumber() + "Invalid selectorType");
				return null;
			} catch (TimeoutException e) {
				System.out.println( Logger.getLineNumber() + "List "+markUp + " not found");
				return null;
			}

		}
		
		//method for assigning values to text fields/areas
		public void assignTextValue(WebElement element, String text){
			
			element.sendKeys(text);
			
			
		}
		
		//method for switching tabs
		public void tabSwitch(WebDriver driver, String tabMarkup, SearchPage sp)throws InterruptedException{
			
			
			SearchPage.waitForElement(driver, tabMarkup, "css-selector").click();
			Thread.sleep(2000);
			
		}
		
		
		public static void waitForFrameLoad(String frameName) {
			WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 20);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		}
		
		//method for switching iframes
		public static void waitForFrameLoad(WebDriver driver, String frameName) {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions
					.frameToBeAvailableAndSwitchToIt(frameName));

		}
		
		
		
		//Wait for Loading message(That comes not somewhere specific, like at the center of a page)
		public void waitWhileLoading(WebDriver driver, SearchPage sp, String markup){
			  
			  try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  
			  WebElement loadingMsg= driver.findElement(By.id(markup));
			  while(loadingMsg.isDisplayed()){
				  
				  
				  
				  
			  }
			  
			  try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  
			  
		  }
		
		
		/*//click on an element
		public  void click(WebElement element) {
	        if (BROWSER_IE11.equalsIgnoreCase(Configuration.getConfig(PROP_BROWSER))) {
	            try{
	                if(element.getTagName().equals("button")){
	                    element.sendKeys("\n");
	                }else element.click();
	            }catch(NullPointerException e){
	                element.sendKeys("\n");
	            }
	           
	        } else {
	            element.click();
	        }
	    }*/
		
		
	   //Get value of selectBox
		public String checkValueOfSelectBox(WebDriver driver,
				String elementMarkUpName) {

			// driver.switchTo().defaultContent();
			Select comboBox = new Select(driver.findElement(By
					.id(elementMarkUpName)));
			String selectedComboValue = comboBox.getFirstSelectedOption().getText();
			System.out.println( Logger.getLineNumber() + "Selected combo value: " + selectedComboValue);

			return selectedComboValue;
		}
		
	  
	  //Select value from a selectBox
		public static void setSelectBox(WebDriver driver, String elementMarkUpName,
				String selectorType, String text) {
			WebElement selectbox = null;
			switch (selectorType) {
			case "id":
				selectbox = driver.findElement(By.id(elementMarkUpName));
				break;
			case "class-name":
				selectbox = driver.findElement(By.className(elementMarkUpName));
				break;
			case "css-selector":
				selectbox = driver.findElement(By.cssSelector(elementMarkUpName));
				break;
			case "x-path":
				selectbox = driver.findElement(By.xpath(elementMarkUpName));
				break;
			}
			new Select(selectbox).selectByVisibleText(text);
		}	
		
		
	 //Get value from a text field/area
	 public String getTextValue(WebElement element){
		
		 return element.getAttribute("value");
		 
		 	 
	 }
	
		
	 //Go to specific node/return children nodes
		public static WebElement getParent(WebDriver driver, WebElement element){
			
			WebElement parentNode= element.findElement(By.xpath("parent::*"));
			
			return parentNode;
			
		}
		
		
		public static WebElement getPrecedingSibling(WebDriver driver, WebElement element){
			
			WebElement precedingSibling= element.findElement(By.xpath("preceding-sibling::*"));
			
			return precedingSibling;
			
		}
		
		
		public static WebElement getFollowingSibling(WebElement element){
			WebElement followingSibling = element.findElement(By.xpath("following-sibling::*"));
			
			return followingSibling;
		}
		
	    public static WebElement getFollowingSibling(WebDriver driver, WebElement element){
			
	    	
			WebElement followingSibling= element.findElement(By.xpath("following-sibling::*"));
			
			return followingSibling;
			
		}
		
		
		
		public List<WebElement> getAllChildren(WebDriver driver, WebElement element){
			
			List<WebElement> children= element.findElements(By.xpath(".//*"));
			
			return children;
			
		}
		
		/*
		public WebElement navigateUpTheHierarchy(WebDriver driver, WebElement element){
			
			WebElement parentNode= element.findElement(By.xpath("parent::div/parent::div/parent::div/parent::li"));
			return parentNode;
		}
		*/
		
		
		//Return a list of specific ancestors with specific attribute value
		public List<WebElement> findAncestor(WebDriver driver, String markup){
				
			
			// The format of [markup] will be like this [//span[@class='open']/ancestor::li[contains(@class,'assignment visible')]], depending on the html tree this content will change 
			
			List<WebElement> liParent= driver.findElements(By.xpath(markup));		
			
			return liParent;
			
			
			
		}	
		
		
	/*	//Another method to go to a specific ancestor of an element
		public void findPredecessor(WebElement element, String markup){
			
			//markup will have a format similar to this ""parent::div/parent::div/parent::div/parent::li""
			WebElement liParent= element.findElement(By.xpath(markup));
			
			
			
		}*/
		
		
		  public String[] getChildrenWindows(WebDriver driver, SearchPage sp){
			  
			  String windows[]= new String[10]; 
			  	  
			  	  //Get all the window handles in a set
			        Set <String> handles =driver.getWindowHandles();
			        Iterator<String> it = handles.iterator();
			        //iterate through your windows
			        
			        int i = 0;
			        while (it.hasNext()){
			        
			        windows[i]=it.next();
			        i++;
			        
			        //driver.switchTo().window(windows[1]);
			        }//end while
			        
			        return windows;
			        
			       
			  	  
			  	  
			    }//end function 	
		  
		 
		  //Draw an area (for example make red-acts)
		  public void drawArea(String markup) throws InterruptedException{
			  
			  
			    //Get reference of element to drag
			    WebElement element= Driver.getDriver().findElement(By.cssSelector(markup));
			  
				//Click button for red-act
			    
				//Perform drag operation
				Utils.waitLong();

				Actions actions = new Actions(Driver.getDriver()); 
				actions.dragAndDropBy(element,100, 100).perform(); 
				
				Utils.waitLong();

			  
		  }
		  
		  //Drag and drop an element
        public void dragAndDropElement(String sourceDiv, String destinationDiv){
        	
        	
        	WebElement element = Driver.getDriver().findElement(By.xpath(sourceDiv));
     	    WebElement target = Driver.getDriver().findElement(By.xpath(destinationDiv));

     	    (new Actions(Driver.getDriver())).dragAndDrop(element, target).perform();
     	    
     	    try {
     			Thread.sleep(3000);
     		} catch (InterruptedException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
        	
        	
        	
        	
        }
        
        //Hover action(for example navigate through a menu)
        public void performHoverAction(WebElement element){
        	
        	Actions actions = new Actions(Driver.getDriver());
        	
        	actions.moveToElement(element).build().perform();
    	    element.click();
        	
        	
        	
        	
        	
        }
        
        
        
        public static String getCurrentURL(){
        	
        	
    		return Utils.baseURL;
    	}
    	
    	public static void setCurrentURL(){
    		
    		String url = Driver.getDriver().getCurrentUrl();
    		//String[] splittedUrl= url.split("case");
    		String[] splittedUrl= url.split("case");
    		Utils.baseURL= splittedUrl[0];
    		
    		 
    		
    	}
       
		
    	public void dragAndDropElement2(WebElement topField, WebElement bottomField){
    		
    		
    		 Actions builder = new Actions(Driver.getDriver());

    	        Action dragAndDrop = builder.clickAndHold(bottomField)
    	           .moveToElement(topField)
    	           .release(topField)
    	           .build();

    	        dragAndDrop.perform();
    	}	
    	
    	
    	public static boolean isAscendingOrdered(List<String> items){
    		for(int i = 0; i < items.size() - 1; i++){
    			if(items.get(i).compareToIgnoreCase(items.get(i+1)) > 0)
    				return false;
    		}   
    		
    		return true;
    	}
    	
    	
    	public static boolean isDescendingOrdered(List<String> items){
    		for(int i = 0; i < items.size() - 1; i++){
    			if(items.get(i).compareToIgnoreCase(items.get(i+1)) < 0){
    				return false;
    			}
    		}   
    		
    		return true;
    	}
    	
    	
    	public static void main(String[] args) {
			List<String> list = java.util.Arrays.asList("v", "k", "a");
			
			System.out.println( Logger.getLineNumber() + isDescendingOrdered(list));
		}
}
