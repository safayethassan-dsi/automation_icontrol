package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.platform.util.Logger;

public class ViewPreferences{
	static WebDriver driver = Driver.getDriver(); 
	
	Preferences pt= new Preferences();
	
	public void setColumns(WebDriver driver, SearchPage sp,String[] columnNames){
		/*
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		sp.clickSettingsMenu(driver, "Preferences", sp);
		Thread.sleep(5000);
		//switchToFrame(1);
		pt.switchPreferenceTab(driver,"view");
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		*/
		//Select all check-boxes
		Utils.waitMedium();
		pt.waitFor(driver, 15);
		Utils.waitForElement(driver, "checkAllCols", "id").click();
		pt.waitFor(driver, 10);
		
		//Remove selection from all check-boxes
		Utils.waitForElement(driver, "checkAllCols", "id").click();
		pt.waitFor(driver, 8);
			
		WebElement ul= driver.findElement(By.id("columns"));

		List<WebElement> liChildren= ul.findElements(By.cssSelector("li"));
		if(liChildren.size()==0){
			
			  System.out.println( Logger.getLineNumber() + "List of li nodes for all check-boxes not found");
		      //result.nl().record("List of li nodes for all check-boxes not found");
		      Logger.log("List of li nodes for all check-boxes not found");
		      //result.fail();
			
		}	
		
		
		
		for(WebElement liChild : liChildren){
					WebElement label= liChild.findElement(By.cssSelector("label"));
					WebElement input= label.findElement(By.cssSelector("input"));
					int i=0;
					for(int j=0;j<columnNames.length;j++){	
						if(input.getAttribute("value").equalsIgnoreCase(columnNames[j])){
							input.click();
							pt.waitFor(driver, 2);
							//Click again if necessary
							if(!input.isSelected()){
								input.click();
							}
							i++;
							break;
						}//end if
						if(i>=columnNames.length){
							break;
						}
					}//end nested for	
		}//end for
		
		
		
		Utils.waitMedium();
		
	}//end function

	
	
	public static void selectPreferenceTab(String tabName){
		By preferenceTablocator = By.cssSelector("#preferences > li > a");
		List<WebElement> tabs = driver.findElements(preferenceTablocator);
		for(WebElement tab : tabs){
			if(tab.getText().trim().equalsIgnoreCase(tabName)){
				tab.click();
				
				if(tabName.equalsIgnoreCase("File Type")){
					
				}
				
				break;
			}
		}
	}	
		
	
	
	
}//end class
