package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.platform.util.Logger;

public class WorkflowFieldManager implements RecenseoSupport{
	public static By workflowDropdownLocator = By.id("wflow-select-wflow");
	public static By fieldsOfFieldSetupLocator = By.cssSelector("#fields-fields-list li");
	public static By workflowAvailableFieldLocator = By.cssSelector("#wflow-template-content tr");
	public static By picklistItemLocator = By.cssSelector("#field-picklist li a");
	public static By hoveredItemLocator = By.cssSelector(".hoverDiv[style*='block'] button");
	public static By childpicklistTextfieldEditLocator = By.cssSelector(".picklist-item");
	//public static By addPicklistItemLocator = By.cssSelector("#field-picklist div button span");
	public static By addPicklistItemLocator = By.xpath("//span[text()='Add Item']");
	//public static By picklistEditFieldLocator = By.cssSelector("#field-picklist textarea");
	public static By picklistEditFieldLocator = By.cssSelector(".picklist-add textarea");
	public static By picklistCreateBtnLocator = By.cssSelector(".picklist-add span:nth-child(1)");
	public static By picklistCancelBtnLocator = By.cssSelector(".picklist-add span:nth-child(2)");
	
	final private static WorkflowFieldManager workflowFieldManager = new WorkflowFieldManager();  

	public static void switchTab(String tabName){
		workflowFieldManager.getElements(By.className("ui-tabs-anchor")).stream()
							.filter(tab -> tab.getText().equals(tabName))
							.findFirst().get().click();
		
		//workflowFieldManager.waitFor(10);
		
		//System.out.println( Logger.getLineNumber() + tabName + " selected...");
	}
	
	
	public static void switchToWorkflowTemplates(){
		switchTab("Workflow Templates");
		workflowFieldManager.waitForNumberOfElementsToBeGreater(workflowAvailableFieldLocator, 0);
		System.out.println( Logger.getLineNumber() + "'Workflow Templates' selected...");
	}
	
	public static void switchToFieldSetup(){
		switchTab("Field Setup");
		workflowFieldManager.waitForNumberOfElementsToBeGreater(fieldsOfFieldSetupLocator, 0);
	}
	
	public static void selectFieldFromFieldSetup(String fieldName){
		workflowFieldManager.getElements(fieldsOfFieldSetupLocator).stream()
				.filter(f -> f.getText().trim().equals(fieldName)).findFirst().get().click();
		
		workflowFieldManager.waitFor(1);
		
		System.out.println( Logger.getLineNumber() + "Field selected: " + fieldName);
	}

	public static void selectWorkflow(String workflowName){
		workflowFieldManager.selectFromDrowdownByText(workflowDropdownLocator, workflowName);
		workflowFieldManager.waitFor(10);
	}
	
	public static void createParentPicklistItem(String picklistName){
		workflowFieldManager.tryClick(addPicklistItemLocator);
		workflowFieldManager.editText(picklistEditFieldLocator, picklistName);
		workflowFieldManager.tryClick(picklistCreateBtnLocator, 2);		
		
		System.out.println( Logger.getLineNumber() + "Picklist created with name: " + picklistName + "...");
	}
	
	public static void createChildPicklistItem(String picklistName, String childPicklistName){
		List<WebElement> picklistItems = workflowFieldManager.getElements(picklistItemLocator);
		
		boolean isPicklistFound = false;
		
		for(WebElement picklist : picklistItems){
			if(workflowFieldManager.getText(picklist).equals(picklistName)){
				workflowFieldManager.hoverOverElement(picklist);
				workflowFieldManager.tryClick(hoveredItemLocator);
				
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				
				jse.executeScript("arguments[0].click();", workflowFieldManager.getElements(hoveredItemLocator).get(0));
				
				workflowFieldManager.editText(childpicklistTextfieldEditLocator, childPicklistName);
				workflowFieldManager.tryClick(By.xpath("//span[text()='Create']"), 2);
				
				System.out.println( Logger.getLineNumber() + "Nested picklist created under '" + picklistName + "' with name '" + childPicklistName + "'...");
				
				isPicklistFound = true;
				break;
			}
		}
		
		if(!isPicklistFound){
			System.out.println( Logger.getLineNumber() + "Picklist name '" + picklistName + "' not found in the list...");
		}
		
	}
	
	
	public static void deletePicklistItem(String picklistName){
		List<WebElement> picklistItems = workflowFieldManager.getElements(picklistItemLocator);
		
		boolean isPicklistFound = false;
		
		for(WebElement picklist : picklistItems){
			if(workflowFieldManager.getText(picklist).equals(picklistName)){
				workflowFieldManager.hoverOverElement(picklist);
				
				JavascriptExecutor jse = (JavascriptExecutor)Driver.getDriver();
				
				jse.executeScript("arguments[0].click();", workflowFieldManager.getElements(hoveredItemLocator).get(1));
				
				workflowFieldManager.acceptAlert();
				
				workflowFieldManager.waitFor(2);
				
				System.out.println( Logger.getLineNumber() + "Picklist item deleted with name '" + picklistName + "''...");
				
				isPicklistFound = true;
				break;
			}
		}
		
		if(!isPicklistFound){
			System.out.println( Logger.getLineNumber() + "Picklist name '" + picklistName + "' not found in the list...");
		}
		
	}
	
	
	public static List<String> getListOfAvaiableField(){
		List<WebElement> availableItemsList = workflowFieldManager.getElements(By.cssSelector(".available-field > td:nth-child(2)"));
		Function<WebElement, String> function = WebElement::getText;
		
		return workflowFieldManager.getListOfItemsFrom(availableItemsList, function);
	}
	
	public static List<String> getListOfAvaiableExtractedField(){
		List<String> allAvailableFieldList = getListOfAvaiableField();
		
		return allAvailableFieldList.stream().filter(s -> s.startsWith("Extracted")).collect(Collectors.toList());
	}
	
	public static void addField(String fieldName){
		workflowFieldManager.getElements(By.className("available-field")).stream()
							.filter(field -> field.getText().trim().equals(fieldName))
							.findFirst().get().findElement(By.tagName("td")).click();
		
		workflowFieldManager.waitFor(5);
		
		System.out.println( Logger.getLineNumber() + "Field '" + fieldName + "' is added...");				
	}
	
	public static void removeFiled(String fieldName){
		workflowFieldManager.getElements(By.className("template-field-description")).stream()
							.filter(f -> f.getText().trim().equals(fieldName))
							.findFirst().get().findElement(By.xpath(".."))
							.findElement(By.tagName("td")).click();
		
		workflowFieldManager.waitFor(5);
		
		System.out.println( Logger.getLineNumber() + "Field '" + fieldName + "' is removed...");
	}
	

	public static void setDisPlayType(String fieldName, String DisplayType){
		List<WebElement> fields= workflowFieldManager.getElements(By.className("template-field-description"));
		
		for(int j=0; j<fields.size(); j++){
			if(fields.get(j).getText().equalsIgnoreCase(fieldName)){
				WebElement sibling= SearchPage.getFollowingSibling(fields.get(j));
				WebElement selectNode= sibling.findElement(By.cssSelector("select"));
				//new Select(selectNode).selectByVisibleText(DisplayType);
				workflowFieldManager.selectFromDrowdownByText(selectNode, DisplayType);
				
				System.out.println( Logger.getLineNumber() + "Type of '" + fieldName + "' is changed to '" + DisplayType + "'...");
				
				break;
			}
		}
	}
 

	public static void setWorkflow(String workflow){
		workflowFieldManager.selectFromDrowdownByText(workflowDropdownLocator, workflow);
		workflowFieldManager.waitForNumberOfElementsToBeGreater(workflowAvailableFieldLocator, 0);
		//workflowFieldManager.waitFor(5);
		System.out.println( Logger.getLineNumber() + "Workflow changed to '" + workflow + "'...");
	}

	//This method adds a field 
	public static void addAvailableField(String fieldName){
		
		String name = null;
		List<WebElement> availableFields= workflowFieldManager.getElements(By.cssSelector("tr[id*='available-field-']"));
		for(WebElement element:availableFields){
			
			List<WebElement> children= element.findElements(By.cssSelector("td"));
			//The second child node contains the name of the field
			name= children.get(1).getText();
			if(name.contains(fieldName)){
				
				//add the field, to do this use the first child of the list named children(the child of the first child is the needed node)
				children.get(0).findElement(By.cssSelector("span")).click();
				break;
				
			}
			
		}
	}
	
	
	public static void removeAllExtractedFieldFromSelectedWF(){
		List<WebElement> availableFileds = workflowFieldManager.getElements(By.cssSelector(".wflow-field-remove + td"));
		
		for(int i = 0; i < availableFileds.size(); i++){
			String fieldValue = availableFileds.get(i).getText().trim();
			if(fieldValue.startsWith("Extracted")){
				workflowFieldManager.getParentOf(availableFileds.get(i)).findElement(By.cssSelector(".wflow-field-remove")).click();
				workflowFieldManager.waitForNumberOfElementsToBePresent(By.cssSelector(".wflow-field-remove + td"), availableFileds.size() - 1);
				System.out.println( Logger.getLineNumber() + "Field removed from list: " + fieldValue);
				availableFileds = workflowFieldManager.getElements(By.cssSelector(".wflow-field-remove + td"));
				i = 0;
			}
		}
	}
}
