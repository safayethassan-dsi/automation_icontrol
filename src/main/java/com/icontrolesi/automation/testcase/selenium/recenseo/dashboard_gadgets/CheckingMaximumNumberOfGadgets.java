package com.icontrolesi.automation.testcase.selenium.recenseo.dashboard_gadgets;

import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class CheckingMaximumNumberOfGadgets extends TestHelper{
	@Test
	public void test_c152_CheckingMaximumNumberOfGadgets(){
		new SelectRepo(AppConstant.DEM0);
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int totalGadgets = SearchPage.Gadgets.getGadgetCount();
		
		int totalGadgetsToBeCreated = 24 - totalGadgets;
		
		for(int i = 1; i <= totalGadgetsToBeCreated; i++){
			SearchPage.Gadgets.addGadgetFromDashboard();
		}
		
		int totalGadgetsAfter = SearchPage.Gadgets.getGadgetCount();
		
		List<String> gadgetsList = getListOfItemsFrom(SearchPage.Gadgets.gadgetTitleLocator, gadgetTitle -> gadgetTitle.getText().trim()) ;
		
		System.out.println( Logger.getLineNumber() + "Maximum number of Gadgets created....");
		
		System.out.println( Logger.getLineNumber() + "Now " + totalGadgetsAfter + " in the Dashboard...");
		
		System.out.println( Logger.getLineNumber() + "Creating Gadget after " + totalGadgetsAfter + "-th count....");
		
		SearchPage.Gadgets.addGadgetFromDashboard();
		
		String alertText = getAlertMsg();
		
		softAssert.assertEquals(alertText, "You already have reached to maximum limit.", "*) User navigates to tab 'Reports' with name 'RunReportsFromDashboardGadgetsTest':: ");
		
		for(String gadgetName : gadgetsList){
			SearchPage.Gadgets.removeGadgetWithName(gadgetName);
		}
		
		softAssert.assertAll();
	}
}
