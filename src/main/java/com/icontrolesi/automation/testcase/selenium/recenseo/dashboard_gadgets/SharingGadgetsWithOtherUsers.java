package com.icontrolesi.automation.testcase.selenium.recenseo.dashboard_gadgets;

import java.util.Date;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class SharingGadgetsWithOtherUsers extends TestHelper{
	private String gadgetName = "RunReportsFromDashboardGadgetsTest" + new Date().getTime(); 
	@Test
	 public void test_c183_SharingGadgetsWithOtherUsers() {
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.createGadgetByName(gadgetName);
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		if(!SearchPage.Gadgets.isGadgetExist(gadgetName)){
			SearchPage.Gadgets.addGadgetFromDashboard();
		}
		
		SearchPage.Gadgets.shareGadgetWithName(gadgetName, AppConstant.ICEAUTOTEST_3);
		
		SearchPage.logout();
		
		performLoginWithCredential(AppConstant.ICEAUTOTEST_3, AppConstant.PASS3);
		
		new SelectRepo(AppConstant.DEM0);
		
		boolean isGadgetShared = SearchPage.isGadgetExistWithName(gadgetName);
		
		softAssert.assertTrue(isGadgetShared, "The event of closing the Gadget is recored:: ");
		
		SearchPage.Gadgets.removeGadgetWithName(gadgetName);
		
		softAssert.assertAll();
	 }
}
