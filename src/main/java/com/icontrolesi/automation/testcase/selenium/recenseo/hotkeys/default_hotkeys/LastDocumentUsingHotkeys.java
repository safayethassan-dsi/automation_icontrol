package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.default_hotkeys;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class LastDocumentUsingHotkeys extends TestHelper{
	@Test
	public void test_c1708_LastDocumentUsingHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		Hotkeys.pressHotkey(true, Keys.END);
		waitFor(10);
		
		int documentNumber = DocView.getHighlightedDocumentNumber();
		
		int currentDocumentNumberInSelectBox = DocView.getCurrentDocumentNumber();
		
		Assert.assertEquals(documentNumber,  currentDocumentNumberInSelectBox, "*** The last document in the current workflow displayed:: ");
	}
}
