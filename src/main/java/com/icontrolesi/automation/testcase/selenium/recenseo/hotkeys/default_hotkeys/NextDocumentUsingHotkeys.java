package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.default_hotkeys;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class NextDocumentUsingHotkeys extends TestHelper{
	@Test
	public void test_c1706_NextDocumentUsingHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		int totalDocInView = getTotalElementCount(DocView.gridRowLocator);
		
		int docToSelect = getRandomNumberInRange(1, totalDocInView - 1);
		
		DocView.clickOnDocument(docToSelect);
		
		Hotkeys.pressHotkey(true, Keys.PAGE_DOWN);
		waitFor(10);
		
		int documentNumber = DocView.getHighlightedDocumentNumber();
		
		Assert.assertEquals(documentNumber, docToSelect + 1, "*** The document has been saved:: ");
	}
}
