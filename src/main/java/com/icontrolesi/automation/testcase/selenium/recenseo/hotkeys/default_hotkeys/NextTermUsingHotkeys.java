package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.default_hotkeys;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class NextTermUsingHotkeys extends TestHelper{
	@Test
	public void test_c1712_NextTermUsingHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		int totalDocInView = getTotalElementCount(DocView.gridRowLocator);
		
		int docToSelect = getRandomNumberInRange(2, totalDocInView);
		
		DocView.clickOnDocument(docToSelect);
		
		DocView.selectView("Text");
		
		int totalTerm = getTotalElementCount(DocView.termLocator);
		
		boolean isPrevTermHighlighted = false;
		if(totalTerm == 0){
			throw new SkipException("No term to selected for the document no. " + docToSelect + ".");
		}else{
			Hotkeys.pressHotkey(false, Keys.ARROW_DOWN);
			isPrevTermHighlighted = getAttribute(DocView.termLocator, "class").contains("Selected");
		}
		
		Assert.assertTrue(isPrevTermHighlighted, "*** The previous term in the currently selected document should be highlighted:: ");
	}
}
