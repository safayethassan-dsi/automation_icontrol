package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.default_hotkeys;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class ViewHistory extends TestHelper{
	@Test
	public void test_c1701_ViewHistory(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		Hotkeys.pressHotkey(true, "h");
		
		waitFor(15);
		
		boolean isHistorydViewSelected = getAttribute(DocView.historyViewLocator, "class").endsWith("active");
		
		Assert.assertTrue(isHistorydViewSelected, "*** The document displayed in History view:: ");
	}
}
