package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.personalized_hotkeys;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class ViewTextUsingCustomHotkeys extends TestHelper{
	@Test
	public void test_c1713_ViewTextUsingCustomHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.setPersonalizedHotkey(Hotkeys.viewTextKey, "V");
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		Hotkeys.pressHotkey(true, "v");
		
		waitFor(15);
		
		boolean isTextViewSelected = getAttribute(DocView.textViewLocator, "class").endsWith("active");
		
		Assert.assertTrue(isTextViewSelected, "*** The document displayed in Text view:: ");
	}
}
