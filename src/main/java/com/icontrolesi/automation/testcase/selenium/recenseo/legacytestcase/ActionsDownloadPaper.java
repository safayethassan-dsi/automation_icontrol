package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Download;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class ActionsDownloadPaper extends TestHelper{
	@Test
	public void test_c144_ActionsDownloadPaper(){
		handleActionsDownloadPaper(driver);
	}

	private void handleActionsDownloadPaper(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Annotation");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
	    SearchPage.addSearchCriteria("filename");
	    SearchPage.setOperatorValue(0, "equals");
	    SearchPage.setCriterionValue(0, "Paper");
	   
	    performSearch();
				    
	    DocView.selectAllDocuments();
	     
	    new DocView().clickActionMenuItem("Prepare Download");
	    
	    switchToFrame(1);
	    
	    tryClick(By.name("include_ppdat"));
	    selectFromDrowdownByText(By.name("ppset_selected"), "CDOCDEMO");
	    tryClick(By.name("mergePdfs"));
	    tryClick(By.name("useSeparator"));
	    
	    new DocView().clickSubmitButton();
		
		String downloadConfirmationMsg = getElement(By.tagName("body")).getText().trim();
 		
	    softAssert.assertTrue(downloadConfirmationMsg.contains("Recenseo is preparing your download in the background."), "4) Submit the job:: ");
	    
	    tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	    waitFor(driver, 120);
	    
	    List<WebElement> downloadLinks =  getElements(By.className("BlueAnchor"));
	    
	    WebElement downloadableLink = downloadLinks.get(downloadLinks.size() - 1);
	    
	    String downloadableLinkName;
	    
	    if(Configuration.getConfig("selenium.browser").equals("chrome")){
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":|-", "_");
	    }else{
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":|-", "_");
	    }
	    
	    System.out.println( Logger.getLineNumber() + "FileName: " + downloadableLinkName);
	    
	    String downloadableFile = AppConstant.DOWNLOAD_DIRECTORY + downloadableLinkName;
	    String extractedFolderLocation = downloadableFile.replaceAll("\\.zip", "");
	    
	    downloadableLink.click();
	     
	    waitForFileDownloadToComplete(downloadableFile, 1800);
	    
	    unzip(downloadableFile, extractedFolderLocation);
	    
	    File [] listOfFiles = new File(extractedFolderLocation).listFiles();
	    
	    
	    PDDocument doc;
	    int totalPage = 0;
	    for(File file : listOfFiles){
	    	if(file.getName().matches("merged\\d+\\.pdf")){
	    		try {
	    			doc = PDDocument.load(file);
	    			totalPage = doc.getNumberOfPages();
	    			System.out.println( Logger.getLineNumber() + "Pages for " + file.getName() + " is: " + totalPage);
	    			softAssert.assertTrue(totalPage >= 400 && totalPage <= 600);
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		}
	    	}
	    }
	    
	    FileMethods.deleteFileOrFolder(downloadableFile);
	    FileMethods.deleteFileOrFolder(extractedFolderLocation);
	 
	    System.out.println( Logger.getLineNumber() + "Total Page found: " + totalPage);
	  
	    softAssert.assertTrue((totalPage >= 500), "Page count down not match:: ");
	    
	    getElements(By.name("chkDownload")).stream().forEach(link -> link.click());
	    Download.deleteSelectDownloads();
		
	    softAssert.assertAll();
	   }
	
	/*
	 * 	This method wait for a file to be downloaded with a maximum waiting period.
	 */
	public void waitForFileDownloadToComplete(String fileName, int waitTime){
		int currentTime = 0;
		while(true){
			if(currentTime >= waitTime){
				System.err.println("File download not completed. Estimated time has over.");
				break;
			}
			File file = new File(fileName);
			File partFile = new File(fileName+".part");
			if(file.exists() && partFile.exists() == false){
				System.err.println(file.getName() + " downloaded completely...");
				break;
			}else{
				waitFor(driver, 5);
			}
			
			
			currentTime += 5;
		}
	}
	
	public static void main(String[] args) {
		System.out.println( Logger.getLineNumber() + AppConstant.DOWNLOAD_DIRECTORY);
	}
}
