package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

public class ActionsGlobalEdit_NoAccess extends TestHelper{
	@Test
	public void test_c170_ActionsGlobalEdit_NoAccess(){
		try{
			new SelectRepo(AppConstant.DEM0);
			
			SearchPage.setWorkflow("Search");
	        
	        SprocketMenu.openUserManager();		
	        UserManager.loadUserSettings(AppConstant.USER);
	        boolean isGlobalEditEnabled = UserManager.getGeneralAccess("Global Edit");
	        
	        if(isGlobalEditEnabled){
	        	UserManager.setGeneralAccess("Global Edit", false);
	        }
	        
	        boolean isGlobalEditDisabled = !UserManager.getGeneralAccess("Global Edit");
	        
	        softAssert.assertEquals(isGlobalEditDisabled, true, "2. Test id does NOT have have Global Edit Access:: ");
	        
	        SearchPage.goToDashboard();
	        waitForFrameToLoad(Frame.MAIN_FRAME);
	        
	        performSearch();
	        
	        tryClick(DocView.actionMenuLocator);
	        
	        int globalEditItem = getTotalElementCount(By.linkText("Global Edit"));
	        
	        softAssert.assertEquals(globalEditItem, 0, "Verify that under Actions there is no Global Edit menu option:: ");
	        
	        
	        SprocketMenu.openUserManager();		
	        UserManager.loadUserSettings(AppConstant.USER);
	        isGlobalEditEnabled = UserManager.getGeneralAccess("Global Edit");
	        
	        if(!isGlobalEditEnabled){
	        	UserManager.setGeneralAccess("Global Edit", true);
	        }
	        
	        
	        SearchPage.goToDashboard();
	        
	        waitForFrameToLoad(Frame.MAIN_FRAME);
	        
	        performSearch();
	        
	        new DocView().clickActionMenuItem("Global Edit");
	        
	        softAssert.assertAll();
		}catch (Exception e) {
			System.err.println(e.getMessage());
		}finally{
			if(!UserManager.isUserManagerPage()){
				SearchPage.goToDashboard();
				waitForFrameToLoad(Frame.MAIN_FRAME);
				SprocketMenu.openUserManager();		
			}
			
	        UserManager.loadUserSettings(AppConstant.USER);
	        boolean isGlobalEditEnabled = UserManager.getGeneralAccess("Global Edit");
	        
	        if(!isGlobalEditEnabled){
	        	System.out.println("Resetting Global Edit permission.....");
	        	UserManager.setGeneralAccess("Global Edit", true);
	        }
	        	
		}
	}
}
