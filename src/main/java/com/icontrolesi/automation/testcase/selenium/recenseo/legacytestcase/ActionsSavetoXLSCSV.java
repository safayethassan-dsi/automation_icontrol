package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
 
	1) Run a Search where Bates = All Produced 
	2) Verify Grid view displays the Bates and Folder Name columns. Use Show/Hide columns to adjust as necessary 
	3) Move the columns so that Bates and Folder data are NOT the last columns in the grid (drag/drop to adjust) 
	4) From Grid View click on the ""Actions"" drop down menu and click on save results to xls 
	5) Choose 'Most Recent Bates History' (If pop up doesn't come up to choose this option you will have to go to Settinges>General Preferences then choose you save to excel preferences) and Continue 
	6) Open the .csv in the downloaded zip file 
	7) Confirm the columns, document count, and the order of the documents match the Recenseo display exactly. Examine the data for any instances of data/heading mis-alignment (pay special attention to Bates, Folder, and surrounding columns). 
	 
	8) From Grid view, Click on the ""Actions"" drop down menu again and click on save results to xls 
	9) Choose 'Full Bates History' (If pop up doesn't come up to choose this option you will have to go to Settinges>General Preferences then choose you save to excel preferences) and Continue 
	10) Open the .csv in the downloaded zip file (may need to go to the Downloads section to find the saved .zip file) 
	11) Confirm the columns, document count, and the order of the documents match the Recenseo display exactly. Examine the data for any instances of data/heading mis-alignment (pay special attention to Bates, Folder, and surrounding columns).  In this instance, there should be multiple bates columns - one for each production set.
*/

public class ActionsSavetoXLSCSV extends TestHelper{
	@Test
	public void test_c165_ActionsSavetoXLSCSV(){
		String cmd = "REG ADD \"HKEY_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\New Windows\" /F /V \"PopupMgr\" /T REG_SZ /D \"yes\"";
		try {
		    Runtime.getRuntime().exec(cmd);
		    System.out.println( Logger.getLineNumber() + "Popup blocker set...");
		} catch (Exception e) {
		    System.out.println( Logger.getLineNumber() + "Error ocured!");
		}
		handleActionsSavetoXLSCSV(driver);
	}

    private void handleActionsSavetoXLSCSV(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
		DocView.checkAllColumns();
		DocView.closePreferenceWindow();
    	
        SearchPage.setWorkflow("Search");
        DocView docView = new DocView();
     
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(0, "All Produced");
        
        performSearch();
        
        boolean isGridBatesAndFolderNameDisplayed = false;
        try{
        	getElement(By.id("jqgh_grid_bates"));
        	getElement(By.id("jqgh_grid_foldername"));
        	isGridBatesAndFolderNameDisplayed= true;
        }catch(NoSuchElementException e){
        	e.printStackTrace();
        }
        
        Assert.assertTrue(isGridBatesAndFolderNameDisplayed, "2) Verify Grid view displays the Bates and Folder Name columns:: ");
        
        docView.clickActionMenuItem("Save result set to an excel file");
       
        WebElement divElement = Utils.waitForElement("save2ExcelDialog", "id");
        WebElement buttonElement = SearchPage.getFollowingSibling(divElement).findElement(By.tagName("div")).findElements(By.tagName("button")).get(1);
        click(buttonElement);

        //skipped checking inside csv file

        docView.clickActionMenuItem("Save result set to an excel file");
        click(Utils.waitForElement("archive", "name"));
        Utils.waitShort();

        divElement = Utils.waitForElement("save2ExcelDialog", "id");
        buttonElement = SearchPage.getFollowingSibling(divElement).findElement(By.tagName("div")).findElements(By.tagName("button")).get(1);
        click(buttonElement);
        acceptAlert(driver);
    }
}
