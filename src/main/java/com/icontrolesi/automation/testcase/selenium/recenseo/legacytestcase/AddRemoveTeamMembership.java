package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;


public class AddRemoveTeamMembership extends TestHelper{
	By caseUsersLocator = By.cssSelector("#availableUsers > tbody > tr[style$='table-row;']");
	By teamUsersLocator = By.cssSelector("#teamUsers > tbody > tr[class^='ui-widget-content']");
	
	@Test(enabled = false)
	public void test_c1680_AddRemoveTeamMembership(){
		handleAddRemoveTeamMembership(driver);
	}

	private void handleAddRemoveTeamMembership(WebDriver driver){
	    new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");
		
		SprocketMenu.openUserManager();
		
		UserManager.switchTab("teams");
		UserManager.selectTeam("team1");
		
		int prevCaseUsersCount = getTotalElementCount(caseUsersLocator);
		int prevTeamUsersCount = getTotalElementCount(teamUsersLocator);
		
		UserManager.selectUserFromAllUsersInCase(false);
		
		int finalCaseUsersCount = getTotalElementCount(caseUsersLocator);
		int finalTeamUsersCount = getTotalElementCount(teamUsersLocator);
		
		UserManager.deselectAllUsersFromTeamUsers();
		
		softAssert.assertEquals(prevCaseUsersCount - 1, finalCaseUsersCount, "(5) Verify that the users are moved correctly from the \"All Users in CASE\" table to the \"Users in Currently Loaded Team\" table:: ");
		softAssert.assertEquals(prevTeamUsersCount + 1, finalTeamUsersCount, "(5) Verify that the users are moved correctly from the \"All Users in CASE\" table to the \"Users in Currently Loaded Team\" table:: ");
		
		UserManager.selectUserFromAllUsersInCase(true);
		
		waitFor(driver, 15);
		
		softAssert.assertEquals(getText(UserManager.performAssignmentExcludingLocator), "Perform team assignment(s), EXCLUDING conflicted users", "(7) Verify a pop-up presents option - Perform team assignment(s), EXCLUDING conflicted users:: ");
		softAssert.assertEquals(getText(UserManager.performAssignmentIncludingLocator), "Perform team assignment(s). INCLUDING conflicted users", "(7) Verify a pop-up presents option - Perform team assignment(s), EXCLUDING conflicted users:: ");
		
		UserManager.closeConfirmAddUserDialog();
		
		/*prevCaseUsersCount = getTotalElementCount(caseUsersLocator);
		prevTeamUsersCount = getTotalElementCount(teamUsersLocator);
		
		tryClick(UserManager.performAssignmentIncludingLocator);
		
		finalCaseUsersCount = getTotalElementCount(caseUsersLocator);
		finalTeamUsersCount = getTotalElementCount(teamUsersLocator);
		
		UserManager.deselectAllUsersFromTeamUsers();
		
		softAssert.assertEquals(prevCaseUsersCount - 1, finalCaseUsersCount, "(9) Select the option to \"Perform Team Assignments, INCLUDING conflicted users\"-  Confirm user is added to the new team:: ");
		softAssert.assertEquals(prevTeamUsersCount + 1, finalTeamUsersCount, "(9) Select the option to \"Perform Team Assignments, INCLUDING conflicted users\"-  Confirm user is added to the new team:: ");
		*/
		
		//filter by User's full name to get assigned and unassigned team(san gives desired result: Sanjoy and Habib)
		editText(By.name("userFullName"), "san");
		UserManager.selectAllAvailableUserFromUsersCase();
		
		
		softAssert.assertEquals(getText(UserManager.performAssignmentExcludingLocator), "Perform team assignment(s), EXCLUDING conflicted users", "(7) Verify a pop-up presents option - Perform team assignment(s), EXCLUDING conflicted users:: ");
		softAssert.assertEquals(getText(UserManager.performAssignmentIncludingLocator), "Perform team assignment(s). INCLUDING conflicted users", "(7) Verify a pop-up presents option - Perform team assignment(s), EXCLUDING conflicted users:: ");
		
		prevCaseUsersCount = getTotalElementCount(caseUsersLocator);
		prevTeamUsersCount = getTotalElementCount(teamUsersLocator);
		
		tryClick(UserManager.performAssignmentExcludingLocator);
		
		finalCaseUsersCount = getTotalElementCount(caseUsersLocator);
		finalTeamUsersCount = getTotalElementCount(teamUsersLocator);
		
		softAssert.assertEquals(prevCaseUsersCount - 1, finalCaseUsersCount, "(13) Select the option to \"Perform Team Assignments, INCLUDING conflicted users\"-  Confirm user is added to the new team:: ");
		softAssert.assertEquals(prevTeamUsersCount + 1, finalTeamUsersCount, "(13) Select the option to \"Perform Team Assignments, INCLUDING conflicted users\"-  Confirm user is added to the new team:: ");
		
		softAssert.assertAll();
	}
}
