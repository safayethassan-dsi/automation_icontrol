package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

import com.icontrolesi.automation.platform.util.Logger;

public class AdminTemplateManager extends TestHelper{
	@Test
	public void test_c411_AdminTemplateManager(){
	 	new SelectRepo(AppConstant.DEM0);

		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		WorkflowFieldManager.switchToWorkflowTemplates();
		WorkflowFieldManager.selectWorkflow("QC Objective Coding");
		WorkflowFieldManager.removeAllExtractedFieldFromSelectedWF();
		
		List<String> availableExtractedFieldList = WorkflowFieldManager.getListOfAvaiableExtractedField();
		
		String fieldName = "";
		if(availableExtractedFieldList.size() == 0){
			throw new SkipException("No extracted field avaiable to add. Skipping the test cases...");
		}else{
			fieldName = availableExtractedFieldList.get(0);
			System.out.println( Logger.getLineNumber() + "Avaiable: "+ availableExtractedFieldList.get(0) + "***********");
			WorkflowFieldManager.addField(fieldName);
		}
		
		SearchPage.goToDashboard();
		
		switchToDefaultFrame();
		
		SearchPage.setWorkflow("QC Objective Coding");
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		boolean isFieldVisibleInCodingTemplate = getListOfItemsFrom(ReviewTemplatePane.availableFieldLocator, f -> getText(f).replace(":", "")).contains(fieldName);
		
		softAssert.assertTrue(isFieldVisibleInCodingTemplate, "(4) Open the folder [EmailFoster, Ryan e-mail and confirm the field information is now visible:: ");
		SprocketMenu.openWorkflowAndFieldManager();
		WorkflowFieldManager.switchToWorkflowTemplates();
		WorkflowFieldManager.selectWorkflow("QC Objective Coding");
		WorkflowFieldManager.removeFiled(fieldName);
		
		//SearchPage.setWorkflow("QC Objective Coding");
		SearchPage.goToDashboard();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		boolean isFiledInVisibleInCodingTemplate = !getListOfItemsFrom(ReviewTemplatePane.availableFieldLocator, f -> getText(f).replace(":", "")).contains(fieldName);
		
		softAssert.assertTrue(isFiledInVisibleInCodingTemplate, "(4) Open the folder [EmailFoster, Ryan e-mail and confirm the field information is now visible:: ");
	
		softAssert.assertAll();
	}
}
