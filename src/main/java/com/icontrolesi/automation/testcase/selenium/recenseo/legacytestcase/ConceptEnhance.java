package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * mantis# 15235
 * 
 * @author Ishtiyaq Kamal
 *
 */

public class ConceptEnhance extends TestHelper{

	private static final String DATABASE_NAME = "Recenseo Demonstration Database 2.0";
	// Selectors
	private static final String UL_NODE_SELECTOR = "ul[class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content']";
	private static final String TEXT_FIELD_SELECTOR = "input[class='CriterionValue ui-autocomplete-input']";
	private static final String NEXT_BUTTON_SELECTOR = "button[id=nextTerm]";
	// Log messages
	private static final String SUGGESTION_FAIL = "Suggestions for text [%s] not %s";
	
	private static List<WebElement> ulChildren;
	

	@Test
	public void test_c1686_ConceptEnhance(){
		handleConceptEnhance(driver);
	}

	protected void handleConceptEnhance(WebDriver driver){
		new SelectRepo(DATABASE_NAME);

		waitForFrameToLoad(Frame.MAIN_FRAME);

		// Select Search Criteria
		SearchPage.addSearchCriteria("Document Content (Full Text)");
		
		// Change option to [any of these words]
		WebElement ChooseOperatorType = getElement(By.className("CriterionOperator"));
		new Select(ChooseOperatorType).selectByVisibleText("any of these words");

		// Declare Necessary Web Elements
		WebElement textField = getElement(By.cssSelector(TEXT_FIELD_SELECTOR));
		WebElement ulNode = getElement(By.cssSelector(UL_NODE_SELECTOR));

		// Assign partial text to the designated text field
		textField.sendKeys("fraud");
		waitForNumberOfElementsToBePresent(By.cssSelector("body > ul > li"), 4);
		textField.clear();
		
		// Retrieve the suggested words for the text
		ulChildren = ulNode.findElements(By.cssSelector("li"));
		
		Assert.assertTrue(ulChildren.isEmpty() == false, String.format(SUGGESTION_FAIL, "fraud", "visible"));
		
		textField.sendKeys("fraud civil");
		waitForNumberOfElementsToBePresent(By.cssSelector("body > ul > li"), 3);		
		textField.clear();
		
		ulChildren = ulNode.findElements(By.cssSelector("li"));
		
		Assert.assertTrue(ulChildren.isEmpty() == false, String.format(SUGGESTION_FAIL, "Fraud civil", "visible"));

		textField.sendKeys("fraud civil district");
		waitForNumberOfElementsToBePresent(By.cssSelector("body > ul > li"), 4);
		textField.clear();
		
		// Retrieve the suggested words for the new combined text
		ulChildren = ulNode.findElements(By.cssSelector("li"));
		
		Assert.assertTrue(ulChildren.isEmpty() == false, String.format(SUGGESTION_FAIL, "Fraud civil district",	"visible"));
		
		textField.sendKeys(" criminal laws courts");

		performSearch();
	
		clickOnViewPanelLink("Text");
		
		//Click the button labeled Highlights
		List<String> searchQuery = Arrays.asList(new String[] { "fraud","civil", "district", "criminal", "laws", "courts" });
		click(SearchPage.waitForElement("toggleHighlightTerms", "id"));
		
		waitFor(3);

		int queryFound=getElements(By.cssSelector("span[class=Term]")).size();
		
		System.out.println( Logger.getLineNumber() + "Totla Found: "+queryFound);
		
		Assert.assertTrue(queryFound >= 1, "(4)Confirm that search terms are listed in text view at the bottom:: ");
		
		// Click the "Next button"
		WebElement nextButton = getElement(By.cssSelector(NEXT_BUTTON_SELECTOR));
		nextButton.click();

		waitFor(2);

		int loopCounter = 0;
		boolean textPresent = false;

		while (loopCounter < searchQuery.size()) {
			try {
				WebElement highlightedTerm = getElement(By.cssSelector("span[class='Term Selected']"));
				waitFor(1);
				System.out.println( Logger.getLineNumber() + highlightedTerm.getText());
				if(searchQuery.contains(highlightedTerm.getText().toLowerCase())) {
					textPresent = true;
					break;
				}
				nextButton.click();
				waitFor(1);
				loopCounter++;
			} catch (NoSuchElementException exception) {
				nextButton.click();
				waitFor(1);
				loopCounter++;
			}
		}

		Assert.assertTrue(textPresent, "(4)The terms are highlighting correctly in the text section:: ");
	}

	
	private void clickOnViewPanelLink(String viewPanelLinkToClick){
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);

		String cssForLink = "#review"+viewPanelLinkToClick+" > a > span[class=ui-button-text]";
		System.out.println( Logger.getLineNumber() + "\nSpan ***: "+getElement(By.cssSelector("#review"+viewPanelLinkToClick)).getAttribute("class"));
		if(getElement(By.cssSelector("#review"+viewPanelLinkToClick)).getAttribute("class").contains("active") == false){
			// Click the Text link from the View panel
			tryClick(By.cssSelector(cssForLink));
			waitForAttributeIn(By.cssSelector("#review"+viewPanelLinkToClick), "class", "active");
			waitForFrameToLoad("VIEWER");
		}
	}
}
