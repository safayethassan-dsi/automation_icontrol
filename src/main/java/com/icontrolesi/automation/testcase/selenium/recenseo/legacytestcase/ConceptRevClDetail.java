package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ConceptReviewAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Ishtiyaq Kamal
 * 
 *         DB: DEM4 (1) Go to search page. Go to ConceptReview section. (2) Left
 *         mouse click on the second cluster to get the details window. (3)
 *         Confirm that the cluster terms section contain same terms listed in
 *         the cluster folder label. (4) Note the number of documents in this
 *         cluster folder and in this state. (5) Close cluster details and check
 *         the box next to this cluster and click open. (6) Confirm the number
 *         of docs in the browse/view section match the details number. (7)
 *         Return to search page. Switch to privilege review state. (8) Open
 *         cluster details. Confirm Total docs in cluster folder stayed the same
 *         but total docs in state changed. (9) Return to search page. Add
 *         cluster to search page. (10) Run Search and Confirm Total Docs is the
 *         same as listed in cluster details. (11) Repeat process for 1
 *         sub-clusters of the original cluster.
 *
 */


public class ConceptRevClDetail extends TestHelper{
	@Test
	public void test_c97_ConceptRevClDetail(){
		handleConceptRevClDetail(driver);
	}
 
    protected void handleConceptRevClDetail(WebDriver driver){
    	new SelectRepo(AppConstant.DEM4);
  		
  		SearchPage.setWorkflow("Search");
  		
  		ConceptReviewAccordion.open();

        // Left click on the first cluster
        String[] clusterNamelabel;
        String[] clusterNameDetailsWindow = new String[1000];

        // Get cluster terms from label
        List<WebElement> spanNodesForClusters = getElements(By.cssSelector("span[class='clusterName']"));
        if (spanNodesForClusters.size() == 0) {
            System.out.println( Logger.getLineNumber() + "List containing cluster terms in label not found");
        }

        clusterNamelabel = spanNodesForClusters.get(0).getText().split(",");

        for (int j = 0; j < clusterNamelabel.length; j++) {
            System.out.println( Logger.getLineNumber() + "in Label: " + clusterNamelabel[j]);
        }

        // Left click on the first cluster
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", spanNodesForClusters.get(0));
        waitFor(5);

        WebElement clusterTermList = getElement(By.id("cluster-termsList"));
        List<WebElement> liNodesofClusterList = clusterTermList.findElements(By.cssSelector("li"));
       
        int index = 0;
        for (int i = 0; i < liNodesofClusterList.size(); i++) {
            clusterNameDetailsWindow[index] = liNodesofClusterList.get(i).getText();
            index++;
        }

        String[] NewClusterLabel;
         
        NewClusterLabel=Arrays.copyOf(clusterNamelabel, clusterNamelabel.length-1);
        
        System.out.println( Logger.getLineNumber() + "After deleting last entry from label array...............");

        for (int j = 0; j < NewClusterLabel.length; j++) {
            NewClusterLabel[j] = NewClusterLabel[j].trim();
            System.out.println( Logger.getLineNumber() + "Cluster name on label:" + NewClusterLabel[j]);
      }

        // Check if all cluster names in label are present in the details window
        boolean compare_arrays = Arrays.asList(clusterNameDetailsWindow).containsAll(Arrays.asList(NewClusterLabel));

        System.out.println( Logger.getLineNumber() + compare_arrays+" : CompareArrays");
        
        Assert.assertTrue(compare_arrays, "(3)Confirm that the cluster terms section contain all terms listed in the cluster folder label:: ");

        for (int j = 0; j < index; j++) {
            System.out.println( Logger.getLineNumber() + "in Window:" + clusterNameDetailsWindow[j]);
        }

        // Get doc counts in state
        long docsInState = ConceptReviewAccordion.getTotalDocsInState();
        System.out.println( Logger.getLineNumber() + "docsInState: "+docsInState);

        // Get doc counts in folder
        long docsInFolder = ConceptReviewAccordion.getTotalDocsInCluster();
        System.out.println( Logger.getLineNumber() + "docsInFolder: "+docsInFolder);

        // Press Close button
        ConceptReviewAccordion.closeClusterDetailsModal();
        ConceptReviewAccordion.selectFirstCluster();
        ConceptReviewAccordion.openSelectedForView();

        long docCountInSearch = 0;

        waitForVisibilityOf(By.id("totalDocs"));

        System.out.println( Logger.getLineNumber() + DocView.getDocmentCount());

        docCountInSearch = DocView.getDocmentCount();
        System.out.println( Logger.getLineNumber() + "Doc count in grid view : "+docCountInSearch);
          
        Assert.assertEquals(docCountInSearch, docsInFolder, "(6)Confirm the number of docs in the browse/view section match the details number:: ");

        SearchPage.goToDashboard();
        SearchPage.setWorkflow("Privilege Review");
        
        //driver.switchTo().defaultContent();
        waitForFrameToLoad("clusterFrame");
        waitForNumberOfElementsToBeGreater(By.cssSelector("body > div.treeViewContainer.filetree.treeview > li > span > span.clusterName"), 0);
      
        // Left click on the first cluster
        spanNodesForClusters = getElements(By.cssSelector("span.clusterName"));
       
        // spanNodesForClusters.get(0).click();
        click(spanNodesForClusters.get(0));
        waitFor(5);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("MAIN");
        //SearchPage.clickAccordion("folder");
        ConceptReviewAccordion.open();
        
        WebElement cluster= getElement(By.cssSelector("span[class='clusterName']"));
        js.executeScript("arguments[0].click();", cluster);
        waitFor(5);

        // Get doc counts in state
        long docsInStateInPrivReview = ConceptReviewAccordion.getTotalDocsInState();
        System.out.println( Logger.getLineNumber() + docsInStateInPrivReview);

        // Get doc counts in folder
        long docsInFolderInPrivilegeReview = ConceptReviewAccordion.getTotalDocsInCluster();
        System.out.println( Logger.getLineNumber() + docsInFolderInPrivilegeReview);
        
        System.out.println( Logger.getLineNumber() + "docsInStateInPrivReview"+docsInStateInPrivReview);
        System.out.println( Logger.getLineNumber() + "docsInState"+docsInState);
        System.out.println( Logger.getLineNumber() + "docsInFolderInPrivilegeReview"+docsInFolderInPrivilegeReview);
        System.out.println( Logger.getLineNumber() + "docsInFolder"+docsInFolder);

        boolean isClusterInfoOk = (docsInStateInPrivReview != docsInState) && (docsInFolderInPrivilegeReview == docsInFolder);
        
        Assert.assertTrue(isClusterInfoOk, "Open cluster details. Confirm Total docs in cluster folder stayed the same but total docs in state changed:: ");
        
        ConceptReviewAccordion.closeClusterDetailsModal();

        // Press Clear button
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        WebElement clear_button = getElement(By.id("clearButton"));
        click(clear_button);

        ConceptReviewAccordion.open();
        ConceptReviewAccordion.selectFirstCluster();
        ConceptReviewAccordion.addSelectedToSearch();

        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        performSearch();
       
        long docCountInSearchInPreReview = 0;

        docCountInSearchInPreReview = DocView.getDocmentCount();

        System.out.println( Logger.getLineNumber() + "In folder: " + docsInFolderInPrivilegeReview);
        System.out.println( Logger.getLineNumber() + "In Search: " + docCountInSearchInPreReview);

        Assert.assertEquals(docsInFolderInPrivilegeReview, docCountInSearchInPreReview, "(10)Run Search and Confirm Total Docs is the same as listed in cluster details:: ");
    }
}