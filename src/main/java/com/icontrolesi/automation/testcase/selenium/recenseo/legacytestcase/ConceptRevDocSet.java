package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ConceptReviewAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * 
 * @author Ishtiyaq Kamal
 *
 */

public class ConceptRevDocSet extends TestHelper{
	@Test
	public void test_c96_ConceptRevDocSet(){
		handleConceptRevDocSet(driver);
	}
	
	protected void handleConceptRevDocSet(WebDriver driver){
		new SelectRepo(AppConstant.DEM4);

		SearchPage.setWorkflow("Search");

		ConceptReviewAccordion.open();
		ConceptReviewAccordion.openSelectedForView(2);
				
		int docCountInGridView = DocView.getDocmentCount();
		
		DocView.clickOnCompleteDocFamily("Complete Doc Family");
		
		int docCountForDocFamily = DocView.getDocmentCount();
	
		Assert.assertTrue(docCountForDocFamily > docCountInGridView, "(5)Confirm doc count increased due to adding family docs:: ");	
	}
}
