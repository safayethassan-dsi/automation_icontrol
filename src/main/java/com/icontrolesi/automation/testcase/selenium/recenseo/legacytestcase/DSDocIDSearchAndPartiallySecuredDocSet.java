package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * 
 *   "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page, search for Document id 105 
	(5) Select ""Manage Document Security"" in the Actions Menu and block document 105 from ""Document Security"" Team but make sure document 104 and 106 are not blocked 
	(6) Confirm the ""Test User"" ID finds 0 documents when Searching for Document id 105  
	(7) Confirm that when ""Test User"" ID  opens Folder /Email/Johnson, Jeff e-mail2, that Document ID 105 is not present 
	(8) Select Document ID 104 from the Grid View 
	(9) Click on the Doc Family Tab and confirm Document ID 105 is not found as part of the document family but 106 is found 
	(10) Click Back to Search Results 
	(11) Click to ""Complete Doc Family"" in the Upper Left Corner and confirm Document ID 105 is not added as part of the document family but 106 is."
 */

public class DSDocIDSearchAndPartiallySecuredDocSet extends TestHelper{
   String user2 = Configuration.getConfig("recenseo.username2");
   String pass2 = Configuration.getConfig("recenseo.password2");
	
   @Test
   public void test_c1641_DSDocIDSearchAndPartiallySecuredDocSet(){
	   //loginToRecenseo();
	   handleDSDocIDSearchAndPartiallySecuredDocSet(driver);
   }

    private void handleDSDocIDSearchAndPartiallySecuredDocSet(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
    	DocView.checkSelectedColumns("Document ID");
    	DocView.closePreferenceWindow();
        
        SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(user2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.addSearchCriteria("Document ID");
        SearchPage.setOperatorValue(0, "is in list");
        SearchPage.setCriterionValueTextArea(0, "104\n105\n106");

        performSearch();
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();  
        
        DocView.selectDocumentByID(105);
        
        new DocView().clickActionMenuItem("Manage Document Security");

        switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(1));
        
        ManageDocumentSecurity.blockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();  
        
        SearchPage.logout();
		
        performLoginWithCredential(user2, pass2);	
        
        new SelectRepo(AppConstant.DEM0);
        
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document ID");
        SearchPage.setCriterionValue(0, "105");
        
        tryClick(SearchPage.runSearchButton);
        
        String searchMsg = getAlertMsg();
        
        softAssert.assertEquals(searchMsg, "Your search did not find any documents, please revise it and try again.", "6) \"Test User\" ID finds 0 documents when Searching for Document id 105:: ");
        
        FolderAccordion.open();
        FolderAccordion.expandFolder("Email");
        FolderAccordion.openFolderForView("Johnson, Jeff e-mail2");
        
        DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.closePreferenceWindow();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
        
        boolean isDocumentIDFound = DocView.isDocumentIDFound("105");
        
        softAssert.assertFalse(isDocumentIDFound, "(7) Confirm that when \"Test User\" ID opens Folder /Email/Johnson, Jeff e-mail2, that Document ID 105 is not present:: ");
        
        DocView.clickOnDocumentByID(104);
        
        DocView.selectViewByLink("Doc Family");
        
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        boolean isDocWithId_105_Hidden = !DocView.isDocumentIDFound("105");
        boolean isDocWithId_106_Found = DocView.isDocumentIDFound("106");
        
        softAssert.assertTrue(isDocWithId_105_Hidden, "(9) Click on the Doc Family Tab and confirm Document ID 105 is not found as part of the document family:: ");
        softAssert.assertTrue(isDocWithId_106_Found, "(9) Click on the Doc Family Tab and confirm Document ID 106 is found as part of the document family:: ");
        
        //DocView.clickBackToSearchResult();
        //DocView.clickOnCompleteDocFamily(driver);
        
        softAssert.assertAll();
    }

}