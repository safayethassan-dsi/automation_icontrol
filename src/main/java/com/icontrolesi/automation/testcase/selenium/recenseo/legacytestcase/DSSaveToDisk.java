package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Download;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page, and run a search for Document ID 1973 - 2472  
	(5) Select ""Manage Document Security"" in the Actions Menu and confirm document 1973 is not blocked from Document Security Team  
	(6) Create 3 Prepare Downloads:   
	(a) Only Document id 1973  
	(b) 100 Documents including 1973  
	(c) 99 Documents NOT including 1973  
	(7) On the Downloads Page Share all 3 downloads with Everyone 
	(8) Confirm that ""Test User"" ID can access all 3 shared downloads from the Downloads Page 
	(9) Select ""Manage Document Security"" in the Actions Menu and block Document 1973 from Document Security Team  
	(10) Verify that ""Test User"" ID can no longer access the downloads from (a) and (b), but CAN access that from (c)."

*/

public class DSSaveToDisk extends TestHelper{
   
	@Test(enabled = true)
	public void test_c1542_DSSaveToDisk(){
		handleDSSaveToDisk(driver);
	}

    private void handleDSSaveToDisk(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.addSearchCriteria("Document ID");
        SearchPage.setOperatorValue(0, "is between");
        SearchPage.setCriterionValue(0, "1973");
        SearchPage.setCriterionValue(1, "2472");
        
        SearchPage.setSortCriterion();
        SearchPage.setSortCriterionValue(1, "Document ID");
        
        performSearch();
        
        DocView.setNumberOfDocumentPerPage(100);
        DocView.selectAllDocuments();
        
        //DocView.selectDocumentsWitIDs("1973");
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();
        
        tryClick(DocView.selectAllDocumentLocator);
        
        tryClick(By.id("jqg_grid_1973"));//select only 1973
        
        new DocView().clickActionMenuItem("Prepare Download");
        //switchToFrame(1);
        switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(1));
        
        new DocView().clickSubmitButton();
        
        tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	    waitFor(20);
	    
	    List<WebElement> downloadLinks =  getElements(By.name("chkDownload"));
	    
	    for(int i = 0; i < downloadLinks.size(); i++){
	    	downloadLinks.get(i).click();
	    }
        
	    
	   Download.deleteSelectDownloads();
	   Download.closeDownloadWindow();
	   
	   DocView.selectAllDocuments();
	   
	   new DocView().clickActionMenuItem("Manage Document Security");
	   switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(2));
       
       ManageDocumentSecurity.unBlockTeam("Document Security");
       ManageDocumentSecurity.closeWindow();
       
       new DocView().clickActionMenuItem("Prepare Download");
       switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(3));
       
       new DocView().clickSubmitButton();
       
       tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	   waitFor(20);
	   
	   Download.closeDownloadWindow();
	   
	  /* new DocView().clickActionMenuItem("Manage Document Security");
	   switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(4));
       
       ManageDocumentSecurity.unBlockTeam("Document Security");
       ManageDocumentSecurity.closeWindow();*/
	   
	   tryClick(By.id("jqg_grid_1973")); //deselect
	   
	  /* new DocView().clickActionMenuItem("Manage Document Security");
	   switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(5));
       
       ManageDocumentSecurity.blockTeam("Document Security");
       ManageDocumentSecurity.closeWindow();*/
       
       new DocView().clickActionMenuItem("Prepare Download");
       driver.switchTo().frame(getElements(By.cssSelector("iframe[src*='javascript']")).get(4));
       
       new DocView().clickSubmitButton();
       
       tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	   waitFor(20);
	   
	   Download.closeDownloadWindow();
	   
	   new DocView().clickActionMenuItem("Prepare Download");
	   driver.switchTo().frame(getElements(By.cssSelector("iframe[src*='javascript']")).get(5));
       
       new DocView().clickSubmitButton();
       
       tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	   waitFor(20);
       
       getElements(By.name("chkDownload")).stream().forEach(link -> link.click());
       
       tryClick(By.cssSelector("#viewPref input[value='2']"));
       
       Download.shareSelectedDownloads();
       
       SearchPage.logout();
		
       performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);	
       
       new SelectRepo(AppConstant.DEM0);
      
       SearchPage.setWorkflow("Search");
       
       SearchPage.openDownloadWindow();
       
       int totalDownloadsForTestUser = getTotalElementCount(By.className("BlueAnchor"));
       
       softAssert.assertEquals(totalDownloadsForTestUser, 3, "8) Confirm that \"Test User\" ID can access all 3 shared downloads from the Downloads Page:: ");
       
       DocView.clickCrossBtn();
       
       SearchPage.logout();
       
       performLoginToRecenseo();
       
       new SelectRepo(AppConstant.DEM0);
   	
   	   SearchPage.setWorkflow("Search");
       
       SearchPage.addSearchCriteria("Document ID");
       SearchPage.setOperatorValue(0, "is between");
       SearchPage.setCriterionValue(0, "1973");
       SearchPage.setCriterionValue(1, "2472");
       
       SearchPage.setSortCriterion();
       SearchPage.setSortCriterionValue(1, "Document ID");
       
       performSearch();
       
       DocView.setNumberOfDocumentPerPage(100);
       
       DocView.selectDocumentsWitIDs("1973");
       
       new DocView().clickActionMenuItem("Manage Document Security");
       switchToFrame(getElements(By.cssSelector("iframe[src*='javascript']")).get(0));
       
       ManageDocumentSecurity.blockTeam("Document Security");
       ManageDocumentSecurity.closeWindow();
       
       SearchPage.logout();
		
       performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);	
       
       new SelectRepo(AppConstant.DEM0);
      
       SearchPage.setWorkflow("Search");
       
       SearchPage.openDownloadWindow();
       
       totalDownloadsForTestUser = getTotalElementCount(By.className("BlueAnchor"));
       
       softAssert.assertEquals(totalDownloadsForTestUser, 1, "10) Verify that \"Test User\" ID can no longer access the downloads from (a) and (b), but CAN access that from (c):: ");

       softAssert.assertAll();
    }
}