package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ConceptReviewAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM4 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page and Open the cluster ""wat, niets, alleen, laten, toe, heel, vind, mee, aan, waar, verder, probleem, mi"" 
	(5) Confirm Documents are all Blocked for ""Document Security"" Team by Selecting ""Manage Document Security"" in the Actions Menu 
	(6) Confirm the ""Test User"" ID cannot Open or Search ""wat, niets, alleen, laten, toe, heel, vind, mee, aan, waar, verder, probleem, mi"" cluster 
	(7) Confirm Cluster Details Pop-up counts for ""wat, niets, alleen, laten, toe, heel, vind, mee, aan, waar, verder, probleem, mi"" cluster and subclusters show 0 documents."

*/

public class DSSecureCluster extends TestHelper{
	@Test
    public void test_c412_DSSecureCluster(){
    	new SelectRepo(AppConstant.DEM4);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"DEM4 Admins\" Team:: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        ConceptReviewAccordion.open();
        ConceptReviewAccordion.selectFirstCluster();
        ConceptReviewAccordion.openSelectedForView();
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.blockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();
        
        SearchPage.logout();
        
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
        
        new SelectRepo(AppConstant.DEM4);
    	
    	SearchPage.setWorkflow("Search");
    	
    	ConceptReviewAccordion.open();
    	ConceptReviewAccordion.selectFirstCluster();
    	ConceptReviewAccordion.expandConceptReviewItem(1);
    	
    	int totalDocsInSelectedCluster = ConceptReviewAccordion.getTotalDocsInCluster();
    	
    	ConceptReviewAccordion.closeClusterDetailsModal();
    	
    	softAssert.assertEquals(totalDocsInSelectedCluster, 0, "7) Confirm Cluster Details Pop-up counts for 0 document:: ");
    	
    	tryClick(ConceptReviewAccordion.viewSelected);
    	
    	String alertMsg = getAlertMsg();
    	
    	System.out.println( Logger.getLineNumber() + alertMsg + "***");
    	
    	softAssert.assertEquals(alertMsg, "Your search did not find any documents, please revise it and try again.");
    	
    	softAssert.assertAll();    	
    }
}