package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *    Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Load the ""Document Security"" Team and add the ""Test User"" ID, removing it from other teams as needed 
	(4) Return to Search Page and Open the EDRM folder in DEM0 DB (193,360 docs)  
	(5) Confirm Documents are all Blocked for ""Document Security"" Team by Selecting ""Manage Document Security"" in the Actions Menu 
	(6) Confirm the ""Test User"" ID cannot Open or Search ""EDRM"" folder and that the Folder no longer appears on Folder Tab.
 */

public class DSSecureFolder extends TestHelper{
   @Test
   public void test_c378_DSSecureFolder(){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"DEM4 Admins\" Team:: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        FolderAccordion.open();
        FolderAccordion.openFolderForView("EDRM");
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        ManageDocumentSecurity.blockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();
        
        SearchPage.logout();
        
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
        
        new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	FolderAccordion.open();
    	
    	getElements(FolderAccordion.folderItemLocator).stream().filter(folder -> getText(folder).equals("EDRM")).findFirst().get().click();
    	
    	String alertMsg = getAlertMsg();
    	
    	softAssert.assertEquals(alertMsg, "No documents were found in the selected folder(s). Please select a different folder(s) and try again.", "7) Confirm the \"Test User\" ID cannot Open or Search \"EDRM\" folder and that the Folder no longer appears on Folder Tab:: ");
    	
    	softAssert.assertAll();    	
    }
}