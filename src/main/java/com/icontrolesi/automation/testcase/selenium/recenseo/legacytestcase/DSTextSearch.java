package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *   "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page, run a full text search for Documents Containing ""Foster"" which should find 3839 documents  
	(5) Select ""Manage Document Security"" in the Actions Menu and block 100 Documents from ""Document Security"" Team  
	(6) Confirm the ""Test User"" ID finds 3739 documents when running a full text search for Documents Containing ""Foster""  
	(7) Select ""Manage Document Security"" in the Actions Menu and block all 3839 documents  from ""Document Security"" Team  
	(8) Confirm the ""Test User"" ID finds 0 documents when running a full text search for Documents Containing ""Foster"""
 */

public class DSTextSearch extends TestHelper{
	@Test
	public void test_c1638_DSTextSearch(){
		handleDSTextSearch(driver);
	}

    private void handleDSTextSearch(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.addSearchCriteria("Document Content (Full Text)");
        SearchPage.setCriterionValue(0, "Foster");

        performSearch();
        
        int totalDocument = DocView.getDocmentCount();
        
        softAssert.assertEquals(totalDocument, 3839, "(4) Return to Search Page, run a full text search for Documents Containing \"Foster\" which should find 3839 documents:: ");
        
        DocView docView = new DocView();
        
        docView.clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        ManageDocumentSecurity.unBlockTeam("Document Security");	//unblock all documents first
        ManageDocumentSecurity.closeWindow();   
        
        DocView.setNumberOfDocumentPerPage(100);
        
        DocView.selectAllDocuments();
        
        docView.clickActionMenuItem("Manage Document Security");
        
        switchToFrame(getElements(By.cssSelector("iframe[src^='javascript']")).get(1));
        
        ManageDocumentSecurity.blockTeam("Document Security");
        
        ManageDocumentSecurity.closeWindow();      
        
        SearchPage.logout();
		
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
        
        new SelectRepo(AppConstant.DEM0);
       
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document Content (Full Text)");
        SearchPage.setCriterionValue(0, "Foster");
	       
        performSearch();
        
        int totalDocumentForSecondUser = DocView.getDocmentCount();
        
        softAssert.assertEquals(totalDocumentForSecondUser, totalDocument - 100, "(6) Confirm the \"Test User\" ID finds 3739 documents when running a full text search for Documents Containing \"Foster\":: ");
        
        SearchPage.logout();
		
		performLoginToRecenseo();
        
        new SelectRepo(AppConstant.DEM0);
       
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document Content (Full Text)");
        SearchPage.setCriterionValue(0, "Foster");
       
        performSearch();
        
        docView.clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        ManageDocumentSecurity.blockTeam("Document Security");
        
        ManageDocumentSecurity.closeWindow();
        
        SearchPage.logout();
        
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);

		new SelectRepo(AppConstant.DEM0);
        
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document Content (Full Text)");
        SearchPage.setCriterionValue(0, "Foster");

        tryClick(SearchPage.runSearchButton, 2);
        
        String alertText = getAlertMsg();
        
        softAssert.assertEquals(alertText, "Your search did not find any documents, please revise it and try again.", "(8) Confirm the \"Test User\" ID finds 0 documents when running a full text search for Documents Containing \"Foster\":: ");
        
        SearchPage.clearSearchCriteria();
        
        switchToDefaultFrame();
        SearchPage.logout();
		
		performLoginToRecenseo();
        
        new SelectRepo(AppConstant.DEM0);
       
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Document Content (Full Text)");
        SearchPage.setCriterionValue(0, "Foster");
       
        performSearch();
        
        docView.clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        
        softAssert.assertAll();
    }
}