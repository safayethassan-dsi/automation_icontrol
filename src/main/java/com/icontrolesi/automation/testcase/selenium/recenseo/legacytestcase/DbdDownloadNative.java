package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.util.Calendar;
import java.util.zip.ZipFile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

public class DbdDownloadNative extends TestHelper{
	@Test
	public void test_c229_DbdDownloadNative(){
		handleDbdDownloadNative(driver);
	}

	private void handleDbdDownloadNative(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.deselectAllDocument();
		DocView.checkSelectedColumns("File Name");
		DocView.closePreferenceWindow();

		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		DocView.sortByColumn("File Name");
		DocView.sortByColumn("File Name");
		
		DocView.clickOnDocument(1);
		
		String documentName = getElement(By.cssSelector("td[aria-describedby='grid_cfil']")).getText();
		String [] temp = documentName.split("\\.");
		String fileExt = temp[temp.length - 1];
		
		int documentId = DocView.getDocmentId();
		
		ReviewTemplatePane.openDownloadWindow();
		ReviewTemplatePane.openFileTypePrefs();
		ReviewTemplatePane.setDownloadPreferenceAllTo("Native");
		ReviewTemplatePane.closeFileTypePrefs();
		ReviewTemplatePane.clickIncludeCompleteDocFamily();
		
		WebElement checkerElement = Utils.waitForElement(Driver.getDriver(), "doc_set", "id");
		
		softAssert.assertTrue(checkerElement.isSelected(), "(5) Confirm that only the 'Include Complete Doc Family' Option is available and select it:: ");
		
		Calendar cal = Calendar.getInstance();
		long year = cal.get(Calendar.YEAR);
		long month = cal.get(Calendar.MONTH) + 1;
		long day = cal.get(Calendar.DAY_OF_MONTH);
		
		//String folderPath = Configuration.getConfig("selenium.download_dir");
		String folderPath = AppConstant.DOWNLOAD_DIRECTORY;
		String fileMatcher = year+"\\.0*"+month+"\\.0*"+day + "\\s\\d{2}(-|_)\\d{2}(-|_)\\d{2}\\.zip";
	        
	    File [] files = FileMethods.getListOfFiles(folderPath, fileMatcher);
				
		FileMethods.deleteFiles(files);
		
		tryClick(By.id("submitJob"));
		waitFor(15);
		
		ZipFile zipEntry = getZipFile(FileMethods.getListOfFiles(folderPath, fileMatcher)[0].getAbsolutePath());
		
        boolean isDownloadOk = false;
		
	    isDownloadOk = isZipContentsOk(zipEntry, "(\\d+)\\.\\d+.*");
		
	    closeZipFile(zipEntry);
        
        softAssert.assertTrue(isDownloadOk, "(7) Open the zip file and make sure that the file(s) downloaded correctly and naming is correct and Confirm all documents in the family are included and Named for Doc ID and Original file name:: ");
	     
	    ReviewTemplatePane.closePopupWindow();
       
	    ReviewTemplatePane.openDownloadWindow();
       
        tryClick(By.name("include_ppdat"), 1);
       
        files = FileMethods.getListOfFiles(folderPath, fileMatcher);
		
	    FileMethods.deleteFiles(files);
       
        tryClick(By.id("submitJob"), 15);
       
        zipEntry = getZipFile(FileMethods.getListOfFiles(folderPath, fileMatcher)[0].getAbsolutePath());
       
        isDownloadOk = isZipContentsOk(zipEntry, "1\\."+documentId+".\\w+-\\d{7}-\\w+-\\d{7}\\." + fileExt);
       
        softAssert.assertTrue(isDownloadOk, "(10) Open the zip file and make sure that the file(s) downloaded correctly and confirm files are now named for Doc ID.Bates Number -Bates Number:: ");
       
        softAssert.assertAll();
	}
}