package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

import com.icontrolesi.automation.platform.util.Logger;


public class DbdPost extends TestHelper{
	private int selectedRowNumber = 5;
	public By selectedRowLocator = By.cssSelector("#grid > tbody > tr:nth-child(" + selectedRowNumber + ")");

	@Test
	public void test_c283_DbdPost(){
		handleDbdPost(driver);
	}
	
	
	private void handleDbdPost(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.closePreferenceWindow();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Loose Files");
		
		switchToDefaultFrame();
		SearchPage.setWorkflowFromGridView("QC Objective Coding");
		
		if(DocView.isAllDocumentsOutOfState()){
			DocView.openWFSManagerWindow();
			DocView.addDocsToWorkflow("QC Objective Coding");
			SearchPage.gotoViewDocuments();
		}
		
				
		DocView.clickOnDocument(5);
		
		int previousDocumentID = DocView.getDocmentId();
		
		ReviewTemplatePane.postDocument();
		
		DocView.gotoPreviousDocument();
		waitFor(15);
		
		int currentDocumentID = DocView.getDocmentId();
		
		softAssert.assertNotEquals(previousDocumentID, currentDocumentID, "4) Once you have posted document out of workflow, then use the back navigation arrow and confirm it will not go back to the document you just posted:: ");
		
		DocView.selectDocumentBySpecificNumber(5);
		
		currentDocumentID = DocView.getDocmentId();
		
		String selectedRowColor = getCssValue(selectedRowLocator, "color");
		String selectedRowState = getAttribute(selectedRowLocator, "class");
		
		System.out.println( Logger.getLineNumber() + "*******************Color: "+ selectedRowColor);
		System.out.println( Logger.getLineNumber() + "^^^^^^^^^^^^^^^^^^^State: "+ selectedRowState);
		
		softAssert.assertEquals(currentDocumentID, 5, "5) Using the \"Go To\" box enter the hit no of the doc you posted and hit return. Confirm that you go to the right document, and that \"Post\" is now grayed out.:: ");
		softAssert.assertEquals(selectedRowColor, "rgba(51, 51, 51, 1)", "5) Using the \"Go To\" box enter the hit no of the doc you posted and hit return. Confirm that you go to the right document, and that \"Post\" is now grayed out.:: ");
		
		DocView.clickOnDocument(7);
		
		int documentIdInView = DocView.getDocmentId();
		
		boolean isDocumentIDFieldExist = getElements(DocView.codingTemplateFieldLocator).stream()
				.filter(field -> getText(field).startsWith("Document ID:"))
				.count() == 1;
		
		if(!isDocumentIDFieldExist){
			SprocketMenu.openWorkflowAndFieldManager();
			WorkflowFieldManager.switchToWorkflowTemplates();
			WorkflowFieldManager.selectWorkflow("QC Objective Coding");
			WorkflowFieldManager.addField("Document ID");
			
			SearchPage.gotoViewDocuments();
		}
		
		
		int documentIdInCodingTemplate = DocView.getDocmentIdFromCodingTemplate();
		
		softAssert.assertEquals(documentIdInView, documentIdInCodingTemplate, "7) Confirm that the coding in the right hand review pane matches coding in the Grid for the fields below (fields may need to be added to grid view):: ");
		
		DocView.selectView("History");
		String currentData = getText(By.cssSelector("td[aria-describedby='docHistory_newVal']"));
		String oldData = getText(By.cssSelector("td[aria-describedby='docHistory_prevVal']"));
		
		System.out.println( Logger.getLineNumber() + currentData + ":" + oldData);
		
		softAssert.assertAll();
	}
}
