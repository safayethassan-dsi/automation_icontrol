package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class DbdPrintFromImageDefaultProdSetSelection extends TestHelper{
	@Test
	public void test_c254_DbdPrintFromImageDefaultProdSetSelection(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.switchPreferenceTab("Bates");
		selectFromDrowdownByText(DocView.defaultProductionSet, "CDOCDEMO");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, "26");
		
		performSearch();
		
		ReviewTemplatePane.openPrintingWindow();
		
		String defaultProductionSetVal = getSelectedItemFroDropdown(ReviewTemplatePane.printBatesDropdownLocator);
		
		softAssert.assertEquals(defaultProductionSetVal, "(CDOCDEMO) TITEST-000597-000607");
		
		
		SearchPage.goToDashboard();
		
		DocView.openPreferenceWindow();
		DocView.switchPreferenceTab("Bates");
		selectFromDrowdownByText(DocView.defaultProductionSet, "DEMOTEST2012");
		DocView.closePreferenceWindow();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();
		
		ReviewTemplatePane.openPrintingWindow();
		
		String defaultProductionSetVal_02 = getSelectedItemFroDropdown(ReviewTemplatePane.printBatesDropdownLocator);
		
		softAssert.assertEquals(defaultProductionSetVal_02, "(DEMOTEST2012) DEM0-TEST0002555-0002565");
		
		softAssert.assertAll();		
	}
}
