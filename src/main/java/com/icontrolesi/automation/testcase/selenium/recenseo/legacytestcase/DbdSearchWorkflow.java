package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Shaheed Sagar
 *
 *	"DB: DEMO 
	(1) Choose a workflow state to which test id has access (for example, Issue Review)  
	(2) Go to Settings\Administrative Settings  
	(3) Choose Manage Users  
	(4) Remove your access to the workflow you are working in  
	(5) Leave Repository and Return  
	(6) Confirm that you no longer have access to the removed work flow and that you are now in Search Workflow.  
	(7) Choose a new workflow state  
	(8) Post a Document in that workflow and confirm in Coding History that the right workflow was posted."
 */

public class DbdSearchWorkflow extends TestHelper{
	String selectableWorkflowOtherThanSearch = "";
	UserManager userManager = new UserManager();
	
	@Test
	public void test_c376_DbdSearchWorkflow(){
		try{
			handleSearchWorkflow(driver);
		}finally {
			/*SprocketMenu.openUserManager();
			
			userManager.checkUser(Configuration.getConfig("recenseo.username"));
			userManager.clickLoadSettings();
			userManager.setWorkflowStateAccess(selectableWorkflowOtherThanSearch, UserManager.ASSIGN);
			userManager.clickUpdateAndSave();*/
			
			System.out.println( Logger.getLineNumber() + "Workflow state access for '" + selectableWorkflowOtherThanSearch + "' is granted...");
		}
	}

	private void handleSearchWorkflow(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		List<WebElement> list = getElements(By.cssSelector("#workflows > div > div > div > div"));
		
		System.out.println( Logger.getLineNumber() + list.get(0).getAttribute("data-name").trim() + "&&&&&&&&&");
		
		selectableWorkflowOtherThanSearch = getElements(By.cssSelector("#workflows > div > div > div > div")).stream()
															.filter(workflow -> !workflow.getAttribute("data-name").trim().equals("Search"))
															.findFirst().get().getAttribute("data-name").trim();
		
		System.out.println( Logger.getLineNumber() + "Current Workflow: " + selectableWorkflowOtherThanSearch);
		
		SearchPage.setWorkflow(selectableWorkflowOtherThanSearch);
		
		SprocketMenu.openUserManager();
		
		UserManager.checkUser(AppConstant.USER);
		userManager.clickLoadSettings();
		UserManager.setWorkflowStateAccess(selectableWorkflowOtherThanSearch, UserManager.NO_ACCESS);
		userManager.clickUpdateAndSave();
		
		System.out.println( Logger.getLineNumber() + "Workflow state access for '" + selectableWorkflowOtherThanSearch + "' is revoked...");
		
		SprocketMenu.selectRepoFromSecondaryMenu("Recenseo Demonstration Database 2.0");
		
		SprocketMenu.selectRepoFromSecondaryMenu("Recenseo Demo DB");	//get back to initial repo
		
		switchToDefaultFrame();
		String currentWorkflow= SearchPage.getCurrentWorkFlow();
		
		System.out.println( Logger.getLineNumber() + currentWorkflow+"***************");
		
		List<WebElement> workflowList = getElements(By.cssSelector("#workflows > div > div > div > div > a"));
		
		System.out.println( Logger.getLineNumber() + workflowList.get(0).getText()+"************");
		System.out.println( Logger.getLineNumber() + getElement(By.cssSelector("#workflows > div > div > div > div > a")).getText().trim() + "$$$$$$$$$$$$$$$");
		
		boolean isWorkflowRemoved = workflowList.stream().filter(w -> w.getText().trim().equals("Issue Review")).count() == 0;
		
		softAssert.assertTrue(isWorkflowRemoved, "6) Access for workflow '" + " is removed:: ");
		softAssert.assertEquals(currentWorkflow, "Search", "6) Workflow is reset to 'Search':: ");
		
		switchToDefaultFrame();
		
		String workflowForPosting = getElements(By.cssSelector("#workflows > div > div > div > div")).stream()
				.filter(workflow -> !workflow.getAttribute("data-name").trim().equals("Search"))
				.findFirst().get().getAttribute("data-name").trim();
		
		System.out.println( Logger.getLineNumber() + "Workflow for posting: " + workflowForPosting);
		
		SearchPage.setWorkflow(workflowForPosting);
		
		performSearch();
		
		DocView.clickOnDocument(5);
		ReviewTemplatePane.postDocument();
		
		DocView.selectView("History");
		
		String currentData = getText(By.cssSelector("td[aria-describedby='docHistory_newVal']"));
		String oldData = getText(By.cssSelector("td[aria-describedby='docHistory_prevVal']"));
		
		softAssert.assertTrue(oldData.contains(workflowForPosting), "8) Post a Document in that workflow and confirm in Coding History that the right workflow was posted (Current workflow exists):: ");
		softAssert.assertFalse(currentData.contains(workflowForPosting), "8) Post a Document in that workflow and confirm in Coding History that the right workflow was posted (Current workflow removed):: ");
	}
}
