package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
* 
* @author Shaheed Sagar
*
*DB: DEM4 
	(1) Search for Doc ID 276823 from the search page.  
	(2) Go to the "Thread" tab and confirm:  
	- Recenseo displays: "You are viewing the email thread for doc id 276823"  
	- the Grid View displays doc 276823 and 2 other email sorted in ascending date order  
	- The subject data for all of the email look to be part of the same thread  
	(3) Use the "Complete Doc Families" button "+" and confirm:  
	- Recenseo adds and highlights the proper number of attachments for each email.  
	(4) View one of the email, other than 276823  
	(5) Verify that you can use the "Back To Search Results" to go back to your original search regardless of which email in the thread you were viewing.
*/

public class DbdThread extends TestHelper{
	@Test
	public void test_c255_DbdThread(){
		handleDbdThread(driver);
	}

	private void handleDbdThread(WebDriver driver){
		new SelectRepo(AppConstant.DEM4);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Title", "Document Date", "Document ID");
		DocView.clickCrossBtn();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setOperatorValue(0, "equals");
		SearchPage.setCriterionValue(0, "276823");

		performSearch();
		
		DocView dv = new DocView();
		DocView.selectViewByLink("Thread");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		String recenseoPopupForThread = getText(By.id("breadcrumb_msg"));
		
		softAssert.assertEquals(recenseoPopupForThread, "You are viewing the email thread for doc id 276823", "2a.) You are viewing the email thread for doc id 276823:: ");
			
		int docsCount = DocView.getDocmentCount();
		
		List<WebElement> dateElements = getElements(DocView.documentDateColumnLocator);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		boolean isOrderedByDate = true;
		for(int i = 0; i < dateElements.size() - 1; i++){
			try {
				Date date1 = dateFormat.parse(dateElements.get(i).getText());
				Date date2 = dateFormat.parse(dateElements.get(i + 1).getText());
				if(date1.compareTo(date2) > 0){
					isOrderedByDate = false;
					break;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		softAssert.assertEquals(docsCount, 3, "2b.) Gridview displays 3 documents including 276823...:: "); 
		softAssert.assertTrue(isOrderedByDate, "2b.) Gridview displays 3 documents including 276823 sorted in ascending date order:: ");
		
		
		List<WebElement> documentSubjectList = getElements(By.cssSelector("td[aria-describedby='grid_subj']"));
		
		for(WebElement subject : documentSubjectList){
			softAssert.assertTrue(subject.getText().contains("981225"), "2c.) The subject data for all of the email look to be part of the same thread:: ");
		}
		
		dv.clickCompleteDocFamily();
		docsCount = DocView.getDocmentCount();
			
		softAssert.assertEquals(docsCount, 6, "3) Recenseo adds and highlights the proper number of attachments for each email:: "); 
		
		DocView.clickOnDocument(4);
		
		DocView.clickBackToSearchResult();
		List<WebElement> documentList = getElements(By.cssSelector("td[aria-describedby='grid_inv_id']"));
		int documentID = DocView.getDocmentId();
		
		softAssert.assertEquals(documentList.size(), 1, "5.) Back To Search Results' goes back to search regardless of which email in the thread it was viewing(Count must be 1):: ");
		softAssert.assertEquals(documentID, 276823, "5.) Back To Search Results' goes back to search regardless of which email in the thread it was viewing:: ");
		
		softAssert.assertAll();
	}
}
