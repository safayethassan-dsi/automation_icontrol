package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;


import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class DeleteReviewAssignment extends TestHelper{
	public static String assigneeName = "Abdul Awal";
	String assignmentNameToOpen = "DeleteReviewAssignment_13538";
	String assignmentWorkflow = "Search";
	String assignmentPriority = "3";
	String assignmentName = "DeleteReviewAssignment_13538_" + new Date().getTime();
	
	By visibleAssignmentLocator = By.cssSelector("#assignmentList > li[class$='visible']");
	
	@Test
	public void test_c1667_DeleteReviewAssignment(){
		handleDeleteReviewAssignment(driver);
	}


	private void handleDeleteReviewAssignment(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.selectAllFilterByUsers();
		ReviewAssignmentAccordion.selectAllFilterByStatus();
		ReviewAssignmentAccordion.openAssignment(assignmentNameToOpen);
		
		DocView dc= new DocView();
		dc.clickActionMenuItem("Manage Review Assignments");
		
	    switchToFrame(1);
	    waitFor(10);
	    
	    //ManageReviewAssignment.gotoFiltersPanel();
	    //ReviewAssignmentAccordion.selectAllFilterByStatus();
	    //ReviewAssignmentAccordion.selectAllFilterByUsers();
	    
	    ManageReviewAssignment.createAssignment(assignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    //ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    
	    String alertMsg = ManageReviewAssignment.deleteAssignment(assignmentName);
	    
	    boolean isProperMsgAppeared = alertMsg.contains("Deleting an assignment will delete the assignment and remove all of the documents assigned under it.");
	   
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    
	    String assignmentSearchMsg = getText(By.cssSelector("#noAssignments[style='display: list-item;']"));
	    
	    softAssert.assertTrue(isProperMsgAppeared, "4) Verify a warning pop-up box appears. Click OK to delete the assignment:: ");
	    softAssert.assertEquals(assignmentSearchMsg, "No assignments found!", "5) Verify the assignment was deleted:: ");
	
	    ManageReviewAssignment.createAssignment(assignmentName + "_1", assignmentWorkflow, assigneeName, assignmentPriority);
	    ManageReviewAssignment.createAssignment(assignmentName + "_1", assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    ManageReviewAssignment.gotoAdminPanel();
	    
	    tryClick(By.id("toggleAllAssignments"), 1);
	    
	    String alertMsg2 = ManageReviewAssignment.clickDeleteAssignment();
	    
	    isProperMsgAppeared = alertMsg2.contains("Deleting an assignment will delete the assignment and remove all of the documents assigned under it.");
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName + "_1");
	    
	    assignmentSearchMsg = getText(By.cssSelector("#noAssignments[style='display: list-item;']"));
	    
	    softAssert.assertTrue(isProperMsgAppeared, "7)  Verify a warning pop-up box appears. Click OK to delete the assignment. :: ");
	    //softAssert.assertEquals((totalAssignmentsBeforeDeleting - totalAssignmentsAfterDeleting), 0, "8) Verify the assignments were deleted:: ");
	    softAssert.assertEquals(assignmentSearchMsg, "No assignments found!", "8) Verify the assignments were deleted:: ");
	
	    softAssert.assertAll();
	}
}