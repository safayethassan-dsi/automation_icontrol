package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

public class DeleteTeam extends TestHelper{
	String teamName = "test_c1679_UpdateTeam_" + new Date().getTime();
	
	@Test
	public void test_c1676_DeleteTeam(){
		handleDeleteTeam(driver);
	}

	private void handleDeleteTeam(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
		SprocketMenu.openUserManager();

		UserManager.countTabSwitch = 0;
		UserManager.createTeam(teamName, "Team of Ishtiyaq","USER");
		
        UserManager.deleteTeam(teamName);	
		
		boolean teamExists = UserManager.teamExists(teamName);
        
        Assert.assertFalse(teamExists, "(5) Verify that the team is no longer available in the [Available Teams] drop down:: ");
  }
}
