package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * mantis# 15093
 * 
 * @author Ishtiyaq Kamal
 *
 */

public class DocCount extends TestHelper{
	@Test
	public void test_c100_DocCount(){
		handleDocCount(driver);
	}
	
	
	protected void handleDocCount(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
 		
 		SearchPage.setWorkflow("Search");

		FolderAccordion.open();

		long totalDocsInSearch = FolderAccordion.getTotalDocCount();
		System.out.println( Logger.getLineNumber() + "Total docs count in Search: " + totalDocsInSearch);

		SearchPage.gotoViewDocuments();

		// Get count of total Docs from Grid
		
		long totalDocsInGridView = DocView.getDocmentCount();
		
		System.out.println( Logger.getLineNumber() + "Doc count in Grid Section: " + totalDocsInGridView);
		
		Assert.assertEquals(totalDocsInSearch, totalDocsInGridView, "(3)Click Browse (View) and get the total number of documents listed in the grid view section and make sure this number matches the search page number from the folder section:: ");

		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.clearSearchCriteria();

		switchToDefaultFrame();
	
		SearchPage.setWorkflow("Issue Review");
		
		// Get doc count under Folders section
		FolderAccordion.open();
		long totalDocsInSearchForIssueReview = FolderAccordion.getTotalDocCount();
		System.out.println( Logger.getLineNumber() + "Doc count in Folders Section for Issue Review: " + totalDocsInSearchForIssueReview);

		// Check if page has been refreshed
		/*if (totalDocsInSearchForIssueReview != totalDocsInGridView) {
			System.out.println( Logger.getLineNumber() + "Page Refresh has taken place");
			result.nl().record("Page Refresh has taken place");
			Logger.log("Page Refresh has taken place");
		} else {
			System.out.println( Logger.getLineNumber() + "Page Refresh has not taken place");
			result.nl().record("Page Refresh has not taken place");
			Logger.log("Page Refresh has not taken place");
			result.fail();
		}*/

		// Click "Run search" button
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();

		// Get doc count from grid when workflow is "Issue Review"
		long totalDocsInGridViewForIssueReview = DocView.getDocmentCount();
		System.out.println( Logger.getLineNumber() + "Doc count in Grid section for Issue Review: " + totalDocsInGridViewForIssueReview);

		Assert.assertEquals(totalDocsInSearchForIssueReview, totalDocsInGridViewForIssueReview, "(7)Click [Run Search] to search the whole database and confirm number has not changed:: ");

		DocView dc=new DocView();
		dc.clickActionMenuItem("Workflow state manager");
		System.out.println( Logger.getLineNumber() + "Frame# " + getTotalElementCount(By.tagName("iframe")));
		// Switch to frame inside Work-flow manager
		switchToFrame(1);
		
		waitFor(10);;

		long doc_count_issue_review = Long.parseLong(getText(By.cssSelector("#summaryContainer > p:nth-child(1)")).replaceAll("\\D+", ""));
		System.out.println( Logger.getLineNumber() + "Review count in Workflow manager: "	+ doc_count_issue_review);
		
		Assert.assertEquals(doc_count_issue_review, totalDocsInSearchForIssueReview, "(8)Select Workflow State Manager from Actions Menu and confirm number of documents in Issue Review number matches  the search page number from the folder section:: ");

		SearchPage.goToDashboard();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		FolderAccordion.open();
		long docCountInSearchForIssueReview = FolderAccordion.getTotalDocCount();

		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.clearSearchCriteria();
		SearchPage.addSearchCriteria("Workflow");

		performSearch();

		// Doc count in grid view consisting only docs in state [issue review]
		long docCountInGridForIssueReview = DocView.getDocmentCount();
		System.out.println( Logger.getLineNumber() + "Doc count in Grid section for docs in Issue Review state : " + docCountInGridForIssueReview);
		
		Assert.assertEquals(doc_count_issue_review, docCountInSearchForIssueReview, "(11) Run Search and confirm the total number of documents listed in the grid view section matches the search page number from the folder section:: ");
		
		SearchPage.goToDashboard();
		SearchPage.setWorkflow("Issue Review");
		SearchPage.clearSearchCriteria();
		
		waitForFrameToLoad("folderFrame");
		FolderAccordion.openFolderInfoDetail();
		
		long docCountInFolder = FolderAccordion.getTotaDocCountInFolderInfo();
		
		FolderAccordion.closeFolderInfoDetail();
		FolderAccordion.openFolderForView("EDRM");

		// Get total doc count from Grid
		long docCountInGridViewForIssueReviewState2 = DocView.getDocmentCount();
		System.out.println( Logger.getLineNumber() + "After opening ERDM folder, the total Doc count in Grid section  : "	+ docCountInGridViewForIssueReviewState2);
		
		System.out.println( Logger.getLineNumber() + "Total doc count on search page within folder: "+docCountInFolder);

		Assert.assertEquals(docCountInFolder, docCountInGridViewForIssueReviewState2, "(15)Confirm the right total number of documents are found:: ");

		SearchPage.goToDashboard();
		
		// Clear Search with "Clear" button
		waitForFrameToLoad(Frame.MAIN_FRAME);
		SearchPage.clearSearchCriteria();

		// Select EDRM folder and add to Search Criteria
		FolderAccordion.open();
		FolderAccordion.selectFolder("EDRM");
		FolderAccordion.addSelectedToSearch();
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		performSearch();

		// Get total doc count from Grid
		long docCountInGridIssueReviewState3 = DocView.getDocmentCount();
		
		Assert.assertEquals(docCountInGridIssueReviewState3, docCountInFolder, "(18) Run Search and Confirm the right total number of documents are found:: ");

		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		FolderAccordion.open();
		Select select = new Select(getElement(By.id("folder-showPermission")));
		select.selectByVisibleText("Current Workflow");
		waitFor(8);
		
		FolderAccordion.openFolderInfoDetail();
		
		docCountInFolder = FolderAccordion.getTotaDocCountInFolderInfo();
		
		FolderAccordion.closeFolderInfoDetail();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
			// Select Search Criteria
		new Select(getElement(By.id("addList"))).selectByVisibleText("Workflow");

		FolderAccordion.open();
		FolderAccordion.selectFolder("EDRM");
		FolderAccordion.addSelectedToSearch();
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();
		
		long doc_count_grid_issue_review_state_4 = DocView.getDocmentCount();
		System.out.println( Logger.getLineNumber() + "After adding EDRM folder to search and worflow being [issue review] the total Doc count in Grid section  : "
						+ doc_count_grid_issue_review_state_4);

		Assert.assertEquals(doc_count_grid_issue_review_state_4, docCountInFolder, "(20) Run Search and Confirm the number of documents found matches the search page number from the EDRM folder Information Page:: ");
	}
}