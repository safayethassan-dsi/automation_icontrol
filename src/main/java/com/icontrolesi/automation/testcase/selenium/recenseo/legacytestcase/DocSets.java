package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;

/**
 * 
 * 		"DB: DEM0 
 *		(1) Search for Doc ID 1177.  
 *		(2) Select the Undock button in the  upper-right corner of the Document Viewer Window  
 *		(3) Click on the ""Doc Family"" Tab and confirm that it now displays 3 documents total in your grid view.  
 *		(4) Click on one of the documents added by the ""Doc Family"" inclusion and confirm you can code fields and post without error 
 *		(5) Click ""Back to Search Results"" and ensure you are taken back to the proper document and that you are left with only the original document in grid view."
 *	
 * 
 */

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class DocSets extends TestHelper{
	@Test
	public void test_c237_DocSets(){
		handleDocSets(driver);
	}

	private void handleDocSets(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
	
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.clickCrossBtn();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, "1177");
		
		performSearch();
		
		DocView dc = new DocView();
		
		DocView.selectViewByLink("Doc Family");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int docCount = DocView.getDocmentCount();
				
		softAssert.assertEquals(docCount, 3, "(3) Click on the [Doc Family] Tab and confirm that it now displays 3 documents total in your grid view:: ");
		
		WebElement codingCommentBox = getElement(DocView.commentInCodingTemplate);
		String oldValue = codingCommentBox.getAttribute("value");
		System.out.println( Logger.getLineNumber() + "old : "+oldValue);
		codingCommentBox.clear();
		String newValue = "EDITED";
		codingCommentBox.sendKeys(newValue);
		System.out.println( Logger.getLineNumber() + "keys sent");
		dc.clickPostButton();
		DocView.clickOnDocument(2);
		String updatedComment = getText(DocView.commentInCodingTemplate);
		System.out.println( Logger.getLineNumber() + "updated : "+updatedComment);
		
		softAssert.assertEquals(updatedComment, newValue.trim(), "(4) Click on one of the documents added by the 'Doc Family' inclusion and confirm you can code fields and post without error:: ");
		
		editText(DocView.commentInCodingTemplate, oldValue);
		dc.clickPostButton();
		
		DocView.clickBackToSearchResult();
		
		softAssert.assertEquals(getText(DocView.documentIDColumnLocator), "1177", "(5) Click 'Back to Search Results' and ensure you are taken back to the proper document and that you are left with only the original document in grid view:: ");
		
		softAssert.assertAll();
	 }
}
