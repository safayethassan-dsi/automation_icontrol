package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;


/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
	(1) Perform this step after spending 2 hours testing other tests in Grid View 
	(2) In Search Open Folder ""Loose Documents"" 
	(3) Code two documents in Objective Coding and post and note if the speed is slower than previously tested when moving to the next doc"
	*/

public class DocbyDocSpeedTest extends TestHelper{
   @Test(enabled = false)
   public void test_c284_DocbyDocSpeedTest(){
	   //loginToRecenseo();
	   handleDocbyDocSpeedTest(driver);
   }

    private void handleDocbyDocSpeedTest(WebDriver driver){
    	new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");

        SearchPage.setWorkflow(Driver.getDriver(), "Search");
       
        SearchPage sp = new SearchPage();

        performSearch();
        
        DocView docView = new DocView();
        long endTime = System.currentTimeMillis()+(2*60*60*1000);
        long loadingTime=0;
        while(System.currentTimeMillis()<endTime){
        	for(int i = 1; i <= 5; i++){
        		long startTime = System.currentTimeMillis();
            	DocView.clickOnDocument(Driver.getDriver(), i);
            	long stopTime = System.currentTimeMillis();
            	loadingTime = stopTime-startTime;
            	Utils.waitLong();
                if(System.currentTimeMillis()>endTime) break;
            }
        }
        
        long startTime = System.currentTimeMillis();
    	DocView.clickOnDocument(Driver.getDriver(), 1);
    	long stopTime = System.currentTimeMillis();
    	
    	long loadingTimeAfter2hour = stopTime - startTime;
    	
    	//Assert.assertEquals(, expected);
    	
    	//Utils.handleResult("Regular Loading Time = "+loadingTime+" ms \nLoading Time after 2 hour = "+loadingTimeAfter2hour+" ms", result, true);
    }
}