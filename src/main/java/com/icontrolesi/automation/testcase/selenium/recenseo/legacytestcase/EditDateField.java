package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class EditDateField extends TestHelper{
public static int warningPresent=0;
public WebElement div;

@Test
public void test_c219_EditDateField(){
	//loginToRecenseo();
	handleEditDateField(driver);
}

private void handleEditDateField(WebDriver driver){
			SearchPage sp= new SearchPage();
	 		
	 		new SelectRepo(AppConstant.DEM0);
			
			SearchPage.setWorkflow("Search");
	 		
	 		performSearch();
            
            //Assign value to date field
            ReviewTemplatePane rt= new ReviewTemplatePane();
            rt.editDateField(driver, sp, "2015-10-12");
            
            //Click the Save button
            ReviewTemplatePane.saveDocument();
            
            waitFor(driver,2);
            
            //Wait for saving to take place
            while(!SearchPage.waitForElement(driver, "btnSaveToDB", "id").isEnabled()){
            	//do nothing            	
            }
            
            
            //Check if the entered value was saved
            String dateFieldValue= SearchPage.waitForElement(driver, "date", "id").getAttribute("value");
                       
            Assert.assertEquals(dateFieldValue, "2015-10-12 00:00:00.0", "(2a) Add a completely new date:: ");
            
            //Test partial date entry
            rt.editDateField(driver, sp, "0000");
            
            //Click the Save button
            ReviewTemplatePane.saveDocument();

            //Check if alert message is present
            String alertMsg = getAlertMsg(driver);
            
            Assert.assertTrue(alertMsg.contains("Please enter a valid date."), "(2c)Test a partial date entry where the Day, Month, or Year (or some combination) are unknown:: ");
		}//end function
}//end class
