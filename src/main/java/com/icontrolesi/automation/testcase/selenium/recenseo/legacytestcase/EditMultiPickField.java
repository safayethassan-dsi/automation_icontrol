package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;

import com.icontrolesi.automation.platform.util.Logger;


public class EditMultiPickField extends TestHelper{
@Test
public void test_c232_EditMultiPickField(){
	//loginToRecenseo();
	handleEditMultiPickField(driver);
}

private void handleEditMultiPickField(WebDriver driver){
	Driver.setWebDriver(driver);
	
	SearchPage sp= new SearchPage();
	
	new SelectRepo(AppConstant.DEM0);
	
	SearchPage.setWorkflow(driver, "Issue Review");
	
	SprocketMenu sm= new SprocketMenu();
	sm.selectSubMenu("Tools", "Workflow and Field Manager");
	
	waitFor(driver, 10);
	
	List<WebElement> tab= getElements(By.cssSelector("a[class='ui-tabs-anchor']"));
	//getElement(By.id("ui-id-3"));
	click(tab.get(1));

	waitFor(driver, 10);
	
	List<WebElement> fields= getElements(By.cssSelector("td[class='template-field-description']"));
	for(int j=0;j<fields.size();j++){
		if(fields.get(j).getText().equalsIgnoreCase("Privileged")){
			WebElement sibling= SearchPage.getFollowingSibling(driver, fields.get(j));
			WebElement selectNode= sibling.findElement(By.cssSelector("select"));
			new Select(selectNode).selectByVisibleText("Multi-pick w/ add");
		}
	}
	
	
      for(int j=0;j<fields.size();j++){
		if(fields.get(j).getText().equalsIgnoreCase("Confidentiality")){
			WebElement sibling= SearchPage.getFollowingSibling(driver, fields.get(j));
			WebElement selectNode= sibling.findElement(By.cssSelector("select"));
			new Select(selectNode).selectByVisibleText("Multi-pick w/ add");
		}
	}
	
	
      waitFor(driver, 5);
	
	//driver.get("https://recenseotest.icontrolesi.com/QuickReviewWeb/case?id=148");
	String appUrl= Configuration.getConfig("recenseo.url");
	driver.get(appUrl);
	System.out.println( Logger.getLineNumber() + appUrl);
	String stringUrl= appUrl+"case?id=148";
	System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
	Driver.getDriver().get(stringUrl);
	
	switchToDefaultFrame();
	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
	FolderAccordion.open();
	FolderAccordion.openFolderForView("Loose Files");
	
	//Check that there are atleast two "Other" links
	List<WebElement> otherLinks= getElements(By.linkText("Other"));
	System.out.println( Logger.getLineNumber() + "Number of Other Links: "+otherLinks.size());
	
	String step3="pass";
	
	if(otherLinks.size()>=2){
		System.out.println( Logger.getLineNumber() + "More than one [Other] links found:pass");
	    //result.nl().record("More than one [Other] links found:pass");
	    Logger.log("More than one [Other] links found:pass");
	}else{
		System.out.println( Logger.getLineNumber() + "More than one [Other] links found:fail");
	    //result.nl().record("More than one [Other] links found:fail");
	    Logger.log("More than one [Other] links found:fail");
	    //result.fail();
	    step3="fail";
	}
	
	//Check that for "Privileged" field you have checkbox
	List<WebElement> checkBoxes= getElements(By.cssSelector("input[id*=priv_][type=checkbox]"));
	if(checkBoxes.size()<0){
		step3="fail";
	}
	
	//Check that there are more than one "Show All" link
	List<WebElement> showLinks= getElements(By.linkText("+ Show All"));
	System.out.println( Logger.getLineNumber() + "Number of Show Links: "+showLinks.size());
	
	/*if(showLinks.size()>=2){
		System.out.println( Logger.getLineNumber() + "More than one [Show All] links found:pass");
	    result.nl().record("More than one [Show All] links found:pass");
	    Logger.log("More than one [Show All] links found:pass");
	}else{
		System.out.println( Logger.getLineNumber() + "More than one [Show All] links found:fail");
	    result.nl().record("More than one [Show All] links found:fail");
	    Logger.log("More than one [Show All] links found:fail");
	    result.fail();
	    step3="fail";
	}*/
	
	System.out.println( Logger.getLineNumber() + "Step 3:"+step3);
	
	/*if(step3.equalsIgnoreCase("pass")){
		result.nl().record("Confirm at least 2 fields have Multi-ADD picklists.:pass");
	    Logger.log("Confirm at least 2 fields have Multi-ADD picklists.:pass");
	}else{
		result.nl().record("Confirm at least 2 fields have Multi-ADD picklists.:fail");
	    Logger.log("Confirm at least 2 fields have Multi-ADD picklists.:fail");
	    result.fail();
	}*/
	
	
	//Click Other link and create items
	int count=0;
	//otherLinks= getElements(By.linkText("Other"));
	otherLinks = getElements(By.cssSelector("fileldset.more-less div > a[href='#']"));
	for(int i=0;i<otherLinks.size();i++){
		//click(otherLinks.get(i));
		System.out.println( Logger.getLineNumber() + "Other: "+otherLinks.get(i).getText());
		otherLinks.get(i).click();
		waitFor(driver, 3);
		getElement(By.id("newPickFieldEntry")).sendKeys("New Entry"+ Integer.toString(count));
		//Click Submit button
		List<WebElement> buttons= getElements(By.cssSelector("span[class='ui-button-text']"));
		for(int j=0;j<buttons.size();j++){
			if(buttons.get(j).isDisplayed()){
				if(buttons.get(j).getText().equalsIgnoreCase("Submit")){
					click(SearchPage.getParent(driver, buttons.get(j)));
				}
			}
		}
		count++;
	}
   }//end function
}//end class
