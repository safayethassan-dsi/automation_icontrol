package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;
/**
 *
 * @author Ishtiyaq Kamal
 *

*/

public class EditRadioMultiPick extends TestHelper{
	@Test
	public void test_c225_EditRadioMultiPick(){
		handleEditRadioMultiPick(driver);
	}

    private void handleEditRadioMultiPick(WebDriver driver){
    	new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");
        
        SearchPage.setWorkflow(driver, "Privilege Review");
        
        performSearch();        
        
        SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.setDisPlayType("Priv Reason", "Multi-pick w/ add");
		
		DocView dc= new DocView();
		//Close the interface
		dc.clickCrossButton();
		
		Utils.waitMedium();
		
		//Click [Show All] for Priv Reason
		
		//Check for the presence of the same text as before
    }
}