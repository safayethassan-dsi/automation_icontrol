package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class EditTextField extends TestHelper{

	@Test
	public void test_c244_EditTextField(){
		handleEditTextField(driver);
	}

	private void handleEditTextField(WebDriver driver){
	      	SearchPage sp= new SearchPage();
			
			new SelectRepo(AppConstant.DEM0);
			
			SearchPage.setWorkflow("Search");
			
			FolderAccordion.open();
			FolderAccordion.openFolderForView("Loose Files");
			
			driver.switchTo().defaultContent();
			SearchPage.setWorkflowFromGridView(driver, "Objective Coding");
			
			
			ReviewTemplatePane rt= new ReviewTemplatePane();
			
			editText(DocView.commentInCodingTemplate, "small Edited Comment");
			
			ReviewTemplatePane.saveDocument();
			
			waitFor(driver, 8);
			
			String longText="This is a big comment-This is a big comment-This is a big comment-This is a big comment-This is a big comment-This is a big comment-This is a big comment-This is a big comment-This is a big comment-This is a big comment-10";
			
			editText(DocView.commentInCodingTemplate, longText);
			
			ReviewTemplatePane.saveDocument();
			
			waitFor(driver, 8);

			DocView.clickOnDocument(driver, 3);
			DocView.clickOnDocument(driver, 1);
			
			//Get value of text field
			String commentText= getText(DocView.commentInCodingTemplate);
			System.out.println( Logger.getLineNumber() + commentText);
			
			Assert.assertTrue(commentText.contains(longText), "(4) Attempt to paste a large volume of text into same field and verify that changes are captured and text is not truncated after save:: ");
	}
}