package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class GridActionAddingDocs2 extends TestHelper{
public static int warningPresent=0;

public static String assigneeName = "Abdul Awal";
String assignmentNameToOpen = "GridActionAddingDocs2_13531";
String assignmentWorkflow = "Privilege Review";
String assignmentPriority = "3";
String assignmentName = "GridActionAddingDocs2_13531_" + new Date().getTime();


@Test
public void test_c1660_GridActionAddingDocs2(){
	//loginToRecenseo();
	handleGridActionAddingDocs2(driver);
}


private void handleGridActionAddingDocs2(WebDriver driver){
	boolean isReviewAssignmentOpen = false;
	new SelectRepo(AppConstant.DEM0);
	
	SearchPage.setWorkflow("Search");
	
	ReviewAssignmentAccordion.open();
	try{
		ReviewAssignmentAccordion.openAssignment(assignmentNameToOpen);
	}catch (Exception e) {
		System.out.println( Logger.getLineNumber() + "Documents not found for the assignment: " + assignmentNameToOpen + ". Adding documents for the Assignment.");
		System.out.println( Logger.getLineNumber() + e.getCause()+"****");
		if(e.getMessage().contains("Your search did not find any documents, please revise it and try again.")){
			acceptAlert();
			SearchPage.clearSearchCriteria();
			SearchPage.addSearchCriteria("Addressee");
			performSearch();
			
			new DocView().clickActionMenuItem("Manage Review Assignments");
			switchToFrame(1);
			
			ReviewAssignmentAccordion.searchForAssignment(assignmentNameToOpen);
		    
		    ReviewAssignmentAccordion.selectAssignment(assignmentNameToOpen);	
		    //ManageReviewAssignment.addDocumentsToSelectedAssignment();
		    ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
		    tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
		    
		    isReviewAssignmentOpen = true;
		}
	}
	
	if(!isReviewAssignmentOpen){
		DocView dc = new DocView();
		dc.clickActionMenuItem("Manage Review Assignments");
		switchToFrame(1);
	}
    
    ManageReviewAssignment.createAssignment(assignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
    
    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
    
    ReviewAssignmentAccordion.selectAssignment(assignmentName);	
    
    ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
    
    String appearedWarningMsg = getText(By.cssSelector("#conflictContainer > p:nth-child(4)"));
    
    boolean isAppearedWarningOk = appearedWarningMsg.matches("- Of the \\d+ selected documents, \\d+ \\(including PENDING\\) are in other assignment\\(s\\)\\.");
    
    softAssert.assertTrue(isAppearedWarningOk, "7) Verify warning that documents are already in another assignment. Select reassign all:: ");
    
    
    tryClick(ManageReviewAssignment.resolutionChoiceReassignAll);
    tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
    acceptAlert();
     
	ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
	
	ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	
	String assignmentStatus = getText(By.cssSelector("div[class='right stats']"));
	
    softAssert.assertNotEquals(assignmentStatus, "Empty", "8) Verify documents were added to the new assignment:: ");
    
    //ReviewAssignmentAccordion.searchForAssignment(assignmentNameToOpen);
    
    ReviewAssignmentAccordion.searchForAssignment(assignmentNameToOpen);
    
    assignmentStatus = getText(By.cssSelector("div[class='right stats']"));
    
    softAssert.assertEquals(assignmentStatus, "Empty", "8) Verify documents were removed from the old assignment:: ");
        
    //removeSelectedDocumentsFromAssignment(assignmentName);
    ManageReviewAssignment.deleteAssignment(assignmentName);
    
    ReviewAssignmentAccordion.searchForAssignment(assignmentNameToOpen);
    ReviewAssignmentAccordion.selectAssignment(assignmentNameToOpen);
    ManageReviewAssignment.addDocumentsToSelectedAssignment();
    
    softAssert.assertAll();
}


	public void addSelectedDocumentsToAssignment(String assignmentName){
	ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	waitForInVisibilityOf(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	}
	
	public void removeSelectedDocumentsFromAssignment(String assignmentName){
	ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	waitForInVisibilityOf(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	}
	
	public String getAssignmentStatus(String assignmentName){
	List<WebElement> assignmentList = getElements(By.cssSelector("#assignmentList > li > div > div > span:nth-child(2)"));
	String assignmentStatus = "";
	for(int i = 0; i < assignmentList.size(); i++){
		if(assignmentList.get(i).getText().trim().equals(assignmentName)){
			assignmentStatus = getText(By.cssSelector("#assignmentList > li[class$='visible']  > div > div > div > span"));
		}
	}
	
	return assignmentStatus;
	}

}//end class
