package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class GridActionAddingDocs3 extends TestHelper{
	public static int warningPresent=0;
	String assigneeName = "Abdul Awal";
	String assignmentNameToOpen = "GridActionAddingDocs3_13532";
	String assignmentWorkflow = "Issue Review";
	String assignmentPriority = "3";
	String assignmentName = "GridActionAddingDocs3_13532_" + new Date().getTime();
	
	@Test
	public void test_c1661_GridActionAddingDocs3(){
		handleGridActionAddingDocs3(driver);
	}


	private void handleGridActionAddingDocs3(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.openAssignment(assignmentNameToOpen);
		
		DocView dc= new DocView();
		dc.clickActionMenuItem("Manage Review Assignments");
		
	    switchToFrame(1);
	    
	    ManageReviewAssignment.createAssignment(assignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    
	    ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	    
	    ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	    
	    String appearedWarningMsg = getText(By.cssSelector("#conflictContainer > p:nth-child(3)"));
	    
	    boolean isAppearedWarningOk = appearedWarningMsg.matches("- Of the \\d+ selected documents, \\d+ \\(including PENDING\\) are in other assignment\\(s\\)\\.");
	    
	    softAssert.assertTrue(isAppearedWarningOk, "7) Verify warning that documents are already in another assignment. Select reassign all:: ");
	   
	    tryClick(By.name("resolutionChoice"));
	    tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	    acceptAlert(driver);
	     
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		ReviewAssignmentAccordion.searchForAssignment(assignmentName);
		
		String assignmentStatus = getAssignmentStatus(assignmentName);
		
	    softAssert.assertNotEquals(assignmentStatus, "Empty", "8) Verify documents were added to the new assignment:: ");
	    
	    //ReviewAssignmentAccordion.searchForAssignment(assignmentNameToOpen);
	    
	    ReviewAssignmentAccordion.selectAssignment(assignmentNameToOpen);
	    
	    assignmentStatus = getAssignmentStatus(assignmentNameToOpen);
	    
	    softAssert.assertEquals(assignmentStatus, "Empty", "8) Verify documents were removed from the old assignment:: ");
	        
	    removeSelectedDocumentsFromAssignment(assigneeName);
	    
	    softAssert.assertAll();
}


public void addSelectedDocumentsToAssignment(String assignmentName){
	ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	waitForInVisibilityOf(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
}

public void removeSelectedDocumentsFromAssignment(String assignmentName){
	ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	waitForInVisibilityOf(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
}

public String getAssignmentStatus(String assignmentName){
	List<WebElement> assignmentList = getElements(By.cssSelector("#assignmentList > li[class$='visible']  > div > div > span:nth-child(2)"));
	String assignmentStatus = "";
	for(int i = 0; i < assignmentList.size(); i++){
		if(assignmentList.get(i).getText().trim().equals(assignmentName)){
			assignmentStatus = getText(By.cssSelector("#assignmentList > li[class$='visible']  > div > div > div > span"));
		}
	}
	
	return assignmentStatus;
}
}
