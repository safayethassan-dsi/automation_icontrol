package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;


public class GridActionAddingDocs4 extends TestHelper{
	@Test
	public void test_c1662_GridActionAddingDocs4(){
		handleGridActionAddingDocs4(driver);
	}
	
	private void handleGridActionAddingDocs4(WebDriver driver){
	        new SelectRepo(AppConstant.DEM0);

	        SearchPage sp= new SearchPage();
			
			SearchPage.setWorkflow("Search");
			
			TagAccordion.open();
			TagAccordion.expandTag("Automated - Recenseo Test");
			TagAccordion.selectTag("13530 - TVP-0044: Grid Action - Manage Review Assignment - - Adding documents to an assignment", true);
			TagAccordion.openSelected();
				
			//Open Manage Review Assignment 
			DocView dv= new DocView();
			dv.clickActionMenuItem("Manage Review Assignments");
			
			switchToFrame(1);
            waitFor(10);
            //Switch to Manage tab and select multiple assignments
			ManageReviewAssignment managerev= new ManageReviewAssignment();
			managerev.switchTab("a[href='#actionManageAssignments']");
			waitFor(4);
	
            //Select 2 assignments
			List<WebElement> checkBoxes=getElements(By.className("chkAssignment"));
			
			checkBoxes.get(0).click();
			checkBoxes.get(1).click();
			
			waitFor(4);
			click(SearchPage.waitForElement("button[onclick='validateAndAddDocument()']", "css-selector"));
			waitFor(4);
			
			String alertText = getAlertMsg(driver);
			
			Assert.assertTrue(alertText.contains("Can not add same document to multiple assignments! Select a single assignment"), 
					"4) Verify that popup error displays 'Can not add same document to multiple assignments! Select a single assignment:: ");
		}
}
