package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class GridActionCloningAssignments extends TestHelper{
	public static String assigneeName = "Abdul Awal";
	public static final int SORTING_TIME = 50;
	public static final String SORTING_MESSAGE = "Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";

	
	@Test
	public void test_c1664_GridActionCloningAssignments(){
		handleGridActionCloningAssignments(driver);
	}

	private void handleGridActionCloningAssignments(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Email");
		
		new DocView().clickActionMenuItem("Manage Review Assignments");
		
		switchToFrame(1);
		
		// Select an assignment that has docs in it
		WebElement assignmentWithDocs = SearchPage.waitForElement("open", "class-name");
		System.out.println( Logger.getLineNumber() + assignmentWithDocs.getText());
		// Get the total doc count
		List<WebElement> DocCountNodes = assignmentWithDocs.findElements(By.tagName("span"));
		
		String totalDocCount = DocCountNodes.get(1).getText();
		System.out.println( Logger.getLineNumber() + totalDocCount);

		waitFor(10);
		WebElement workflow = getElement(By.cssSelector("#assignmentList > li > div > div:nth-child(2) > span"));

		getElement(By.cssSelector("#assignmentList > li > input")).click();
	
		// Click the button "Clone"
		click(SearchPage.waitForElement("button[onclick*=CLONE]", "css-selector"));

		waitFor(5);

		// Assign name
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println( Logger.getLineNumber() + dateFormat.format(date));
		String assignmentName = "Ishtiyaq" + dateFormat.format(date);
		SearchPage.waitForElement("input[name='assignmentName']", "css-selector").sendKeys(assignmentName);

		waitFor(5);

		// Get list of workflows present in the select box
		String HiddenWorkFlow = null;
		String FirstVisibleWorkFlow = null;
		//WebElement workFlowListNode = SearchPage.waitForElement("select[name='assignmentWorkflow']", "css-selector");
		//List<WebElement> workFlowOptions = workFlowListNode.findElements(By.cssSelector("option"));
		List<WebElement> workFlowOptions = getElements(By.cssSelector("#detailContainer > label:nth-child(3) > select:nth-child(2) > option"));

		for (int i = 0; i < workFlowOptions.size(); i++) {
			System.err.println(i + "****" +workFlowOptions.get(i).getCssValue("display"));
			if ((workFlowOptions.get(i).getCssValue("display").equals("block")) && i > 0) {
				FirstVisibleWorkFlow = workFlowOptions.get(i).getText().trim();
			}
			System.out.println( Logger.getLineNumber() + workFlowOptions.get(i).getAttribute("style"));
			if (workFlowOptions.get(i).getCssValue("display").equals("none")) {
				HiddenWorkFlow = workFlowOptions.get(i).getText().trim();
			}
		}

		System.out.println( Logger.getLineNumber() + "Hidden Workflow:" + HiddenWorkFlow);
			
		softAssert.assertEquals(HiddenWorkFlow, workflow.getText().trim(), "5) Verify that you can't clone an assignment to the same workflow:: ");

		// Set Workflow
		//SearchPage.setSelectBox("select[name='assignmentWorkflow']", "css-selector", FirstVisibleWorkFlow);
		SearchPage.setSelectBox(By.cssSelector("select[name='assignmentWorkflow']"), FirstVisibleWorkFlow);

		waitFor(5);

		// Set Assignee
		//SearchPage.setSelectBox("select[name='assignmentAssignee']", "css-selector", "iceautotest");
		SearchPage.setSelectBox(By.cssSelector("select[name='assignmentAssignee']"), assigneeName);
		// Press Create button
		click(SearchPage.waitForElement("button[name='btnAdminUpdate']", "css-selector"));

		waitFor(10);

		acceptAlert(driver);
		
		SearchPage.goToDashboard();

		waitForFrameToLoad(Frame.MAIN_FRAME);
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.sortAssignmentBy("Name");
		ReviewAssignmentAccordion.searchForAssignment(assignmentName);
		
		String status = getText(By.cssSelector("span.pending"));
		int pendingDocsNumber = Integer.parseInt(status.replaceAll("\\D+", ""));
		System.out.println( Logger.getLineNumber() + "Total docs: " + pendingDocsNumber);
		
		softAssert.assertNotEquals(pendingDocsNumber, 0, "7) Verify that if any documents needed review in the original assignment that they are PENDING in the cloned assignment:");

		ReviewAssignmentAccordion.openReviewTab();
		// Click Open
		//click(SearchPage.waitForElement("button[onclick='openAssignments()']", "css-selector"));
		ReviewAssignmentAccordion.selectAssignment(assignmentName);
		ReviewAssignmentAccordion.openSelectedAssignment();

		String docCount = SearchPage.waitForElement("totalDocs", "id").getText();
	
		softAssert.assertEquals(docCount, totalDocCount, "6)Verify that the cloned assignment has the same documents as the original (open from search page):: ");

		new DocView().clickActionMenuItem("Manage Review Assignments");
		switchToFrame(1);
		waitFor(5);

		// Go to the Find tab and search for the newly created assignment
		click(SearchPage.waitForElement("a[href='#actionFindAssignments']", "css-selector"));

		waitFor(5);

		SearchPage.waitForElement("findAssignment", "id").sendKeys(assignmentName);

		click(SearchPage.waitForElement("button[onclick='performSearch()']", "css-selector"));

		waitFor(5);

		// Delete assignment
		// Go to the Admin tab
		click(SearchPage.waitForElement("a[href='#actionAdminAssignments']", "css-selector"));
		// Check the box for the assignment
		// click(SearchPage.waitForElement("input[class='chkAssignment']",
		// "css-selector"));
		SearchPage.waitForElement("input[class='chkAssignment']", "css-selector").click();

		// Click the Delete button
		click(SearchPage.waitForElement("button[onclick='deleteAssignment()']", "css-selector"));
		// Switch to alert and accept it
		acceptAlert(driver);
		
		softAssert.assertAll();
	}
}
