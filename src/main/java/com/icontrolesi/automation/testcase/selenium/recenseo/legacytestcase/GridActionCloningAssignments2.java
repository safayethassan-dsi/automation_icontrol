package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class GridActionCloningAssignments2 extends TestHelper{
	private String assignmentName = "GridActionCloningAssignments2_" + new Date().getTime();
	private String assigneeName = "Abdul Awal";
	private String assignmentWorkflow = "Issue Review";
	private String clonedAssignmentWorkflow = "Test";
	private String assignmentPriority = "3";
	
	String cloningAlertMsg = "Sorry, the assignment could not be cloned. Some of the documents are already in an assignment in the target workflow";
	
	@Test
	public void test_c1683_GridActionCloningAssignments2(){
		handleGridActionCloningAssignments2(driver);
	}

	private void handleGridActionCloningAssignments2(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		//SearchPage.addSearchCriteria("Document ID");
		performSearch();		
		
		DocView dc= new DocView();
		dc.clickActionMenuItem("Manage Review Assignments");
		
	    switchToFrame(1);
	    
	    ManageReviewAssignment.createAssignment(assignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	    
	    /*ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	    ManageReviewAssignment.resolveConflict();*/
	    ManageReviewAssignment.addDocumentsToSelectedAssignment();
	    
	    String clonedAssignmentName = assigneeName + new Date().getTime();
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    ReviewAssignmentAccordion.selectAssignment(assignmentName);
	    ManageReviewAssignment.cloneAssignment(clonedAssignmentName, clonedAssignmentWorkflow, assigneeName, assignmentPriority);
	    
	    SearchPage.goToDashboard();
	    
	    waitForFrameToLoad(Frame.MAIN_FRAME);
	    
	    ReviewAssignmentAccordion.open();
	    ReviewAssignmentAccordion.searchForAssignment(clonedAssignmentName);
	    ReviewAssignmentAccordion.selectAssignment(clonedAssignmentName);
	    ReviewAssignmentAccordion.openSelectedAssignment();
	    
	    //some kind of verification goes here
	    
	    dc.clickActionMenuItem("Manage Review Assignments");
		
	    switchToFrame(1);
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    ReviewAssignmentAccordion.selectAssignment(assignmentName);
	    ManageReviewAssignment.cloneAssignment(clonedAssignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    String alertText = getAlertMsg(driver);
	    
	    softAssert.assertEquals(alertText, cloningAlertMsg, "8) Verify that a dialog displays warning the user that the new clone can't be created because documents are already in another assignment:: ");
	    
	    softAssert.assertAll();
	}
}
