package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;


public class GridActionRemoveDocuments extends TestHelper{
public static final int SORTING_TIME = 50;
public static final String SORTING_MESSAGE = "Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";
public static int alertPresent=0;

private String userName = "Abdul Awal";

	@Test
	public void test_c1668_GridActionRemoveDocuments(){
		//handleGridActionRemoveDocuments(driver);
	}

	/*private void handleGridActionRemoveDocuments(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
	
			SearchPage sp= new SearchPage();
			
			SearchPage.setWorkflow("Search");
			
			TagAccordion.open();
			TagAccordion.expandTag("Automated - Recenseo Test");
			TagAccordion.selectTag("13530 - TVP-0044: Grid Action - Manage Review Assignment - - Adding documents to an assignment", true);
			TagAccordion.addSelectedToSearch();
			TagAccordion.openSelected();
			
			DocView dc = new DocView();
				dc.clickActionMenuItem("Workflow state manager");    
			
			    //Switch to frame inside Work-flow manager
			    switchToFrame(1);
			    
			    //Check whether for issue review the remove button is enabled(This means there are docs in issue review)
			    try{
			    	
			    	WebElement radioButtonRemoveDoc= getElement(By.cssSelector("input[id='remove_Issue_review']"));
			    	click(radioButtonRemoveDoc);
			    	//Click Next
			    	click(SearchPage.waitForElement("button[id='confirm']", "css-selector"));
			    	//Click "Go" to confirm removal
			    	click(SearchPage.waitForElement("button[id='changeWorkflowState']", "css-selector"));
			    	waitFor(8);
			    	
			    	driver.switchTo().defaultContent();
			    	waitForFrameToLoad(Frame.MAIN_FRAME);
			    	//Close the pop-up
			    	DocView.clickCrossBtn();
			    }catch(NoSuchElementException e){
			    	driver.switchTo().defaultContent();
			    	SearchPage.waitForFrameLoad("MAIN");
			    }  
			
			 
			SearchPage.goToDashboard();
			
			waitForFrameToLoad(Frame.MAIN_FRAME);
			performSearch();
			
			dc.clickActionMenuItem("Manage Review Assignments");
			
			switchToFrame(1);
			 
			waitFor(20);
			String assignmentName = "DemoAssignment_" + (new Date().getTime());
			ManageReviewAssignment.createAssignment(assignmentName, "Issue Review", userName, "");
			
			ReviewAssignmentAccordion.searchForAssignment(assignmentName);
			
			ReviewAssignmentAccordion.selectAssignment(assignmentName);
			
	          
			ManageReviewAssignment.addDocumentsToSelectedAssignment();
	          
	       
		       		   
		       		Assert.assertEquals(alertPresent, 1, "4)Verify a warning pop-up box appears. Click OK to remove ALL documents from the selected assignment:: ");
		        			
		       		boolean isDocumentsRemoved = getElements(By.cssSelector("span[class='empty']")).size() > 0;
		       		
		       		Assert.assertTrue(isDocumentsRemoved, "(5)Verify documents were removed from the assignment.:: ");
		    }
		     
		      
		     //result.nl().record("====PART2====");
		     
		     //Find the created assignment
		     ManageReviewAssignment manageRev= new ManageReviewAssignment();
		     //Switch to Find tab
		     ManageReviewAssignment.switchTab("a[href='#actionFindAssignments']");
		     
		     //Search for the new assignment
		     manageRev.findAssignment(assignmentName);
		     //Add docs to the assignment
		     manageRev.addDocsToAssignment(driver, sp, "none");
		     
		     //Open the assignment from search page
		     //driver.switchTo().defaultContent(); 
		 	 //getElement(By.xpath("/html/body/header/ul/li[2]/a")).click(); 
		     SearchPage.goToDashboard();
		     
		 	driver.switchTo().defaultContent();
		    SearchPage.waitForFrameLoad(driver, Frame.MAIN_FRAME);
		    
		    //Click Accordion menu for Revew assignment
		    //SearchPage.clickAccordion(driver, "queue");
		    ReviewAssignmentAccordion.open();
		    ReviewAssignmentAccordion.openFindTab();
		    
		    System.out.println( Logger.getLineNumber() + "Assignment Name"+assignmentName);
		    
		    //Search for the new assignment
		    manageRev.findAssignment(driver, assignmentName, sp);
		    
		    //Get the doc count of the assignment
		    String docCount= sp.getDocCountOfAssignment();
		    
		    //Open the assignment
		    List<WebElement> checkBoxes= getElements(By.cssSelector("input[class='chkAssignment']"));
	 	    
		    for(int i=0;i<checkBoxes.size();i++){
		    	if(checkBoxes.get(i).isDisplayed()){
		    		click(checkBoxes.get(i));
		    	}
		    }
		    
		    //click(SearchPage.waitForElement(driver, "input[class='chkAssignment']", "css-selector"));
	 	    sp.tabSwitch(driver, "a[href='#actionReview']", sp);
	 	    ReviewAssignmentAccordion.openReviewTab();
	 	    
	 	    //From Grid view open first 5 docs
	 	   
	 	    	//Wait for checkbox elements
	 	        switchToDefaultFrame();
	 	        waitForFrameToLoad(Frame.MAIN_FRAME);
	 	    	SearchPage.waitForElement(driver, "input[class='cbox']", "css-selector");
	 	    	List<WebElement> gridCheckBoxes= getElements(By.cssSelector("input[class='cbox']"));
	 	    	System.out.println( Logger.getLineNumber() + gridCheckBoxes.size());
	 	    	
	 	    	for(int i=1;i<=5;i++){
	 	    		click(gridCheckBoxes.get(i));
	 	    	}
		    
	 	    	waitFor(driver, 5);
	 	    
	 	    //Open Manage review Assignments from Actions menu
	 	    DocView dv= new DocView();
	 	    dv.clickActionMenuItem(driver, sp, "Manage Review Assignments");
	 	    
	 	    waitFor(driver, 5);
			switchToFrame(1);
			SearchPage.waitForCompletion(driver, sp);
	
		    
			//From the Find tab look for the review assignment
			
			//Find Assignment
		    //manageRev.switchTab(driver, "a[href='#actionFindAssignments']", sp);
			
			ReviewAssignmentAccordion.openFindTab();
		    
		    //Search for the new assignment
		    manageRev.findAssignment(driver, assignmentName, sp);
		    
		    alertPresent=0;
		    
		    manageRev.removeSelectedDocsManageTab(driver, sp);
			
		   Assert.assertEquals(alertPresent, 1, "10)Verify a warning pop-up box appears. Click OK to remove ALL documents from the selected assignment:: "); 
		    
		    //Check whether the number of docs now in the assignment is as expected
		    //Find Assignment
		    //manageRev.switchTab(driver, "a[href='#actionFindAssignments']", sp);
		   ReviewAssignmentAccordion.openFindTab();
		    
		    //Search for the new assignment
		    manageRev.findAssignment(driver, assignmentName, sp);
		    
		    
		    WebElement spanParent= SearchPage.waitForElement(driver, "span[class='open']", "css-selector");
		    List<WebElement>spanChildren= spanParent.findElements(By.cssSelector("span"));
		    String docCountInAssignment= spanChildren.get(1).getText();
		    System.out.println( Logger.getLineNumber() + docCountInAssignment);
		    
		    String docCountInAssignment = sp.getDocCountOfAssignment();
		    
		    
		   int expectedDocCount=  (Integer.parseInt(docCount)-5);
		    
	       if(Integer.parseInt(docCountInAssignment)==expectedDocCount){
		    	//result.nl().record("11)Verify the review set now contains the correct number of documents:pass ");
	 		    Logger.log("11)Verify the review set now contains the correct number of documents:pass");
	   	    }else{
		    	//result.nl().record("11)Verify the review set now contains the correct number of documents:fail ");
	 		    Logger.log("11)Verify the review set now contains the correct number of documents:fail");
	   	        //result.fail();
		    }
	       
	        Assert.assertEquals(docCountInAssignment, expectedDocCount, "11)Verify the review set now contains the correct number of documents:: ");
		   
		    //Check the box beside the assignment
		    ManageReviewAssignment mt= new ManageReviewAssignment();
		    //mt.selectAllAssignments(driver, sp);
		    waitFor(driver, 8);
		    
		    //Delete assignment from Admin tab
		    mt.removeDocumentsAdminTab(driver, sp);
		    
		    waitFor(driver, 4);
		    
			// Return to Search page
			//driver.switchTo().defaultContent();
			//getElement(By.xpath("/html/body/header/ul/li[2]/a")).click();
			SearchPage.goToDashboard();
			
			
			System.out.println( Logger.getLineNumber() + "part3");
		    //result.nl().record("====PART3====");
		    
		   
		    waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		    
		    TagAccordion.open();
			TagAccordion.expandTag(driver, "Automated - Recenseo Test");
			TagAccordion.selectTag(driver, "13530 - TVP-0044: Grid Action - Manage Review Assignment - - Adding documents to an assignment", true);
			TagAccordion.addSelectedToSearch();
			TagAccordion.openSelected();
			
			
			dc.clickActionMenuItem(driver, "Manage Review Assignments");
			
			switchToFrame(1);
			//waitFor(driver, 20);
		  	//Click the New button
			click(SearchPage.waitForElement(driver, "button[onclick*=CREATE]", "css-selector"));
			
			waitFor(driver, 7);
			
			
			//Create new assignment
			
		//Assign name
		/*dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		date = new Date();
		System.out.println( Logger.getLineNumber() + dateFormat.format(date));
	    assignmentName="Ishtiyaq" + dateFormat.format(date);
	    SearchPage.waitForElement(driver, "input[name='assignmentName']", "css-selector").sendKeys(assignmentName);
	    
	    
	    //Assign Workflow
	    SearchPage.setSelectBox(driver, "select[name='assignmentWorkflow']", "css-selector", "Issue Review");
	  
	    waitFor(driver, 5);
		
	    //Set Assignee
	    //SearchPage.setSelectBox(driver, "select[name='assignmentAssignee']", "css-selector", Configuration.getConfig("recenseo.username"));
	    SearchPage.setSelectBox(driver, "select[name='assignmentAssignee']", "css-selector", userName);
	    
	    //Set Priority
	    //SearchPage.setSelectBox(driver, "select[name='assignmentPriority']", "css-selector", "1");
	    //Press Create button
	    click(SearchPage.waitForElement(driver, "button[name='btnAdminUpdate']", "css-selector"));
	    
	 		//Thread.sleep(10000);
		SearchPage.waitForCompletion(driver, sp);
	
	    //
	    //Go to the Find tab and search for the newly created assignment
	    click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	      
	    waitFor(driver, 5);
	   
	    SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	  
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	    
	   	//Thread.sleep(5000);
		SearchPage.waitForCompletion(driver, sp);
	    
	     //Go back to Manage tab
	      click(SearchPage.waitForElement(driver, "a[href='#actionManageAssignments']", "css-selector"));
	
	      //Click the check-box beside the assignment
	      checkboxes= getElements(By.cssSelector("input[class='chkAssignment']"));
	      for(int i=0;i<checkboxes.size();i++){
	    	  if(checkboxes.get(i).isDisplayed()){
	    		  click(checkboxes.get(i));
	    		  break;
	    	  }
	      }
	
	      
	     //Attempt to add all documents to assignments
	     click(SearchPage.waitForElement(driver, "button[onclick='validateAndAddDocument()']", "css-selector"));
	     
	     conflictContainer= SearchPage.waitForElement(driver, "conflictContainer", "id");
	     System.out.println( Logger.getLineNumber() + conflictContainer.getTagName());
	     
	     waitFor(driver, 5);
	     //List<WebElement> pNodes= conflictContainer.findElements(By.cssSelector("p"));
	     children= sp.getAllChildren(driver, conflictContainer);
	     
	     //Get content of "conflict" message
	     List<WebElement> pNodes= conflictContainer.findElements(By.cssSelector("p"));
	     //System.out.println( Logger.getLineNumber() + pNodes.get(1).getText());
	     
	     //Click Continue
	     click(SearchPage.waitForElement(driver, "button[onclick*='resolveConflict']", "css-selector"));
	     
	     waitFor(driver, 10);
	    
	     //Go to the Find tab and search for the newly created assignment
	       click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	       
	       waitFor(driver, 10);
	      
	   	   SearchPage.waitForElement(driver, "findAssignment", "id").clear(); 	
	       SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	     
	       click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	       
	       waitFor(driver, 5);
	   	   
	       //Click the button to remove documents and check if alert present
	       mt= new ManageReviewAssignment();
	       mt.removeSelectedDocsManageTab(driver, sp);
	       
	       //Go to the Find tab and search for the newly created assignment
	       click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	       
	       waitFor(driver, 10);
	   	SearchPage.waitForElement(driver, "findAssignment", "id").clear(); 	
	    SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	     
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	     //Go to the Find tab and search for the newly created assignment
	       //click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	    waitFor(driver, 10);
	      	   //SearchPage.waitForElement(driver, "findAssignment", "id").clear(); 	
	       //SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	       
	       //Check if assigment is empty
	       
	     //Verify the documents were removed
	    
	    	boolean isDocumentsRemoved = getElements(By.cssSelector("span[class='empty']")).size() > 0;
	   		
	   		Assert.assertTrue(isDocumentsRemoved, "(16)Verify documents were removed from the assignment.:: ");
		           
			//result.nl().record("====PART4====");	
			//Create two more assignments 
			mt.switchTab(driver, "a[href='#actionAdminAssignments']", sp);
			//mt.createAssignment(driver, "Part4Assignment"+dateFormat.format(date), "Issue Review", Configuration.getConfig("recenseo.username"), sp);
			mt.createAssignment(driver, "Part4Assignment"+dateFormat.format(date), "Issue Review", userName, sp);
			//mt.createAssignment(driver, "Part4Assignment-2"+dateFormat.format(date), "Issue Review", Configuration.getConfig("recenseo.username"), sp);
			mt.createAssignment(driver, "Part4Assignment-2"+dateFormat.format(date), "Issue Review", userName, sp);
			
			//Switch to the Find tab
			mt.switchTab(driver, "a[href='#actionFindAssignments']", sp);
			mt.findAssignment(driver, "Part4Assignment"+dateFormat.format(date), sp);
			
			//Add documents to the assignment
			mt.addDocsToAssignment(driver, sp, "null");
			
			//Switch to the Find tab
			mt.switchTab(driver, "a[href='#actionFindAssignments']", sp);
			mt.findAssignment(driver, "Part4Assignment-2"+dateFormat.format(date), sp);
					
			//Add documents to the assignment
			mt.addDocsToAssignment(driver, sp, "none");
			
			//Switch to Find tab
			mt.switchTab(driver, "a[href='#actionFindAssignments']", sp);
			mt.findAssignment(driver, "Part4Assignment", sp);
					
			//Delete both the assignments
			
			//Go to "Admin" tab and check the box beside it
			mt.switchTab(driver, "a[href='#actionAdminAssignments']", sp);   
	        
			waitFor(driver, 2);
	        
	        mt.selectAllAssignments(driver, sp);
	        
			//Click "Remove Documents button"
	        click(SearchPage.waitForElement(driver, "button[onclick='removeAllDocuments()']", "css-selector"));
	        
	        String alertText = getAlertMsg(driver);
	    
			
			//Switch to Find tab and check whether the assignments are empty
			mt.switchTab(driver, "a[href='#actionFindAssignments']", sp);
			mt.findAssignment(driver, "Part4Assignment", sp);
			
			//Check whether the assignments are empty
			boolean isAssignmentEmpty = getElements(By.className("empty")).size() > 0;
			
			Assert.assertTrue(isAssignmentEmpty, "Verify assignments are empty:: ");
	}//end function
	
	
	private void makeDocsReadyForAssignment(WebDriver driver, String assignmentName){
		SearchPage sp= new SearchPage();
		WebElement conflictContainer= SearchPage.waitForElement(driver, "conflictContainer", "id");
		List<WebElement> radioButtons= conflictContainer.findElements(By.cssSelector("input"));
		//Click ReAssign all radio button
		radioButtons.get(1).click();
		
		waitFor(driver, 2);
		
		//Click Continue
	    click(SearchPage.waitForElement(driver, "button[onclick*='resolveConflict']", "css-selector"));
	
	    acceptAlert(driver);
	    waitFor(driver, 20);
	    
		//Check for the same assignment again
	  //Go to the Find tab and search for the same assignment again
	    click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	   
	    waitFor(driver, 5);
	    
	    SearchPage.waitForElement(driver, "findAssignment", "id").clear();
	    waitFor(driver, 2);
	    SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	  
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	    
	    waitFor(driver, 5);
	    
	    //Go to "Manage" tab and check the box beside it
	    click(SearchPage.waitForElement(driver, "a[href='#actionManageAssignments']", "css-selector"));
	
	    List<WebElement> checkboxes= getElements(By.cssSelector("input[class='chkAssignment']"));
	    for(int i=0;i<checkboxes.size();i++){
	  	  if(checkboxes.get(i).isDisplayed()){
	  		  click(checkboxes.get(i));
	  		  break;
	  	  }//end if
	  	}//end for
	    
	    //Click the "Remove Selected Documents" button
	    click(SearchPage.waitForElement(driver, "button[onclick='removeDocument()']", "css-selector"));
	    
	   acceptAlert(driver);
	    
	  //Go to the Find tab and search for the newly created assignment
	    click(SearchPage.waitForElement(driver, "a[href='#actionFindAssignments']", "css-selector"));
	     
	    waitFor(driver, 5);
	    
		SearchPage.waitForElement(driver, "findAssignment", "id").clear(); 	
	    SearchPage.waitForElement(driver, "findAssignment", "id").sendKeys(assignmentName);   
	  
	    click(SearchPage.waitForElement(driver, "button[onclick='performSearch()']", "css-selector"));
	    
	    waitFor(driver, 5);
		          	
	     //Go back to Manage tab
	      click(SearchPage.waitForElement(driver, "a[href='#actionManageAssignments']", "css-selector"));
	
	      //Click the check-box beside the assignment
	      checkboxes= getElements(By.cssSelector("input[class='chkAssignment']"));
	      for(int i=0;i<checkboxes.size();i++){
	    	  if(checkboxes.get(i).isDisplayed()){
	    		  click(checkboxes.get(i));
	    		  break;
	    	  }
	    	}
	      
	     //Attempt to add all documents to assignments
	     click(SearchPage.waitForElement(driver, "button[onclick='validateAndAddDocument()']", "css-selector"));
	    
	    
	}*/
}//end class
