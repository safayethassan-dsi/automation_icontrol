package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class GridActionRemovingDocuments extends TestHelper{
public static final int SORTING_TIME = 50;
public static final String SORTING_MESSAGE = "Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";

private String userName = "Abdul Awal";

	@Test
	public void test_c1663_GridActionRemovingDocuments(){
		handleGridActionRemoveDocuments(driver);
	}

	private void handleGridActionRemoveDocuments(WebDriver driver){
		   new SelectRepo(AppConstant.DEM0);
	
			SearchPage sp= new SearchPage();
			
			SearchPage.setWorkflow("Search");
			
			FolderAccordion.open();
			FolderAccordion.openFolderForView("Email");
				
			DocView dc = new DocView();
			dc.clickActionMenuItem("Manage Review Assignments");
			
			switchToFrame(1);
			   
			String assignmentName = "DemoAssignment_" + (new Date().getTime());
			ManageReviewAssignment.createAssignment(assignmentName, "Issue Review", userName, "");
			
			ReviewAssignmentAccordion.searchForAssignment(assignmentName);
			
			ReviewAssignmentAccordion.selectAssignment(assignmentName);
			
			ManageReviewAssignment.gotoAdminPanel();
			
//			new ManageReviewAssignment().deleteAssignment();
			
			ReviewAssignmentAccordion.searchForAssignment(assignmentName);
			
			Assert.assertEquals(getText(By.id("noAssignments")), "No assignments found!", "Assignment removed");
	}
}