package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * 
 * @author Ishtiyaq Kamal
 * 
 * DB: DEM0

1) From the Search page, under Folders, select the folder 'Email/Foster, Ryan e-mail' and Open it
2) Verify the documents populate correctly in Grid View
3) Confirm the icon representing file type (Paper, Email, etc�) is accurate. Be sure to check icons for Paper, Email, Word, PDF, Excel, etc. It should now work for pptx, docx, and xlsx as well.
4) NEW FOR 2013.5: Verify that Recenseo sizes the first column so that all icons and attachment counts are visible, with a reasonable number of columns displayed (not too few or too many).
 *
 */

public class GridFileTypeIcon extends TestHelper{
   @Test
   public void test_c146_GridFileTypeIcon(){
	   //loginToRecenseo();
	   handleGridFileTypeIcon(driver);
   }
   
    protected void handleGridFileTypeIcon(WebDriver driver) {
    	new SelectRepo(AppConstant.DEM0);
  		
  		SearchPage.setWorkflow("Search");

  		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		List<WebElement> mimeTypeList = getElements(By.id("mimeTypeImage"));
		
		for(WebElement mimeType : mimeTypeList){
			String fileType = mimeType.getAttribute("class");
			String fileIconSet = getCssValue(mimeType, "background-image").replaceAll("url\\(|\\)|\"", "").split("/")[6];
			if(fileType.equals("DefaultImage")){
				softAssert.assertEquals(fileIconSet, "text.jpg", "Icon representing 'Text' is accurate:: ");
			}else if(fileType.equals("XlsImage")){
				softAssert.assertEquals(fileIconSet, "xls_16.png", "Icon representing 'Excel' is accurate:: ");
			}else if(fileType.equals("DocImage")){
				softAssert.assertEquals(fileIconSet, "doc_16.png", "Icon representing 'Doc' is accurate:: ");
			}else if(fileType.equals("PptImage")){
				softAssert.assertEquals(fileIconSet, "ppt_16.png", "Icon representing 'Power Point Document' is accurate:: ");
			}else if(fileType.equals("PdfImage")){
				softAssert.assertEquals(fileIconSet, "pdf.jpg", "Icon representing 'PDF' is accurate:: ");
			}else if(fileType.equals("HtmlImage")){
				softAssert.assertEquals(fileIconSet, "html.jpg", "Icon representing 'HTML' is accurate:: ");
			}else{
				softAssert.assertEquals(fileIconSet, "Unknown", "Unknow type is not cheked yet...");
			}
		}
		 
		softAssert.assertAll();
    }    
}
