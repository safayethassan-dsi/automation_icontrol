package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class GridHeaderInfo extends TestHelper{
    @Test
    public void test_c173_GridHeaderInfo(){
    	//loginToRecenseo();
    	handleGridHeaderInfo(driver);
    }
 
    protected void handleGridHeaderInfo(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage sp= new SearchPage();
    	
    	SearchPage.setWorkflow("Search");
    	
    	performSearch();
    	
    	DocView.setNumberOfDocumentPerPage(50);
    	
    	String[] gridString= sp.getGridCountString(driver);
    	System.out.println( Logger.getLineNumber() + "grid 5 : "+gridString[5]);
    	
    	String DocCountInGrid= gridString[5].replaceAll(",", "");
    	String LastRowNumber= gridString[3].replaceAll(",", "");
    	
    	Select comboBox = new Select(getElement(By.cssSelector("select[class='ui-pg-selbox']")));
		String selectedComboValue = comboBox.getFirstSelectedOption().getText().replaceAll(",", "");
    	
    	String DocCountPerPage=  selectedComboValue;
    	System.out.println( Logger.getLineNumber() + "DocCountPerPage: " + DocCountPerPage);
    	System.out.println( Logger.getLineNumber() + "LastRowNumber: " + LastRowNumber);
    	//System.out.println( Logger.getLineNumber() + "doc_count_search: " + doc_count_search);
    	System.out.println( Logger.getLineNumber() + "DocCountInGrid: " + DocCountInGrid);
    	
    	String docCountCodingTemplate= getElement(By.id("totalDocs")).getText().replaceAll(",", "");
    	
    	softAssert.assertTrue(DocCountPerPage.equalsIgnoreCase(LastRowNumber) && docCountCodingTemplate.equalsIgnoreCase(DocCountInGrid), "(3)Verify the [View X - Y of Z] string at top-right matches expectations:: ");
    	
    	String MaxPageNumber = getText(By.id("sp_1_grid_toppager")).replaceAll(",", "");
    	
    	System.out.println( Logger.getLineNumber() + "Max page number:"+MaxPageNumber);
    	
    	 //Go to last page
        //click(getElement(By.cssSelector("span[class='ui-icon ui-icon-seek-end']")));
    	DocView.gotoLastPage();

      //Count Number of docs on last page
      List<WebElement> rowNums= getElements(By.cssSelector("td[class='ui-state-default jqgrid-rownum']"));
     
      int totalDocOnlastPage= rowNums.size();
      System.out.println( Logger.getLineNumber() + totalDocOnlastPage);
      
      int TotalDocs = (Integer.parseInt(MaxPageNumber)-1)*50 + totalDocOnlastPage;
      System.out.println( Logger.getLineNumber() + TotalDocs);  
      
      
      softAssert.assertTrue(TotalDocs>=Integer.parseInt(DocCountInGrid), "(4)Verify the Page Navigation indicates the appropriate number of pages:: ");
      
      DocView.openQueryInfo();
      
      //Get total hits returned
      WebElement QueryInfoDiv= getElement(By.id("queryInfoDialog"));
      String HitCountString= QueryInfoDiv.findElement(By.cssSelector("p")).getText();
      System.out.println( Logger.getLineNumber() + HitCountString);
      
      String HitCountStringArray[]= HitCountString.split("\\s+");
      System.out.println( Logger.getLineNumber() + HitCountStringArray[2]);
       
      //Get total count in current workflow
      String CurrentHitCountString= getElement(By.id("qiTotalWfState")).getText();
      System.out.println( Logger.getLineNumber() + CurrentHitCountString);
      
      softAssert.assertTrue((HitCountStringArray[2].matches("[0-9]+,*[0-9]+") && CurrentHitCountString.matches("[0-9]+,*[0-9]+")), "(5)The pop-up window should include information about hits returned, number of hits in current workflow state, and the search criteria used:: ");
      //Go back to Search page
      //driver.switchTo().defaultContent(); 
      //getElement(By.xpath("/html/body/header/ul/li[2]/a")).click();
      SearchPage.goToDashboard();
      
      //Perform a search with the search criteria: "Author is not empty"
      
      waitForFrameToLoad(Frame.MAIN_FRAME);
    
      //Select Search Criteria
      SearchPage.addSearchCriteria("Author");
      
      waitForVisibilityOf(By.className("CriterionOperator"));
      //Set operator "is not empty"
      new Select(getElement(By.className("CriterionOperator"))).selectByVisibleText("is not empty");
      
      
      //Run Search
  	  performSearch();
  	  
  	WebElement countCodingTemplate= getElement(By.id("totalDocs"));
  	String docCount= countCodingTemplate.getText();
  	  
  	System.out.println( Logger.getLineNumber() + docCount);  
  	
  	//Click the Query info button
    DocView.openQueryInfo();
  	
    QueryInfoDiv= getElement(By.id("queryInfoDialog"));
    HitCountString= QueryInfoDiv.findElement(By.cssSelector("p")).getText();
    System.out.println( Logger.getLineNumber() + HitCountString);
    
    HitCountStringArray= HitCountString.split("\\s+");
    System.out.println( Logger.getLineNumber() + HitCountStringArray[2]);
    
    //Get total count in current workflow
    CurrentHitCountString= getElement(By.id("qiTotalWfState")).getText();
    System.out.println( Logger.getLineNumber() + CurrentHitCountString);
    
    //Check workflow description in Query info
    QueryInfoDiv= getElement(By.id("queryInfoDialog"));
    List<WebElement> pNodesChildren= QueryInfoDiv.findElements(By.cssSelector("p"));
    //The third p node has the search criteria info
    String searchCriteriaInfo= pNodesChildren.get(2).getText();
    
    System.out.println( Logger.getLineNumber() + searchCriteriaInfo);
    
    softAssert.assertTrue((HitCountStringArray[2].equalsIgnoreCase(docCount) && searchCriteriaInfo.contains("Criteria: Author NOT_EMPTY ( )")), "(6)Return to the Search screen and perform other searches using different criteria. Verify the Query Info window always reflects the search performed:: ");
    
    DocView.closeQueryInfo();
      
      //switchToDefaultFrame();
 	  SearchPage.goToDashboard(); 
 	  
 	  SearchPage.setWorkflow("Privilege Review");
 	  
 	  performSearch();
 	  
 	  //Open workflow manager 
 	  DocView dc= new DocView();
 	  dc.clickActionMenuItem("Workflow state manager");
 	  
 	  switchToFrame(1);
	  
	  //Get count for privilege review
	  WebElement sumContainer= SearchPage.waitForElement("div[id='summaryContainer']", "css-selector");
	  WebElement table= sumContainer.findElement(By.cssSelector("table"));
	  WebElement tbody= table.findElement(By.cssSelector("tbody"));
	  WebElement tr= tbody.findElement(By.cssSelector("tr"));
	  List<WebElement> tdNodes= tr.findElements(By.cssSelector("td"));
	  System.out.println( Logger.getLineNumber() + tdNodes.get(1).getText());
	  String countWorkFlowManager=tdNodes.get(1).getText().trim();
	 
	  SearchPage.goToDashboard();  
	  
	  waitForFrameToLoad(Frame.MAIN_FRAME);
	  performSearch();
 
	 //Get z index of native doc
	 WebElement docPanel= SearchPage.waitForElement("doc_panel", "id");
	 String panelZindex= docPanel.getAttribute("z-index");
	  
      
    //Get total count in current workflow from Query Info
      DocView.openQueryInfo();
    	
      CurrentHitCountString= getElement(By.id("qiTotalWfState")).getText();
      System.out.println( Logger.getLineNumber() + CurrentHitCountString);
      
      while(getElement(By.id("qiTotalWfState")).getText().contains("Loading doc count...")){
    	  //do nothing here
      }
      
      
      CurrentHitCountString= getElement(By.id("qiTotalWfState")).getText();
      System.out.println( Logger.getLineNumber() + "QueryInfo count: "+CurrentHitCountString);
      System.out.println( Logger.getLineNumber() + "WorkFlowMan count: "+countWorkFlowManager);
      
      softAssert.assertTrue((CurrentHitCountString.trim()).equalsIgnoreCase(countWorkFlowManager.trim()), "7) Change the Workflow State to [Privilege Review] (or something other than Search) and use the [Query Info] button. Verify the number of documents in the Workflow state is correct:: ");
      
      //Check overlay
      WebElement queryInfoDialog= SearchPage.waitForElement("queryInfoDialog", "id");
      WebElement parent= SearchPage.getParent(queryInfoDialog);
      System.out.println( Logger.getLineNumber() + "Area described by:"+parent.getAttribute("aria-describedby"));
      //System.out.println( Logger.getLineNumber() + "z Index value:"+zIndexValue);
      System.out.println( Logger.getLineNumber() + "z index css attribute:"+parent.getCssValue("z-index"));
        
      System.out.println( Logger.getLineNumber() + "z index of doc: "+panelZindex);
      
      softAssert.assertTrue((parent.getCssValue("z-index")!=null), "8) Click the [Query Info] or [Show/Hide Columns] buttons. Verify the pop-up window does not hide behind or overlay the native document displayed.:: ");
    }
}
