package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/*
 * TVP-0012: Grid - Hits per page
 * "DB: DEM0 
 1) From the Search screen, click Run Search (no parameters are needed) 
 2) Use the drop down menu at the top-middle of the grid view to change the number of documents displayed on each page of the grid 
 3) Verify the number of total pages changes after changing the number of docs per page 
 4) Navigate through several pages to: 
 4a) confirm the row count is as expected 
 4b) the grid loads quickly 
 5) Log out or Recenseo and back in  
 6) Confirm that the changed number of documents displayed on each page is saved"
 */

public class GridHitsPerPage extends TestHelper{
	private static int THRESHOLD = 15;
	private static int SAVED_DOC_PER_PAGE;

	@Test
	public void test_c164_GridHitsPerPage(){
		handleGridHitsPerPage(driver);
	}

	private void handleGridHitsPerPage(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		performSearch();
		
		List<String> options = new ArrayList<String>();

		WebElement docPerPage = SearchPage.waitForElement(driver, "ui-pg-selbox", "class-name");
		
		WebElement totalPage = SearchPage.waitForElement(driver, "span[id='sp_1_grid_toppager']", "css-selector");
		String oldPageCount = totalPage.getText();

		List<WebElement> selectOptions = new Select(docPerPage).getOptions();
		for (WebElement option : selectOptions) {
			options.add(option.getText());
		}
		String selectedOption = new Select(docPerPage).getFirstSelectedOption().getText();
		options.remove(selectedOption);
		Random rand = new Random();
		int selectIndex = rand.nextInt(options.size());

		String selectedDocPerPage = options.get(selectIndex);
		SAVED_DOC_PER_PAGE = Integer.valueOf(selectedDocPerPage);

		//SearchPage.setSelectBox(driver, By.className("ui-pg-selbox"), selectedDocPerPage);
		DocView.setNumberOfDocumentPerPage(SAVED_DOC_PER_PAGE);
		String newPageCount = totalPage.getText();

		Assert.assertNotEquals(newPageCount, oldPageCount, "3. Verify the number of total pages changes after changing the number of docs per page:: ");

		WebElement nextButton = SearchPage.waitForElement(driver, "span[class='ui-icon ui-icon-seek-next']", "css-selector");
		String selector = "td[class='ui-state-default jqgrid-rownum'][aria-describedby='grid_rn']";

		int repeat = 3;

		while (repeat > 0) {
			click(nextButton);
			int time = checkLoadingTime(driver);
			System.out.println( Logger.getLineNumber() + "Time taken to load: " + time);
			switchToDefaultFrame();
			waitForFrameToLoad(driver, Frame.MAIN_FRAME);
			List<WebElement> column = getElements(By.cssSelector(selector));
			int rowCount = column.size();
			
			Assert.assertEquals(rowCount, SAVED_DOC_PER_PAGE, "4a.) Confirm the row count is as expected :: ");
			Assert.assertTrue(time <= THRESHOLD, "4b.) Page loads quickly ( within " + THRESHOLD + "seconds ):: ");
			repeat--;
		}

		SearchPage sp= new SearchPage();
		sp.logout();
		
		performLoginToRecenseo();

		new SelectRepo(AppConstant.DEM0);
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		performSearch();
		
		docPerPage = SearchPage.waitForElement(driver, "ui-pg-selbox","class-name");
		
		selectOptions = new Select(docPerPage).getOptions();
		selectedOption = new Select(docPerPage).getFirstSelectedOption().getText();
		int newDocPerPage = Integer.valueOf(selectedOption);

		Assert.assertEquals(newDocPerPage, SAVED_DOC_PER_PAGE, "6. Confirm that the changed number of documents displayed on each page is saved:: ");
	}

	private int checkLoadingTime(WebDriver driver) {
		long time = System.nanoTime();
		long endTime = 0;
		WebElement loadGrid = SearchPage.waitForElement(driver, "load_grid", "id");
		while (loadGrid.isDisplayed()) {
			endTime = System.nanoTime();
		}
		return (int) ((long) (endTime - time) / 1000000000);

	}
}
