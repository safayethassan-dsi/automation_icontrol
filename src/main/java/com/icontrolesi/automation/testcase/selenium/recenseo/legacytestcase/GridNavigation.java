package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * 
 * @author Ishtiyaq Kamal
 *
 */

public class GridNavigation extends TestHelper{
	@Test
	public void test_c168_GridNavigation(){
		handleGridNavigation(driver);
	}

    @SuppressWarnings("resource")
	protected void handleGridNavigation(WebDriver driver) {
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    
        performSearch();
    	
    	int doc_count = DocView.getDocmentCount();
    	
    	softAssert.assertTrue((doc_count > 100000) , "(1)From the Search screen, click the Run Search button (no parameters are needed). Search results should be more than 100,000 documents:: ");
    
    	int pageNumberBeforeNavigation = DocView.getCurrentPageNumber();
    	
    	DocView.gotoNextPage();
    
    	int currentPageNumber = DocView.getCurrentPageNumber();
    	
    	softAssert.assertEquals(currentPageNumber, pageNumberBeforeNavigation + 1, "Next button working correctly:: ");
    	
    	DocView.gotoPreviousPage();
    	
    	currentPageNumber = DocView.getCurrentPageNumber();
    	
    	softAssert.assertEquals(currentPageNumber, pageNumberBeforeNavigation, "Previous button working correctly:: ");
    	
    	DocView.gotoLastPage();
    	
    	currentPageNumber = DocView.getCurrentPageNumber();
    	
    	int lastPageNumber = Integer.parseInt(getText(By.id("sp_1_grid_toppager")).replaceAll("\\D+", ""));
    	
    	softAssert.assertEquals(currentPageNumber, lastPageNumber, "Last Page button working correctly::");
    	
    	DocView.gotoFirstPage();
    	
    	currentPageNumber = DocView.getCurrentPageNumber();
    	
    	softAssert.assertEquals(currentPageNumber, 1, "First Page button working correctly::");
    	
    	int randomPageNumberToJumpOn = getRandomNumberInRange(5, lastPageNumber);
    	
    	DocView.gotoPageNumber(randomPageNumberToJumpOn);
    	
    	int numberOfDocumentsPerPage = DocView.getNumberOfDocumentsPerPage();
    	
    	int startingPageCount = Integer.parseInt(new Scanner(getText(By.cssSelector("#grid_toppager_right > div"))).useDelimiter("[A-Za-z-]+").next().replaceAll("\\D+", ""));
    	
    	int expectedStartingHit = ((randomPageNumberToJumpOn - 1) * numberOfDocumentsPerPage) + 1;
    	
    	softAssert.assertEquals(startingPageCount, expectedStartingHit, "3) Specified page is loaded using page number box ( Page number entered: " + randomPageNumberToJumpOn + "):: ");
    	
    	randomPageNumberToJumpOn = getRandomNumberInRange(5, lastPageNumber);
    	
    	DocView.gotoPageNumber(randomPageNumberToJumpOn);
    	
    	startingPageCount = Integer.parseInt(new Scanner(getText(By.cssSelector("#grid_toppager_right > div"))).useDelimiter("[A-Za-z-]+").next().replaceAll("\\D+", ""));
    	
    	expectedStartingHit = ((randomPageNumberToJumpOn - 1) * numberOfDocumentsPerPage) + 1;
    	
    	softAssert.assertEquals(startingPageCount, expectedStartingHit, "3) Specified page is loaded using page number box ( Page number entered: " + randomPageNumberToJumpOn + "):: ");
   
    	softAssert.assertAll();
    }  
}
