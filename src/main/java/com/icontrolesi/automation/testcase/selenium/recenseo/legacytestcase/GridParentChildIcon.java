package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
 
	1) On the Search page open the ""Email/Foster, Ryan e-mail"" folder 
	2) Review the PARENT and ATTACHMENT icons for consistency indicating that parent documents are immediately followed in Grid by the right attachments 
	3) Select a sample set of parents and verify in doc-by-doc that the proper attachments follow immediately after"
*/

public class GridParentChildIcon extends TestHelper{
    @Test
    public void test_c105_GridParentChildIcon(){
    	//loginToRecenseo();
    	handleGridParentChildIcon(driver);
    }

    private void handleGridParentChildIcon(WebDriver driver){
    	Driver.setWebDriver(driver);
    	
        new SelectRepo(AppConstant.DEM0);
        
        DocView.openPreferenceWindow();
        DocView.checkAllColumns();
        DocView.clickCrossBtn();
        
        SearchPage.setWorkflow("Search");
        
        FolderAccordion.open();
        FolderAccordion.expandFolder(driver, "Email");
        //FolderAccordion.selectFolder(driver, "Foster, Ryan e-mail");
        FolderAccordion.openFolderForView("Foster, Ryan e-mail");
        //FolderAccordion.openFolderForView("Foster, Ryan e-mail");
        
        List<WebElement> allEls = getElements(By.cssSelector("td[aria-describedby='grid_mimeType']"));
        int aCount = 0;
        for(int i=1;i<3;i++){
        	List<WebElement> els = allEls.get(i).findElements(By.tagName("span"));
        	if(els.size()>1){
        		if(els.get(1).getText().equalsIgnoreCase("A")){
        			aCount++;
        		}
        	}
        }
        
        //Utils.handleResult("3) Select a sample set of parents and verify in doc-by-doc that the proper attachments follow immediately after", result, (aCount==2));
        Assert.assertTrue((aCount == 2), "3) Select a sample set of parents and verify in doc-by-doc that the proper attachments follow immediately after:: ");
     }
}