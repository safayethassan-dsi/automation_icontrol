package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
 
	1) On the Search screen, open the Review Assignments section 
	2) Click on the Filter tab 
	3) In the Status box, select/deselect a check box 
	    All - shows all assignments (empty, complete, open, etc.) 
	    Completed - Only assignments that are completed (No pending, no unreviewed). 
	    Pending - Only assignments that have documents in pending status 
	    Empty - Only empty assignments 
	    Open - Assignments that are not empty, or complete, i.e. with documents that are either pending or need review 
	    Pending - filter currently broken. 
	4) When options are selected/unselected, the list of assignments below it should immediately be updated to reflect those options. Verify that correct assignments are shown. 
	"
*/

public class GridStatusfilters extends TestHelper{
    @Test
    public void test_c1658_GridStatusfilters(){
    	//loginToRecenseo();
    	handleGridStatusfilters(driver);
    }

    private void handleGridStatusfilters(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		switchToDefaultFrame();
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		ReviewAssignmentAccordion.open();
		
		ReviewAssignmentAccordion.selectFilterByUsers("Mine");
		ReviewAssignmentAccordion.selectFilterByUsers("Team");
		ReviewAssignmentAccordion.selectFilterByUsers("Unassigned");
		
		ReviewAssignmentAccordion.selectFilterByStatus("All");
		
		boolean isStatusCompletedFound = isFilterWorkingOk("Completed");
		boolean isStatusPendingFound = isFilterWorkingOk("Pending");
		boolean isStatusEmptyFound = isFilterWorkingOk("Empty");
		boolean isStatusOpenFound = isFilterWorkingOk("Pending");
		
		Assert.assertTrue(isStatusCompletedFound || isStatusPendingFound || isStatusEmptyFound || isStatusOpenFound, "Status Filter seletion works for- All:: ");
		
		ReviewAssignmentAccordion.deSelectFilterByStatus("All");
		
		ReviewAssignmentAccordion.selectFilterByStatus("Completed");
		
		Assert.assertTrue(isFilterWorkingOk("Completed"), "Status Filter seletion works for- Completed:: ");
		
		ReviewAssignmentAccordion.deSelectFilterByStatus("Completed");
		ReviewAssignmentAccordion.selectFilterByStatus("Pending");
		
		Assert.assertTrue(isFilterWorkingOk("Pending"), "Status Filter seletion works for- Pending:: ");
		
		ReviewAssignmentAccordion.deSelectFilterByStatus("Pending");
		ReviewAssignmentAccordion.selectFilterByStatus("Empty");
		
		Assert.assertTrue(isFilterWorkingOk("Empty"), "Status Filter seletion works for- Empty:: ");
		
		
		ReviewAssignmentAccordion.deSelectFilterByStatus("Empty");
		ReviewAssignmentAccordion.selectFilterByStatus("Open");
		
		Assert.assertTrue(isFilterWorkingOk("Pending"), "Status Filter seletion works for- Open:: ");		
	}
	
	
	public boolean isFilterWorkingOk(String filter){
		List<WebElement> assigneeList = getElements(By.cssSelector("#assignmentList > li[style$='block;'] > div > div.header.ui-helper-clearfix > div > span." + filter.toLowerCase()));
		for(WebElement assignee : assigneeList){
			System.out.println( Logger.getLineNumber() + "*******"+assignee.getText().replaceAll("\\d+", ""));
			if(assignee.getText().replaceAll("\\d+", "").trim().equalsIgnoreCase(filter) == false)
				return false;
		}
		
		return true;
	}
}
