package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;
import java.util.Set;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
	1) From the Search screen, click Run Search (no parameters are needed) 
	2) In Grid View, if not already the case, add the �Tag List� column to the display (If �Tag List� Column needs to be added to the columns click on the Show/Hide columns button (wrench) on the top left side.) 
	3) Find a document that has a tag listed,  
	4) Verify that the tag window in the coding template OR manage tags also indicates the document is in each of the tags from the ""Tags"" column. 
	5) Using the coding template OR manage tags, ADD the document to a Tag, Remove the document from a Tag, Hit Save Changes, and verify those changes are reflected in the Tags column. 
	6) Return to Search Page, select TAGS section and open the TAGS that were associated with that document to confirm that the document is actually in those TAGS or removed from those TAGS. 
	7) Confirm the �Tag List� column only shows Tags the user has permission to see."
*/

public class GridTagIcon extends TestHelper{
	String tagName = "Dbd61TagsWidget_" + new Date().getTime();
	String firstTag = tagName + "_1";
	
	String firstTagPermission = "Internal";
	
    @Test
    public void test_c178_GridTagIcon(){
    	//loginToRecenseo();
    	handleGridTagIcon(driver);
    }

    private void handleGridTagIcon(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
        
        DocView.openPreferenceWindow();
        DocView.checkSelectedColumns("Tags", "Document ID");
        DocView.closePreferenceWindow();
        
        SearchPage.setWorkflow("Search");
        
      /*  SearchPage.addSearchCriteria("Author");
        SearchPage.setCriterionValue(0, "jeff");*/
        
        performSearch();
        
        DocView.selectDocumentBySpecificNumber(1);
        tryClick(By.cssSelector("td[aria-describedby='grid_cb']"));
        
        Set<String> tagsForSelectedDocument = DocView.getTagsNameSplittedAsSet(1);
        
        Stream.of(tagsForSelectedDocument).sorted().forEach(t -> System.out.print(t + " "));
        
        String tagToOpenForView = (String) tagsForSelectedDocument.toArray()[0];
        
        int selectedDocumentId = DocView.getDocmentId();
        
        System.out.println( Logger.getLineNumber() + "Document ID: " +  selectedDocumentId);
        
        ReviewTemplatePane.selectTagWidgetFilter("Applied Tags");
        
        Set<String> appliedTags = ReviewTemplatePane.getAppliedTagsAsSet();
        
        System.out.println( Logger.getLineNumber() + tagsForSelectedDocument + "\n" + appliedTags);
        
        softAssert.assertEquals(tagsForSelectedDocument, appliedTags, "4) Verify that the tag window in the coding template OR manage tags also indicates the document is in each of the tags from the Tags column.:: ");
     
        TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(firstTag, firstTagPermission);
		TagManagerInGridView.addDocumentsForTag(firstTag);
		TagManagerInGridView.close();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		tagsForSelectedDocument = DocView.getTagsNameSplittedAsSet(1);
		
		softAssert.assertTrue(tagsForSelectedDocument.contains(firstTag), "5) Adding document to a tag reflected in Tags column in GridView:: ");
		
		TagManagerInGridView.open();
		TagManagerInGridView.removeDocumentsForTag(firstTag);
		TagManagerInGridView.close();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		tagsForSelectedDocument = DocView.getTagsNameSplittedAsSet(1);
		
		softAssert.assertFalse(tagsForSelectedDocument.contains(firstTag), "5) Removinmg document to a tag reflected in Tags column in GridView:: ");
        
		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		TagAccordion.open();
		TagAccordion.openTagForView(tagToOpenForView);
		
		boolean isDocumentFoundForTag = DocView.isDocumentIDFound(selectedDocumentId);
		
		softAssert.assertTrue(isDocumentFoundForTag, "6) Return to Search Page, select TAGS section and open the TAGS that were associated with that document to confirm that the document is actually in those TAGS or removed from those TAGS:: ");
		
		tagsForSelectedDocument = DocView.getTagsNameSplittedAsSet(1);		
		
		ReviewTemplatePane.selectTagWidgetFilter("Applied Tags");
		
		appliedTags = ReviewTemplatePane.getAppliedTagsAsSet();
		
		System.out.println( Logger.getLineNumber() + tagsForSelectedDocument + "\n" +appliedTags);
		
		softAssert.assertEquals(tagsForSelectedDocument, appliedTags, "7) Confirm the �Tag List� column only shows Tags the user has permission to see:: ");
		
		softAssert.assertAll();
    }
}