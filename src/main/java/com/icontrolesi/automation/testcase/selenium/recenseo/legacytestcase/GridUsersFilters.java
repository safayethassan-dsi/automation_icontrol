package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/*
 * DB: DEM0

1) On the Search screen, open the Review Assignments section
2) Click on the Filter tab
3) In the Users box, Select/deselect a check box
    Mine - Should only show assignments assigned to you
    Team - Show assignments for everyone you can assign to
    Unassigned - Assignments not assigned to anyone (should have a checkout button).
4) When options are selected/unselected, the list of assignments below it should immediately be updated to reflect those options. Verify that correct assignments are shown.
 * 
 */



public class GridUsersFilters extends TestHelper{
	public static final int SORTING_TIME = 50;
	public static final String SORTING_MESSAGE = ":";
	public static final String userName = "iceautotest";

	@Test
	public void test_c1657_GridUsersFilters(){
		handleGridUsersFilters(driver);
	}

	private void handleGridUsersFilters(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		switchToDefaultFrame();
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		ReviewAssignmentAccordion.open();
		
		ReviewAssignmentAccordion.selectFilterByStatus("All");
		
		ReviewAssignmentAccordion.selectOnlyOneUserFilter("Mine");
		
		Assert.assertTrue(isFilterWorkingOk(Configuration.getConfig("recenseo.username")), "Filter seletion works for- Mine:: ");
		
		ReviewAssignmentAccordion.selectOnlyOneUserFilter("Team");
		
		Assert.assertFalse(isFilterWorkingOk(Configuration.getConfig("recenseo.username")), "Filter seletion works for- Team:: ");
		Assert.assertFalse(isFilterWorkingOk("Unassigned"), "Filter seletion works for- Team:: ");
		
		ReviewAssignmentAccordion.selectOnlyOneUserFilter("Unassigned");
		
		Assert.assertTrue(isFilterWorkingOk("Unassigned"), "Filter seletion works for- Unassigned:: ");
	}
	
	
	public boolean isFilterWorkingOk(String filter){
		List<WebElement> assigneeList = getElements(By.cssSelector("#assignmentList > li[style$='block;'] > div > div.detail.ui-helper-clearfix > div > span"));
		
		for(WebElement assignee : assigneeList){
			if(assignee.getText().equals(filter) == false)
				return false;
		}
		
		return true;
	}
}
