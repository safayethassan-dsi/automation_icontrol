package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


/*
 *
	  "DB: DEM0 
	 
	Test in both IE and Firefox, although Firefox may be the only one that allows you to properly run the test. 
	 
	1) Run a search where known results are more than 500 docs. 
	2) Before the page is fully loaded, click the check box to select all documents. 
	3) When the page fully loads, ensure all documents have been selected. To confirm, go to Actions\Global Edit, before selecting a field to edit the tool should say ""You are working with 500 documents."" 
	4) Cancel Global Edit and return to Grid View 
	5) Select one document (click on the data in any column, NOT the checkbox) and confirm that all fields expand to show all data and that document template shows the document selected. The document's line in Gridview should turn green. 
	6) With the other document still selected, click the check box of another document and confirm fields do not expand and template does not change to that document."

 */

public class GridViewDocSelection extends TestHelper{
@Test
public void test_c162_GridViewDocSelection(){
	//loginToRecenseo();
	handleGridViewDocSelection(driver);
}

private void handleGridViewDocSelection(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
		DocView.setNumberOfDocumentPerPage(500);
		DocView.selectAllDocuments();
		
		DocView.openGlobalEditWindow();
		
		String globalEditConfirmationMsg = getText(By.cssSelector("#editorContainer > p"));
		
		softAssert.assertEquals(globalEditConfirmationMsg, "You are working with 500 documents. Please select an attribute from the list to edit:", "3) To confirm, go to ActionsGlobal Edit, before selecting a field to edit the tool should say [You are working with 500 documents:: ");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		getElements(By.xpath("//span[text()='Close']")).get(1).click();
		waitFor(5);
		
		int documentRandomSelectionNumber = getRandomNumberInRange(1, 500);
		
		System.out.println( Logger.getLineNumber() + "*******Test: " + getCssValue(By.cssSelector("#grid > tbody > tr:nth-child(" + (documentRandomSelectionNumber + 1) + ") > td:nth-child(3)"), "white-space"));
		
		tryClick(DocView.selectAllDocumentLocator);
		DocView.clickOnDocument(documentRandomSelectionNumber);
		
		By selectedItemsLocator = By.cssSelector("#grid > tbody > tr:nth-child(" + (documentRandomSelectionNumber + 1) + ")> td:nth-child(3)");
		
		System.out.println( Logger.getLineNumber() + getCssValue(selectedItemsLocator, "white-space")+"**************");
		System.out.println( Logger.getLineNumber() + "Test: " + getCssValue(By.cssSelector("#grid > tbody > tr:nth-child(" + (documentRandomSelectionNumber + 1) + ") > td:nth-child(3)"), "white-space"));
		
		String bgColorForSelectedDocument = getColorFromElement(By.cssSelector("#grid > tbody > tr:nth-child(" + (documentRandomSelectionNumber + 1) + ") > td:nth-child(3)"), "background-color");
		
		int documentIDForSpecificNumber = Integer.parseInt(getElements(DocView.documentIDColumnLocator).get(documentRandomSelectionNumber - 1).getText().trim());
		int currentDocumentID = DocView.getDocmentId();
		
		softAssert.assertEquals(documentIDForSpecificNumber, currentDocumentID, "5) Document template shows the document selected:: ");
		softAssert.assertEquals(bgColorForSelectedDocument, "#A4F7CB", "5) The document's line in Gridview should turn green:: ");
		softAssert.assertEquals(getCssValue(selectedItemsLocator, "white-space"), "pre-wrap", "5) The document's line in Gridview all fields are expanded to show all data:: ");
		
		System.out.println( Logger.getLineNumber() + "Selcted document's color: " + bgColorForSelectedDocument);
		
		documentRandomSelectionNumber = getRandomNumberInRange(1, 500);
		
		DocView.checkDocumentBySpecificeNumber(documentRandomSelectionNumber);
		
		int checkedDocumentId = Integer.parseInt(getElements(DocView.documentIDColumnLocator).get(documentRandomSelectionNumber - 1).getText().trim());
		int currentlySelectedDocumentNumber = DocView.getDocmentId();
		
		softAssert.assertNotEquals(checkedDocumentId, currentlySelectedDocumentNumber, "6) With the other document still selected, click the check box of another document and confirm fields do not expand and template does not change to that document:: ");
		
		softAssert.assertAll();
	}
}
