package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ViewPreferences;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Milon
 *
 *         DB: DEM0
 * 
 *         1) Run a folder search, taking care to select a folder that contains
 *         multiple distinct values in at least one field (Confidentiality, for
 *         example). 2) On Gridview, select the "Find Records" button
 *         (magnifying glass in upper-left) to launch the "Find Records dialog".
 *         3) Select a field to search on and search criteria. Hit the Find
 *         button 4) Verify Recenseo properly filters your results. 5) Use the
 *         "Add Rule" button to add a second filter rule. Select a field to
 *         search on and search criteria. Hit the Find button 6) Verify Recenseo
 *         properly filters your results. 7) Use the "Add Sub Group" button
 *         (combined with the 2 rules from the last step to create a filter with
 *         a rule joined (by OR) with a sub-group that contains two filter
 *         rules. Make sure your filter columns include radio button/multi-pick
 *         fields (like Relevant, Confidentiality, etc) as well as text fields
 *         (like Author, Addressee, etc). Hit the Find button. 8) Verify
 *         Recenseo properly filters the results. Record doc count. 9) Return to
 *         search page. Under the Recent Searches section the last search should
 *         be logged (added Filter rules should NOT be added to the original
 *         search criteria). Verify Recenseo correctly captured the Search.
 */

public class GridViewFindRecords extends TestHelper{
	@Test(enabled = false)
	public void test_c1682_GridViewFindRecords(){
		handleGridViewFindRecords(driver);
	}

	private void handleGridViewFindRecords(WebDriver driver){
		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");
		SearchPage.setWorkflow("Search");

		FolderAccordion.open();
		FolderAccordion.expandFolder(driver, "Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");

		WebElement searchesDiv = Utils.waitForElement(Driver.getDriver(), "ui-id-8", "id");
		WebElement historyDiv = searchesDiv.findElement(By.tagName("div"));
		WebElement ulList = historyDiv.findElement(By.tagName("ul"));
		List<WebElement> dates = ulList.findElements(By.tagName("li"));

		int previousRecordsNumber = 0;

		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
		Date curDate = new Date();
		String datess = dateFormat.format(curDate);

		System.out.println( Logger.getLineNumber() + datess);

		String[] splited = datess.split("\\s+");

		for (WebElement e : dates) {
			String intoText = e.getText();
			String[] splitedStrings = intoText.split("\\s+");
			if (splitedStrings[1].equalsIgnoreCase(splited[0]) && splitedStrings[3].equalsIgnoreCase(splited[2])) {
				if (splitedStrings[2].charAt(0) == splited[1].charAt(0)	&& splitedStrings[2].charAt(1) == splited[1].charAt(1))
					previousRecordsNumber++;
			}
		}

		System.out.println( Logger.getLineNumber() + "Previous Rocords: " + previousRecordsNumber);

		SearchPage sp = new SearchPage();
		DocView docView = new DocView();
		// docView.clickSearchOnTop();
		// Driver.getDriver().switchTo().defaultContent();
		// SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");

		
		/*folderAccordion.checkSubFolder(Driver.getDriver(), "Email>Foster, Ryan e-mail");
		Utils.waitShort();
		//folderAccordion.openFolder(Driver.getDriver());
		FolderAccordion.open();
		Utils.waitMedium();
		Driver.getDriver().switchTo().defaultContent();

		SearchPage.waitForFrameLoad(Driver.getDriver(), Frame.MAIN_FRAME);*/
		
		FolderAccordion.open();
		FolderAccordion.expandFolder(driver, "Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");

		//Utils.handleResult("1. Run a folder search, taking care to select a folder that contains multiple distinct values in at least one field", result, true);

		// docView.waitWhileLoading(Driver.getDriver(), sp);

		docView.selectAllDocuments(Driver.getDriver(), sp);

		DocView.openPreferenceWindow();
		ViewPreferences viewPreferences = new ViewPreferences();
		viewPreferences.setColumns(Driver.getDriver(), sp, new String[] { "inv_id", "subj", "addr", "date" });
		docView.clickCrossButton();
		SearchPage.waitForFrameLoad(Driver.getDriver(), Frame.MAIN_FRAME);

		WebElement dataTag = Utils.waitForElement(Driver.getDriver(), "search_grid_top", "id");
		WebElement divTag = dataTag.findElement(By.tagName("div"));
		WebElement spanTag = divTag.findElement(By.tagName("span"));
		spanTag.click();

		//Utils.handleResult("2. On Gridview, select the 'Find Records' button", result, true);

		Utils.waitMedium();

		// docView.clickActionMenuItem(Driver.getDriver(), sp, "Workflow state
		// manager");
		// Driver.getDriver().switchTo().frame(1);

		WebElement element2 = Driver.getDriver().findElement(By.cssSelector("input[title ='Add rule'"));
		element2.click();
		Utils.waitShort();

		element2 = Driver.getDriver().findElement(By.cssSelector("input[title ='Add rule'"));
		element2.click();

		Utils.waitShort();

		WebElement element = Driver.getDriver()
				.findElement(By.cssSelector("div[class='header ui-widget ui-widget-header ui-corner-top'"));
		WebElement element1 = Utils.getFollowingSibling(Driver.getDriver(), element);
		Select select1 = new Select(element1.findElements(By.tagName("select")).get(0));
		select1.selectByVisibleText("Addressee");
		Utils.waitShort();

		Select select2 = new Select(element1.findElements(By.tagName("select")).get(1));
		select2.selectByVisibleText("contains");
		Utils.waitShort();

		WebElement text = Utils.getFollowingSibling(Driver.getDriver(),
				element1.findElements(By.tagName("select")).get(1));
		WebElement text1 = text.findElement(By.tagName("input"));
		text1.clear();
		text1.sendKeys("New; ryanEditTest");

		WebElement element3 = Utils.getFollowingSibling(Driver.getDriver(), element1);

		Select select3 = new Select(element3.findElements(By.tagName("select")).get(0));
		select3.selectByVisibleText("Document ID");
		Utils.waitShort();

		Select select4 = new Select(element3.findElements(By.tagName("select")).get(1));
		select4.selectByVisibleText("greater or equal");
		Utils.waitShort();

		text = Utils.getFollowingSibling(Driver.getDriver(), element3.findElements(By.tagName("select")).get(1));
		WebElement text2 = text.findElement(By.tagName("input"));
		text2.clear();
		text2.sendKeys("100");

		Select op = new Select(Driver.getDriver().findElement(By.cssSelector("select[class='opsel']")));
		op.selectByVisibleText("OR");
		Driver.getDriver().findElement(By.id("fbox_grid_search")).click();
		Utils.waitMedium();
		docView.waitWhileLoading(Driver.getDriver());
		WebElement crossId = Utils.waitForElement(Driver.getDriver(), "searchhdfbox_grid", "id");
		WebElement spanId = crossId.findElement(By.tagName("a"));
		WebElement crossSpan = spanId.findElement(By.tagName("span"));
		crossSpan.click();

		docView.waitWhileLoading(Driver.getDriver());

		/*Utils.handleResult(
				"7) Use the 'Add Sub Group' button(combined with the 2 rules from the last step to create a filter with "
						+ "a rule joined (by OR) with a sub-group that contains two filter "
						+ "rules. Make sure your filter columns include radio button/multi-pick "
						+ "fields (like Relevant, Confidentiality, etc) as well as text fields "
						+ "(like Author, Addressee, etc). Hit the Find button.",
				result, true);*/

		WebElement totalData = Driver.getDriver().findElement(By.cssSelector("div[class='ui-paging-info']"));
		String str = totalData.getText();
		String[] splitedString = str.split("\\s+");
		int totalString = splitedString.length;
		int totalNumber = Integer.parseInt(splitedString[totalString - 1]);
		String docCount = docView.getTotalDocumentCount(Driver.getDriver(), sp);
		int total = Integer.parseInt(docCount);
		if (totalNumber == total) {
			//Utils.handleResult("8) Verify the Summary screen reflects the number of documents and workflow state changes to be made and click Go",	result, true);
		} else
			//Utils.handleResult("8) Verify the Summary screen reflects the number of documents and workflow state changes to be made and click Go",	result, false);

		docView.clickSearchOnTop();
		Utils.waitMedium();

		Driver.getDriver().switchTo().defaultContent();
		SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");

		

		Utils.waitShort();

		searchesDiv = Utils.waitForElement(Driver.getDriver(), "ui-id-8", "id");
		historyDiv = searchesDiv.findElement(By.tagName("div"));
		ulList = historyDiv.findElement(By.tagName("ul"));
		dates = ulList.findElements(By.tagName("li"));

		int currentRecordsNumber = 0;

		dateFormat = new SimpleDateFormat("MMM dd yyyy");
		curDate = new Date();
		datess = dateFormat.format(curDate);

		splited = datess.split("\\s+");

		for (WebElement e : dates) {
			String intoText = e.getText();
			String[] splitedStrings = intoText.split("\\s+");
			if (splitedStrings[1].equalsIgnoreCase(splited[0]) && splitedStrings[3].equalsIgnoreCase(splited[2])) {
				if (splitedStrings[2].charAt(0) == splited[1].charAt(0)
						&& splitedStrings[2].charAt(1) == splited[1].charAt(1))
					currentRecordsNumber++;
			}
		}
		System.out.print("Current Rocords: " + currentRecordsNumber);
		/*if (currentRecordsNumber == (previousRecordsNumber + 1))
			Utils.handleResult(
					"9) Return to " + "search page. Under the Recent Searches section the last search should "
							+ "be logged (added Filter rules should NOT be added to the original "
							+ "search criteria). Verify Recenseo correctly captured the Search.",
					result, true);
		else
			Utils.handleResult(
					"9) Return to " + "search page. Under the Recent Searches section the last search should "
							+ "be logged (added Filter rules should NOT be added to the original "
							+ "search criteria). Verify Recenseo correctly captured the Search.",
					result, false);*/

	}

}
