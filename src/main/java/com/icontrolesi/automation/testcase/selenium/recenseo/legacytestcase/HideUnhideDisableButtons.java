package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class HideUnhideDisableButtons extends TestHelper{
@Test(enabled = false)
public void test_c1653_HideUnhideDisableButtons(){
	//loginToRecenseo();
	handleHideUnhideDisableButtons(driver);
}


private void handleHideUnhideDisableButtons(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
			
			SearchPage sp = new SearchPage();
			
			sp.clickSelectedSavedSearch("iControl_032 - Complicated Saved Search");
			
			performSearch();
			
			DocView dc= new DocView();
			
			String docCount= dc.getTotalDocumentCount(Driver.getDriver(), sp);
			System.out.println( Logger.getLineNumber() + "Doc count: "+docCount);
			
			SearchPage.goToDashboard();
			
			//Hide the second search group
			waitForFrameToLoad(driver, Frame.MAIN_FRAME);
			SearchPage.hideSearchGroup(0);
			
			performSearch();
			
			String currentDocCount= dc.getTotalDocumentCount(Driver.getDriver(), sp);;
			
			docCount = docCount.replace(",", "");
			currentDocCount=currentDocCount.replace(",", "");
			
			/*if(docCount.equalsIgnoreCase(currentDocCount)){
				Utils.handleResult("(5) Re-run the search and confirm the total number of results did not change. ", result, true);
			}else{
				Utils.handleResult("(5) Re-run the search and confirm the total number of results did not change. ", result, false);
			}*/
			
			//Return to Search Page
			SearchPage.goToDashboard();
			
			//Disable second search group
			//SearchPage.disableGroup(1);
			
			performSearch();
			
			//get doc count
			currentDocCount= dc.getTotalDocumentCount(Driver.getDriver(), sp);;
			System.out.println( Logger.getLineNumber() + "Doc count: "+currentDocCount);
			
			//Remove comma if present in the doc count
			
			docCount = docCount.replace(",", "");
			currentDocCount=currentDocCount.replace(",", "");
			
			String docCountWhenDisabled= currentDocCount;
			
           /* if(Integer.parseInt(docCount)<(Integer.parseInt(currentDocCount))){
				Utils.handleResult("(8) Re-run the search and confirm the total number of results has increased dramatically", result, true);
			}else{
				Utils.handleResult("(8) Re-run the search and confirm the total number of results has increased dramatically", result, false);
			}*/
            
            //Return to Search Page
            SearchPage.goToDashboard();
			
		  //Remove the second search group
		  //sp.removeGroup(1);
		  
		  //run search and check doc count
		  
		  performSearch();
		  
			currentDocCount= dc.getTotalDocumentCount(Driver.getDriver(), sp);;
			System.out.println( Logger.getLineNumber() + "Doc count: "+currentDocCount);
			
			//Remove comma if present in the doc count
			
			
			currentDocCount=currentDocCount.replace(",", "");
			
		/*	
          if((docCountWhenDisabled.equalsIgnoreCase(currentDocCount))){
				Utils.handleResult("(11) Re-run the search and confirm the total number of results is the same as when the group was disabled ", result, true);
			}else{
				Utils.handleResult("(11) Re-run the search and confirm the total number of results is the same as when the group was disabled ", result, false);
			}*/
		  
		  SearchPage.goToDashboard();
		
		  waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		//Try to modify the value of the select box beside the label "Documents" (make it "Not in")
		SearchPage.setSelectBox(driver, "CriterionOperator", "class-name", "not in");  
		
		//Check if value has been updated
		Select dropdown  = new Select(getElement(By.className("CriterionOperator")));
		WebElement tmp= dropdown.getFirstSelectedOption();
		String s = tmp.getText();
		  
		/*if(s.equalsIgnoreCase("not in")){
			Utils.handleResult("(12) Confirm that none of these buttons block the ability to modify search criteria (specifically check bates searches)", result, true);
		}else{
			Utils.handleResult("(12) Confirm that none of these buttons block the ability to modify search criteria (specifically check bates searches)", result, false);
		}*/
    }//end function
}//end class
