package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;
/**
 * 
 * @author Shaheed Sagar
 * (1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Select two users from the "Users" table in the USERS tab, click on the "Load Settings" button and note down their access settings. 
	(3) Click the "Cancel" button. 
	(4) Select a different user from the "Users" table, click on the "Load Settings" button. 
	(5) Confirm this user has settings different from the previous users 
	(6) Select the original two users and click on "Copy Settings" 
	(7) A confirmation page should prompt "Copy and apply the accesses of the loaded user onto the selected user(s)?".  Click "OK". 
	(8) Select all three users and click on "Load Settings".   
	(9) Verify that no cells in the "General Access" and "Workflow State Access" tables have color other than WHITE or GREEN, and the access for all 3 users matches the original settings of the third user (the one used to copy to the other two). 
	(10) Verify that the Teams for the users have not changed.
 *
 */
public class MUCopyAccessSettings extends TestHelper{
	@Test(enabled = false)
	public void test_c1670_MUCopyAccessSettings(){
		handleMUCopyAccessSettings(driver);
	}

	private void handleMUCopyAccessSettings(WebDriver driver){
		Driver.setWebDriver(driver);
		
		SearchPage sp = new SearchPage();

		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");

		Driver.getDriver().switchTo().defaultContent();	
		SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");
		
		UserManager userManager = new UserManager();
		
		SprocketMenu sprocketMenu = new SprocketMenu();
		//sprocketMenu.selectSubMenu(Driver.getDriver(), sp, "Tools", "User Manager");
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			
			System.err.println(alert.getText());
			alert.accept();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		
		userManager.clickLoadSettings();
		
		String oldTeamName = sp.getValueOfSelectedTeam(driver, By.id("teamSelect"));
		
		System.out.println( Logger.getLineNumber() + oldTeamName);
		
		WebElement annotationCreationElement = Utils.waitForElement(Driver.getDriver(), "gen_access_1", "id");
		if(!annotationCreationElement.isSelected()){
			annotationCreationElement.click();
			userManager.clickUpdateAndSave();
		}
		
		try{
			userManager.clickCancelButton();
		}catch(ElementNotVisibleException e){
			e.printStackTrace();
		}
		
		userManager.checkUser("aawal");
		userManager.checkUser("iceautotest");
		userManager.clickLoadSettings();
		annotationCreationElement = Utils.waitForElement(Driver.getDriver(), "gen_access_1", "id");
		/*if(!annotationCreationElement.isSelected()){
			Utils.handleResult("(5) Confirm this user has settings different from the previous users", result, true);
		}*/
		
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		
		userManager.clickCopySettingsButton();
		try {
	        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 15);
	        wait.until(ExpectedConditions.alertIsPresent());
	        
	        Alert alert = Driver.getDriver().switchTo().alert();
	        
	        System.out.println( Logger.getLineNumber() + alert.getText());
	 
	        alert.accept();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		userManager.checkUser("aawal");
		
		userManager.clickLoadSettings();
		
		annotationCreationElement = Utils.waitForElement(Driver.getDriver(), "gen_access_1", "id");
		/*if(!annotationCreationElement.isSelected()){
			Utils.handleResult("(9) Verify that no cells in the 'General Access' and 'Workflow State Access' tables have color other than WHITE or GREEN, and the access for all 3 users matches the original settings of the third user (the one used to copy to the other two).", result, true);
		}else{
			Utils.handleResult("(9) Verify that no cells in the 'General Access' and 'Workflow State Access' tables have color other than WHITE or GREEN, and the access for all 3 users matches the original settings of the third user (the one used to copy to the other two).", result, false);
		}*/
		
		userManager.clickCancelButton();
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		userManager.clickLoadSettings();
		String teamName = sp.checkValueOfSelectBox(Driver.getDriver(),"teamSelect");
		/*if(teamName.equalsIgnoreCase(oldTeamName)){
			Utils.handleResult("(10) Verify that the Teams for the users have not changed.", result, true);
		}else{
			Utils.handleResult("(10) Verify that the Teams for the users have not changed.", result, false);
		}*/
		
		
		//reverting the settings back
		userManager.checkUser("azaman");
		userManager.clickLoadSettings();
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Annotation Creation");
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Copy Previous");
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Redaction Creation");
		
		List<WebElement> radioElements = Driver.getDriver().findElements(By.id("wfs_access_1"));
		WebElement element = userManager.getProperElement(radioElements, UserManager.ASSIGN);
		userManager.setState(element);
		radioElements = Driver.getDriver().findElements(By.id("wfs_access_3"));
		element = userManager.getProperElement(radioElements, UserManager.ASSIGN);
		userManager.setState(element);
		
		userManager.clickUpdateAndSave();
		try {
	        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 30);
	        wait.until(ExpectedConditions.alertIsPresent());
	        
	        Alert alert = Driver.getDriver().switchTo().alert();
	        
	        System.out.println( Logger.getLineNumber() + alert.getText());
	        
	        alert.accept();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		userManager.checkUser("jlumby");
		userManager.clickLoadSettings();
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Copy Previous");
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Prepare Download");
		
		radioElements = Driver.getDriver().findElements(By.id("wfs_access_1"));
		element = userManager.getProperElement(radioElements, UserManager.USE);
		userManager.setState(element);
		radioElements = Driver.getDriver().findElements(By.id("wfs_access_3"));
		element = userManager.getProperElement(radioElements, UserManager.USE);
		userManager.setState(element);
		
		userManager.clickUpdateAndSave();
		try {
	        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 30);
	        wait.until(ExpectedConditions.alertIsPresent());
	        
	        Alert alert = Driver.getDriver().switchTo().alert();
	        
	        System.out.println( Logger.getLineNumber() + alert.getText());
	
	        alert.accept();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
