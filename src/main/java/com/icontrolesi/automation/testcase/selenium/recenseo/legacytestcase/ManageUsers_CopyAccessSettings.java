package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

public class ManageUsers_CopyAccessSettings extends TestHelper{
	
	@Test
	public void test_1670_ManageUsers_CopyAccessSettings(){
		new SelectRepo(AppConstant.DEM0);

    	UserManager.open();
    	UserManager.loadUsersSettings("azaman", "demo");
    	
    	List<WebElement> generalAccessRows = getElements(UserManager.generalAccessRowLocator);
    	
    	List<String> generalAccessSettingsForLoadedUsers = new ArrayList<String>();
    	
    	for(WebElement generalAccess : generalAccessRows){
    		WebElement generalAccessCheckbox = generalAccess.findElement(By.cssSelector("td input"));
    		if(generalAccessCheckbox.isSelected()){
    			generalAccessSettingsForLoadedUsers.add(generalAccess.findElement(By.cssSelector("td[aria-describedby='generalAccesses_access']")).getText().trim());
    		}
    	}
    	
    	
    	UserManager.cancelLoadedSettings();
    	
    	UserManager.loadUserSettings("wakther");
    	
    	UserManager.selectUsers("azaman", "demo");
    	
    	UserManager.clickCopySettingsButton();
    	
    	String alertText = getAlertMsg();
    	
    	softAssert.assertEquals(alertText, "Copy and apply the accesses of the loaded user onto the selected user(s)?", "7) A confirmation page is prompted:: ");
    	
    	UserManager.loadUsersSettings("azaman", "demo", "wakhter");
    	
    	boolean isAllOk = getElements(By.cssSelector("td[aria-describedby='generalAccesses_access']")).stream()
    												.allMatch(access -> getCssValue(access, "background-color").equals("white"));
    	
    	getElements(By.cssSelector("td[aria-describedby='generalAccesses_access']")).stream().forEach(System.out::println);
    	
    	
    	
    
    	
    	
        softAssert.assertAll();        
	}
	
}
