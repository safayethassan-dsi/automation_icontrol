package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;

public class ManageUsers_GeneralAccessSettings extends TestHelper{
	
	@Test
	public void test_c377_ManageUsers_GeneralAccessSettings(){
		new SelectRepo(AppConstant.DEM0);

    	SearchPage.setWorkflow("Objective Coding");
    	
    	
    	performSearch();
    	
    	UserManager.open();
    	UserManager.loadUserSettings(AppConstant.USER);
		UserManager.setWorkflowStateAccess("Objective Coding", UserManager.NO_ACCESS);
		UserManager.updateLoadedSettings();
		
		SelectRepo.selectRepoFromSecondaryMenu(AppConstant.DEM4);
		SelectRepo.selectRepoFromSecondaryMenu(AppConstant.DEM0); 	//re-open DEM0
    	
    	String selectedWF = SearchPage.getCurrentWorkFlow();
    	boolean isObjectiveCodingRemoved = !getListOfItemsFrom(SearchPage.workflowList, wf -> getText(wf)).stream().anyMatch(wfText -> wfText.equals("Objective Coding"));
    	
    	softAssert.assertNotEquals(selectedWF, "Objective Coding", "5) Recenseo automatically selects a different wf state:: ");
    	softAssert.assertTrue(isObjectiveCodingRemoved, "5) Users do not have access to select the state that you removed access for:: ");
        
    	waitForFrameToLoad(Frame.MAIN_FRAME);
    	performSearch();
    	
    	SearchPage.setWorkflowFromGridView("Privilege Review");
    	
    	boolean isAllDocumentsPostedForSelectedWF = DocView.isAllDocumentsOutOfState();
    	
    	DocView.openWFSManagerWindow();
    	
    	long countForObjectiveCoding = getElements(DocView.wfsSummaryRowLocator).stream()
    										.filter(wfs -> wfs.findElement(By.tagName("td")).getText().trim().equals("Objective Coding"))
    										.count();
    	
    	if(isAllDocumentsPostedForSelectedWF){
    		System.out.println( Logger.getLineNumber() + "Documents are out of the state. Adding docuements to the current WFS...");
    		DocView.addDocsToWorkflow(selectedWF);
    	}
    	
    	DocView.closeWFSManageerWindow();
    	
    	getElements(DocView.gridRowLocator).stream()
    			.filter(row -> !getAttribute(row, "class").contains("NotInState"))
    			.findFirst()
    			.get()
    			.click();
    	
    	softAssert.assertEquals(countForObjectiveCoding, 0, "6)  cannot manage the removed workflow state (it should not even be listed):: ");
    	
    	ReviewTemplatePane.postDocument();
    	int currentDocumentNumber = DocView.getCurrentDocumentNumber();
    	DocView.clickOnDocument(currentDocumentNumber - 1);
    	
    	DocView.selectView("History");
    	
    	String wfStateInHistory = getText(DocView.docHistoryState);
    	String actionInHistory = getText(DocView.docHistoryAction);
    	String timeInHistory = getText(DocView.docHistoryTime);
    	
    	softAssert.assertEquals(wfStateInHistory, selectedWF, "8) Coding History shows changes being made in NEW workflow state:: ");
    	softAssert.assertEquals(actionInHistory, "Document Post", "8) Coding History shows changes being made in NEW workflow Action:: ");
    	//check for whether time is ok
    	
    	UserManager.open();
    	UserManager.setGeneralAccess("Redaction Creation", false);
    	UserManager.setGeneralAccess("Annotation Creation", false);
    	UserManager.setGeneralAccess("Global Edit", false);
    	UserManager.setGeneralAccess("Prepare Download", false);
    	
    	SelectRepo.selectRepoFromSecondaryMenu(AppConstant.DEM4);
		SelectRepo.selectRepoFromSecondaryMenu(AppConstant.DEM0); 	//re-open DEM0
    	
    	
        softAssert.assertAll();        
	}
	
	
	public static void clear(By textfieldLocator){
		WebElement textField = Driver.getDriver().findElement(textfieldLocator);
		String text = textField.getText();
		for(int i = 0; i < text.length(); i++){
			textField.sendKeys(Keys.BACK_SPACE);
		}
	}
	
	
	public static void clear(WebElement textfield){
		String text = textfield.getText();
		for(int i = 0; i < text.length(); i++){
			textfield.sendKeys(Keys.BACK_SPACE);
		}
	}
}
