package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;
import java.util.stream.IntStream;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

public class ManageUsers_InterfaceDisplayAndUserFilterSelection extends TestHelper{
	
	@Test
	public void test_c1672_ManageUsers_InterfaceDisplayAndUserFilterSelection(){
		new SelectRepo(AppConstant.DEM0);

    	SearchPage.setWorkflow("Search");
        
        UserManager.open();
        
        UserManager.checkTabEementsCleanlyVisible();
        UserManager.checkSortingForUserHeadersWorks();
        UserManager.checkFilteringForUserHeaderWorks();
        
        UserManager.checkUser(AppConstant.USER);
        List<String> availableIDList = getListOfItemsFrom(UserManager.availableUserIDColumnLocator, item -> getText(item));
        
        int index = IntStream.range(0, availableIDList.size())
        					 .filter(i -> availableIDList.get(i).equals(AppConstant.USER))
        					 .findFirst().getAsInt();
        
        boolean isExpectedUserSelected = isElementChecked(By.cssSelector("#availableUsers tr:nth-child("+ (index + 2) + ") input"));
        
        softAssert.assertTrue(isExpectedUserSelected, "e) The user can select specific users by hitting the check box :: ");
        
        UserManager.filterByTeam("Document Security");
        
        List<String> filteredUsersList = getListOfItemsFrom(UserManager.availableUserIDColumnLocator, user -> getText(user));
        
        UserManager.selectAllAvailableUserFromUsersCase();
        
        long totalSelectedUsers = getElements(UserManager.availableUserSelectBoxLocator).stream().filter(checkbox -> checkbox.isSelected()).count();
        
        UserManager.loadAllSelectedUserSettings();
        UserManager.clickInfoButton();
        
        List<String> loadedUsersList = getListOfItemsFrom(UserManager.loadedUserIDColumn, user -> getText(user));
        
        softAssert.assertEquals(totalSelectedUsers, filteredUsersList.size(), "***) All filtered users selected:: ");
        softAssert.assertEquals(loadedUsersList, filteredUsersList, "5) Selected Users\" grid on the left-hand side ONLY list the users selected previously:: ");
        
        UserManager.clickBackButton();
        UserManager.cancelLoadedSettings();
        clear(UserManager.userTeamFilterLocator);
        
        List<String> allAvailableUsersList = getListOfItemsFrom(UserManager.availableUserIDColumnLocator, user -> getText(user));
        
        UserManager.selectAllAvailableUserFromUsersCase();
        
        long totalSelectedUsersWithoutFiltering = getElements(UserManager.availableUserSelectBoxLocator).stream().filter(checkbox -> checkbox.isSelected()).count();
        
        UserManager.loadAllSelectedUserSettings();
        UserManager.clickInfoButton();
        
        List<String> loadedUsersListWithoutFiltering = getListOfItemsFrom(UserManager.loadedUserIDColumn, user -> getText(user));
        
        softAssert.assertEquals(totalSelectedUsersWithoutFiltering, allAvailableUsersList.size(), "***) All users selected (Without filtering applied):: ");
        softAssert.assertEquals(loadedUsersListWithoutFiltering, allAvailableUsersList, "8) ALL Selected Users\" grid on the left-hand side ONLY list the users selected previously:: ");
        
        softAssert.assertAll();        
	}
	
	
	public static void clear(By textfieldLocator){
		WebElement textField = Driver.getDriver().findElement(textfieldLocator);
		String text = textField.getText();
		for(int i = 0; i < text.length(); i++){
			textField.sendKeys(Keys.BACK_SPACE);
		}
	}
	
	
	public static void clear(WebElement textfield){
		String text = textfield.getText();
		for(int i = 0; i < text.length(); i++){
			textfield.sendKeys(Keys.BACK_SPACE);
		}
	}
}
