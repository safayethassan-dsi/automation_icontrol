package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 * 
 * @author Shaheed Sagar
 * ""(1) in DEM0, open folder  \Email\Foster, Ryan e-mail 
	(2) From the Actions Menu in Grid View, Select Save Result set to LFP file format 
	(3) Open File and confirm all document with images are accounted for and that images come from right database"
 *
 */
public class POISaveToLFP extends TestHelper{
	@Test
	public void test_c1637_POISaveToLFP(){
		handlePOISaveToLFP(driver);
	}

	private void handlePOISaveToLFP(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);

		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
	
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMd");
        String fileMatchPattern = AppConstant.USER + sdf.format(date) + "\\d+\\.lfp";
        
        File [] fileListToDelete = FileMethods.getListOfFiles(AppConstant.DOWNLOAD_DIRECTORY, fileMatchPattern);
        FileMethods.deleteFiles(fileListToDelete);
        
		DocView docView = new DocView();
		docView.clickActionMenuItem("Save result set to LFP file format");
		waitFor(5);
		
		fileListToDelete = FileMethods.getListOfFiles(AppConstant.DOWNLOAD_DIRECTORY, fileMatchPattern);
		
		boolean isCorrectDBListed = true;
		Set<String> idList = new TreeSet<>();
		
		if(fileListToDelete.length != 1){
			System.out.println( Logger.getLineNumber() + "No files downloaded...");
			throw new SkipException("Either no files downloaded with the pattern expected or there are multiple files that arises conflicts!");
		}else{
			try{
			BufferedReader br = new BufferedReader(new FileReader(fileListToDelete[0]));
			String line = "";
			while((line = br.readLine()) != null){
				 
				 String [] temp = line.split(",");
				 String id = temp[1].split("-")[1];
				 
				 if(!line.contains("DEM0")){
					 isCorrectDBListed = false;
					 break;
				 }
				 
				 idList.add(id);
			}
			 br.close();
			}catch (Exception e) {
				System.err.println(e.getMessage());
			}
			
		}
		
		List<String> documentIdListInGridView = getListOfItemsFrom(DocView.documentIDColumnLocator, id -> getText(id)); 
		Set<String> documentIdSetInGridView = new TreeSet<>(documentIdListInGridView);
		
		softAssert.assertTrue(isCorrectDBListed, String.format("***) Correct Database (%s) listed:: ", "dem0"));
		softAssert.assertEquals(idList, documentIdSetInGridView, "***) All document IDs are accounted for:: ");	
		
		softAssert.assertAll();
	}
}
