package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class PersistentTextHighlighting extends TestHelper{
	@Test
	public void test_c1540_PersistentTextHighlighting(){
		handlePersistentTextHighlighting(driver);
	}
	

	private void handlePersistentTextHighlighting(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.clickCrossBtn();

		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document Content (Full Text)");
		
		SearchPage.setOperatorValue(0, "this exact phrase");
		SearchPage.setCriterionValue(0, "American Airlines");

		performSearch();
		
		DocView.sortByColumn("Document ID");
		
	    DocView.selectView("Text");
		
		DocView.openHighlightWindow();
		
		softAssert.assertEquals(getColorFromElement(By.id("0_anchor"), "background-color"), "#C2D985", "(3) Select Document ID 44 from search results and view text to confirm \"American Airlines\" is highlighted along with other words highlighted in other colors:: ");
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(2);
		
		DocView.selectView("Text");
		
		DocView.openHighlightWindow();
		
		softAssert.assertEquals(getColorFromElement(By.id("0_anchor"), "background-color"), "#C2D985", "(3) Select Document ID 132 from search results and view text to confirm \"American Airlines\" is highlighted along with other words highlighted in other colors:: ");
		
		softAssert.assertAll();
	}
}
