package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Ishtiyaq Kamal
 * 
 * DB: DEM0

"DB: WPTP 
(1) Go to Search and search for Document ID 101151.  
(2) Select the printer icon in the Review Template pane. 
(3) Verify that ""As in Production"" and ""Print Redactions"" are selected. 
(4) Add the ""Print Complete Doc Family"" option and hit [Submit] 
(5) Verify that the generated PDF is 10 pages long, that the page numbering on the top-right corner verifies all pages are in order, and that the appropriate bates numbers are printed on the bottom of the page."

 *
 */

public class PrintIncludeDocSet extends TestHelper{
   @Test(enabled = false)
   public void test_c242_PrintIncludeDocSet(){
	   //loginToRecenseo();
	   handlePrintIncludeDocSet(driver);
   }

     
    protected void handlePrintIncludeDocSet(WebDriver driver){
    	new SelectRepo(AppConstant.DEM4);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, "258387");
		
		performSearch();
        
        DocView.selectView("Imaged");
        
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        tryClick(By.id("btnPrint"));
        tryClick(By.id("all_doc_set"));
        
        List<WebElement> spanChildren= getElements(By.cssSelector("span[class='ui-button-text']"));
        System.out.println( Logger.getLineNumber() + spanChildren.size());
        for(int i=0;i<spanChildren.size();i++){
        	if(spanChildren.get(i).isDisplayed()){	
        		System.out.println( Logger.getLineNumber() + "displayed");
        		if(spanChildren.get(i).getText().contains("Submit")){
        			click(SearchPage.getParent(spanChildren.get(i)));
        			break;
        		}
        	}
        }
        
        Set <String> handles =driver.getWindowHandles();
        Iterator<String> it = handles.iterator();
        //iterate through your windows
        while (it.hasNext()){
	        String newwin = it.next();
	        driver.switchTo().window(newwin); 
	        System.out.println( Logger.getLineNumber() + driver.getTitle());
        
        }
        driver.switchTo().defaultContent();
        List<WebElement> allElements = getElements(By.tagName("*"));
        
        
        /*System.out.println( Logger.getLineNumber() + SearchPage.waitForElement("numPages", "id").getText());
        String pageCount= SearchPage.waitForElement("numPages", "id").getText();*/
        String pageCount = "";
        System.out.println( Logger.getLineNumber() + "Size: "+allElements.size());
        for(WebElement tag : allElements){
        	if(tag.getAttribute("id").equals("numPages")){
        		System.out.println( Logger.getLineNumber() + "Page count: " + tag.getText() +"***");
        		break;
        	}
        }
        
        System.out.println( Logger.getLineNumber() + pageCount + "*****");
        /*if(pageCount.contains("11")){
        	result.record("(5)Verify that the generated PDF is 10 pages long, that the page numbering on the top-right corner verifies all pages are in order:pass");
        } else{
        	result.record("(5)Verify that the generated PDF is 10 pages long, that the page numbering on the top-right corner verifies all pages are in order:fail");
            result.fail();
        }*/
    }
}
