package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


/**
 * 
 * 		@author abdul awal
 *
 *		"Adding documents to an assignment Pt 2 (continuation of mantis #0013530: TVP-0044) 
 
		DB: DEM0 
		 
		1) On the Search screen, open an existing review assignment. 
		2) From Grid View go to Actions\Manage Review Assignments 
		3) On the Admin tab click the New button. 
		4) Create an assignment which contains your name and the current date, *choose the same workflow as the opened assignment*, Assign to, and priority. 
		5) Select ""Create"" 
		6) On the Manage tab add the docs from the other review assignment to the new assignment. 
		7) Verify warning that documents are already in another assignment. Select reassign all 
		8) Verify documents were removed from the old assignment and added to the new."

 */


public class ReviewAssignmentsAddingDocs extends TestHelper{
	String assigneeName = "Abdul Awal";
	String assignmentNameToOpen = "ReviewAssignmentsAddingDocs_c1659";
	String assignmentWorkflow = "Privilege Review";
	String assignmentPriority = "3";
	String assignmentName = assigneeName + new Date().getTime();
	
	@Test
	public void test_c1659_ReviewAssignmentsAddingDocs(){
		handleReviewAssignmentsAddingDocs(driver);
	}

	private void handleReviewAssignmentsAddingDocs(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		DocView dc= new DocView();
		dc.clickActionMenuItem("Manage Review Assignments");
		
	    switchToFrame(1);
	    
	    ManageReviewAssignment.createAssignment(assignmentName, assignmentWorkflow, assigneeName, assignmentPriority);
	    
	    ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	    
	    ReviewAssignmentAccordion.selectAssignment(assignmentName);	
	    
	    ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
	    
	    String appearedWarningMsg = getText(By.cssSelector("#conflictContainer > p:nth-child(4)"));
	    System.out.println( Logger.getLineNumber() + appearedWarningMsg+ "***");
	    softAssert.assertTrue(appearedWarningMsg.matches("- Of the \\d+ selected documents, \\d+ \\(including PENDING\\) are in other assignment\\(s\\)\\."));
	    
	    waitForInVisibilityOf(ManageReviewAssignment.resolutionChoiceReassignAll);
	    tryClick(ManageReviewAssignment.resolutionChoiceReassignAll, 1);
	    tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	    acceptAlert();
	     
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		ReviewAssignmentAccordion.searchForAssignment(assignmentName);
		
		String assignmentStatus = getAssignmentStatus(assignmentName);
		
	    softAssert.assertTrue(assignmentStatus.contains("Pending"), "8) Verify documents are listed as PENDING in the new assignment:: ");
	    
	    ManageReviewAssignment.deleteAssignment(assigneeName);
	    
	    softAssert.assertAll();
	}


	public void addSelectedDocumentsToAssignment(String assignmentName){
		ReviewAssignmentAccordion.selectAssignment(assignmentName);	
		ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
		tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
		waitForInVisibilityOf(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	}
	
	public void removeSelectedDocumentsFromAssignment(String assignmentName){
		ReviewAssignmentAccordion.selectAssignment(assignmentName);	
		ManageReviewAssignment.clickAddSelectedDocumentsToAssignment();
		tryClick(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
		waitForInVisibilityOf(ManageReviewAssignment.continueBtnForAddingSelectedDocumentsLocator);
	}
	
	public String getAssignmentStatus(String assignmentName){
		List<WebElement> assignmentList = getElements(By.cssSelector("#assignmentList li[style$='block;'] span.name"));
		String assignmentStatus = "";
		for(int i = 0; i < assignmentList.size(); i++){
			if(assignmentList.get(i).getText().trim().equals(assignmentName)){
				assignmentStatus = getText(By.cssSelector("#assignmentList > li[style$='block;'] div.right.stats"));
			}
		}
		
		return assignmentStatus;
	}
	
	public static void main(String[] args) {
		String data = "- Of the 17178 selected documents, 17178 (including PENDING) are in other assignment(s).";
		System.out.println( Logger.getLineNumber() + data.matches("- Of the \\d+ selected documents, \\d+ \\(including PENDING\\) are in other assignment\\(s\\)\\."));
	}
}
