package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


/*
 * "DB: DEM0 
(1) Go to search page.  Open the Review Assignments Section.  
(2) Check the box next to an existing assignment (not an empty one).  
(3) Note the number of documents in the assignment.  
(4) On the Review Tab, Click the Open Button 
(5) Confirm number of documents matches expectations.  
(6) Review the Values in the File Name field to find an extension that occurs multiple times 
(7) Return to the Search Page and Clear Search 
(8) Check the box next to the same assignment. 
(9) On the Review Tab, Click the Add button.  
(10) Add Search Criteria: File Name contains whatever extension noted above. 
(11) Run Search and note the results.  
(12) Return to the Search Page and Change Criteria to File Name does not contain extension 
(13) Run Search and note the results.  
(14) Verify the two values add up to the total number of documents in the assignment. "

 * 
 * 
 * 
 */



public class RwAssignSearchDocCounts extends TestHelper{
	String assignmentName = "RwAssignSearchDocCounts_c1826";
	
    @Test
    public void test_c1826_RwAssignSearchDocCounts(){
    	//loginToRecenseo();
    	handleRwAssignSearchDocCounts(driver);
    }

	
    protected void handleRwAssignSearchDocCounts(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
			SearchPage.setWorkflow("Search");
		
			ReviewAssignmentAccordion.open();
			ReviewAssignmentAccordion.searchForAssignment(assignmentName);
			
			int numberOfDocumentsInAssignments = Integer.valueOf(getText(By.cssSelector("div.right.stats > span > span:nth-child(2)")));
			
			ReviewAssignmentAccordion.selectAssignment(assignmentName);
			ReviewAssignmentAccordion.openSelectedAssignment();
			
			int numberOfDocumentsInGridView = DocView.getDocmentCount();
	        
			softAssert.assertEquals(numberOfDocumentsInAssignments, numberOfDocumentsInGridView, "(5)Confirm the number of documents matches expectation:: ");
	    
			String multipleTimesOccuringExtension = "xls";
	        
	        SearchPage.goToDashboard();
	       
	        waitForFrameToLoad(Frame.MAIN_FRAME);
	        
	        SearchPage.clearSearchCriteria();
	        
	        ReviewAssignmentAccordion.open();
	        ReviewAssignmentAccordion.searchForAssignment(assignmentName);
	        ReviewAssignmentAccordion.selectAssignment(assignmentName);
	        ReviewAssignmentAccordion.openReviewTab();
	        ReviewAssignmentAccordion.addSelectedToSearch();
	        
	        switchToDefaultFrame();
	        waitForFrameToLoad(Frame.MAIN_FRAME);
	        
	        SearchPage.addSearchCriteria("File Name");
	        SearchPage.setCriterionValue(0, multipleTimesOccuringExtension);
	       
	        performSearch();
	        
	        int numberOfDocumentInSearchWithExt = DocView.getDocmentCount();
	        
	        SearchPage.goToDashboard();
	        
	        waitForFrameToLoad(Frame.MAIN_FRAME);
	        
	        SearchPage.setOperatorValue(1, "does not contain");
	        SearchPage.setCriterionValue(0, multipleTimesOccuringExtension);
	        
	        performSearch();
	        
	        int numberOfDocumentInSearchWithoutExt = DocView.getDocmentCount();
	        
	        softAssert.assertEquals((numberOfDocumentInSearchWithExt + numberOfDocumentInSearchWithoutExt), numberOfDocumentsInGridView, "(14) Verify the two values add up to the total number of documents in the assignment:: ");
	        
	        softAssert.assertAll();
    }
}
