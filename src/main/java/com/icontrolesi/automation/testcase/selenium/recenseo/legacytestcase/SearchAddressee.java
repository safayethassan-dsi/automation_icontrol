package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * @author atiq,ishtiyaq
 * DB: DEM0
(1) Go to search page and run a new search with the criteria: Addressee contains "Fred"
(2) Run search. Confirm results are correct
(3) Return to search page and run a new search with the criteria: Addressee equals "james.bruce@enron.com"
(4) Run search. Confirm results are correct
 * 
 * 
 */
public class SearchAddressee extends TestHelper{
    @Test
    public void test_c94_SearchAddressee(){
    	//loginToRecenseo();
    	handleSearchAddressee(driver);
    }

    protected void handleSearchAddressee(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
        
        DocView.openPreferenceWindow();
		DocView.checkAllColumns();
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
	    SearchPage.addSearchCriteria("Addressee");
        SearchPage.setCriterionValue(0, "Fred");

        performSearch();
     
        boolean isSearchForFredOk = DocView.isColumnContains(DocView.documentAddresseeLocator, "Fred");
        
        softAssert.assertTrue(isSearchForFredOk, "(2) Run search. Confirm results are correct:Fred is present in all rows");

        SearchPage.goToDashboard();
       
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("Addressee");
        SearchPage.setOperatorValue(0, "equals");
        SearchPage.setCriterionValue(0, "james.bruce@enron.com");
       
        performSearch();

        boolean isSearchResultOk = DocView.isColumnContains(DocView.documentAddresseeLocator, "james.bruce@enron.com");

        softAssert.assertTrue(isSearchResultOk, "3) james.bruce@enron.com is present in the Addressee list of the first row:: ");

        softAssert.assertAll();
    }
}
