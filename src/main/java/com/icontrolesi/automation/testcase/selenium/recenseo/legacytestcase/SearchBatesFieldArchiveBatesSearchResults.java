package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
	(1) Go to search page and run a new search with the criteria: Bates search in range = TEST- 1 thru 23 
	(2) Run search. Confirm results are correct 
	(3) Return to the search page 
	(4) Ensure all the search criteria is still present (prefix, begdoc, enddoc, set, etc..) 
	(5) Return to search page and run a new search with the criteria: Bates search in list: 
	TEST-|0000019|0000026 
	TEST-|0000199|0000205 
	TEST-|0000004|0000006 
	TEST-|0000015|0000018 
	TEST-|0000019|0000034 
	TEST-|0000037|0000042 
	TEST-|0000045|0000045 
	TEST-|0000065|0000068 
	TEST-|0000102|0000105 
	(6) Run search. Confirm results are correct 
	(7) Test the ""Archive"" function of each by checking ""Search archived bates "" setting in Preferences of the following 
	a) In Range TEST- 1 thru 23 
	b) All produced in CDOCDEMO 
	c) None produced in CDOCDEMO 
	(8) Uncheck the ""Search archived bates "" setting in Preferences and run the same 3 tests while checking ""archive"" when searching 
	(9) Confirm the results for both sets of searches are the same 
	(10)..."
*/

public class SearchBatesFieldArchiveBatesSearchResults extends TestHelper{
    @Test
    public void test_c58_SearchBatesFieldArchiveBatesSearchResults(){
    	//loginToRecenseo();
    	handleSearchBatesFieldArchiveBatesSearchResults(driver);
    }

    private void handleSearchBatesFieldArchiveBatesSearchResults(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Bates Number");
		DocView.closePreferenceWindow();
        
        SearchPage.setWorkflow("Search");
       
        By criterionInputDropdownLocator = By.cssSelector("#criterion2 > div.CriterionContent.ui-helper-reset > div.CriterionInput > select:nth-child(1)");
        By criterionInputStartLocator = By.cssSelector("#criterion2 > div.CriterionContent.ui-helper-reset > div.CriterionInput > span:nth-child(2) > input:nth-child(1)");
        By criterionInputEndLocator = By.cssSelector("#criterion2 > div.CriterionContent.ui-helper-reset > div.CriterionInput > span:nth-child(2) > input:nth-child(2)");
        
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(1, "TEST-");
        SearchPage.setCriterionValue(0, "1");
        SearchPage.setCriterionValue(1, "23");
        
        performSearch();
        
        List<WebElement> allBates = getElements(DocView.documentBatesColumnLocator);
        
        boolean isBatesNumberInRange = isBatesInRange(1, 23);
        
        softAssert.assertTrue(isBatesNumberInRange, "(2) Run search. Confirm results are correct:: ");
        
        SearchPage.goToDashboard();
        SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
        
        String selectedOption = getSelectedItemFroDropdown(criterionInputDropdownLocator);
        String startValue = getText(criterionInputStartLocator);
        String endValue = getText(criterionInputEndLocator);
        
        boolean isAllCriteriaPresent = selectedOption.equals("TEST-") && startValue.equals("1") && endValue.equals("23");

        softAssert.assertTrue(isAllCriteriaPresent, "(4) Ensure all the search criteria is still present (prefix, begdoc, enddoc, set, etc..):: ");
       
        SearchPage.clearSearchCriteria();
        
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(0, "In List");
        SearchPage.setCriterionValueTextArea(0, "TEST-|0000019|0000026\nTEST-|0000199|0000205\nTEST-|0000004|0000006\nTEST-|0000015|0000018\nTEST-|0000019|0000034\nTEST-|0000037|0000042\nTEST-|0000045|0000045\nTEST-|0000065|0000068\nTEST-|0000102|0000105");

        performSearch();
        
        allBates = getElements(DocView.documentBatesColumnLocator);
        
        isBatesNumberInRange = false;
        for(WebElement e : allBates){
        	if(e.getText().contains("0000019") || e.getText().contains("0000026") || e.getText().contains("0000199") || e.getText().contains("0000105")){
        		isBatesNumberInRange = true;
        		break;
        	}
        }
        
        softAssert.assertTrue(isBatesNumberInRange, "(5) Run search. Confirm results are correct:: ");
        
        SearchPage.goToDashboard();
        SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
      
        DocView.openPreferenceWindow();
        DocView.switchPreferenceTab("Bates");
        DocView.checkBatesArchiveOption();
        DocView.closePreferenceWindow();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(1, "TEST-");
        SearchPage.setCriterionValue(0, "1");
        SearchPage.setCriterionValue(1, "23");
        
        performSearch();
        
        boolean isBatesInRange = isBatesInRange(1, 23);
        
        softAssert.assertTrue(isBatesNumberInRange, "(6a) Confirm the results for both sets of searches are the same:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
       
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(0, "All produced In");
        SearchPage.setOperatorValue(1, "CDOCDEMO");
        
        performSearch();
        
        boolean isSearchOkFor_ = isAllProduceIn("CDOCDEMO");
        
        softAssert.assertTrue(isSearchOkFor_, "(6b)  \"Archive\" function of each by checking is working (All Produced In):: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(0, "Not Produced In");
        SearchPage.setOperatorValue(1, "CDOCDEMO");
        
        performSearch();
        
        boolean isSearchNotInCDOCDEMO = !isAllProduceIn("CDOCDEMO");
        
        softAssert.assertTrue(isSearchNotInCDOCDEMO, "(6c)  \"Archive\" function of each by checking is working (Not Produced In):: ");
      
        DocView.openPreferenceWindow();
        DocView.switchPreferenceTab("Bates");
        DocView.unCheckBatesArchiveOption();
        DocView.closePreferenceWindow();
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(1, "TEST-");
        SearchPage.setCriterionValue(0, "1");
        SearchPage.setCriterionValue(1, "23");
        checkArchiveOption();
        
        performSearch();
        
        isBatesInRange = isBatesInRange(1, 23);
        
        softAssert.assertTrue(isBatesInRange, "(7a) Confirm the results for both sets of searches are the same:: ");
        
        //for 7b
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
       
        SearchPage.clearSearchCriteria();
        
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(0, "All produced In");
        SearchPage.setOperatorValue(1, "CDOCDEMO");
        checkArchiveOption();
        
        performSearch();
        
        boolean isAllProducedInCDOCDEMO = isAllProduceIn("CDOCDEMO");
        
        softAssert.assertTrue(isAllProducedInCDOCDEMO, "(7b)  \"Archive\" function of each by checking is working (All Produced In):: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        
        SearchPage.addSearchCriteria("Bates Number");
        SearchPage.setOperatorValue(0, "Not Produced In");
        SearchPage.setOperatorValue(1, "CDOCDEMO");
        checkArchiveOption();
        
        performSearch();
        
        boolean isAllProducedNotInCDOCDEMO = !isAllProduceIn("CDOCDEMO");
        
        softAssert.assertTrue(isAllProducedNotInCDOCDEMO, "(7b)  \"Archive\" function of each by checking is working (All Produced In):: ");
      
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        String operator_01 = SearchPage.getOperatorValue(0);
        String operator_02 = SearchPage.getOperatorValue(1);
        boolean isArchiveChecked = isArchiveOptionChecked();
        
        softAssert.assertEquals(operator_01, "Not Produced In", "9) Return to search page, ensure all the search criteria is still present (Not Produced In):: ");        
        softAssert.assertEquals(operator_02, "CDOCDEMO", "9) Return to search page, ensure all the search criteria is still present (CDOCDEMO):: "); 
        softAssert.assertTrue(isArchiveChecked, "9) Return to search page, ensure all the search criteria is still present (Archive toggle is checked):: "); 
        
        softAssert.assertAll();
      }
    
    boolean isArchiveOptionChecked(){
    	return isElementChecked(By.cssSelector("div.CriterionInput  input[type='checkbox']"));
    }
    
    void checkArchiveOption(){
    	if(isArchiveOptionChecked()){
    		System.out.println( Logger.getLineNumber() + "Archive option is already selected...");
    	}else{
    		tryClick(By.cssSelector("div.CriterionInput  input[type='checkbox']"), 1);
    		System.out.println( Logger.getLineNumber() + "Archive option is selected...");
    	}
    }
    
    boolean isBatesInRange(int startValue, int endValue){
    	boolean isBatesNumberInRange = true;
        List<WebElement> allBates = getElements(DocView.documentBatesColumnLocator);
        
        for(WebElement batesNumber : allBates){
        	String [] temp = batesNumber.getText().split("-");
        	int startBatesNumber = Integer.parseInt(temp[1].replaceAll("\\D+", ""));
        	if((startBatesNumber >= startValue && startBatesNumber <= endValue) == false){
        		return false;
        	}
        }
        
        return isBatesNumberInRange;
    }
    
    boolean isAllProduceIn(String archiveName){
    	 boolean isSearchOkFor_ = true;
         List<WebElement> allBates = getElements(DocView.documentBatesColumnLocator);
         
         for(WebElement batesNumber : allBates){
         	if(!batesNumber.getText().contains(archiveName)){
         		return false;
         	}
         }   
         
         return isSearchOkFor_;
    }
}
