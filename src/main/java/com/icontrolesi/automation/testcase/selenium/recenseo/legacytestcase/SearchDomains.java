package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DomainsAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * @author atiq,ishtiyaq
 * DB: DEM0
(1) Click the "Domains" tab on the search page.
(2) Click the check box next to "Sort by frequency" toggle back and forth between the default (alphabetical) sort and by frequency.
(3) Confirm that the list of domains below change.
(4) Add a domain to search criteria and ensure that the results return in a timely matter.
 * 
 * 
 */

public class SearchDomains extends TestHelper{
  String[] domains = new String[10];
  String[] domains_to_compare_with = new String[10];
	
   @Test
   public void test_c57_SearchDomains(){
	   //loginToRecenseo();
	   handleSearchDomains(driver);
   }


    protected void handleSearchDomains(WebDriver driver) {
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		DomainsAccordion.open();
		  
		  WebElement domain_list= getElement(By.id("domainList"));
		  List<WebElement> all_li= domain_list.findElements(By.cssSelector("li"));
		  System.out.println( Logger.getLineNumber() + all_li.size());
		  
		 for(int i=0;i<=5;i++){
		  
		  WebElement span= all_li.get(i).findElement(By.cssSelector("span"));
		  
		  System.out.println( Logger.getLineNumber() + span.getAttribute("id"));
		  
		  String span_id= span.getAttribute("id");
		  
		  WebElement domain= getElement(By.id("domainName" + span_id));
		  
		  System.out.println( Logger.getLineNumber() + domain.getText());
		  
		  domains[i]=domain.getText();
		  
          }
          
		  //Copy values of the "domains" array into another array
		  System.arraycopy(domains, 0, domains_to_compare_with, 0, domains.length); 
		 
		  DomainsAccordion.toggleSortByFrequency();
		
		// SearchPage.waitForCompletion(Driver.getDriver(), sp);
		  
		  
		  //After sorting again obtain the list of domains (first 5)
		  domain_list= getElement(By.id("domainList"));
		  all_li= domain_list.findElements(By.cssSelector("li"));
		  
		  
		  for(int i=0;i<=5;i++){
			  
			  WebElement span= all_li.get(i).findElement(By.cssSelector("span"));
			  
			  System.out.println( Logger.getLineNumber() + span.getAttribute("id"));
			  
			  String span_id= span.getAttribute("id");
			  
			  WebElement domain= getElement(By.id("domainName" + span_id));
			  
			  System.out.println( Logger.getLineNumber() + domain.getText());
			  
			  domains[i]=domain.getText();
			  
	          }
		  
		  boolean compare_arrays= Arrays.equals(domains, domains_to_compare_with);
		  
		  Assert.assertTrue((!compare_arrays), "(3)Confirm that the list of domains below change after clicking Sort by frequency:: ");
		  
		  //Add the first domain to the search criteria and run a search
		  	
		  	//Click the checkbox for the first domain
		    List<WebElement> domains_checkboxes=getElements(By.cssSelector("input[type='checkbox'][name='chkDomain']"));
		    if(domains_checkboxes.size()==0){
		        Logger.log("Checkbox beside first domain not found");
		    }
		    
		    domains_checkboxes.get(0).click();
		    
		    try{
		    	WebElement addDomain= getElement(By.cssSelector("button[role='button'][onclick='addDomain()']"));
		    	addDomain.click();
		    }
		      catch(NoSuchElementException e){
			    System.out.println( Logger.getLineNumber() + "Element for adding Domain to search criteria not found");
			}	

		  
		    switchToDefaultFrame();
		    waitForFrameToLoad(Frame.MAIN_FRAME);
		    
		    long start_time = System.currentTimeMillis();
		  
		    performSearch();
		   
		   
		    long end_time= System.currentTimeMillis();
		    
		    long time_for_search_result= 2 + (end_time-start_time)/1000;
		    
		    System.out.println( Logger.getLineNumber() + "Time for search result: " + time_for_search_result);
		    
		    Assert.assertTrue(time_for_search_result <= 15, "Time for search result is maintained (15secs):: ");
	}
}
