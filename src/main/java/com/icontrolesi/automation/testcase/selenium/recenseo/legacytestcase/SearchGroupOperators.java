 package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * mantis# 15098
 * @author Ishtiyaq Kamal
 * 
 * DB: DEM0
(1) Go to search page and create a new search with the criteria:
Query Match Operator: AND (all)
Title Contains = developer
Author contains = nobis
Addressee contains = johnson
(2) Run Search and Confirm one document found
(3) Return to search page and add a new "Search Group" from add search criteria. Confirm you can move title and author into this new group by clicking and dragging them. Set Group Match Operator to OR (any)
(4) Run search. Confirm more results are now found.
(5) Return to search page and set Group Match Operator to AND. Toggle AND/OR (click it), and make sure it toggles, and toggles the between criteria text.
(6) Run search. Confirm results match original results.
 *
 */

public class SearchGroupOperators extends TestHelper{
    @Test
    public void test_c62_SearchGroupOperators(){
    	//loginToRecenseo();
    	handleSearchGroupOperators(driver);
    }


    protected void handleSearchGroupOperators(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0); 
    	SearchPage.setWorkflow("Search");
		
    	SearchPage.addSearchCriteria("Title");
    	SearchPage.addSearchCriteria("Author");
    	SearchPage.addSearchCriteria("Addressee");
    	
    	SearchPage.setCriterionValue(0, "developer");
    	SearchPage.setCriterionValue(1, "nobis");
    	SearchPage.setCriterionValue(2, "johnson");
    	
    	performSearch();
    	
    	int totalDocumentFound = DocView.getDocmentCount();
    	
    	softAssert.assertEquals(totalDocumentFound, 1, "1) Run Search and Confirm one document found:: ");
    	
    	SearchPage.goToDashboard();
    	
    	waitForFrameToLoad(Frame.MAIN_FRAME);
    	
    	SearchPage.addSearchCriteria("Search Group");
    	
    	Actions actions = new Actions(driver);
    	
    	WebElement titleElement = getElement(By.id("criterion2"));
    	WebElement authorElement = getElement(By.id("criterion3"));
    	
    	WebElement groupLocation = getElement(By.id("criterion5"));
    	
    	actions.dragAndDrop(titleElement, groupLocation).perform();
    	actions.dragAndDrop(authorElement, groupLocation).perform();
    	
    	tryClick(By.cssSelector("#criterion5 .MatchOperator"), 2);
    	
    	performSearch();
    	
    	totalDocumentFound = DocView.getDocmentCount();
    	
    	softAssert.assertNotEquals(totalDocumentFound, 1, "3) More than 1 results are now found:: ");
    	
    	SearchPage.goToDashboard();
    	
    	waitForFrameToLoad(Frame.MAIN_FRAME);
    	tryClick(By.cssSelector("#criterion3 .MatchOperator"), 2);
    	
    	performSearch();
    	
    	totalDocumentFound = DocView.getDocmentCount();
    	
    	softAssert.assertEquals(totalDocumentFound, 1, "5) Results match original results:: ");
    	
    	softAssert.assertAll();		
	}
}
