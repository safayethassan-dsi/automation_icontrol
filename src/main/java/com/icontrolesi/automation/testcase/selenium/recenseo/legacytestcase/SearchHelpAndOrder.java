package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;


/**
 * @author atiq,ishtiyaq
 */
/*
 *
 * DB: DEM4
(1) Go to search page and Run a new search: Author contains "ryan"
(2) As you type "ryan" confirm suggested terms are those that start with the same string of characters (confirm no negative number appears with suggested terms).
(3) Run search and confirm author field has the correct value.
(4) Re-run this search with a different previously suggested term for "ryan" and confirm results.
(5) Re-run this search with Sort By: Add = Author and Ascending
(6) Confirm data is sorted correctly in Author field.
(7) Re-run this search and change Sort By: Add = Document ID and Descending.
(8) Confirm data is sorted correctly in document ID field.
 */
public class SearchHelpAndOrder extends TestHelper{
	String searchTerm = "ryan";
    @Test
    public void test_c102_SearchHelpAndOrder(){
    	//loginToRecenseo();
    	handleSearchHelpAndOrder(driver);
    }


    protected void handleSearchHelpAndOrder(WebDriver driver){
        new SelectRepo(AppConstant.DEM4);
        
        DocView.openPreferenceWindow();
        DocView.checkSelectedColumns("Author", "Document ID");
        DocView.closePreferenceWindow();
       
        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Author");
        SearchPage.setCriterionValue(0, searchTerm);
        waitFor(2);
       
        List<String> auto_data_items= getListOfItemsFrom(By.cssSelector("ul.ui-autocomplete li"), s -> getText(s).toLowerCase());
        
        boolean isSuggestionOk = auto_data_items.stream().allMatch(s -> s.contains("ryan")); 
       
        softAssert.assertTrue(isSuggestionOk, "(2)As you type [ryan] confirm suggested terms are those that start with the same string of characters:: ");
      
        SearchPage.setSortCriterion();
        SearchPage.setSortCriterionValue(1, "Author");
        
        performSearch();
       
        boolean isSearchOk = getListOfItemsFrom(DocView.documentAuthorColumnLocator, a -> getText(a).toLowerCase()).stream()
        						.allMatch(a -> a.contains(searchTerm));
        
        softAssert.assertTrue(isSearchOk, "(3)Run search and confirm author field has the correct value:: ");
       
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
      
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("Author");
        SearchPage.setCriterionValue(0, searchTerm);
        
        performSearch();
        
        System.out.println( Logger.getLineNumber() + getListOfItemsFrom(DocView.documentAuthorColumnLocator, a -> getText(a).toLowerCase()));
        
        isSearchOk = getListOfItemsFrom(DocView.documentAuthorColumnLocator, a -> getText(a).toLowerCase()).stream()
				.allMatch(a -> a.contains(searchTerm));

        softAssert.assertTrue(isSearchOk, "(4)Run search and confirm author field has the correct value:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("Author");
        SearchPage.setCriterionValue(0, searchTerm);
        SearchPage.setSortCriterion();
        SearchPage.setSortCriterionValue(1, "Author");
     
        performSearch();
        
        isSearchOk = Utils.isAscendingOrdered(getListOfItemsFrom(DocView.documentAuthorColumnLocator, a -> getText(a)));
				
        softAssert.assertTrue(isSearchOk, "(4)Run search and confirm author field has the correct value:: ");

        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.clearSearchCriteria();
        SearchPage.addSearchCriteria("Author");
        SearchPage.setCriterionValue(0, searchTerm);
        SearchPage.setSortCriterion();
        SearchPage.setSortCriterionValue(1, "Author");
        SearchPage.setSortCriterionOrder(1, "Descending");
     
        performSearch();
        
        System.out.println( Logger.getLineNumber() + getListOfItemsFrom(DocView.documentAuthorColumnLocator, a -> getText(a)));
        isSearchOk = Utils.isDescendingOrdered(getListOfItemsFrom(DocView.documentAuthorColumnLocator, a -> getText(a).toLowerCase()));
				
        softAssert.assertTrue(isSearchOk, "(7) Run search and confirm author field has the correct value:: ");

        softAssert.assertAll();
    }   
}