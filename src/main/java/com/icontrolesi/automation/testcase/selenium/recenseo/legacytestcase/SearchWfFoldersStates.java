package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;


/**
 * mantis# 15099
 * @author Ishtiyaq Kamal
 * DB: DEM0
(1) Go to search page. Under folders section check the folder "Email/Foster, Ryan e-mail" and click open.
(2) Verify all 36 docs display as expected.
(3) Return to the search page and add the criteria: Workflow State: Document is in = Privilege Review.
(4) Run search & Confirm documents that are not in privilege review are removed from results
 *
 */

public class SearchWfFoldersStates extends TestHelper {
	By criterionOperator = By.className("CriterionOperator");
	
	@Test
	public void test_c61_SearchWfFoldersStates(){
		handleWfFoldersStates(driver);
	}
    
    @SuppressWarnings("unused")
	protected void handleWfFoldersStates(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.clickUserMenu();
    	
    	if(!getElements(By.cssSelector("#workflows a")).stream().anyMatch(item -> getText(item).equals("Privilege Review"))){
    		SprocketMenu.openUserManager();
    		UserManager.loadUserSettings(AppConstant.USER);
    		UserManager.setWorkflowStateAccess("Privilege Review", UserManager.ASSIGN);
    		SearchPage.goToDashboard();
    		SelectRepo.selectRepoFromSecondaryMenu("Recenseo Demo DB");    	
    		switchToDefaultFrame();
    	}
    	
    	SearchPage.closeUserMenu();
    	
    	SearchPage.setWorkflow("Search");
 
 		FolderAccordion.open(); 
 		FolderAccordion.expandFolder("Email");
 		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
 		
	 	String doc_count_all_workflows=""; 
	 
	 	int docCount = DocView.getDocmentCount();
	 	
	 	softAssert.assertEquals(docCount, 36, "(2) Verify all 36 docs display as expected:: ");
	 	
	 	DocView.openWFSManagerWindow();
	 
	 	int dcPrevReviewWorkFlowMan = DocView.getDocumentsCountForWFState("Privilege Review");
	 			
	 	SearchPage.goToDashboard();
	 	
	 	waitForFrameToLoad(Frame.MAIN_FRAME);
	 	
	 	SearchPage.addSearchCriteria("Workflow");
	 	selectFromDrowdownByText(getElements(criterionOperator).get(2), "Privilege Review");
	 	
	 	performSearch();
	 	
	 	int doc_count_privilege_review = DocView.getDocmentCount();
	 	
	 	DocView.openWFSManagerWindow();
	 	
	 	int totalDocInPrivReview = DocView.getDocumentCountForWFS("Privilege Review");
	 	
	 	System.out.println( Logger.getLineNumber() + totalDocInPrivReview + "*****");
	 	
	 	List<String> rowStateValues = getListOfItemsFrom(DocView.gridRowLocator, item -> getAttribute(item, "class"));
	 	
	 	int outOfWFS = 0;
	 	for(String classVal : rowStateValues){
	 		System.out.println( Logger.getLineNumber() + (outOfWFS + 1) + ": " +classVal);
	 		if(classVal.endsWith("NotInState"))
	 			outOfWFS++;
	 	}

	    softAssert.assertEquals(doc_count_privilege_review, dcPrevReviewWorkFlowMan, "(4)Run search & Confirm documents that are not in privilege review are removed from results:: ");
	    softAssert.assertEquals(outOfWFS, doc_count_privilege_review - totalDocInPrivReview, "*** Documents that are not in privilege review are removed from results:: ");
	    
	    softAssert.assertAll();
    }
}
