package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * DB: DEM0
(1) Go to search page. Open the Review Assignments Section.
(2) Check the select all box and Select Add to search criteria from the Review tab
(3) When added to search criteria, make sure all assignments added.
(4) De-Select All and confirm all are unselected
 * 
 * 
 *
 */

public class SelectAllCheckbox extends TestHelper{
   @Test
   public void test_c1656_SelectAllCheckbox(){
	   //loginToRecenseo();
	   handleSelectAllCheckbox(driver);
   }
    

   protected void handleSelectAllCheckbox(WebDriver driver) {
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.selectAllFilterByStatus();
		ReviewAssignmentAccordion.selectAllFilterByUsers();
		ReviewAssignmentAccordion.openReviewTab();
		ReviewAssignmentAccordion.selectAllAssignment();
		ReviewAssignmentAccordion.addSelectedToSearch();
		
		int totalVisibleAssignment = ReviewAssignmentAccordion.getVisibleAssignmentCount();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		String addedAssignmentsAsCriteria = getText(By.cssSelector(".CriterionContent div.CriterionContent"));
		
		int totalAssignmentAddedInCriteria = addedAssignmentsAsCriteria.split(",").length;
		
		softAssert.assertEquals(totalAssignmentAddedInCriteria, totalVisibleAssignment, "3) All Assignments have been added to search criteria:: ");
		
		waitForFrameToLoad(Frame.REVIEW_ASSIGNMENTS_FRAME);
		
		ReviewAssignmentAccordion.deSelectAllAssignment();
		
		long deselectedAssignmentsCount = getElements(ReviewAssignmentAccordion.visibleAssignmentLocator).stream().filter(a -> !a.isSelected()).count();
		
		softAssert.assertEquals(deselectedAssignmentsCount, totalVisibleAssignment, "4) Unchecking 'Select All' de-selects all the assignments:: ");
		
		softAssert.assertAll();
	}
}
