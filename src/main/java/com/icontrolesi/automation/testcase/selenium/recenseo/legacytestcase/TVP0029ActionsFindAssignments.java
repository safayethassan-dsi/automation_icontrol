package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageReviewAssignment;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class TVP0029ActionsFindAssignments extends TestHelper{
    @Test
    public void test_c215_TVP0029ActionsFindAssignments(){
    	//loginToRecenseo();
    	handleTVP0029ActionsFindAssignments(driver);
    }
    


    protected void handleTVP0029ActionsFindAssignments(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
		new DocView().clickActionMenuItem("Manage Review Assignments");
		switchToFrame(1);
		
		ReviewAssignmentAccordion.selectAllFilterByStatus();
		ReviewAssignmentAccordion.selectAllFilterByUsers();
		ManageReviewAssignment.gotoFindPanel();
		ManageReviewAssignment.addFilter("Assigned To");
		ManageReviewAssignment.addFilterCriteriaText(1, AppConstant.USER);
		ManageReviewAssignment.clickFindAssignment();
	 	
	  	List<String> assigneeList = getListOfItemsFrom(By.className("assignee"), assignee -> assignee.getText().trim());
	  	
	  	for(String assignee : assigneeList){
	  		softAssert.assertEquals(assignee, AppConstant.USER, "6) Assignments are filtered to only include those assigned to the specified user:: ");
	  	}
	  	
	  	ManageReviewAssignment.clickClearFilteredAssignment();
	  	ManageReviewAssignment.addFilter("Priority");
	
	  	checkForPriorityValues("1");
	  	
	  	checkForPriorityValues("2");
	  	
	  	checkForPriorityValues("3");
		
		softAssert.assertAll();
	}
    
    public void checkForPriorityValues(String priority){
    	System.out.println( Logger.getLineNumber() + "Checking filtering for priority value: " + priority);
    	ManageReviewAssignment.addFilterCriteriaText(1, priority);
		ManageReviewAssignment.clickFindAssignment();
		
		List<String> priorityList = getListOfItemsFrom(By.cssSelector("span[class^='priority priority-']"), p -> p.getText().trim());
		
		for(String p : priorityList){
	  		softAssert.assertEquals(p, priority, "10) Assignments with the selected priority are shown (Priority == " + priority + "):: ");
	  	}
    }
}
