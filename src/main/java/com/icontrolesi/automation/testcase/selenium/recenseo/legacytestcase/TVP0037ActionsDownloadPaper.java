package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Download;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class TVP0037ActionsDownloadPaper extends TestHelper{
@Test
public void test_c106_TVP0037ActionsDownloadPaper(){
	//loginToRecenseo();
	handleTVP0037ActionsDownloadPaper(driver);
}


	private void handleTVP0037ActionsDownloadPaper(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
	
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.selectFolder("Email");
		FolderAccordion.addSelectedToSearch();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria("Document Name");
		SearchPage.setOperatorValue(1, "is not empty");
		
		performSearch();
	    
		DocView.selectAllDocuments();
		
	    new DocView().clickActionMenuItem("Prepare Download");
	    
	    switchToFrame(1);
	    //switchToFrame(getElement(By.cssSelector("iframe[src^='javascript']")));
	    
	    Download.openFileTypePrefs();
	    Download.setDownloadPreferenceAllTo("Imaged");
	    Download.closeFileTypePrefs();
		
		new DocView().clickSubmitButton();
		
		String downloadConfirmationMsg = getText(By.tagName("body"));
			
	    softAssert.assertTrue(downloadConfirmationMsg.contains("Recenseo is preparing your download in the background."), "3) Submit the job:: ");
	    
	    tryClick(By.xpath("//a[text()='Click HERE to go directly to \"Downloads\" now']"), By.className("BlueAnchor"));
	    waitFor(driver, 15);
	    
	    List<WebElement> downloadLinks =  getElements(By.className("BlueAnchor"));
	    
	    WebElement downloadableLink = downloadLinks.get(downloadLinks.size() - 1);
	    
	    String downloadableLinkName;
	    
	    if(Configuration.getConfig("selenium.browser").equals("chrome")){
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":", "-");
	    }else{
	    	downloadableLinkName = getText(downloadableLink).replaceAll(":", "_");
	    }
	    
	    System.out.println( Logger.getLineNumber() + "FileName: " + downloadableLinkName);
	    
	    String downloadableFile = AppConstant.DOWNLOAD_DIRECTORY + downloadableLinkName;
	    String extractedFolderLocation = downloadableFile.replaceAll("\\.zip", "");
	    
	    downloadableLink.click();
	     
	    Download.waitForFileDownloadToComplete(downloadableFile, 1800);
	    
	    unzip(downloadableFile, extractedFolderLocation);
	    
	    File [] listOfFiles = new File(extractedFolderLocation).listFiles();
	    
	    boolean isDownloadedContentsOk = true;
	    for(File file : listOfFiles){
	    	String fileName = file.getName();
	    	System.err.println(fileName+"&&&&&&&&&&&");
	    	if(!(fileName.endsWith(".pdf") || fileName.equals("CDOC_SUMMARY.TXT"))){
	    		isDownloadedContentsOk = false;
	    		break;
	    	}
	    }
	    
	    
	    FileMethods.deleteFileOrFolder(downloadableFile);
	    FileMethods.deleteFileOrFolder(extractedFolderLocation);
		
		softAssert.assertTrue(isDownloadedContentsOk,  "4) Documents that have images downloaded as PDFs:: ");
		
		softAssert.assertAll();
	}
}
