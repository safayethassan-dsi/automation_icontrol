package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 *
 * @author Shaheed Sagar
 *
 *         "DB:DEM0
 * 
 *         1) On the Search screen, open the folder Email\Foster, Ryan Email. If
 *         all of the documents are not in the current review queue (wf state),
 *         add them, before continuing with this test. 2) From Grid View select
 *         25 documents 3) Go to Actions\Global Edit 4) Text Field (like
 *         Comment) - Perform and verify each of the following Global Edit
 *         tests: 4a) Replace all Data - Set Comment to ""Mary had a little""
 *         (removing all previous comment data) 4b) Append - add the value
 *         ""lamb"" to make the comment ""Mary had a little lamb"" 4c) Prepend -
 *         add the value ""My friend"" to make the comment ""My friend Mary had
 *         a little lamb"" 4d) Find/Replace - Use ""little lamb"" and ""large
 *         frog"" to make the comment ""My friend Mary had a large frog""
 * 
 *         5) Multi-pick (Issue codes are generally a good example) - Perform
 *         and verify each of the following Global Edit tests: 5a) Replace
 *         existing data with Selected Values 5b) Remove Selected Values from
 *         Existing Data 5c) Remove All Values from Existing Data 5d) Append
 *         Selected Values to Existing Data
 * 
 *         6) Radio button (Responsive might be a good example) - Perform and
 *         verify each of the following Global Edit tests: 6a) Use the 'Remove
 *         value' check box to blank out all data 6b) Select another value to
 *         update the documents
 * 
 *         In addition to the variations above your testing must also verify: -
 *         You can perform global edits on multiple fields at one time - The
 *         Global edit summary for each test accurately reflects what you are
 *         attempting to do - On the Summary page, the "Complete Search"
 *         checkbox will move the selected documents to the next workflow state.
 *         Does not work when performed in Search workflow.
 */

public class TVP0038ActionsGlobalEdit extends TestHelper{
	@Test
	public void test_c174_TVP0038ActionsGlobalEdit(){
		handleTVP0038ActionsGlobalEdit(driver);
	}
	
	
	private void handleTVP0038ActionsGlobalEdit(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");

		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		String commentBeforeAnyModification = DocView.getComment();
		
		DocView.setNumberOfDocumentPerPage(25);
		
		DocView.selectAllDocuments();

		DocView.openGlobalEditWindow();
		
		DocView.GlobalEdit.addAttributesForGlobalEdit("Comment (comm)");
		
		selectFromDrowdownByText(By.cssSelector("#fieldEditContainer > fieldset > div > select"), "Replace All Data");
		editText(By.name("comm"), "Mary had a little");
		DocView.GlobalEdit.clickNextButton();
		DocView.GlobalEdit.clickConfirmButton();
		
		
		
		/*tryClick(By.xpath("//span[text()='Go Back To Edit Page']"), By.id("editField"));
		
		addAttributesForGlobalEdit("Issues (issu)");
		tryClick(By.cssSelector("#fieldEditContainer div[id] ul a i"));
		clickNextButton();
		clickConfirmButton();
		
		
		DocView.clickCrossBtn();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int documentToSelect = getRandomNumberInRange(1, 25);
		
		DocView.clickOnDocument(documentToSelect);
		
		String replacedComment = DocView.getComment();
		
		
		softAssert.assertNotEquals(commentBeforeAnyModification, replacedComment, "4a) 'Replace all Data' worked (Previous data changed):: ");
		softAssert.assertEquals(replacedComment, "Mary had a little", "4a) 'Replace all Data' worked (New data have been written):: ");
		*/
		
		softAssert.assertAll();
	}
	

}
