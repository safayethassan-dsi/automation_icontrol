package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

/**
 * mantis# 15519
 * @author Ishtiyaq Kamal
 * 
 * DB: DEM0
(1) Go to search page and open tags section.
(2) Left mouse click on a Tag, and then from the TAGS INFO note the number of docs in the Tag
(3) Close tag info and check the box next to the same tag and click open.
(4) Confirm same number of documents return in Grid view.
(5) Repeat process on any sub tags of the current tag
 *
 */

public class Tags extends TestHelper{
   
	@Test
	public void test_c60_Tags(){
		handleTags(driver);
	}

	    
   protected void handleTags(WebDriver driver) {
	 	new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		TagAccordion.openTagInfoDetail();
		
		long docCountInTagInfo =  TagAccordion.getTotalDocsCount();
	  
		TagAccordion.closeTagInfoDetail();
	
		TagAccordion.selectFirstTag();
		TagAccordion.openSelected();
		
		long docCountInGridView = DocView.getDocmentCount();
		
	    Assert.assertTrue((docCountInTagInfo == docCountInGridView), "(4)Confirm same number of documents return in Grid view:: ");
   }
}
