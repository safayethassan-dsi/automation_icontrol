package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Three Users (or three User IDs, an ""Admin"" ID and two ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm that one ""Test User"" ID is on the same Team as the ""Admin"" ID and that the other ""Test User"" ID is on a different team. 
	(4) Return to the Search Page and Open the Paper Documents Folder 
	(5) Select any document and select the ""Annotate"" button below the image 
	(6) Create an annotation on the document and choose for it to be Accessible by Team members 
	(7) Confirm that the ""Test User"" ID on the same team can view the annotation on that document 
	(8) Confirm that the ""Test User"" ID on the different team cannot view the annotation on that document 
	(9) Right click on the original annotation to ""Edit Comment/Reason"" and change the annotation to be Accessible by Everyone 
	(10) Confirm that both ""Test User"" ID can now view the Annotation"

*/

public class TeamRestrictionsAnnotations extends TestHelper{
   String savedSearchName = "TeamRestrictionsSavedSearches_" + new Date().getTime();
   
   @Test(enabled = false)
   public void test_c380_TeamRestrictionsAnnotations(){
	   //loginToRecenseo();
	   handleTeamRestrictionsAnnotations(driver);
   }

    private void handleTeamRestrictionsAnnotations(WebDriver driver){
   	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists("iceautotest3");
        
        softAssert.assertTrue(isUserExist, "(3) Confirm that one Test User ID is on the same Team as the Admin ID and that the other Test User ID is on a different team:: ");
      
        
        UserManager.selectTeam("DEM0 Team 1");
        
        isUserExist = UserManager.isUserExists("iceautotest4");
        
        softAssert.assertTrue(isUserExist, "(3) Confirm that one Test User ID is on the same Team as the Admin ID and that the other Test User ID is on a different team:: ");
        
        
        
        SearchPage.goToDashboard();
      
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        /*SearchPage.addSearchCriteria(driver, "Author");
    	SearchPage.createSavedSearch(savedSearchName, "", "");*/
        FolderAccordion.open();
        FolderAccordion.openFolderForView("Paper Documents");
    	
       // tryClick(By.xpath("//span[text()='Annotate']"), 1);
        
            
        SearchPage.logout();
        
        performLoginWithCredential("iceautotest2", "1Control%");
        
        new SelectRepo(AppConstant.DEM0);
        SearchPage.setWorkflow("Search");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        int totalSearchResultFound = SearchesAccordion.getSavedSearchCount(savedSearchName);
        
        softAssert.assertEquals(totalSearchResultFound, 1, "5. Confirm that the \"Test User\" ID on the same team can view the Saved Search and Run it to see the documents:: ");
        
        
        SearchPage.logout();
        
        performLoginWithCredential("iceautotest4", "1Control%");
        
        new SelectRepo(AppConstant.DEM0);
        SearchPage.setWorkflow("Search");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        totalSearchResultFound = SearchesAccordion.getSavedSearchCount(savedSearchName);
        
        softAssert.assertEquals(totalSearchResultFound, 0, "6. Confirm that the \"Test User\" ID on the different team cannot view the Saved Search or Run it to see the documents:: ");
       
        softAssert.assertAll();
    }
}
