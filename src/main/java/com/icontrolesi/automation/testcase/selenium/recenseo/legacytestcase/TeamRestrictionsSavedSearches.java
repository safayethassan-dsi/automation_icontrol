package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Three Users (or three User IDs, an ""Admin"" ID and two ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm that one ""Test User"" ID is on the same Team as the ""Admin"" ID and that the other ""Test User"" ID is on a different team. 
	(4) Return to the Search Page and Create a new Search that is Saved for Team Members Only 
	(5) Confirm that the ""Test User"" ID on the same team can view the Saved Search and Run it to see the documents 
	(6) Confirm that the ""Test User"" ID on the different team cannot view the Saved Search or Run it to see the documents 
	(7) Return to the Search Page and create a new Saved Search with the same name that is Public 
	(8) Confirm that you are prompted to overwrite existing search 
	(9) Confirm that both ""Test User"" ID can now view the Saved Search and Run it to see the documents"

*/

public class TeamRestrictionsSavedSearches extends TestHelper{
   String user3 = AppConstant.ICEAUTOTEST_3;
   String user4 = AppConstant.ICEAUTOTEST_4;
   String pass3 = AppConstant.PASS3;
   String pass4 = AppConstant.PASS4;
   
   String savedSearchName = "TeamRestrictionsSavedSearches_" + new Date().getTime();
   
   @Test
	public void test_c1644_TeamRestrictionsSavedSearches(){
		handleTeamRestrictionsSavedSearches(driver);
	}

    private void handleTeamRestrictionsSavedSearches(WebDriver driver){
       	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("DEM0 Admins");
        
        boolean isUserExist = UserManager.isUserExists(user3);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm that one Test User ID is on the same Team as the Admin ID and that the other Test User ID is on a different team:: ");
      
        
        UserManager.selectTeam("DEM0 Team 1");
        
        isUserExist = UserManager.isUserExists(user4);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm that one Test User ID is on the same Team as the Admin ID and that the other Test User ID is on a different team:: ");
        
        SearchPage.goToDashboard();
      
        waitForFrameToLoad(Frame.MAIN_FRAME);
        SearchPage.addSearchCriteria("Author");
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	    
        SearchPage.logout();
        
        performLoginWithCredential(user3, pass3);
        
        new SelectRepo(AppConstant.DEM0);
        SearchPage.setWorkflow("Search");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        int totalSearchResultFound = SearchesAccordion.getSavedSearchCount(savedSearchName);
        
        softAssert.assertEquals(totalSearchResultFound, 1, "5. Confirm that the \"Test User\" ID on the same team can view the Saved Search and Run it to see the documents:: ");
        
        SearchPage.logout();
        
        performLoginWithCredential(user4, pass4);
        
        new SelectRepo(AppConstant.DEM0);
        SearchPage.setWorkflow("Search");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        totalSearchResultFound = SearchesAccordion.getSavedSearchCount(savedSearchName);
        
        softAssert.assertEquals(totalSearchResultFound, 0, "6. Confirm that the \"Test User\" ID on the different team cannot view the Saved Search or Run it to see the documents:: ");
        
     /*   
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        SearchPage.addSearchCriteria("Author");
    	SearchPage.createSavedSearch(savedSearchName, "", "");*/
       
        softAssert.assertAll();
    }
}