package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class TranslateOnFlyMicrosoft extends TestHelper{
public static int warningPresent=0;
public WebElement div;
public String textValueBeforeTranslate,textValueAfterTranslate,textInDocument;
public int matchNum=0;
public String Dictionary="";


@Test
public void test_c245_TranslateOnFlyMicrosoft(){
	//loginToRecenseo();
	handleTranslateOnFlyMicrosoft(driver);
}


	@SuppressWarnings("resource")
	private void handleTranslateOnFlyMicrosoft(WebDriver driver){
		try {
	    	Dictionary = new Scanner( new File("./src/main/resources/extra/Dictionary.txt") ).useDelimiter("\\A").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	
 		new SelectRepo(AppConstant.DEM0);
 		
 		SearchPage.setWorkflow("Search");
 	    SearchPage.addSearchCriteria("Document ID"); 
        SearchPage.setOperatorValue(0, "is in list");
        SearchPage.setCriterionValueTextArea(0, "1225637\n1225635");
        
        performSearch();
        	
        checkTranslation();
        
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
       List<WebElement> documentInList = getElements(By.cssSelector("td[aria-describedby='grid_mimeType']")); 
       
       click(documentInList.get(1)); 
       
       checkTranslation();
       
       softAssert.assertAll();
	}


	public void checkTranslation(){
		DocView.selectView("Text");
	   
	    textValueBeforeTranslate= getText(By.cssSelector("#page0 > pre"));
	    
	    tryClick(By.xpath("//button[text()='Show Translation']"), 15);
	    
	    textValueAfterTranslate = getText(By.cssSelector("#page0 > pre"));
	    System.out.println( Logger.getLineNumber() + textValueAfterTranslate);
	    matchNum=0;
	    
	    String[] splittedText = textValueAfterTranslate.split("\\s+");
	    for(int i=0;i<splittedText.length;i++){
	    	if(Dictionary.contains(splittedText[i])){
	    		System.out.println( Logger.getLineNumber() + "String in Document"+splittedText[i]);
	    		matchNum++;
	    		if(matchNum==5){
	    			break;
	    		}
	    	}
	    }
	    
	    System.out.println( Logger.getLineNumber() + matchNum);
	   
	    softAssert.assertEquals(matchNum, 5, "(2b)Confirm the text is now in English:: ");
	    
	   /* String translatedBy = getText(By.className("attribution"));
	    softAssert.assertTrue((translatedBy.contains("translated by Google") || translatedBy.contains("by Microsoft")), "2(c)Verify Recenseo displays the [Translated by Google] at the bottom of the text page:: ");
	    */
	    tryClick(By.xpath("//button[text()='Show Original']"), 15);
	    
	   div= SearchPage.waitForElement("page0", "id");
	   textValueAfterTranslate = getText(By.cssSelector("#page0 > pre"));
	   System.out.println( Logger.getLineNumber() + textValueAfterTranslate);
	   
	   softAssert.assertEquals(textValueBeforeTranslate.toLowerCase(), textValueBeforeTranslate.toLowerCase(), "2(e) Confirm Text is now in Portuguese or Arabic:: ");
	}
}

