package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;


public class UpdateTeam extends TestHelper{
	String teamName = "test_c1679_UpdateTeam_" + new Date().getTime();
	String updatedTeamName = teamName + "_Edited";
	
	@Test
	public void test_c1679_UpdateTeam(){
		handleUpdateTeam(driver);
	}

	private void handleUpdateTeam(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
		
        SprocketMenu.openUserManager();
		
		UserManager.createTeam(teamName, "Team of Ishtiyaq","USER");
		
		UserManager.updateTeam(teamName, updatedTeamName ,"Team of IshtiyaqEdited","SUPPORT");
		
		List<String> teamList = getItemsValueFromDropdown(By.id("teamSelect"));
		
		int updatedTeamsLocation = IntStream.range(0, teamList.size()).filter(i -> teamList.get(i).equals(updatedTeamName)).findFirst().getAsInt();
		
		int teamToSelect = updatedTeamsLocation > 0 ? updatedTeamsLocation - 1 : 0;
		
		UserManager.selectTeam(teamToSelect);
		UserManager.selectTeam(updatedTeamName);
		
		String[] teamProperties= UserManager.teamProperties(updatedTeamName);
		System.out.println( Logger.getLineNumber() + "Name"+teamProperties[0]);
		System.out.println( Logger.getLineNumber() + "Description"+teamProperties[1]);
		System.out.println( Logger.getLineNumber() + "Name"+teamProperties[0]);
		
		boolean isTeamOk = teamProperties[0].equalsIgnoreCase(updatedTeamName)
							&&teamProperties[1].equalsIgnoreCase("Team of IshtiyaqEdited")
							&&teamProperties[2].equalsIgnoreCase("SUPPORT"); 
		
		UserManager.deleteTeam(updatedTeamName);
			
		Assert.assertTrue(isTeamOk, "(7) Verify that the team's name, description and role properly reflects the changes done previously:: ");
	}
}
