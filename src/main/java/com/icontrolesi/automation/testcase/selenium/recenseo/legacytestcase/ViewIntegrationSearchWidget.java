package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


public class ViewIntegrationSearchWidget extends TestHelper{
	@Test(enabled = true)
	public void test_c223_ViewIntegrationSearchWidget(){
		handleViewIntegrationSearchWidget(driver);
	}


private void handleViewIntegrationSearchWidget(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, "26");
		
		SearchPage.addSearchCriteria("Document Content (Full Text)");
		SearchPage.setCriterionValue(1, "Disposition");
		
		performSearch();
		
		DocView.selectView("Imaged");
		
		String oldPageNumber = DocView.getPageBoxValue();
		
		new DocView().imageMenuClicker("Find Annotations");
		
		List<WebElement> allAs = getElements(By.tagName("a"));
		for(WebElement e : allAs){
			if(e.getText().equalsIgnoreCase("DEM0-0010877")){
				click(e);
				DocView.closeFindAnnotationWindow();
				break;
			}
		}
		
		String newPageNumber = DocView.getPageBoxValue();
		
		softAssert.assertNotEquals(newPageNumber, oldPageNumber, "4a) Right click on the image and select 'Find Annotations' to identify the pages with these markers and click on the listed pages to go directly to each page.  Confirm all markers were identified by looking at other pages:: ");
		
		DocView.selectView("Text");
		
		new DocView().scrollToPageNumber("1");
		
		softAssert.assertEquals(getColorFromElement((By.id("term0")), "background-color"), "#FFB7B7", "4b) Select the Text View and verify 'Disposition' is highlighted in the text:: ");
		
		softAssert.assertAll();
	}
}
