package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

public class AddingNewPicklistValueInCodingTemplate extends TestHelper{
	String picklistName = "AddingNewPicklistValueInCodingTemplate_c1692";
	
	@Test
	public void test_c1692_AddingNewPicklistValueInCodingTemplate(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.switchToWorkflowTemplates();
	    WorkflowFieldManager.setDisPlayType("Priv Reason", "Multi-pick w/ add");
	    
	    SearchPage.gotoViewDocuments();
	    
	    WebElement privReasonElement = getParentOf(getElement(By.xpath("//label[text()='Priv Reason:']")));
	    
	    privReasonElement.findElement(By.cssSelector("span.ui-button-text")).click();
	    privReasonElement.findElement(By.tagName("textarea")).sendKeys(picklistName);
	    privReasonElement.findElement(By.cssSelector(".picklist-add span.ui-button-text")).click();
	    
	    ReviewTemplatePane.expandPicklistFor("Priv Reason");
	    
	    boolean isPicklistCreated = privReasonElement.findElements(By.cssSelector(" li a")).stream()
	    								.anyMatch(picklist -> getText(picklist).equals(picklistName));
	    
	    softAssert.assertTrue(isPicklistCreated, "*** A new value is added to the picklist:: ");
	    
	    SprocketMenu.openWorkflowAndFieldManager();
	    
	    WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
	    WorkflowFieldManager.deletePicklistItem(picklistName);
	    
	    softAssert.assertAll();
	}
}
