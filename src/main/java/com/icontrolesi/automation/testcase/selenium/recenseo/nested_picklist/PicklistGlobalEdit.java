package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

public class PicklistGlobalEdit extends TestHelper{
	String picklistName = "PicklistGlobalEdit_c1694";
	String picklistName02 = picklistName + "_2";
	
	@Test
	public void test_c1694_PicklistGlobalEdit(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Priv Reason");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
		
		WorkflowFieldManager.createParentPicklistItem(picklistName);
		
		hoverOverElement(WorkflowFieldManager.picklistItemLocator);
		
		WorkflowFieldManager.createChildPicklistItem(picklistName, picklistName02);
		
		SearchPage.gotoViewDocuments();
		
		DocView.selectAllDocuments();
		
		DocView.openGlobalEditWindow();
		
		DocView.GlobalEdit.addAttributesForGlobalEdit("Priv Reason (prvr)");
		DocView.GlobalEdit.selectEditAction("Append Selected Values to Existing Data");
		
		getElements(By.cssSelector("li[class$='childNode'] a")).stream()
			.filter(item -> getText(item).equals(picklistName02))
			.findFirst().get().findElement(By.tagName("i")).click();
		
		DocView.GlobalEdit.clickNextButton();
		DocView.GlobalEdit.clickConfirmButton();
		DocView.GlobalEdit.close();
		
		String privReasonValue = picklistName + " ➞ " + picklistName02;
		
		boolean isGlobalEditPerformedCorrectly = getListOfItemsFrom(DocView.documentPrivReasonColumnLocator, item -> getText(item)).stream()
													.allMatch(privReason -> privReason.contains(privReasonValue));
		
		softAssert.assertTrue(isGlobalEditPerformedCorrectly, "*** Nested values should show as (Parent) ➞ (Child):: ");
		
		SprocketMenu.openWorkflowAndFieldManager();
	    
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
	    WorkflowFieldManager.deletePicklistItem(picklistName);
	    WorkflowFieldManager.deletePicklistItem(picklistName02);
				
		softAssert.assertAll();
	}
}
