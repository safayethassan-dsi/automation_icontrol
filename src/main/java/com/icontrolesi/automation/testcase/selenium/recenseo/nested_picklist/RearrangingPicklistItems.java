package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

import com.icontrolesi.automation.platform.util.Logger;

public class RearrangingPicklistItems extends TestHelper{
	String picklistName01 = "CreatingNestedValue_c1687";
	String picklistName02 = "CreatingNestedValue_c1687_2";
	
	@Test
	public void test_c1688_RearrangingPicklistItems(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
		
		System.out.println( Logger.getLineNumber() + getTotalElementCount(WorkflowFieldManager.picklistItemLocator)+"*&*&");
		
		WorkflowFieldManager.createParentPicklistItem(picklistName01);
		WorkflowFieldManager.createParentPicklistItem(picklistName02);
		
		List<WebElement> picklists = getElements(By.cssSelector("#field-picklist li[class*='leaf'] a"));
		System.out.println( Logger.getLineNumber() + picklists.size()+"&&**&&");
		
		String firstPickList = getText(picklists.get(0));
		//String secondPickList = getText(picklists.get(1));
		
		Actions actions = new Actions(driver);
	    actions.clickAndHold(picklists.get(0)).moveToElement(picklists.get(1)).release().build().perform();
	    waitFor(2);
	    
	    List<WebElement> picklistAfterMove = getElements(WorkflowFieldManager.picklistItemLocator);
	    
	    System.out.println( Logger.getLineNumber() + firstPickList+"***");
	    
	    System.out.println( Logger.getLineNumber() + picklistAfterMove.get(0).getText());
	    System.out.println( Logger.getLineNumber() + picklistAfterMove.get(1).getText());
	    
	    boolean isSecondPicklistBeingtParent = getAttribute(getParentOf(picklistAfterMove.get(0)), "class").endsWith("parentNode");
	    boolean isDraggedItemBeingChild = getAttribute(getParentOf(picklistAfterMove.get(1)), "class").endsWith("childNode");
	    
	    softAssert.assertNotEquals(firstPickList, getText(picklistAfterMove.get(0)));
	    softAssert.assertTrue(isDraggedItemBeingChild, "*** The draggred item becomes child under the item it was dropped on:: ");
	    softAssert.assertTrue(isSecondPicklistBeingtParent, "*** The item becomes parent under which the dragged item was dropped:: ");
	    //softAssert.assertNotEquals(firstPickList, getText(picklistAfterMove.get(1)));
	    
	    WorkflowFieldManager.deletePicklistItem(picklistName02);
	    WorkflowFieldManager.deletePicklistItem(picklistName01);
	    
	    softAssert.assertAll();
	}
}
