package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

import com.icontrolesi.automation.platform.util.Logger;

public class SelectingParentValueInCodingTemplate extends TestHelper{
	String picklistName01 = "CreatingNestedValue_c1687";
	String picklistName02 = "CreatingNestedValue_c1687_2";
	
	@Test
	public void test_c1690_SelectingParentValueInCodingTemplate(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
		
		System.out.println( Logger.getLineNumber() + getTotalElementCount(WorkflowFieldManager.picklistItemLocator)+"*&*&");
		
		WorkflowFieldManager.createParentPicklistItem(picklistName01);
		WorkflowFieldManager.createParentPicklistItem(picklistName02);
		
		List<WebElement> picklists = getElements(By.cssSelector("#field-picklist li[class*='leaf'] a"));
		System.out.println( Logger.getLineNumber() + picklists.size()+"&&**&&");
		
		Actions actions = new Actions(driver);
	    actions.clickAndHold(picklists.get(0)).moveToElement(picklists.get(1)).release().build().perform();
	    waitFor(2);
	    
	    WorkflowFieldManager.switchToWorkflowTemplates();
	    WorkflowFieldManager.setDisPlayType("Priv Reason", "Multi-pick w/ add");
	    
	    SearchPage.gotoViewDocuments();
	    
	    ReviewTemplatePane.expandPicklistFor("Priv Reason");
	    
	    WebElement privReasonElement = getParentOf(getElement(By.xpath("//label[text()='Priv Reason:']")));
	    
	    
	    System.out.println( Logger.getLineNumber() + privReasonElement.findElement(By.cssSelector(" li a")).getText() + "**^^^^*");
	    
	    WebElement parentPicklistItem = privReasonElement.findElement(By.xpath("//a[text()='" + picklistName02 + "']"));
	    
	    parentPicklistItem.findElement(By.tagName("i")).click();
	    
	    System.out.println( Logger.getLineNumber() + getParentOf(parentPicklistItem).findElements(By.cssSelector("li[class$='childNode'] a")).size() + "**&**&&");
	    System.out.println( Logger.getLineNumber() + getParentOf(parentPicklistItem).findElements(By.cssSelector("li[class$='childNode'] a")).get(0).getText() + "**&**&&");
	    
	    boolean isAnyChildrenSelected = getParentOf(parentPicklistItem).findElements(By.cssSelector("li[class$='childNode'] a")).stream()
	    									.anyMatch(picklist -> getAttribute(picklist, "class").endsWith("checked"));
	    
	    softAssert.assertFalse(isAnyChildrenSelected, "*** Selecting parent won't select any of the children:: ");
	    
	    SprocketMenu.openWorkflowAndFieldManager();
	    
	    WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
	    WorkflowFieldManager.deletePicklistItem(picklistName02);
	    WorkflowFieldManager.deletePicklistItem(picklistName01);
	    
	    softAssert.assertAll();
	}
}
