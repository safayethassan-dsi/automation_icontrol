package com.icontrolesi.automation.testcase.selenium.recenseo.report.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;

import com.icontrolesi.automation.platform.util.Logger;

public class GeneralReport implements CommonActions{
	 final public static By reportMenuLocator = By.cssSelector("#reports > span");  
	 final public static By summarizationAndTallyReportLocator = By.cssSelector("#reports > ul > li");
	 final public static By scopeSelectionWindowHeaderLocator = By.xpath("//span[text()='Scope and Options']");
	 final public static By summarizationAndTallyReportItemLocator = By.cssSelector("#reports > ul > li > ul > li > a");
	 final public static By scopeSelectorDropdownLocator = By.id("scope");
	 final public static By fieldSelectorDropdownLocator = By.id("col");
	 final public static By scopeSavedSearchForScopeLocator = By.id("scopeSavedSearches");
	 final public static By chartContainerLocator = By.cssSelector("div[id^='highcharts-'].highcharts-container");
	 final public static By cancelBtnLocator = By.xpath("//span[text()='Cancel']");
	 final public static By loadBtnLocator = By.xpath("//span[text()='Load']");
	 final public static By hideCheckboxLocator = By.name("keepOpen");
	 final public static By documentCountLocator = By.id("scope_count");
	 
	 final public static By reportSummaryLocator = By.cssSelector("div.report-summary > span");
	 final public static By reportSelectViewLocator = By.name("report-select-view");
	 final public static By addToSearchBtnLocator = By.name("report-btn-search");
	 final public static By viewDocumentBtnLocator = By.name("report-btn-grid");
	 final public static By refreshBtnLocator = By.name("report-btn-refresh");
	 final public static By reportSummaryDrawerLocator = By.className("report-btn-summary-drawer");
	 	 
	 final public static By loadingReportMsgLocator = By.xpath("//span[text()='Loading Report']");
	 
	 final public static By exportOrSaveBtnLocator = By.name("report-menu-button");
	 
	 final public static By reportSettingBtnLocator = By.name("report-btn-options");
	 
	 final public static List<String> scopeSelectionItems = Arrays.asList("All Documents", "Current Search Results", "Saved Search");
	 
	 @SuppressWarnings("serial")
	final public static Map<String, List<String>> scopeToFieldMapper = new HashMap<String, List<String>>()
			 {{
				 put("All Documents", Arrays.asList("", "Document Date", "Extracted Addressee", "Extracted Author", "Extracted BCC", "Priv Reason", "Relevant"));
				 put("Current Search Results", Arrays.asList("", "Document Date", "Extracted Addressee", "Extracted Author", "Extracted BCC", "Priv Reason", "Relevant"));
				 put("Saved Search",Arrays.asList("", "Document Date", "Extracted Addressee", "Extracted Author", "Extracted BCC", "Priv Reason", "Relevant"));
			 }};
	 
	 final static GeneralReport generalReport = new GeneralReport();
	 
	 public static void selectScope(String scope){
		 generalReport.selectFromDrowdownByText(scopeSelectorDropdownLocator, scope);
		 generalReport.waitFor(Driver.getDriver(), 1);
	 }
	 
	 
	 public static void selectFiledForScope(String fieldValue){
		 generalReport.selectFromDrowdownByText(fieldSelectorDropdownLocator, fieldValue);
		 generalReport.waitFor(Driver.getDriver(), 1);
	 }
	 
	 public static void selectSavedSearchForScope(String savedSearchName){
		 generalReport.selectFromDrowdownByText(scopeSavedSearchForScopeLocator, savedSearchName);
		 generalReport.waitFor(Driver.getDriver(), 2);
	 }
	 
	 public static void openSummarizationAndTallyReportItem(String reportNameToOpen){
		 generalReport.switchToDefaultFrame();
		 generalReport.hoverOverElement(reportMenuLocator);
		 generalReport.hoverOverElement(summarizationAndTallyReportLocator);
		 
		 generalReport.getElements(summarizationAndTallyReportItemLocator)
		 		.stream()
		 		.filter(item -> item.getText().trim().equals(reportNameToOpen))
		 		.findFirst()
		 		.get().click();
		 
		 
		 System.out.println( Logger.getLineNumber() + reportNameToOpen + " clicked...");
		 
		 generalReport.waitForFrameToLoad(Frame.MAIN_FRAME);
		 generalReport.waitForInVisibilityOf(SearchPage.runSearchButton);
	 }	 
	 
	 
	 public static void closeScopeSelectionWindow(){
		 generalReport.tryClick(cancelBtnLocator, 1);
	 }
	 
	 public static void loadReportVisualization(){
		 generalReport.tryClick(loadBtnLocator);
		 generalReport.waitForInVisibilityOf(loadingReportMsgLocator);
		 generalReport.waitForVisibilityOf(documentCountLocator);
		 generalReport.waitFor(5);
	 }
	 
	 
	 public static int getDocumentCount(){
		 return Integer.parseInt(generalReport.getText(documentCountLocator).replaceAll("\\D+", ""));
	 }
	 
	 	 
	 public static void openGeneralTallyReport(){
		 openSummarizationAndTallyReportItem("General Tally");
		 generalReport.waitForVisibilityOf(scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void openCustodianReport(){
		 openSummarizationAndTallyReportItem("Custodian");
		 generalReport.waitForVisibilityOf(scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void openFileTypesReport(){
		 openSummarizationAndTallyReportItem("File Types");
		 generalReport.waitForVisibilityOf(chartContainerLocator);
		 generalReport.waitFor(5);
	 }
	 
	 public static void openDocumentsOverTimeReport(){
		 openSummarizationAndTallyReportItem("Documents Over Time");
		 generalReport.waitForVisibilityOf(scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void openPicklistTallyReport(){
		 openSummarizationAndTallyReportItem("Picklist Tally");
		 generalReport.waitForVisibilityOf(scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void openSearchTermSetsReport(){
		 openSummarizationAndTallyReportItem("Search Term Sets");
		 generalReport.waitForVisibilityOf(Driver.getDriver(), scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void openFolderSummaryReport(){
		 openSummarizationAndTallyReportItem("Folder Summary");
		 generalReport.waitForVisibilityOf(Driver.getDriver(), scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void openDataTypesReport(){
		 openSummarizationAndTallyReportItem("Data Types");
		 generalReport.waitForVisibilityOf(Driver.getDriver(), scopeSelectionWindowHeaderLocator);
	 }
	 
	 public static void checkReportToolbar(){
		 	String reportSummaryMsg = generalReport.getText(reportSummaryLocator);
		 	String reportSelectorType = generalReport.getElement(reportSelectViewLocator).getTagName();
		 	String reportBtnSearchTitle = generalReport.getAttribute(addToSearchBtnLocator, "title");
		 	String reportBtnViewTitle = generalReport.getAttribute(viewDocumentBtnLocator, "title");
		 	String reportBtnRefreshTitle = generalReport.getAttribute(refreshBtnLocator, "title");
		 	
		 	TestHelper testHelper = new TestHelper();
		 	
		 	testHelper.softAssert.assertEquals(reportSummaryMsg, "No selections made");
		 	testHelper.softAssert.assertEquals(reportSelectorType, "select");
		 	testHelper.softAssert.assertEquals(reportBtnSearchTitle, "Add selected criteria to search and go to the dashboard");
		 	testHelper.softAssert.assertEquals(reportBtnViewTitle, "Run search with selected criteria and view documents");
		 	testHelper.softAssert.assertEquals(reportBtnRefreshTitle, "Run search with selected criteria and refresh current report");
		 	
		 	testHelper.softAssert.assertAll();
	 }
	 
	 public static void openScopeSelectionWindow(){
		 generalReport.tryClick(reportSettingBtnLocator);
	 }
}
