package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.document_count;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class DisplayingCorrectDocumentsForChangedScope extends TestHelper{
	 String numberPattern = "(\\d{1,3},)*(\\d{3},)*(\\d{3},)*(\\d{3},)*\\d{1,3}";
		
	 @Test
	 public void test_c79_DisplayingCorrectDocumentsForChangedScope() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Email");
		
		int documentCountForSearch = DocView.getDocmentCount();
		 
		GeneralReport.openGeneralTallyReport();
		
		GeneralReport.selectFiledForScope("Document Date");
		
		GeneralReport.loadReportVisualization();
		
		int totalCount = GeneralReport.getDocumentCount();
		String totalCountString = getText(GeneralReport.documentCountLocator);
		
		softAssert.assertEquals(totalCount, documentCountForSearch, "***) Document count matches the correct number:: ");
		
		softAssert.assertTrue(totalCountString.matches(numberPattern), "***) Document count matches the correct format:: ");
		
		GeneralReport.openScopeSelectionWindow();
		
		GeneralReport.selectScope("All Documents");
		GeneralReport.selectFiledForScope("Document Date");
		
		GeneralReport.loadReportVisualization();
		
		totalCount = GeneralReport.getDocumentCount();
		totalCountString = getText(GeneralReport.documentCountLocator);
		
		softAssert.assertEquals(totalCount, 711527, "***) Document count matches the correct number:: ");
		
		softAssert.assertTrue(totalCountString.matches(numberPattern), "***) Document count matches the correct format:: ");
	
		
		softAssert.assertAll();
	  }
}
