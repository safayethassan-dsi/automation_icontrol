package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.document_count;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class DisplayingCorrectDocumentsForDefaultScope extends TestHelper{
	 String numberPattern = "(\\d{1,3},)*(\\d{3},)*(\\d{3},)*(\\d{3},)*\\d{1,3}";
	
	 @Test
	 public void test_c78_DisplayingCorrectDocumentsForDefaultScope() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openGeneralTallyReport();
		
		GeneralReport.selectFiledForScope("Document Date");
		
		GeneralReport.loadReportVisualization();
		
		int totalCount = GeneralReport.getDocumentCount();
		String totalCountString = getText(GeneralReport.documentCountLocator);
		
		softAssert.assertEquals(totalCount, 711527, "***) Document count matches the correct number:: ");
		
		softAssert.assertTrue(totalCountString.matches(numberPattern), "***) Document count matches the correct format:: ");
		
		softAssert.assertAll();
	  }
}
