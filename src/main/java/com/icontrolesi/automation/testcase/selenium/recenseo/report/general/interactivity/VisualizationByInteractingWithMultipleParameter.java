package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.interactivity;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

import com.icontrolesi.automation.platform.util.Logger;

public class VisualizationByInteractingWithMultipleParameter extends TestHelper{
	 By rectangleLocator = By.cssSelector(".highcharts-series-group rect");
	 
	 @Test
	 public void test_c86_VisualizationByInteractingWithMultipleParameter() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		GeneralReport.openFileTypesReport();
		
		//int totalItemToSelect = getRandomNumberInRange(2, getTotalElementCount(rectangleLocator));
		
		String filledColorBeforeSelection = getColorFromElement(rectangleLocator, "fill");
		tryClick(rectangleLocator, 2);
		String filledColorAfterSelection = getColorFromElement(rectangleLocator, "fill");
		getElements(rectangleLocator).stream().forEach(e -> System.out.println( Logger.getLineNumber() + "Color: " + getColorFromElement(e, "fill")));
		System.out.println( Logger.getLineNumber() + filledColorAfterSelection + ", " + filledColorBeforeSelection);
		
		softAssert.assertNotEquals(filledColorBeforeSelection, filledColorAfterSelection, "*** Selected element's color has been changed:: ");
		softAssert.assertEquals(filledColorAfterSelection, "#AA0000", "Selected element's color is properly changed:: ");
		
		softAssert.assertAll();
	  }
}
