package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.report_load_button;

import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ChangingScopeAndMultipleReportSpecificOptionAndLoad extends TestHelper{
	@Test
	public void test_c81_ChangingScopeAndMultipleReportSpecificOptionAndLoad(){
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Report:: ");
		
		String defaultScopeOfReport = getSelectedItemFroDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(defaultScopeOfReport, "All Documents", "(*) Recenseo launches the Report with Default Scope:: ");
		
		List<String> scopeSelectionListItems = getItemsValueFromDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(scopeSelectionListItems, GeneralReport.scopeSelectionItems, "(*) Scope Selection should be a combo-box with Generic options:: ");
		
		GeneralReport.selectScope("Saved Search");
		waitFor(driver, 5);
		GeneralReport.selectSavedSearchForScope("ztest-DEM0--lotus files");
		GeneralReport.selectFiledForScope("Relevant");
		GeneralReport.loadReportVisualization();
		
		int totalDocuments = GeneralReport.getDocumentCount();
		
		softAssert.assertEquals(totalDocuments, 23, "(*) Recenseo regenerates the report to reflect parameter change :: ");
		
		softAssert.assertAll();
	}
}
