package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.reportvisualization;

import java.util.List;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

import com.icontrolesi.automation.platform.util.Logger;

public class CheckNumberFormatting extends TestHelper{
	 String numberPattern = "(\\d{1,3},)*(\\d{3},)*(\\d{3},)*(\\d{3},)*\\d{1,3}";
	 
	 @Test
	 public void test_c140_CheckNumberFormatting() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		GeneralReport.openFileTypesReport();
		
		String totalCount = getText(GeneralReport.documentCountLocator);
		
		boolean isTotalCountProperlyFormatted = totalCount.matches(numberPattern);
		
		softAssert.assertTrue(isTotalCountProperlyFormatted, "***) Total count is properly formatted(Count is: " + totalCount + "):: "); 
		
		List<String> countList = getListOfItemsFrom(By.cssSelector("g.highcharts-data-label text tspan:first-child"), item -> item.getText().trim());
		System.out.println( Logger.getLineNumber() + "Size: "+ countList.size());
		for(String count : countList){
			System.out.println( Logger.getLineNumber() + count+"***");
			softAssert.assertTrue(count.matches(numberPattern), "***) All numbers are properly formatted ( Count is: " + count + ")::");
		}
		
		softAssert.assertAll();
	  }
}
