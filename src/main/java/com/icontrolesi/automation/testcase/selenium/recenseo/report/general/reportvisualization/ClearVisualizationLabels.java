package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.reportvisualization;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ClearVisualizationLabels extends TestHelper{
	 @Test
	 public void test_c141_ClearVisualizationLabels() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		GeneralReport.openFileTypesReport();
		
		boolean isAllLabeled = getElements((By.className("js-ellipse"))).stream().allMatch(l -> !l.getText().trim().equals(""));

		softAssert.assertTrue(isAllLabeled, "***) All data representations are clearly labeled:: ");
		
		softAssert.assertAll();
	  }
}
