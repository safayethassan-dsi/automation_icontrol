package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.reportvisualization;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class CountForEachVisualizationBar extends TestHelper{
	 @Test
	 public void test_c143_CountForEachVisualizationBar() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		GeneralReport.openFileTypesReport();
		
		long noOfInvalidDocumentCount = getElements(By.className("highcharts-text-outline")).stream()
				.filter(d -> Integer.parseInt(getText(d)) < 0).count();
		
		softAssert.assertEquals(noOfInvalidDocumentCount, 0, "2) The appropriate graph should appear and each bar/slice should appear with a document count:: ");
		
		softAssert.assertAll();
	  }
}
