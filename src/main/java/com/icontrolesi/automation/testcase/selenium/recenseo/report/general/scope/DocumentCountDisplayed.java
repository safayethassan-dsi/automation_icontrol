package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.scope;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class DocumentCountDisplayed extends TestHelper{
	 @Test
	 //@Test(enabled=false)
	 public void test_c136_DocumentCountDisplayed() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		tryClick(By.xpath("//span[text()='Cancel']"), By.id("scope_count"));
		
		int totalDocCount = Integer.parseInt(getText(By.id("scope_count")).replaceAll("\\D+", "")); 
		
		softAssert.assertEquals(totalDocCount, 711527, "(*) Scope Selection should be a combo-box with Generic options:: ");
		
		softAssert.assertAll();
	  }
}
