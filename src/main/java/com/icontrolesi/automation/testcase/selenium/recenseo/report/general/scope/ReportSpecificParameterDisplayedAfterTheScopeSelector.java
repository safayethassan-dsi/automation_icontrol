package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.scope;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ReportSpecificParameterDisplayedAfterTheScopeSelector extends TestHelper{
	 List<String> scopeSelectionItems = Arrays.asList("All Documents", "Current Search Results", "Saved Search");
	
	 @Test
	 public void test_c137_ReportSpecificParameterDisplayedAfterTheScopeSelector() {
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = getText(GeneralReport.scopeSelectionWindowHeaderLocator);
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		List<String> fieldListForScope_AllDocuments = getItemsValueFromDropdown(GeneralReport.fieldSelectorDropdownLocator);
		
		softAssert.assertEquals(fieldListForScope_AllDocuments, GeneralReport.scopeToFieldMapper.get("All Documents"), "Report Specific Parameters should be displayed down to the Scope Selector (All Documents):: ");
		
		selectFromDrowdownByText(GeneralReport.scopeSelectorDropdownLocator, "Current Search Results");
		List<String> fieldListForScope_CurrentSearchResults = getItemsValueFromDropdown(GeneralReport.fieldSelectorDropdownLocator);
		
		softAssert.assertEquals(GeneralReport.scopeToFieldMapper.get("Current Search Results"), fieldListForScope_CurrentSearchResults, "Report Specific Parameters should be displayed down to the Scope Selector (Current Search Results):: ");
		
		selectFromDrowdownByText(GeneralReport.scopeSelectorDropdownLocator, "Saved Search");
		List<String> fieldListForScope_SavedSearch = getItemsValueFromDropdown(GeneralReport.fieldSelectorDropdownLocator);
		
		softAssert.assertEquals(fieldListForScope_SavedSearch, GeneralReport.scopeToFieldMapper.get("Saved Search"), "Report Specific Parameters should be displayed down to the Scope Selector (Saved Search):: ");
		
		softAssert.assertAll();
	  }
}
