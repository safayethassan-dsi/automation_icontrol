package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.scope;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ReportsDefaultScopeWhenNoSearchPerformed_AllDocuments extends TestHelper{
	 @Test
	 public void test_c76_ReportsDefaultScopeWhenNoSearchPerformed_AllDocuments() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		String defaultScopeOfReport = getSelectedItemFroDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(defaultScopeOfReport, "All Documents", "(*) Recenseo launches the Report with Default Scope:: ");
		
		softAssert.assertAll();
	  }
}
