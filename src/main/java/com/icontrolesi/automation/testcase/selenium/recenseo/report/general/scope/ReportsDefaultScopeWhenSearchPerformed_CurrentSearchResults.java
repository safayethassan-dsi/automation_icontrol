package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.scope;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ReportsDefaultScopeWhenSearchPerformed_CurrentSearchResults extends TestHelper{
	 @Test
	 public void test_c77_ReportsDefaultScopeWhenSearchPerformed_CurrentSearchResults() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Loose Files");
		
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		String defaultScopeOfReport = getSelectedItemFroDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(defaultScopeOfReport, "Current Search Results", "(*) Recenseo launches the Report with Default Scope:: ");
		
		softAssert.assertAll();
	  }
}
