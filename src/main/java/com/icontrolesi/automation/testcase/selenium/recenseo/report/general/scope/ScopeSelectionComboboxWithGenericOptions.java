package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.scope;

import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ScopeSelectionComboboxWithGenericOptions extends TestHelper{
	 
	 @Test
	 public void test_c135_ScopeSelectionComboboxWithGenericOptions() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Report:: ");
		
		List<String> scopeSelectionListItems = getItemsValueFromDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(scopeSelectionListItems, GeneralReport.scopeSelectionItems, "(*) Scope Selection should be a combo-box with Generic options:: ");
		
		softAssert.assertAll();
	  }
}
