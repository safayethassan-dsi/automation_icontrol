package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.scope_selection;

import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class SettingTheScopeToSavedSearches extends TestHelper{
	 String savedSearcForSelectedScope = "ztest-DEM0--lotus files";
	 @Test
	 public void test_c74_SettingTheScopeToSavedSearches() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		SearchesAccordion.open();
		SearchesAccordion.openSavedSearchForView(savedSearcForSelectedScope);
		
		int totalDocumentsInSearch = DocView.getDocmentCount();
		 
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		String defaultScopeOfReport = getSelectedItemFroDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(defaultScopeOfReport, "Current Search Results", "(*) Recenseo launches the Report with Default Scope:: ");
		
		List<String> scopeSelectionListItems = getItemsValueFromDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(scopeSelectionListItems, GeneralReport.scopeSelectionItems, "(*) Scope Selection should be a combo-box with Generic options:: ");
		
		GeneralReport.selectScope("Saved Search");
		waitFor(driver, 5);
		GeneralReport.selectSavedSearchForScope(savedSearcForSelectedScope);
		GeneralReport.selectFiledForScope("Relevant");
		GeneralReport.loadReportVisualization();
		
		int totalDocuments = GeneralReport.getDocumentCount();
		
		softAssert.assertEquals(totalDocuments, totalDocumentsInSearch, "(*) Recenseo regenerates the report to reflect parameter change :: ");
		
		softAssert.assertAll();
	  }
}
