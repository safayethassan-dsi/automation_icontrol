package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.selection_summary;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class IsAtTheBottomOfThePage extends TestHelper{
	
	@Test
	public void test_c147_IsAtTheBottomOfThePage(){
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openFileTypesReport();
		
		String selectionSummaryBefore = getText(GeneralReport.reportSummaryLocator);
		
		softAssert.assertEquals("No selections made", selectionSummaryBefore, "3) There should be text at the bottom of the screen that says \"No selections made\" (Before selection):: ");
		
		List<WebElement> bars = getElements(By.cssSelector(".highcharts-series-group > g rect"));
		
		int selectionNumber = getRandomNumberInRange(1, bars.size());
		
		for(int i = 1; i <= selectionNumber; i++){
			bars.get(i-1).click();
		}
		
		String reportSummary = getText(GeneralReport.reportSummaryLocator);
		
		softAssert.assertEquals(selectionNumber + " Criteria Selected", reportSummary, "3) There should be text at the bottom of the screen that says \"X criteria selected\":: ");
		
		softAssert.assertAll();	
	}
}
