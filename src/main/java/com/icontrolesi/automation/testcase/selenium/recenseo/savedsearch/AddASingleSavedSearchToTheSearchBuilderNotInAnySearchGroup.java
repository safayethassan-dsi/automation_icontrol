package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class AddASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup extends TestHelper{
	String savedSearchName = "AddASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup_" + new Date().getTime();
	
	String stepMsg7 = "7. Recenseo populates all the search criteria of the selected 'Saved' search on the Search Page(%s):: ";
	
	@Test
	public void test_c13_AddASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup(){
		handleAddASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup();
	}
	
	private void handleAddASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.addSelectedToSearch(savedSearchName);
    	
    	switchToDefaultFrame();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	String queryDescription = getText(SearchPage.queryDescriptLocator);
    	
    	String criterionFieldName = getText(By.cssSelector(".CriterionField > span"));
    	
    	String criterionOperator = getSelectedItemFroDropdown(By.className("CriterionOperator"));
    	
    	String criterionValue = getText(By.cssSelector("input.CriterionValue"));
    	
    	softAssert.assertEquals(queryDescription, savedSearchName, String.format(stepMsg7, "Saved Search Name"));
    	softAssert.assertEquals(criterionFieldName, "Author", String.format(stepMsg7, "Criterion Field name"));
    	softAssert.assertEquals(criterionOperator, "contains", String.format(stepMsg7, "Criterion operator"));
    	softAssert.assertEquals(criterionValue, "", String.format(stepMsg7, "Criterion value"));
    	
    	SearchesAccordion.open();
    	SearchesAccordion.deleteSavedSearch(savedSearchName);
    	
    	softAssert.assertAll();
	}
}
