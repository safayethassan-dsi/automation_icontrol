package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class AddMultipleSavedSearchToTheSearchBuilderInSearchGroup extends TestHelper{
	long timeStamp = new Date().getTime();
	String savedSearchName = "AddMultipleSavedSearchToTheSearchBuilderInSearchGroup_" + timeStamp;
	String savedSearchName2 = "AddMultipleSavedSearchToTheSearchBuilderInSearchGroup_" + timeStamp + "_2";
	String savedSearchGroupName = "SavedSearchDemoGroup";
	
	String stepMsg7 = "7. Recenseo populates all the search criteria of the selected 'Saved' search on the Search Page(%s):: ";
	
	@Test
	public void test_c16_AddMultipleSavedSearchToTheSearchBuilderInSearchGroup(){
		handleAddMultipleSavedSearchToTheSearchBuilderInSearchGroup();
	}
	
	private void handleAddMultipleSavedSearchToTheSearchBuilderInSearchGroup(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	switchToDefaultFrame();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	SearchPage.addSearchCriteria("Author");
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchPage.clearSearchCriteria();
    	
    	SearchPage.addSearchCriteria("Addressee");
    	SearchPage.createSavedSearch(savedSearchName2, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	tryClick(By.cssSelector("#searches-picklist > ul > li > a > i"));
    	tryClick(By.cssSelector("#searches-picklist > ul > li:nth-child(2) > a > i"));
    	SearchesAccordion.addSelectedToSearch();
    	
    	switchToDefaultFrame();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	String [] queryDescriptions = getText(By.id("queryDescription")).split(",");
    	
    	List<WebElement> criterionFieldNames = getElements(By.cssSelector(".CriterionField > span"));
    	
    	List<WebElement> criterionOperators = getElements(By.className("CriterionOperator"));
    	
    	List<WebElement> criterionValues = getElements(By.cssSelector("input.CriterionValue"));
    	
    	String joiningOperator = getText(By.cssSelector("#searchForm > div:nth-child(3) > div:nth-child(4)  > div:nth-child(4)  > span"));
    
    	softAssert.assertEquals(queryDescriptions[0], savedSearchName, String.format(stepMsg7, "Saved Search Name for First Saved Search"));
    	softAssert.assertEquals(getText(criterionFieldNames.get(0)), "Author", String.format(stepMsg7, "Criterion Field name for First Saved Search"));
    	softAssert.assertEquals(getSelectedItemFroDropdown(criterionOperators.get(0)), "contains", String.format(stepMsg7, "Criterion operator for First Saved Search"));
    	softAssert.assertEquals(getText(criterionValues.get(0)), "", String.format(stepMsg7, "Criterion value for First Saved Search"));
    	
    	softAssert.assertEquals(queryDescriptions[1], savedSearchName2, String.format(stepMsg7, "Saved Search Name for Second Saved Search"));
    	softAssert.assertEquals(getText(criterionFieldNames.get(1)), "Addressee", String.format(stepMsg7, "Criterion Field name for Second Saved Search"));
    	softAssert.assertEquals(getSelectedItemFroDropdown(criterionOperators.get(1)), "contains", String.format(stepMsg7, "Criterion operator for Second Saved Search"));
    	softAssert.assertEquals(getText(criterionValues.get(1)), "", String.format(stepMsg7, "Criterion value for Second Saved Search"));
    
    	softAssert.assertEquals(joiningOperator, "OR", "Joining operator for criteria should be OR:: ");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.deleteSavedSearch(savedSearchName);
    	SearchesAccordion.deleteSavedSearch(savedSearchName2);
    	
    	softAssert.assertAll();
	}
}
