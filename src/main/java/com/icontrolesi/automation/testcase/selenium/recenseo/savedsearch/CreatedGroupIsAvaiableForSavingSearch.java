package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class CreatedGroupIsAvaiableForSavingSearch extends TestHelper{
	long date =  new Date().getTime();
	String savedSearchName = "CreatedGroupIsAvaiableForSavingSearch_" + date;
	String savedSearchGroupName = "SavedSearchDemoGroup_" + date;
	
	@Test
	public void test_c1671_CreatedGroupIsAvaiableForSavingSearch(){
		handleCreatedGroupIsAvaiableForSavingSearch();
	}
	
	private void handleCreatedGroupIsAvaiableForSavingSearch(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	driver.get(driver.getCurrentUrl());
    	
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
    	SearchPage.addSearchCriteria("Author");
    	
    	hoverOverElement(SearchPage.searchBoxLocator);
    	tryClick(SearchPage.saveSearchBtnLocator, SearchPage.saveSearchNameLocator);
    	
    	String savedSearchDialogHeader = getText(SearchPage.saveSearchDialogHeaderLocator);
    	
    	softAssert.assertEquals(savedSearchDialogHeader, "Save Search As", "4. Recenseo opens up a \"Save Search As\" dialog box:: ");
    	
    	editText(SearchPage.saveSearchNameLocator, savedSearchName);
    	
    	SearchPage.selectPermissionForSavedSearchInPopup("Private");
    	
    	boolean isSavedSearchGroupDeSelected = (isElementChecked(SearchPage.savedSearchCheckGroupLocator) == false);
    	
    	boolean isSelectGroupDropDownDisabled = (isElementEnabled(SearchPage.savedSearchSelectGroupLocator) == false);
    	
    	softAssert.assertTrue(isSavedSearchGroupDeSelected && isSelectGroupDropDownDisabled, "Default 'Select Group' behavior is working fine:: ");
    	
    	SearchPage.selectCheckGroupForSavedSearch();
    	
    	boolean isSavedSearchGroupSelected = isElementChecked(SearchPage.savedSearchCheckGroupLocator);
    	
    	boolean isSelectGroupDropDownEnabled = isElementEnabled(SearchPage.savedSearchSelectGroupLocator);
    	
    	softAssert.assertTrue(isSavedSearchGroupSelected && isSelectGroupDropDownEnabled, "Selecting 'Save the Search in a Group' enables 'Select Group':: ");
    	
    	List<WebElement> groupList = getItemsFromDropdown(SearchPage.savedSearchSelectGroupLocator);
    	
    	boolean isGroupExistsOnTheList = groupList.stream().anyMatch(t -> t.getText().equals(savedSearchGroupName));
    	
    	softAssert.assertTrue(isGroupExistsOnTheList, "*** The new group, that user created just now, should be on that list:: ");
    	
    	softAssert.assertAll();
	}
}
