package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class CreatingASingleSearchHistoryInstance extends TestHelper{
	String searchName = "Author";
	@Test
	public void test_c36_CreatingASingleSearchHistoryInstance(){
		handleCreatingASingleSearchHistoryInstance();
	}
	
	private void handleCreatingASingleSearchHistoryInstance(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		List<WebElement> todaySavedSearchList = getElements(SearchesAccordion.todayItemLocator);
		
		int totalCountBeforeRunningSearch = 0;
		for(WebElement todaySavedSearch : todaySavedSearchList){
			if(getText(todaySavedSearch).equals(searchName)){
				SearchesAccordion.openSavedSearchInfoDetail(todaySavedSearch);
				totalCountBeforeRunningSearch = SearchesAccordion.getSavedSearchesInstanceCount();
				SearchesAccordion.closeSavedSearchInfoDetail();
				break;
			}
		}
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria(searchName);
		SearchPage.setOperatorValue(0, "is not empty");
		
    	performSearch();
    	
    	SearchPage.goToDashboard();
    	waitForFrameToLoad(Frame.MAIN_FRAME);
    	
    	SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		SearchesAccordion.openSavedSearchInfoDetail(SearchesAccordion.todayItemLocator);
		
		int totalCountAfterRunningSearch = SearchesAccordion.getSavedSearchesInstanceCount();
		
		System.out.println( Logger.getLineNumber() + "Count got from detail: " + totalCountAfterRunningSearch);
		
		softAssert.assertEquals(totalCountAfterRunningSearch, totalCountBeforeRunningSearch + 1, "5. A single search history instance gets created in the 'Search History' tab:: ");
    	
       	softAssert.assertAll();
	}
}
