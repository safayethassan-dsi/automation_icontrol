package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class DisplayOfASingleSavedSearchNotInGroup extends TestHelper{
	String savedSearchName = "DisplayOfASingleSavedSearchNotInGroup_" + new Date().getTime();
	
	@Test
	public void test_c3_DisplayOfASingleSavedSearchNotInGroup(){
		handleDisplayOfASingleSavedSearchNotInGroup();
	}
	
	private void handleDisplayOfASingleSavedSearchNotInGroup(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        FolderAccordion.open();
        FolderAccordion.expandFolder(driver, "Email");
        FolderAccordion.selectFolder(driver, "Foster, Ryan e-mail");
        FolderAccordion.addSelectedToSearch();
        
        switchToDefaultFrame();
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
        SearchPage.createSavedSearch(savedSearchName, "", "");
        
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.searchForSavedSearch(savedSearchName);
        
        int totalSearchResultFound = SearchesAccordion.getSavedSearchCount(savedSearchName);
        
        softAssert.assertEquals(totalSearchResultFound, 1);
        
        WebElement savedSearchElement = driver.findElement(SearchesAccordion.historyItemsLocation);
        
        String nameOfSavedSearch = getText(savedSearchElement);
        
        softAssert.assertEquals(nameOfSavedSearch, savedSearchName);
        
        String checkBoxDisplayProperty = savedSearchElement.findElement(By.cssSelector("a > i")).getCssValue("display").trim();
        
        softAssert.assertEquals(checkBoxDisplayProperty, "inline-block");
        
        SearchesAccordion.deleteSavedSearch(savedSearchName);
        
        softAssert.assertAll();
 	}
}
