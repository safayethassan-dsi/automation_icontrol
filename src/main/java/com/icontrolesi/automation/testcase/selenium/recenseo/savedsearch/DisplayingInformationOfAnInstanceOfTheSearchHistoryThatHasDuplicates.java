package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class DisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasDuplicates extends TestHelper{
	By todayItemLocator = By.cssSelector("#today > ul > li");
	
	String searchCriteria = "Author"; 
	
	String stepMsg8 = "8. Recenseo populates all the search criteria of the selected 'Search History' search on the Search Page ( %s )::";
	
	@Test
	public void test_c45_DisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates(){
		handleDisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates();
	}
	
	private void handleDisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		for(int i = 1; i <= 2; i++){	
			SearchPage.clearSearchCriteria();
			SearchPage.addSearchCriteria("Author");
			performSearch();
			SearchPage.goToDashboard();
			waitForFrameToLoad(Frame.MAIN_FRAME);
		}
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		hoverOverElement(todayItemLocator);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement addCriteriaToSearchBuilder = driver.findElement(SearchesAccordion.savedSearchInfoDetailLocator);
		
		jse.executeScript("arguments[0].click();", addCriteriaToSearchBuilder);
		waitFor(5);
		
		String infoHeader = getText(By.cssSelector("div[aria-describedby='recentInfoDialog'] >  div > span"));
		
		String runDateLabel = getText(By.id("runDateDiv"));
		
		String runTimeLabel = getText(By.cssSelector("#recentInfoDialog >div:nth-of-type(2) > label"));
		
		String resultCount = getText(By.cssSelector("#recentInfoDialog >div:nth-of-type(2) > label:nth-of-type(2)"));
		
		String criteria = getText(By.cssSelector("div > span[id='criteria']"));
		
		softAssert.assertEquals(infoHeader, "Info", String.format(stepMsg8, "***. Info Header"));
		softAssert.assertTrue(runDateLabel.startsWith("Run date: "), String.format(stepMsg8, "a. Run Time (run data within the same day will be consolidated for the same search criteria)"));
		softAssert.assertTrue(resultCount.startsWith("Result count:"), String.format(stepMsg8, "b. Run Time (run data within the same day will be consolidated for the same search criteria)"));
		softAssert.assertTrue(runTimeLabel.startsWith("Run time:"), String.format(stepMsg8, "c. Result Count (run data within the same day will be consolidated for the same search criteria)"));
		softAssert.assertTrue(criteria.startsWith("Criteria:"), String.format(stepMsg8, "d. Criteria"));
			
		softAssert.assertAll();	
			
		softAssert.assertAll();		
	}
}
