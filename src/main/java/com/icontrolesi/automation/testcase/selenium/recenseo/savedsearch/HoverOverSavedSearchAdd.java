package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HoverOverSavedSearchAdd extends TestHelper{
	String savedSearchName = "HoverOverSavedSearchAdd_" + new Date().getTime();
	
	String stepMsg10 = "10. Recenseo populates all the search criteria of the selected 'Saved' search on the Search Page(%s):: ";
	
	By savedSearchHoveredBtnsLocator = By.cssSelector("li[role='treeitem'] > div.hoverDiv");
	
	@Test
	public void test_c30_HoverOverSavedSearchAdd(){
		handleHoverOverSavedSearchAdd();
	}
	
	private void handleHoverOverSavedSearchAdd(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	
    	String editBtnDiplayProperty = getCssValue(savedSearchHoveredBtnsLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "none", "8. Hovering over a search group displays an Edit icon (Before):: ");
    	
    	hoverOverElement(SearchesAccordion.historyItemsLocation);
    	
    	editBtnDiplayProperty = getCssValue(savedSearchHoveredBtnsLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "block", "8. Hovering over a search group displays an Edit icon (After):: ");
    	
    	SearchesAccordion.clickAddBtnForSavedSearch();
    	
    	switchToDefaultFrame();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	String queryDescription = getText(By.id("queryDescription"));
    	
    	String criterionFieldName = getText(By.cssSelector(".CriterionField > span"));
    	
    	String criterionOperator = getSelectedItemFroDropdown(By.className("CriterionOperator"));
    	
    	String criterionValue = getText(By.cssSelector("input.CriterionValue"));
    	
    	softAssert.assertEquals(queryDescription, savedSearchName, String.format(stepMsg10, "Saved Search Name"));
    	softAssert.assertEquals(criterionFieldName, "Author", String.format(stepMsg10, "Criterion Field name"));
    	softAssert.assertEquals(criterionOperator, "contains", String.format(stepMsg10, "Criterion operator"));
    	softAssert.assertEquals(criterionValue, "", String.format(stepMsg10, "Criterion value"));
    	
    	SearchesAccordion.open();
    	SearchesAccordion.deleteSavedSearch(savedSearchName);
    	
    	softAssert.assertAll();
	}
}
