package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HoverOverSavedSearchDelete extends TestHelper{
	String savedSearchName = "HoverOverSavedSearchDelete_" + new Date().getTime();
	
	String stepMsg10 = "10. Recenseo provides a confirmation dialog box \"Are you sure you want to delete this 'Saved' Search?\":: ";
	
	By savedSearchHoveredBtnsLocator = By.cssSelector("li[role='treeitem'] > div.hoverDiv");
	
	@Test
	public void test_c31_HoverOverSavedSearchDelete(){
		handleHoverOverSavedSearchDelete();
	}
	
	private void handleHoverOverSavedSearchDelete(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	
    	String editBtnDiplayProperty = getCssValue(savedSearchHoveredBtnsLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "none", "8. Hovering over a search group displays an Edit icon (Before):: ");
    	
    	hoverOverElement(SearchesAccordion.historyItemsLocation);
    	
    	editBtnDiplayProperty = getCssValue(savedSearchHoveredBtnsLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "block", "8. Hovering over a search group displays an Edit icon (After):: ");
    	
    	SearchesAccordion.clickDeleteBtnForSavedSearch();
    	
    	String alertMsg = getAlertMsg(driver);
    	
    	softAssert.assertEquals(alertMsg, "Are you sure you want to delete this 'Saved' Search?", stepMsg10);
    	
    	String deletedMsg = getAlertMsg(driver);
    	
    	softAssert.assertEquals(deletedMsg, "Saved search was successfully deleted.", "12. Recenseo deletes that particular saved search (Saved Search deleted Message appeared):: ");
    	
    	boolean isSavedSearchDeleted = (SearchesAccordion.isSavedSearchExists(savedSearchName) == false);
    	
    	softAssert.assertTrue(isSavedSearchDeleted, "12. Recenseo deletes that particular saved search:: ");
    	
    	softAssert.assertAll();
	}
}
