package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HoverOverSavedSearchEdit extends TestHelper{
	String savedSearchName = "HoverOverSavedSearchEdit_" + new Date().getTime();
	String editedSavedSearchName = savedSearchName + "_Edited";
	
	By savedGroupEditBtnLocator = By.cssSelector("li[role='treeitem'] > div.hoverDiv");
	
	@Test
	public void test_c29_HoverOverSavedSearchEdit(){
		handleHoverOverSavedSearchEdit();
	}
	
	private void handleHoverOverSavedSearchEdit(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	
    	String editBtnDiplayProperty = getCssValue(savedGroupEditBtnLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "none", "8. Hovering over a search group displays an Edit icon (Before):: ");
    	
    	hoverOverElement(SearchesAccordion.historyItemsLocation);
    	
    	editBtnDiplayProperty = getCssValue(savedGroupEditBtnLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "block", "8. Hovering over a search group displays an Edit icon (After):: ");
    	
    	SearchesAccordion.clickEditBtnForSavedSearch();
    	
    	String editWindowTitle = getText(SearchesAccordion.savedSearchOrGroupWindowHeaderLocator);
    	
    	softAssert.assertEquals(editWindowTitle, "Edit Searches", "10. Recenseo provides the user with an editing dialog box:: ");
    	
    	editText(SearchesAccordion.savedGroupNameLocator, editedSavedSearchName); 
    	
    	tryClick(SearchesAccordion.createGroupBtnLocator);
    	acceptAlert(driver);
    	
    	boolean isSavedSearchEdited = SearchesAccordion.isSavedSearchExists(editedSavedSearchName);
    	
    	softAssert.assertTrue(isSavedSearchEdited, "*** The Saved Search Name is renamed provided user entered unique name during renaming:: ");
    	
    	softAssert.assertAll();
	}
}
