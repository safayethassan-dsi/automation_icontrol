package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HoverOverSavedSearchInfo extends TestHelper{
	String savedSearchName = "HoverOverSavedSearchInfo_" + new Date().getTime();
	
	String stepMsg10 = "10 (%c). Recenseo provides a tool tip for the hovered 'Saved' search which contain the following information (%s):: ";
	
	By savedSearchHoveredBtnsLocator = By.cssSelector("li[role='treeitem'] > div.hoverDiv");
	
	@Test
	public void test_c32_HoverOverSavedSearchInfo(){
		handleHoverOverSavedSearchInfo();
	}
	
	private void handleHoverOverSavedSearchInfo(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	
    	String editBtnDiplayProperty = getCssValue(savedSearchHoveredBtnsLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "none", "8. Hovering over a search group displays an Edit icon (Before):: ");
    	
    	hoverOverElement(SearchesAccordion.historyItemsLocation);
    	
    	editBtnDiplayProperty = getCssValue(savedSearchHoveredBtnsLocator, "display");
    	
    	softAssert.assertEquals(editBtnDiplayProperty, "block", "8. Hovering over a search group displays an Edit icon (After):: ");
    	
    	SearchesAccordion.clickInfoBtnForSavedSearch();
    	
    	isSavedSearchInfoDialogProperlyDisplayed();
    	
    	softAssert.assertAll();
	}
	
	
	private void isSavedSearchInfoDialogProperlyDisplayed(){
		String [] labelsForInfo = {
										"Label",
										"Permission Level",
										"Saved search creator",
										"Creation date",
										"Last modified date",
										"Last run date",
										"Last result document count"
								  }; 
		
		
		By labelLocator = By.cssSelector("#savedInfoDialog > div > label");
		
		List<WebElement> labelList = getElements(labelLocator);
		
		for(int i = 0; i < labelList.size(); i++){
			String labelInfo = labelsForInfo[i];
			String extractedLabel = getText(labelList.get(i)).split(":")[0].trim();
			softAssert.assertEquals(extractedLabel, labelInfo, String.format(stepMsg10, 'a'+i, labelInfo));
		}
	}
}
