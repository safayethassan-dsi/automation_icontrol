package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class HoverOverSearchHistoryAdd extends TestHelper{
	By todayItemLocator = By.cssSelector("#today > ul > li");
	
	String searchCriteria = "Author"; 
	
	@Test
	public void test_c41_HoverOverSearchHistoryAdd(){
		handleHoverOverSearchHistoryAdd();
	}
	
	private void handleHoverOverSearchHistoryAdd(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria(searchCriteria);
		performSearch();
		
		SearchPage.goToDashboard();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		hoverOverElement(todayItemLocator);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement addCriteriaToSearchBuilder = driver.findElement(By.cssSelector("#savedSearches_add > span"));
		
		jse.executeScript("arguments[0].click();", addCriteriaToSearchBuilder);
		waitFor(driver, 5);
		
		switchToDefaultFrame();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		String criterionName = getText(By.cssSelector("div[class='CriterionField'] > span"));
		
		String criterionOperator = getSelectedItemFroDropdown(By.cssSelector("select[class='CriterionOperator']"));
		
		String criterionValue = getText(By.cssSelector("input.CriterionValue"));
		
		softAssert.assertEquals(criterionName, searchCriteria, "8. Recenseo populates all the search criteria of the selected 'Search History' search on the Search Page (Criterion name):: ");
		softAssert.assertEquals(criterionOperator, "contains", "8. Recenseo populates all the search criteria of the selected 'Search History' search on the Search Page (Criterion operator):: ");
		softAssert.assertEquals(criterionValue, "", "8. Recenseo populates all the search criteria of the selected 'Search History' search on the Search Page (Criterion value):: ");
			
		softAssert.assertAll();		
	}
}
