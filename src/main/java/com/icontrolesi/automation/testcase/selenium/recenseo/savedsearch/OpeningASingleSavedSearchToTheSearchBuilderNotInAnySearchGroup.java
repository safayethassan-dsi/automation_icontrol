package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class OpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup extends TestHelper{
	String savedSearchName = "OpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup_" + new Date().getTime();
	
	String stepMsg7 = "7. Recenseo displays all the documents corresponding to the selected 'Saved' search' on the View Documents Page(%s):: ";
	
	@Test
	public void test_c17_OpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup(){
		handleOpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup();
	}
	
	private void handleOpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
	    DocView.checkSelectedColumns("Author");
	    DocView.clickCrossBtn();
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	editText(By.cssSelector("input.CriterionValue"), "jeff.andrews@enron.com");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.addSelectedToSearch(savedSearchName);
    	
    	switchToDefaultFrame();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	performSearch();
    	
    	int totalDocs = DocView.getDocmentCount();
    	
    	List<WebElement> authorList = getElements(DocView.documentAuthorColumnLocator);
    	
    	boolean isSearchContentOk = true;
    	
    	for(WebElement author : authorList){
    		System.out.println( Logger.getLineNumber() + getText(author)+"****");
    		if(getText(author).equals("jeff.andrews@enron.com") == false){
    			isSearchContentOk = false;
    			break;
    		}
    	}
    	
    	softAssert.assertEquals(totalDocs, 7, String.format(stepMsg7, "Total Document number verification"));
    	
    	softAssert.assertTrue(isSearchContentOk, String.format(stepMsg7,"Content verification"));
    	
    	SearchPage.goToDashboard();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	SearchesAccordion.open();
    	SearchesAccordion.deleteSavedSearch(savedSearchName);
    	
    	softAssert.assertAll();
	}
}
