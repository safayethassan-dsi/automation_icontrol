package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class OpeningMultipleSavedSearchToTheSearchBuilderInSearchGroup extends TestHelper{
	long date = new Date().getTime(); 
	String savedSearchGroupName = "SavedSearchDemoGroup_" + date;
	String savedSearchName = "OpeningASingleSavedSearchToTheSearchBuilderNotInAnySearchGroup_" + date;
	String savedSearchName_1 = savedSearchName + "_1";
	String savedSearchName_2 = savedSearchName + "_2";
	
	String stepMsg7 = "7. Recenseo displays all the documents corresponding to the selected 'Saved' search' on the View Documents Page(%s):: ";
	
	@Test
	public void test_c20_OpeningMultipleSavedSearchToTheSearchBuilderInSearchGroup(){
		handleOpeningMultipleSavedSearchToTheSearchBuilderInSearchGroup();
	}
	
	private void handleOpeningMultipleSavedSearchToTheSearchBuilderInSearchGroup(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
	    DocView.checkSelectedColumns("Document ID", "Custodian");
	    DocView.clickCrossBtn();
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.createTopLevelGroup(savedSearchGroupName, "All");
    	
    	switchToDefaultFrame();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	SearchPage.addSearchCriteria("Document ID");
    	selectFromDrowdownByText(By.cssSelector("select.CriterionOperator"), "is between");
    	List<WebElement> valueFields = getElements(By.cssSelector("input[class=CriterionValue]"));
    	editText(valueFields.get(0), "1");
    	editText(valueFields.get(1), "25");
    	
    	SearchPage.createSavedSearch(savedSearchName_1, "", savedSearchGroupName);
    	
    	SearchPage.clearSearchCriteria();
    	
    	SearchPage.addSearchCriteria("Custodian");
    	editText(By.cssSelector("div.CriterionInput > input"), "Foster, Ryan");
    	SearchPage.createSavedSearch(savedSearchName_2, "", savedSearchGroupName);
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	tryClick(By.cssSelector("ul[class='jstree-children'] > li > a > i"));
    	tryClick(By.cssSelector("ul[class='jstree-children'] > li:nth-child(2) > a > i"));
    	
    	SearchesAccordion.clickOpenBtnForViewSavedSearch();
    	
    	int totalDocs = DocView.getDocmentCount();
    	
    	DocView.setNumberOfDocumentPerPage(500);
    	
    	List<WebElement> custodianList = getElements(DocView.documentCustodianColumnLocator);
    	System.out.println( Logger.getLineNumber() + "Total Data: " + custodianList.size());
    	boolean isSearchContentOk = true;
    	
    	for(WebElement custodian : custodianList){
    		if(getText(custodian).startsWith("Foster, Ryan") == false){
    			isSearchContentOk = false;
    			break;
    		}
    	}
    	
    	softAssert.assertEquals(totalDocs, 403, String.format(stepMsg7, "Total Document number verification"));
    	
    	softAssert.assertTrue(isSearchContentOk, String.format(stepMsg7,"Content verification"));
    	
    	SearchPage.goToDashboard();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	SearchesAccordion.open();
    	SearchesAccordion.deleteSavedSearch(savedSearchName_1);
    	SearchesAccordion.deleteSavedSearch(savedSearchName_2);
    	
    	softAssert.assertAll();
	}
}
