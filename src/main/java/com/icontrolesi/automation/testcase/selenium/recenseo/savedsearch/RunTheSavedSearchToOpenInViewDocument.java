package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class RunTheSavedSearchToOpenInViewDocument extends TestHelper{
	String savedSearchName = "HoverOverSavedSearchDelete_" + new Date().getTime();
	
	@Test
	public void test_c33_RunTheSavedSearchToOpenInViewDocument(){
		handleRunTheSavedSearchToOpenInViewDocument();
	}
	
	private void handleRunTheSavedSearchToOpenInViewDocument(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Author");
		DocView.clickCrossBtn();
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	selectFromDrowdownByText(By.cssSelector(".CriterionOperator"), "equals");
    	editText(By.cssSelector("div.CriterionInput > input"), "Jeff");
    	
    	SearchPage.createSavedSearch(savedSearchName, "", "");
    	
    	SearchesAccordion.open();
    	SearchesAccordion.selectFilter("All");
    	SearchesAccordion.searchForSavedSearch(savedSearchName);
    	
    	SearchesAccordion.openSavedSearchWithName(savedSearchName);
    	
    	switchToDefaultFrame();
    	
    	boolean isViewDocumentsHighlighted = getAttribute(By.cssSelector("#review > a"), "class").equals("tab-highlight"); 
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	boolean isMainPanelVisible = getCssValue(By.id("main_panel"), "display").equals("block");
    	
    	softAssert.assertTrue((isViewDocumentsHighlighted && isMainPanelVisible), "8. Recenseo runs a search by using the search criteria in the selected 'Saved' Search and redirects the user to the View Documents Page:: ");
    	
    	List<WebElement> authorNameList = getElements(DocView.documentAuthorColumnLocator);
    	
    	for(WebElement authorName : authorNameList){
    		softAssert.assertEquals(authorName.getText().trim(), "Jeff", "*** Recenseo opens the documents associated with the left-clicked Saved search in the View Documents page:: ");
    	}
    	
    	softAssert.assertAll();
	}
}
