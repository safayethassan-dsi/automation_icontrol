package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Date;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class SavingASearchNotInGroupAndAccessibleOnlyByOwner extends TestHelper{
	String savedSearchName = "SavingASearchNotInGroupAndAccessibleOnlyByOwner_" + new Date().getTime();
	
	@Test
	public void test_c7_SavingASearchNotInGroupAndAccessibleOnlyByOwner(){
		handleSavingASearchNotInGroupAndAccessibleOnlyByOwner();
	}
	
	private void handleSavingASearchNotInGroupAndAccessibleOnlyByOwner(){
		new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	SearchPage.addSearchCriteria("Author");
    	
    	hoverOverElement(SearchPage.searchBoxLocator);
    	tryClick(SearchPage.saveSearchBtnLocator, SearchPage.saveSearchNameLocator);
    	
    	String savedSearchDialogHeader = getText(SearchPage.saveSearchDialogHeaderLocator);
    	
    	softAssert.assertEquals(savedSearchDialogHeader, "Save Search As", "6. Recenseo opens up a \"Save Search As\" dialog box:: ");
    	
    	editText(SearchPage.saveSearchNameLocator, savedSearchName);
    	
    	SearchPage.selectPermissionForSavedSearchInPopup("Private");
    	
    	boolean isSaveToGroupUnChecked = (isElementChecked(SearchPage.savedSearchCheckGroupLocator) == false);
    	
    	boolean isSelectGroupDisabled = (isElementEnabled(SearchPage.savedSearchSelectGroupLocator) == false);
    	
    	softAssert.assertTrue(isSaveToGroupUnChecked && isSelectGroupDisabled, "10. Recenseo keeps the Select Group combo box disabled:: ");
    	
    	tryClick(SearchPage.savedSearchContinueBtnLocator, 30);
    	
    	SearchesAccordion.open();
    	
    	boolean isSearchSaved = SearchesAccordion.isSavedSearchExists(savedSearchName);
    	
    	softAssert.assertTrue(isSearchSaved, "(*) The search is saved in the 'Saved' tabs under the 'Searches' accordion:: ");
    	
    	SearchesAccordion.openSavedSearchInfoDetail(savedSearchName);
    	
    	//String permissionLevel = getText(SavedSearchAccordion.savedPermissionInfoLocator);
    	String permissionLevel = driver.findElement(SearchesAccordion.savedPermissionInfoLocator).getText().trim();
    	System.err.println(permissionLevel+"****");
    	SearchesAccordion.closeSavedSearchInfoDetail();
    	
    	softAssert.assertEquals(permissionLevel, "Mine", "(*) SavedSearch's permission level is ok (Private/Mine):: ");
    	
    	softAssert.assertAll();
	}
}
