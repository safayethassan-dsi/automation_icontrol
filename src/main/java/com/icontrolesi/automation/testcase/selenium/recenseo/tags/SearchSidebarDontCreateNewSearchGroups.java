package com.icontrolesi.automation.testcase.selenium.recenseo.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DomainsAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class SearchSidebarDontCreateNewSearchGroups extends TestHelper{
	List<String> criterionListAdded = new ArrayList<>(Arrays.asList("folders", "tags", "domains", "assignments"));
	
	@Test
	public void test_c1782_SearchSidebarDontCreateNewSearchGroups(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		int criterionGroupTotal = getTotalElementCount(SearchPage.criterionGroupLocator);
		
		FolderAccordion.open();
		FolderAccordion.selectFirstFolder();
		FolderAccordion.addSelectedToSearch();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		TagAccordion.open();
		TagAccordion.selectFirstTag();
		TagAccordion.addSelectedToSearch();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DomainsAccordion.open();
		DomainsAccordion.selectFirstDomain();
		DomainsAccordion.addSelectedToSearchBuilder();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.selectFirstAssignment();
		ReviewAssignmentAccordion.addSelectedToSearch();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int criterionGroupTotal_02 = getTotalElementCount(SearchPage.criterionGroupLocator);
		
		softAssert.assertEquals(criterionGroupTotal, criterionGroupTotal_02, "1)Added a Folder, a Tag, a Domain and a Review Assignment to Search Criteria and NONE of these actions create new search groups:: ");
		
		List<String> criterionExtractedList;
		
		if(Configuration.getConfig("selenium.browser").equals("ie11")){
			criterionExtractedList = getListOfItemsFrom(By.className("CriterionContent"), c -> getText(c).split(":")[0].split("\\s+")[3]);
		}else{
			criterionExtractedList = getListOfItemsFrom(By.className("CriterionContent"), c -> getText(c).split("[\r\n]+")[3].split(":")[0]);
		}
		
		for(int i = 1; i < criterionExtractedList.size(); i++){
			softAssert.assertEquals(criterionExtractedList.get(i), criterionListAdded.get(i - 1), "2) Criterion Added in Order:: ");
		}
		
		softAssert.assertAll();
	}
}
