package com.icontrolesi.automation.testcase.selenium.recenseo.tags;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class SearchTags extends TestHelper{
	  
	@Test
	protected void test_1781_SearchTags(){
	   	//loginToRecenseo();
	 	new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		TagAccordion.openTagInfoDetail();
		
		long docCountInTagInfo =  TagAccordion.getTotalDocsCount();
	  
		TagAccordion.closeTagInfoDetail();
	
		TagAccordion.selectFirstTag();
		TagAccordion.openSelected();
		
		long docCountInGridView = DocView.getDocmentCount();
		
	    Assert.assertTrue((docCountInTagInfo == docCountInGridView), "(4)Confirm same number of documents return in Grid view:: ");
	}
}
