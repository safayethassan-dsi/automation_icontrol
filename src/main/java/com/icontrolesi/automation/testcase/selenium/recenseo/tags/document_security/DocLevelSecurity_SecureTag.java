package com.icontrolesi.automation.testcase.selenium.recenseo.tags.document_security;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

public class DocLevelSecurity_SecureTag extends TestHelper{
String tagName = "SecurityTagTestSubTag";
	
	@Test
	private void test_c1790_DocLevelSecurity_SecureTag(){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"DEM4 Admins\" Team:: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        TagAccordion.open();
        TagAccordion.openTagForView(tagName);
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.blockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();
        
        SearchPage.logout();
        
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
        
        new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	TagAccordion.open();
    	TagAccordion.selectFilter("All");
    	TagAccordion.searchForTag(tagName);
    	
    	getElements(TagAccordion.tagItemLocator).stream().filter(tag -> getText(tag).equals(tagName)).findFirst().get().click();
         	
    	String alertMsg = getAlertMsg();
    	
    	softAssert.assertEquals(alertMsg, "No documents were found in the selected tag(s). Please select a different tag(s) and try again.", "6) Confirm the \"Test User\" ID cannot Open or Search the Tag:: ");
    	
    	TagAccordion.openTagInfoDetail(tagName);
    	
    	int totaDocCountInTag = TagAccordion.getTotalDocsCount();
    	
    	softAssert.assertEquals(totaDocCountInTag, 0, "7) Confirm Tag Details Pop-up counts shows tag as having 0 documents:: "); 
    	
    	softAssert.assertAll();  
    }   
}
