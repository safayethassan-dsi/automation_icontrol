package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;
import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

import com.icontrolesi.automation.platform.util.Logger;

public class AddingDocsToMultipleTagsFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "AddingDocsToMultipleTagsFromManageTagInGrid_c1815_" + timeFrame;
	String tagName2 = tagName + "_02";
	
	@Test
	public void test_c1815_AddingDocsToMultipleTagsFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		
		performSearch();
		
		int totalDocsInSearc = DocView.getDocmentCount();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(tagName, "");
		TagManagerInGridView.createTopLevelTag(tagName2, "");
		TagManagerInGridView.selectTagFilter("All");		
		TagManagerInGridView.searchForTag(tagName);
		
		getElements(TagManagerInGridView.tagItemCheckboxLocator).stream().forEach(t -> t.click());

		tryClick(TagManagerInGridView.addDocumentsBtnOnTopLocator);
		
		String alertMsgForDocumentAdding = getAlertMsg();
		
		List<String> docCountList = getListOfItemsFrom(TagManagerInGridView.tagWiseDocCountLocator, t -> getText(t).replaceAll("\\D+", ""));
				
		int docCountForSelectedTag = Integer.parseInt(getText(TagManagerInGridView.tagWiseDocCountLocator).replaceAll("\\D+", ""));
		
		System.out.println( Logger.getLineNumber() + "test: "+ docCountForSelectedTag);
		
		softAssert.assertEquals(alertMsgForDocumentAdding, "Document(s) were added to tag successfully", "***) Selected documents have been added to the tags:: ");
		softAssert.assertEquals(Integer.parseInt(docCountList.get(0)), totalDocsInSearc, "***) Document count match the number of documents added to selected tag:: ");
		softAssert.assertEquals(Integer.parseInt(docCountList.get(1)), totalDocsInSearc, "***) Document count match the number of documents added to selected tag:: ");
		
		TagManagerInGridView.deleteTag(tagName);
		TagManagerInGridView.deleteTag(tagName2);
		
		softAssert.assertAll();
	}
}
