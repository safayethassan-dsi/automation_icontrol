package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

import com.icontrolesi.automation.platform.util.Logger;

public class AddingDocsToTagFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "AddingDocsToTagFromManageTagInGrid_c1809_" + timeFrame;
	
	@Test
	public void test_c1809_AddingDocsToTagFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		
		performSearch();
		
		int totalDocsInSearc = DocView.getDocmentCount();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		String addingDocMsg = TagManagerInGridView.addDocumentsForTag(tagName);
		
		int docCountForSelectedTag = Integer.parseInt(getText(TagManagerInGridView.tagWiseDocCountLocator).replaceAll("\\D+", ""));
		
		System.out.println( Logger.getLineNumber() + "test: "+ docCountForSelectedTag);
		
		softAssert.assertEquals(addingDocMsg, "Document(s) were added to tag successfully", "***) Selected documents have been added to the tags:: ");
		softAssert.assertEquals(totalDocsInSearc, docCountForSelectedTag, "***) Document count match the number of documents added to selected tag:: ");
		
		TagManagerInGridView.deleteTag(tagName);
		
		softAssert.assertAll();
	}
}
