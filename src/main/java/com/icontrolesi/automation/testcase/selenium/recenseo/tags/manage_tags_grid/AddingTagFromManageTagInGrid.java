package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class AddingTagFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "AddingTagFromManageTagInGrid_c1808_" + timeFrame;
	
	@Test
	public void test_c1808_AddingTagFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		boolean isTagCreated = TagManagerInGridView.isTagFound(tagName);
		
		ManageTags.deleteTag(tagName);
		
		softAssert.assertTrue(isTagCreated, "***) Tag created as expected:: ");
		
		softAssert.assertAll();
	}
}
