package com.icontrolesi.automation.testcase.selenium.recenseo.tags.manage_tags_grid;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class DocCountForManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "AddingTagFromManageTagInGrid_c1808_" + timeFrame;
	
	@Test
	public void test_c1807_DocCountForManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Loose Files");
		
		DocView.setNumberOfDocumentPerPage(500);
		
		int totalDocCountInSearch = DocView.getDocmentCount();
		
		TagManagerInGridView.open();
		
		waitFor(20);

		int totalDocCountInManageTag = TagManagerInGridView.getTotalDocCount();
		
		TagManagerInGridView.close();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int totalItemsToSelect = getRandomNumberInRange(1, totalDocCountInSearch);
		
		List<WebElement> checkBoxList = getElements(DocView.documentCheckboxColumnLocator);
		
		for(int i = 0; i < totalItemsToSelect; i++){
			checkBoxList.get(i).click();
			checkBoxList = getElements(DocView.documentCheckboxColumnLocator);
		}
		
		TagManagerInGridView.open();
		
		waitFor(20);
		
		int totalDocCountInDocSelectedMode = TagManagerInGridView.getTotalDocCount();
		
		softAssert.assertEquals(totalDocCountInSearch, totalDocCountInManageTag, "2) Document count match the total document in search:: ");
		
		softAssert.assertEquals(totalDocCountInDocSelectedMode, totalItemsToSelect, "4) Document count reflects the number of selected documents:: ");
		
		softAssert.assertAll();
	}
}
