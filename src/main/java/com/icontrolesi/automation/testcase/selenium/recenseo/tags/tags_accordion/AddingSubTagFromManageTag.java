package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class AddingSubTagFromManageTag extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String parentTag = "AddingTag_c1794_" + timeFrame;
	String subTag = "AddingTag_c1794_Sub_" + timeFrame;
	
	@Test
	public void test_c1798_AddingSubTagFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		ManageTags.open();
		ManageTags.createTopLevelTag(parentTag, "");
		ManageTags.createSubTagFor(parentTag, subTag, "");
		
		boolean isTagCreated = ManageTags.isTagFound(subTag);
		
		List<WebElement> foundTagList = getElements(ManageTags.tagItemLocator);
		
		boolean isCreatedTagIsAChild = foundTagList.stream().anyMatch(tag -> getParentOf(tag).getAttribute("class").contains("childNode") && getText(tag).equals(subTag));
		
		ManageTags.deleteTag(parentTag);		
		
		softAssert.assertTrue(isTagCreated, "***) Tag created as expected:: ");
		softAssert.assertTrue(isCreatedTagIsAChild, "***) Created tag is a subtag::  "); 
		
		softAssert.assertAll();
	}
}
