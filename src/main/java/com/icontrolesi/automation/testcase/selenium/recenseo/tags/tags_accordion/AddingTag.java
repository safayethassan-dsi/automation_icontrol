package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class AddingTag extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagToBeCreated = "AddingTag_c1794_" + timeFrame;
	
	@Test
	public void test_c1794_AddingTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		TagAccordion.createTag(tagToBeCreated, "");	
		
		boolean isSearchOk = TagAccordion.isTagFound(tagToBeCreated);
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.deleteTag(tagToBeCreated);
		
		softAssert.assertTrue(isSearchOk, "***) Correct tag appeared in the search results:: ");
		
		softAssert.assertAll();
	}
}
