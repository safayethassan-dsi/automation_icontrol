package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class AddingTagFromManageTag extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "AddingTag_c1794_" + timeFrame;
	
	@Test
	public void test_c1797_AddingTagFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		ManageTags.open();
		ManageTags.createTopLevelTag(tagName, "");
		
		boolean isTagCreated = ManageTags.isTagFound(tagName);
		
		ManageTags.deleteTag(tagName);
		
		softAssert.assertTrue(isTagCreated, "***) Tag created as expected:: ");
		
		softAssert.assertAll();
	}
}
