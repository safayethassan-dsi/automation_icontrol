
package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

import com.icontrolesi.automation.platform.util.Logger;

public class DeletingMultipleSelectionFromManageTag extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String topLevelTag = "DeletingSingleTagFromManageTag_c1803_" + timeFrame;
	String topLevelTag2 = topLevelTag + "_2";
	
	String expectedConfirmationMsg = "Deleting a tag will delete the tag and remove all of the documents filed under it. \n" + 
			"It will also delete sub-tag(s) and remove documents filed under them, if there is any.\n" + 
			"Are you sure you want proceed deleting the tag(s) you selected?";
	
	@Test
	public void test_c1803_DeletingMultipleSelectionFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		
		ManageTags.open();
		ManageTags.createTopLevelTag(topLevelTag, "");
		ManageTags.createTopLevelTag(topLevelTag2, "");
		ManageTags.searchForTag(topLevelTag);
		
		getElements(ManageTags.tagItemCheckboxLocator).stream().forEach(tag -> tag.click());	
		
		tryClick(ManageTags.deleteSelectedBtnLocator);
		
		String deleteConfirmationMsg = getAlertMsg();
		
		waitFor(5);
		
		ManageTags.searchForTag(topLevelTag);
		
		int tagCount = getTotalElementCount(ManageTags.tagItemLocator);
		
		System.out.println( Logger.getLineNumber() + deleteConfirmationMsg);
		
		softAssert.assertEquals(deleteConfirmationMsg, expectedConfirmationMsg, "***) Deletig confirmation message appeared correctly:: ");
		softAssert.assertEquals(tagCount, 0, "***) Tags deleted and not found in the system:: ");
		
		softAssert.assertAll();
	}
}
