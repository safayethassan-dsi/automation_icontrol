package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class EditTags extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String parentTag = "AddingTag_c1794_" + timeFrame;
	String tagNameToBeEdited = parentTag + "_Edited";
	
	@Test
	public void test_c1820_EditTags(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		TagAccordion.createTag(parentTag, "");	
		
		boolean isParentTagCreated = TagAccordion.isTagFound(parentTag);
		
		TagAccordion.editTagWithName(parentTag, tagNameToBeEdited, "Mine");
		
		boolean isEditedTagFound = TagAccordion.isTagFound(tagNameToBeEdited);
		boolean isParentTagRemoved = TagAccordion.isTagFound(parentTag);
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.deleteTag(tagNameToBeEdited);
		
		softAssert.assertTrue(isParentTagCreated, "***) Correct tag appeared in the search results:: ");
		softAssert.assertTrue(isEditedTagFound, "***) Tag has been edited:: ");
		softAssert.assertFalse(isParentTagRemoved, "***) Parent tag not found in the system:: ");
		
		softAssert.assertAll();
	}
}
