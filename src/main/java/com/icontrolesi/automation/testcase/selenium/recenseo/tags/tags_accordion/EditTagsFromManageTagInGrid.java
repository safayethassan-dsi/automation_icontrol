package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class EditTagsFromManageTagInGrid extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "EditTagsFromManageTag_c1819_" + timeFrame;
	String editedTagName = tagName + "_Edited";
	
	@Test
	public void test_c1819_EditTagsFromManageTagInGrid(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(tagName, "");
		
		String updateMsg = TagManagerInGridView.editTag(tagName, editedTagName, "");
		
		//boolean isTagRemoved = !TagManagerInGridView.isTagFound(tagName);
		
		boolean isUpdatePerformed = TagManagerInGridView.isTagFound(editedTagName);
		
		//softAssert.assertTrue(isTagRemoved, "***) Tag removed as it was edited with a new name:: ");
		softAssert.assertEquals(updateMsg, "Selected tag was successfully renamed.", "*** Update/Edit message was appeared properly:: ");
		softAssert.assertTrue(isUpdatePerformed, "***) Tag updated as expected:: ");
		
		TagManagerInGridView.deleteTag(editedTagName);
		
		softAssert.assertAll();
	}
}
