package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

import com.icontrolesi.automation.platform.util.Logger;

public class FilteringTagsFromManageTag extends TestHelper{
	long timeFrame = Calendar.getInstance().getTimeInMillis();
	String tagName = "AddingTag_c1794_" + timeFrame;
	
	@Test(enabled = true)
	public void test_c1799_FilteringTagsFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		ManageTags.open();
		
		ManageTags.selectTagFilter("All");
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		
		List<WebElement> tagList = (List<WebElement>) jse.executeScript("return document.querySelectorAll('#tagManage-picklist a')");
		
		//List<WebElement> tagList = getElements(ManageTags.tagItemLocator);
		String firstTag = getText(tagList.get(0));
		String lastTag = getText(tagList.get(tagList.size() - 1));
		
		tagList.stream().map(tag -> getText(tag) +"****&&&&&").forEach(System.err::println);
		
		ManageTags.openTagInfoDetail(firstTag);
		System.out.println( Logger.getLineNumber() + getText(By.id("tagPerm"))+"***");
		TagAccordion.closeTagInfoDetail();
		
		
		ManageTags.openTagInfoDetail(lastTag);
		TagAccordion.closeTagInfoDetail();
		
		boolean isTagCreated = ManageTags.isTagFound(tagName);
		
		ManageTags.deleteTag(tagName);
		
		softAssert.assertTrue(isTagCreated, "***) Tag created as expected:: ");
		
		softAssert.assertAll();
	}
}
