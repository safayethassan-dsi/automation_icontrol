package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tvp;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class ActionManageTags extends TestHelper{
	long timeStamp = Calendar.getInstance().getTimeInMillis();
	
	String topLevelTagName = "GrdiViewManageTags_1783_Top_" + timeStamp;
	String topLevelTagName_Edited = topLevelTagName + "_Edited";
	String subTagName = "GrdiViewManageTags_1783_Sub_" + timeStamp;
	String subTagName_Edited = subTagName + "_Edited";

	@Test
	public void test_c1785_ActionManageTags(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
	
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Email");
		
		int totalDocuments = DocView.getDocmentCount();
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(topLevelTagName, "");
		TagManagerInGridView.addDocumentsForTag(topLevelTagName);
		
		int totalDocsAddedForTopTag = TagManagerInGridView.getDocumentCountForTag(topLevelTagName);
		
		softAssert.assertEquals(totalDocsAddedForTopTag, totalDocuments, "3a. Create a new TAG and add documents too that tag (Confirm numbers):: ");
		
		TagManagerInGridView.createSubTagFor(topLevelTagName, subTagName, "");
		TagManagerInGridView.addDocumentsForTag(subTagName);
		
		int totalDocsAddedForSubTag = TagManagerInGridView.getDocumentCountForTag(subTagName);
		
		
		softAssert.assertEquals(totalDocsAddedForSubTag, totalDocuments, " 3b. Add a sub-Tag and add documents to that tag (Confirm numbers):: ");
		
		TagManagerInGridView.editTag(topLevelTagName, topLevelTagName_Edited, "Mine");
		TagManagerInGridView.searchForTag(topLevelTagName_Edited);
		
		int totalTags = getTotalElementCount(TagManagerInGridView.tagItemLocator);
		
		softAssert.assertEquals(totalTags, 1, "3c. Edit a TAG and confirm changes:: ");
		
		TagManagerInGridView.editTag(subTagName, subTagName_Edited, "Mine");
		TagManagerInGridView.searchForTag(subTagName_Edited);
		
		totalTags = getTotalElementCount(TagManagerInGridView.tagItemLocator);
		
		softAssert.assertEquals(totalTags, 1, "3d. Rename the subtag:: ");
		
		TagManagerInGridView.close();
		
		SearchPage.goToDashboard();
		
		TagAccordion.open();
		TagAccordion.selectFilter("All");
		TagAccordion.searchForTag(topLevelTagName_Edited);
		
		int countForTopTag = getTotalElementCount(TagAccordion.tagItemLocator);
		
		softAssert.assertEquals(countForTopTag, 1, "5. Newly created TopLevel tag is in the list:: ");
		
		TagAccordion.searchForTag(subTagName_Edited);
		
		int countForSubTag = getTotalElementCount(TagAccordion.tagItemLocator);
		
		softAssert.assertEquals(countForSubTag, 1, "5. Newly created Subtag is in the list:: ");
		
		TagAccordion.selectTag(topLevelTagName_Edited);
		TagAccordion.selectTag(subTagName_Edited);
		TagAccordion.openSelected();
		
		TagManagerInGridView.open();
		TagManagerInGridView.deleteTag(topLevelTagName_Edited);
		TagManagerInGridView.deleteTag(topLevelTagName_Edited);
		
		//deleting should be checked
		
		softAssert.assertAll();		
	}
	
}
